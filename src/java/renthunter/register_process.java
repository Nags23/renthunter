/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package renthunter;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Calendar;
import java.sql.*;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//@SuppressWarnings("deprecation")
import java.text.SimpleDateFormat;
import java.util.ResourceBundle;
import javax.servlet.*;
import javax.servlet.http.HttpSession;
import renthunter.SendMail;
public class register_process extends HttpServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String insert = "fail";
        Connection con = null;
        long connected;
        OutputStream out = null;
        PreparedStatement stmt, stmt1, stmt2, stmt3, stmt4, stmt5, stmt6 = null;
        ResultSet rs, rs1, rs2, rs3, rs4, rs5, rs6 = null;
        ResourceBundle bundle = ResourceBundle.getBundle("SelectResource");
        HttpSession session = request.getSession();
        try {
            try {
                String url = bundle.getString("URL");
                String username = bundle.getString("username");
                String password = bundle.getString("password");
                Class.forName(bundle.getString("Driver"));
                con = DriverManager.getConnection(url, username, password);
            } catch (Exception e) {
                System.out.println("Database Connection Error" + e);
            }

            String currdate = "";
            Date date = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            String strDate = sdf.format(date);
            String time = new SimpleDateFormat("h:mm a").format(new Date());
            String dt = strDate + " " + time;
            int year = Calendar.getInstance().get(Calendar.YEAR);
            int yr = year % 100;
            String user = "";
            
           

            String name = request.getParameter("name");
            String mob = request.getParameter("mob");
            String email = request.getParameter("email");
            String pwd = request.getParameter("pwd");
            
              

            
            try {
                con.setAutoCommit(false);
            
            
                   
                    stmt2=con.prepareStatement("insert into users(username,pwd,firstname,email,mob,created_date,type,disabled,cpwd)values(?,?,?,?,?,?,?,?,?)");
                    stmt2.setString(1, mob);
                    stmt2.setString(2, pwd);
                    stmt2.setString(3, name);
                    stmt2.setString(4, email);
                    stmt2.setString(5, mob);
                    stmt2.setString(6, strDate);
                    stmt2.setString(7, "User");
                    stmt2.setString(8, "n");
                    stmt2.setString(9, "n");
                    stmt2.executeUpdate();
                    
                    stmt3 = con.prepareStatement("delete from register_temp where mob='"+mob+"'");
                    stmt3.executeUpdate();
                    
                    
                    con.commit();
                     session.setAttribute("log_user", mob);
                     session.setAttribute("log_pwd", pwd);
                    
                     String smtp = "",port_no="",email1="",pwd1="";
                    stmt2 = con.prepareStatement("select smtp,port,email,pwd from smtp_details");
                    rs2 = stmt2.executeQuery();
                    if(rs2.next()){
                       smtp = rs2.getString("smtp");
                       port_no = rs2.getString("port");
                       email1 = rs2.getString("email");
                       pwd1 = rs2.getString("pwd");
                    }
                    String to_email = email;
                    
                    String temp = "",temp1 = "";
            temp="Dear "+name+","+"<br><br>"+"Thank you for registration with RentHunter,  "+"<br><br>"+"Your Login Credentials are"+"<br><br>"+"Username : "+mob+"<br><br>"+"Password : "+pwd+"<br><br>"+"Note:Please dont share credentials with anyone and change password on first login!!!";
          
                    
                      try {
                         
             SendMail.sendhrEmail(to_email, "Thank you for registration with RentHunter", temp, port_no, smtp, email1,pwd1,"");
                          
            // insert = "mpass";
            } catch (Exception ex) {
              //  insert = "mfail";
            }
                    insert="pass";
                   
            } catch (Exception ex) {
                con.rollback();
                insert = "fail";
                System.out.println("the puot is "+ex);
                    
            }
           // session.setAttribute("insert", insert);
//            RequestDispatcher rq = request.getRequestDispatcher("user_creation");
//                rq.forward(request, response);
            response.sendRedirect("register_temp");
        } catch (Exception e) {
             System.out.println("the ouside ouput is "+e);
            throw new ServletException("Exception in  Servlet", e);
           
        } finally {
            if (out != null) {
                out.close();
            }
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {

                }
            }
        }

    }

    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        this.doGet(request, response);
    }
}
