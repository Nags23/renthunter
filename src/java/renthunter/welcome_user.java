/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package renthunter;


import java.io.IOException;
import java.io.OutputStream;
import java.util.Calendar;
import java.sql.*;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//@SuppressWarnings("deprecation")
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javax.servlet.*;
import javax.servlet.http.HttpSession;

public class welcome_user extends HttpServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String insert = "fail";
        Connection con = null;
        long connected;
        OutputStream out = null;
        PreparedStatement stmt, stmt1, stmt2, stmt3, stmt4, stmt5, stmt6, stmt7,stmt8,stmt9 = null;
        ResultSet rs, rs1, rs2, rs3, rs4, rs5, rs6,rs7,rs8,rs9 = null;
        ResourceBundle bundle = ResourceBundle.getBundle("SelectResource");
        HttpSession session = request.getSession();
        try {
            try {
                String url = bundle.getString("URL");
                String username = bundle.getString("username");
                String password = bundle.getString("password");
                Class.forName(bundle.getString("Driver"));
                con = DriverManager.getConnection(url, username, password);
            } catch (Exception e) {
                System.out.println("Database Connection Error" + e);
            }

            String currdate = "";
            Date date = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            String strDate = sdf.format(date);
            String time = new SimpleDateFormat("h:mm a").format(new Date());
            String dt = strDate + " " + time;
            int year = Calendar.getInstance().get(Calendar.YEAR);
            int yr = year % 100;
            String user = "";

             if(session.getAttribute("user")!=null){
                user=session.getAttribute("user").toString();
                session.setAttribute("user", user);
            }else{
                 RequestDispatcher rq = request.getRequestDispatcher("index.jsp");
            rq.forward(request, response);
            }

            String uid = "";
             stmt1 = con.prepareStatement("select id,firstname,email,mob from users where username='"+user+"'");
             rs1 = stmt1.executeQuery();
             if(rs1.next()){
                 uid = rs1.getString("id");
                 session.setAttribute("dash_fname", rs1.getString("firstname"));
                 session.setAttribute("dash_email", rs1.getString("email"));
                 session.setAttribute("dash_mob", rs1.getString("mob"));
             }
             
             ArrayList<String> dd_id = new ArrayList<String>();
             ArrayList<String> dd_street = new ArrayList<String>();
             ArrayList<String> dd_address = new ArrayList<String>();
             ArrayList<String> dd_floor = new ArrayList<String>();
             ArrayList<String> dd_dep = new ArrayList<String>();
             ArrayList<String> dd_rent = new ArrayList<String>();
             ArrayList<String> dd_area = new ArrayList<String>();
             ArrayList<String> dd_fur = new ArrayList<String>();
             ArrayList<String> dd_cstatus = new ArrayList<String>();
             ArrayList<String> dd_img = new ArrayList<String>();
             
             /*======================================Apartment Details===========================================*/
             stmt = con.prepareStatement("SELECT name_num,street_no,img4,cstatus,id,street_no,address_pro,floor,deposit_amt,rent_amt,area_build,fur FROM post_apartment where cuser='"+uid+"'");
             rs = stmt.executeQuery();
             while(rs.next()){
                 dd_img.add(rs.getString("img4"));
                 dd_cstatus.add("Apartment");
                 dd_id.add(rs.getString("id"));
                 dd_street.add(rs.getString("street_no"));
                 dd_address.add(rs.getString("name_num")+"   "+rs.getString("street_no"));
                 stmt1 = con.prepareStatement("SELECT floor FROM floor_types WHERE id='"+rs.getString("floor")+"'");
                 rs1 = stmt1.executeQuery();
                 if(rs1.next()){
                     dd_floor.add(rs1.getString("floor"));
                 }else{
                     dd_floor.add("");
                 }
                 dd_dep.add(rs.getString("deposit_amt"));
                 dd_rent.add(rs.getString("rent_amt"));
                 dd_area.add(rs.getString("area_build"));
                 stmt2 = con.prepareStatement("SELECT ftype FROM furnished_types WHERE id='"+rs.getString("fur")+"'");
                 rs2 = stmt2.executeQuery();
                 if(rs2.next()){
                     dd_fur.add(rs2.getString("ftype"));
                 }else{
                     dd_fur.add("");
                 }
             }
             
             
             /*======================================Home Details===========================================*/
             stmt = con.prepareStatement("SELECT name_num,street_no,img4,cstatus,id,street_no,address_pro,floor,deposit_amt,rent_amt,area_build,fur FROM post_home where cuser='"+uid+"'");
             rs = stmt.executeQuery();
             while(rs.next()){
                 dd_img.add(rs.getString("img4"));
                 dd_cstatus.add("Home");
                 dd_id.add(rs.getString("id"));
                 dd_street.add(rs.getString("street_no"));
                 dd_address.add(rs.getString("name_num")+"   "+rs.getString("street_no"));
                 stmt1 = con.prepareStatement("SELECT floor FROM floor_types WHERE id='"+rs.getString("floor")+"'");
                 rs1 = stmt1.executeQuery();
                 if(rs1.next()){
                     dd_floor.add(rs1.getString("floor"));
                 }else{
                     dd_floor.add("");
                 }
                 dd_dep.add(rs.getString("deposit_amt"));
                 dd_rent.add(rs.getString("rent_amt"));
                 dd_area.add(rs.getString("area_build"));
                 stmt2 = con.prepareStatement("SELECT ftype FROM furnished_types WHERE id='"+rs.getString("fur")+"'");
                 rs2 = stmt2.executeQuery();
                 if(rs2.next()){
                     dd_fur.add(rs2.getString("ftype"));
                 }else{
                     dd_fur.add("");
                 }
             }
             
             
             /*======================================PG Details===========================================*/
             stmt = con.prepareStatement("SELECT name_num,street_no,img4,cstatus,id,street_no,deposit_amt,rent_amt FROM post_pg where cuser='"+uid+"'");
             rs = stmt.executeQuery();
             while(rs.next()){
                 dd_img.add(rs.getString("img4"));
                 dd_cstatus.add("PG");
                 dd_id.add(rs.getString("id"));
                 dd_street.add(rs.getString("street_no"));
                 dd_address.add(rs.getString("name_num")+"   "+rs.getString("street_no"));
                 dd_floor.add("");
                 dd_dep.add(rs.getString("deposit_amt"));
                 dd_rent.add(rs.getString("rent_amt"));
                 dd_area.add("");
                 dd_fur.add("");
                 
             }
             
              /*======================================Office Details===========================================*/
             stmt = con.prepareStatement("SELECT name_num,street_no,img4,cstatus,id,street_no,deposit_amt,rent_amt FROM post_office where cuser='"+uid+"'");
             rs = stmt.executeQuery();
             while(rs.next()){
                 dd_img.add(rs.getString("img4"));
                 dd_cstatus.add("Office");
                 dd_id.add(rs.getString("id"));
                 dd_street.add(rs.getString("street_no"));
                 dd_address.add(rs.getString("name_num")+"   "+rs.getString("street_no"));
                 dd_floor.add("");
                 dd_dep.add(rs.getString("deposit_amt"));
                 dd_rent.add(rs.getString("rent_amt"));
                 dd_area.add("");
                 dd_fur.add("");
                 
             }
              /*======================================Shop Details===========================================*/
             stmt = con.prepareStatement("SELECT name_num,street_no,img4,cstatus,id,street_no,deposit_amt,rent_amt FROM post_shops where cuser='"+uid+"'");
             rs = stmt.executeQuery();
             while(rs.next()){
                 dd_img.add(rs.getString("img4"));
                 dd_cstatus.add("Shop");
                 dd_id.add(rs.getString("id"));
                 dd_street.add(rs.getString("street_no"));
                 dd_address.add(rs.getString("name_num")+"   "+rs.getString("street_no"));
                 dd_floor.add("");
                 dd_dep.add(rs.getString("deposit_amt"));
                 dd_rent.add(rs.getString("rent_amt"));
                 dd_area.add("");
                 dd_fur.add("");
                 
             }
             
             /*======================================Warehouse Details===========================================*/
             stmt = con.prepareStatement("SELECT name_num,street_no,img4,cstatus,id,street_no,deposit_amt,rent_amt FROM post_warehouse where cuser='"+uid+"'");
             rs = stmt.executeQuery();
             while(rs.next()){
                 dd_img.add(rs.getString("img4"));
                 dd_cstatus.add("Warehouse");
                 dd_id.add(rs.getString("id"));
                 dd_street.add(rs.getString("street_no"));
                 dd_address.add(rs.getString("name_num")+"   "+rs.getString("street_no"));
                 dd_floor.add("");
                 dd_dep.add(rs.getString("deposit_amt"));
                 dd_rent.add(rs.getString("rent_amt"));
                 dd_area.add("");
                 dd_fur.add("");
                 
             }
             
             /*======================================Specials Details===========================================*/
             stmt = con.prepareStatement("SELECT name_num,street_no,img4,cstatus,id,street_no,deposit_amt,rent_amt FROM post_specials where cuser='"+uid+"'");
             rs = stmt.executeQuery();
             while(rs.next()){
                 dd_img.add(rs.getString("img4"));
                 dd_cstatus.add("Specials");
                 dd_id.add(rs.getString("id"));
                 dd_street.add(rs.getString("street_no"));
                 dd_address.add(rs.getString("name_num")+"   "+rs.getString("street_no"));
                 dd_floor.add("");
                 dd_dep.add(rs.getString("deposit_amt"));
                 dd_rent.add(rs.getString("rent_amt"));
                 dd_area.add("");
                 dd_fur.add("");
                 
             }
             
             
             session.setAttribute("dd_id", dd_id);
             session.setAttribute("dd_street", dd_street);
             session.setAttribute("dd_address", dd_address);
             session.setAttribute("dd_floor", dd_floor);
             session.setAttribute("dd_dep", dd_dep);
             session.setAttribute("dd_rent", dd_rent);
             session.setAttribute("dd_area", dd_area);
             session.setAttribute("dd_fur", dd_fur);
             session.setAttribute("dd_cstatus", dd_cstatus);
             session.setAttribute("dd_img", dd_img);
             
            RequestDispatcher rq = request.getRequestDispatcher("welcome_user.jsp");
                rq.forward(request, response);
        } catch (Exception e) {
            throw new ServletException("Exception in  Servlet", e);
        } finally {
            if (out != null) {
                out.close();
            }
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {

                }
            }
        }

    }

    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        this.doGet(request, response);
    }
}
