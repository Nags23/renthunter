/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package masters;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Calendar;
import java.sql.*;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//@SuppressWarnings("deprecation")
import java.text.SimpleDateFormat;
import java.util.ResourceBundle;
import javax.servlet.*;
import javax.servlet.http.HttpSession;

public class define_amenities_process extends HttpServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String insert = "fail";
        Connection con = null;
        long connected;
        OutputStream out = null;
        PreparedStatement stmt, stmt1, stmt2, stmt3, stmt4, stmt5, stmt6 = null;
        ResultSet rs, rs1, rs2, rs3, rs4, rs5, rs6 = null;
        ResourceBundle bundle = ResourceBundle.getBundle("SelectResource");
        HttpSession session = request.getSession();
        try {
            try {
                String url = bundle.getString("URL");
                String username = bundle.getString("username");
                String password = bundle.getString("password");
                Class.forName(bundle.getString("Driver"));
                con = DriverManager.getConnection(url, username, password);
            } catch (Exception e) {
                System.out.println("Database Connection Error" + e);
            }

            String currdate = "";
            Date date = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            String strDate = sdf.format(date);
            String time = new SimpleDateFormat("h:mm a").format(new Date());
            String dt = strDate + " " + time;
            int year = Calendar.getInstance().get(Calendar.YEAR);
            int yr = year % 100;
            String user = "";
            
            if(session.getAttribute("user")!=null){
                user=session.getAttribute("user").toString();
            }else{
                 RequestDispatcher rq = request.getRequestDispatcher("index.jsp");
            rq.forward(request, response);
            }

            String atype = request.getParameter("atype");
            String fclass = request.getParameter("fclass");
            String chk = request.getParameter("chk");
            String ch = "";
            if(chk!=null && chk.length()>0){
                ch = "n";
            }else{
                ch = "y";
            }
              
               String uid = "";
          stmt = con.prepareStatement("select id from users where username='"+user+"'");
          rs = stmt.executeQuery();
          if(rs.next()){
              uid = rs.getString("id");
          }

            try {
                con.setAutoCommit(false);
            
               if(request.getParameter("id")!=null && request.getParameter("id").toString().length()>0){
                     stmt1 = con.prepareStatement("select id from amenities_type where atype='"+atype+"' and id!='"+request.getParameter("id")+"'");
                     rs1 = stmt1.executeQuery();
                     if(rs1.next()){
                         insert = "exists";
                     }else{
                          String id = request.getParameter("id");
                         stmt3 = con.prepareStatement("update amenities_type set atype='"+atype+"',fclass='"+fclass+"',disabled='"+ch+"' where id='"+id+"'");
                         stmt3.executeUpdate();
                         con.commit();
                    insert="pass";
                     }
                     }else{
                       stmt1 = con.prepareStatement("select id from amenities_type where atype='"+atype+"'");  
                       rs1 = stmt1.executeQuery();
                     if(rs1.next()){
                         insert="exists";
                     }else{
                     
                         
                         
                    stmt2=con.prepareStatement("insert into amenities_type(atype,fclass,disabled,cdate,ctime,cuser)values(?,?,?,?,?,?)");
                    stmt2.setString(1, atype);
                    stmt2.setString(2, fclass);
                    stmt2.setString(3, ch);
                    stmt2.setString(4, strDate);
                    stmt2.setString(5, time);
                    stmt2.setString(6, uid);
                    stmt2.executeUpdate();
                    con.commit();
                    insert="pass";
                     }
                }
            } catch (Exception ex) {
                con.rollback();
                insert = "fail";
                System.out.println("the puot is "+ex);
                    
            }
            session.setAttribute("insert", insert);
//            RequestDispatcher rq = request.getRequestDispatcher("user_creation");
//                rq.forward(request, response);
            response.sendRedirect("define_amenities_temp");
        } catch (Exception e) {
             System.out.println("the ouside ouput is "+e);
            throw new ServletException("Exception in  Servlet", e);
           
        } finally {
            if (out != null) {
                out.close();
            }
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {

                }
            }
        }

    }

    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        this.doGet(request, response);
    }
}
