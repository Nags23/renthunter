/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package masters;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.sql.*;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.ResourceBundle;
import javax.servlet.*;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class upload_areas extends HttpServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String insert = "fail";
        Connection con = null;
        long connected;
        OutputStream out = null;
        PreparedStatement stmt, stmt1, stmt2, stmt3, stmt4, stmt5, stmt6, stmt7,stmt8,stmt9 = null;
        ResultSet rs, rs1, rs2, rs3, rs4, rs5, rs6,rs7,rs8,rs9 = null;
        ResourceBundle bundle = ResourceBundle.getBundle("SelectResource");
        HttpSession session = request.getSession();
        try {
            try {
                String url = bundle.getString("URL");
                String username = bundle.getString("username");
                String password = bundle.getString("password");
                Class.forName(bundle.getString("Driver"));
                con = DriverManager.getConnection(url, username, password);
            } catch (Exception e) {
                System.out.println("Database Connection Error" + e);
            }

            String currdate = "";
            Date date = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            String strDate = sdf.format(date);
            String time = new SimpleDateFormat("h:mm a").format(new Date());
            String dt = strDate + " " + time;
            int year = Calendar.getInstance().get(Calendar.YEAR);
            int cmonth = Calendar.getInstance().get(Calendar.MONTH);
            int yr = year % 100;
            String user = "";
            
             if(session.getAttribute("user")!=null){
                user=session.getAttribute("user").toString();
                session.setAttribute("user", user);
            }else{
                 RequestDispatcher rq = request.getRequestDispatcher("index.jsp");
            rq.forward(request, response);
            }
            String uid = "";
            
            stmt = con.prepareStatement("select id from users where username='"+user+"'");
            rs = stmt.executeQuery();
            if(rs.next()){
                uid = rs.getString("id");
            }
            
            String contentType = request.getContentType();
            System.out.println("content type is :"+contentType);
            if (contentType.toLowerCase().contains("multipart")) {
BufferedReader reader = new BufferedReader(new InputStreamReader(request.getInputStream()));
String data= null;
try{
    con.setAutoCommit(false);
do {

    // skip first header lines which contains the header data such as Content-Disposition or Content-Type
        data = reader.readLine();
       
     
    } while(!(data.toLowerCase().contains("content-type")));

    while( (data = reader.readLine()) != null) {
        if(data.startsWith("--")) {
            continue;
        }                   

        if (!data.toLowerCase().contains("webkitformboundary")) {
            if (!data.toLowerCase().contains("content-disposition")) {
                System.out.println("+++++++++++++++++++ data: "+data);
                if(data.length()>0){
                   
                   System.out.println("data at 2 is :"+data.split(",")[0]);
                   if(data.split(",")[0].equalsIgnoreCase("State Name")){
                       
                   }else{
                       try{
                      
                           con.setAutoCommit(false);
                           
                        String sid = "",cid = "";
                        stmt = con.prepareStatement("select id from state_types where sname='"+data.split(",")[0]+"'");
                        rs = stmt.executeQuery();
                        if(rs.next()){
                            sid = rs.getString("id");
                        }
                        
                        stmt = con.prepareStatement("select id from city_types where cname='"+data.split(",")[1]+"'");
                        rs = stmt.executeQuery();
                        if(rs.next()){
                            cid = rs.getString("id");
                        }
                           
                        
//                      String pcode = "";
//                      pcode = data.split(",")[2].trim().substring(0, 2)+Math.floor(Math.random()*10000);
                      stmt6 = con.prepareStatement("SELECT id FROM area_types where sname='"+sid+"' and cname='"+data.split(",")[1]+"' and aname='"+data.split(",")[2]+"'");
                      rs6 = stmt6.executeQuery();
                      if(rs6.next()){
                          
                      }else{
                     stmt2 = con.prepareStatement("insert into area_types(sname,cname,aname,disabled,cdate,ctime,cuser)values(?,?,?,?,?,?,?)");
                    stmt2.setString(1, sid);
                    stmt2.setString(2, cid);
                    stmt2.setString(3, data.split(",")[2]);
                    stmt2.setString(4, "n");
                    stmt2.setString(5, strDate);
                    stmt2.setString(6, time);
                    stmt2.setString(7, uid);
                    stmt2.executeUpdate();
                      }
                     
                       }catch(Exception e){
                           System.out.println("err is "+e);
                       }
                }
                }
            }
        }
    }
    con.commit();
    insert="pass";
}catch(Exception e){
  con.rollback();
  insert="fail";
    System.out.println("err is :"+e);
    e.printStackTrace();
   // session.setAttribute("frsn", e);
}
}
            session.setAttribute("insert", insert);
//            RequestDispatcher rq = request.getRequestDispatcher("code_config_temp");
//                rq.forward(request, response);
            response.sendRedirect("add_areas_temp");
        } catch (Exception e) {
            throw new ServletException("Exception in  Servlet", e);
        } finally {
            if (out != null) {
                out.close();
            }
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {

                }
            }
        }

    }

    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        this.doGet(request, response);
    }
}
