/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package masters;


import java.io.IOException;
import java.io.OutputStream;
import java.util.Calendar;
import java.sql.*;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//@SuppressWarnings("deprecation")
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javax.servlet.*;
import javax.servlet.http.HttpSession;

public class add_cities extends HttpServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String insert = "fail";
        Connection con = null;
        long connected;
        OutputStream out = null;
        PreparedStatement stmt, stmt1, stmt2, stmt3, stmt4, stmt5, stmt6, stmt7,stmt8,stmt9 = null;
        ResultSet rs, rs1, rs2, rs3, rs4, rs5, rs6,rs7,rs8,rs9 = null;
        ResourceBundle bundle = ResourceBundle.getBundle("SelectResource");
        HttpSession session = request.getSession();
        try {
            try {
                String url = bundle.getString("URL");
                String username = bundle.getString("username");
                String password = bundle.getString("password");
                Class.forName(bundle.getString("Driver"));
                con = DriverManager.getConnection(url, username, password);
            } catch (Exception e) {
                System.out.println("Database Connection Error" + e);
            }

            String currdate = "";
            Date date = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            String strDate = sdf.format(date);
            String time = new SimpleDateFormat("h:mm a").format(new Date());
            String dt = strDate + " " + time;
            int year = Calendar.getInstance().get(Calendar.YEAR);
            int yr = year % 100;
            String user = "";

             if(session.getAttribute("user")!=null){
                user=session.getAttribute("user").toString();
                session.setAttribute("user", user);
            }else{
                 RequestDispatcher rq = request.getRequestDispatcher("index.jsp");
            rq.forward(request, response);
            }
             
             ArrayList<String> dd_id = new ArrayList<String>();
             ArrayList<String> dd_sname = new ArrayList<String>();
             
             stmt = con.prepareStatement("select id,sname,disabled from state_types where disabled='n' order by id asc");
             rs = stmt.executeQuery();
             while(rs.next()){
                dd_id.add(rs.getString("id")); 
                dd_sname.add(rs.getString("sname"));
             }
             
             session.setAttribute("dd_id", dd_id);
             session.setAttribute("dd_sname", dd_sname);
             
             ArrayList<String> ss_id = new ArrayList<String>();
             ArrayList<String> ss_sid = new ArrayList<String>();
             ArrayList<String> ss_sname = new ArrayList<String>();
             ArrayList<String> ss_cname = new ArrayList<String>();
             ArrayList<String> ss_disabled = new ArrayList<String>();
             
             stmt1 = con.prepareStatement("select id,sname,cname,disabled from city_types order by cname asc");
             rs1 = stmt1.executeQuery();
             while(rs1.next()){
                 ss_id.add(rs1.getString("id"));
                 stmt2 = con.prepareStatement("select id,sname from state_types where id='"+rs1.getString("sname")+"'");
                 rs2 = stmt2.executeQuery();
                 if(rs2.next()){
                     ss_sid.add(rs2.getString("id"));
                     ss_sname.add(rs2.getString("sname"));
                 }else{
                     ss_sid.add("");
                     ss_sname.add("");
                 }
                 ss_cname.add(rs1.getString("cname"));
                 ss_disabled.add(rs1.getString("disabled"));
             }
             
             session.setAttribute("ss_id", ss_id);
             session.setAttribute("ss_sid", ss_sid);
             session.setAttribute("ss_sname", ss_sname);
             session.setAttribute("ss_cname", ss_cname);
             session.setAttribute("ss_disabled", ss_disabled);

            RequestDispatcher rq = request.getRequestDispatcher("masters/add_cities.jsp");
                rq.forward(request, response);
        } catch (Exception e) {
            throw new ServletException("Exception in  Servlet", e);
        } finally {
            if (out != null) {
                out.close();
            }
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {

                }
            }
        }

    }

    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        this.doGet(request, response);
    }
}
