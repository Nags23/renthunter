/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package guest;


import java.io.IOException;
import java.io.OutputStream;
import java.util.Calendar;
import java.sql.*;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//@SuppressWarnings("deprecation")
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javax.servlet.*;
import javax.servlet.http.HttpSession;

public class search_results extends HttpServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String insert = "fail";
        Connection con = null;
        long connected;
        OutputStream out = null;
        PreparedStatement stmt, stmt1, stmt2, stmt3, stmt4, stmt5, stmt6, stmt7,stmt8,stmt9 = null;
        ResultSet rs, rs1, rs2, rs3, rs4, rs5, rs6,rs7,rs8,rs9 = null;
        ResourceBundle bundle = ResourceBundle.getBundle("SelectResource");
        HttpSession session = request.getSession();
        try {
            try {
                String url = bundle.getString("URL");
                String username = bundle.getString("username");
                String password = bundle.getString("password");
                Class.forName(bundle.getString("Driver"));
                con = DriverManager.getConnection(url, username, password);
            } catch (Exception e) {
                System.out.println("Database Connection Error" + e);
            }

            String currdate = "";
            Date date = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            String strDate = sdf.format(date);
            String time = new SimpleDateFormat("h:mm a").format(new Date());
            String dt = strDate + " " + time;
            int year = Calendar.getInstance().get(Calendar.YEAR);
            int yr = year % 100;
            String user = "";

            String srch_text = request.getParameter("srch_text");
            String srch_type = request.getParameter("srch_type");
             
             ArrayList<String> dd_id = new ArrayList<String>();
             ArrayList<String> dd_street = new ArrayList<String>();
             ArrayList<String> dd_address = new ArrayList<String>();
             ArrayList<String> dd_floor = new ArrayList<String>();
             ArrayList<String> dd_dep = new ArrayList<String>();
             ArrayList<String> dd_rent = new ArrayList<String>();
             ArrayList<String> dd_area = new ArrayList<String>();
             ArrayList<String> dd_fur = new ArrayList<String>();
             ArrayList<String> dd_cstatus = new ArrayList<String>();
             ArrayList<String> dd_img1 = new ArrayList<String>();
             ArrayList<String> dd_img2 = new ArrayList<String>();
             ArrayList<String> dd_img3 = new ArrayList<String>();
             ArrayList<String> dd_img4 = new ArrayList<String>();
             ArrayList<String> dd_img5 = new ArrayList<String>();
             ArrayList<String> dd_img6 = new ArrayList<String>();
             ArrayList<String> dd_name_num = new ArrayList<String>();
             ArrayList<String> dd_bhk = new ArrayList<String>();
             ArrayList<String> dd_btype = new ArrayList<String>();
             ArrayList<String> dd_bin = new ArrayList<String>();
             ArrayList<String> dd_adate1 = new ArrayList<String>();
             ArrayList<String> dd_srch_type = new ArrayList<String>();
             ArrayList<String> dd_kyc_utype = new ArrayList<String>();
             
             ArrayList<String> dd_aname = new ArrayList<String>();
             ArrayList<String> dd_city = new ArrayList<String>();
             ArrayList<String> dd_state = new ArrayList<String>();
             
             if(srch_type.equalsIgnoreCase("home")){
             stmt = con.prepareStatement("SELECT aname,city,state,kyc_utype,DATE_FORMAT(adate,'%d/%m/%Y')AS adate1,area_build_type,area_build_in,bhk,name_num,img1,img2,img3,img4,img5,img6,cstatus,id,street_no,address_pro,floor,deposit_amt,rent_amt,area_build,fur FROM post_home where name_num LIKE '%"+srch_text+"%' || street_no LIKE '%"+srch_text+"%' || address_pro LIKE '%"+srch_text+"%' || land_mark LIKE '%"+srch_text+"%'");
             rs = stmt.executeQuery();
             while(rs.next()){
                 dd_kyc_utype.add(rs.getString("kyc_utype"));
                 dd_srch_type.add("Home");
                 dd_adate1.add(rs.getString("adate1"));
                 dd_btype.add(rs.getString("area_build_type"));
                 dd_bin.add(rs.getString("area_build_in"));
                 dd_name_num.add(rs.getString("name_num"));
                 dd_img1.add(rs.getString("img1"));
                 dd_img2.add(rs.getString("img2"));
                 dd_img3.add(rs.getString("img3"));
                 dd_img4.add(rs.getString("img4"));
                 dd_img5.add(rs.getString("img5"));
                 dd_img6.add(rs.getString("img6"));
                 dd_cstatus.add(rs.getString("cstatus"));
                 dd_id.add(rs.getString("id"));
                 dd_street.add(rs.getString("street_no"));
                 dd_address.add(rs.getString("address_pro"));
                 if(srch_type.equalsIgnoreCase("pg")){
                  dd_floor.add("");   
                 }else{
                 stmt1 = con.prepareStatement("SELECT floor FROM floor_types WHERE id='"+rs.getString("floor")+"'");
                 rs1 = stmt1.executeQuery();
                 if(rs1.next()){
                     dd_floor.add(rs1.getString("floor"));
                 }else{
                     dd_floor.add("");
                 }
                 }
                 dd_dep.add(rs.getString("deposit_amt"));
                 dd_rent.add(rs.getString("rent_amt"));
                 if(srch_type.equalsIgnoreCase("pg")){
                   dd_fur.add("");  
                   dd_area.add("");
                 }else{
                 dd_area.add(rs.getString("area_build"));
                 stmt2 = con.prepareStatement("SELECT ftype FROM furnished_types WHERE id='"+rs.getString("fur")+"'");
                 rs2 = stmt2.executeQuery();
                 if(rs2.next()){
                     dd_fur.add(rs2.getString("ftype"));
                 }else{
                     dd_fur.add("");
                 }
                 
                 stmt2 = con.prepareStatement("SELECT bhk FROM bhk_types WHERE id='"+rs.getString("bhk")+"'");
                 rs2 = stmt2.executeQuery();
                 if(rs2.next()){
                     dd_bhk.add(rs2.getString("bhk"));
                 }else{
                     dd_bhk.add("");
                 }
                 }
                 
                 stmt3 = con.prepareStatement("SELECT aname FROM area_types WHERE id='"+rs.getString("aname")+"'");
             rs3 = stmt3.executeQuery();
             if(rs3.next()){
                 dd_aname.add(rs3.getString("aname"));
             }else{
                 dd_aname.add("");
             }
             
             stmt3 = con.prepareStatement("SELECT cname FROM city_types WHERE id='"+rs.getString("city")+"'");
             rs3 = stmt3.executeQuery();
             if(rs3.next()){
                 dd_city.add(rs3.getString("cname"));
             }else{
                 dd_city.add("");
             }
             
             stmt3 = con.prepareStatement("SELECT sname FROM state_types where id='"+rs.getString("state")+"'");
             rs3 = stmt3.executeQuery();
             if(rs3.next()){
                 dd_state.add(rs3.getString("sname"));
             }else{
                 dd_state.add("");
             }
             }
             
             }
             
             
              if(srch_type.equalsIgnoreCase("apart")){
             stmt = con.prepareStatement("SELECT aname,city,state,kyc_utype,DATE_FORMAT(adate,'%d/%m/%Y')AS adate1,area_build_type,area_build_in,bhk,name_num,img1,img2,img3,img4,img5,img6,cstatus,id,street_no,address_pro,floor,deposit_amt,rent_amt,area_build,fur FROM post_apartment where address_pro LIKE '%"+srch_text+"%' || land_mark LIKE '%"+srch_text+"%'");
           
             rs = stmt.executeQuery();
             while(rs.next()){
                 dd_kyc_utype.add(rs.getString("kyc_utype"));
                 dd_srch_type.add("Apartment");
                 dd_adate1.add(rs.getString("adate1"));
                 dd_btype.add(rs.getString("area_build_type"));
                 dd_bin.add(rs.getString("area_build_in"));
                 dd_name_num.add(rs.getString("name_num"));
                 dd_img1.add(rs.getString("img1"));
                 dd_img2.add(rs.getString("img2"));
                 dd_img3.add(rs.getString("img3"));
                 dd_img4.add(rs.getString("img4"));
                 dd_img5.add(rs.getString("img5"));
                 dd_img6.add(rs.getString("img6"));
                 dd_cstatus.add(rs.getString("cstatus"));
                 dd_id.add(rs.getString("id"));
                 dd_street.add(rs.getString("street_no"));
                 dd_address.add(rs.getString("address_pro"));
                 if(srch_type.equalsIgnoreCase("pg")){
                  dd_floor.add("");   
                 }else{
                 stmt1 = con.prepareStatement("SELECT floor FROM floor_types WHERE id='"+rs.getString("floor")+"'");
                 rs1 = stmt1.executeQuery();
                 if(rs1.next()){
                     dd_floor.add(rs1.getString("floor"));
                 }else{
                     dd_floor.add("");
                 }
                 }
                 dd_dep.add(rs.getString("deposit_amt"));
                 dd_rent.add(rs.getString("rent_amt"));
                 if(srch_type.equalsIgnoreCase("pg")){
                   dd_fur.add("");  
                   dd_area.add("");
                 }else{
                 dd_area.add(rs.getString("area_build"));
                 stmt2 = con.prepareStatement("SELECT ftype FROM furnished_types WHERE id='"+rs.getString("fur")+"'");
                 rs2 = stmt2.executeQuery();
                 if(rs2.next()){
                     dd_fur.add(rs2.getString("ftype"));
                 }else{
                     dd_fur.add("");
                 }
                 
                 stmt2 = con.prepareStatement("SELECT bhk FROM bhk_types WHERE id='"+rs.getString("bhk")+"'");
                 rs2 = stmt2.executeQuery();
                 if(rs2.next()){
                     dd_bhk.add(rs2.getString("bhk"));
                 }else{
                     dd_bhk.add("");
                 }
                 }
                 
                 stmt3 = con.prepareStatement("SELECT aname FROM area_types WHERE id='"+rs.getString("aname")+"'");
             rs3 = stmt3.executeQuery();
             if(rs3.next()){
                 dd_aname.add(rs3.getString("aname"));
             }else{
                 dd_aname.add("");
             }
             
             stmt3 = con.prepareStatement("SELECT cname FROM city_types WHERE id='"+rs.getString("city")+"'");
             rs3 = stmt3.executeQuery();
             if(rs3.next()){
                 dd_city.add(rs3.getString("cname"));
             }else{
                 dd_city.add("");
             }
             
             stmt3 = con.prepareStatement("SELECT sname FROM state_types where id='"+rs.getString("state")+"'");
             rs3 = stmt3.executeQuery();
             if(rs3.next()){
                 dd_state.add(rs3.getString("sname"));
             }else{
                 dd_state.add("");
             }
             }
             
             }
              
              
              if(srch_type.equalsIgnoreCase("pg")){
             stmt = con.prepareStatement("SELECT aname,city,state,kyc_utype,DATE_FORMAT(adate,'%d/%m/%Y')AS adate1,name_num,img1,img2,img3,img4,img5,img6,cstatus,id,street_no,address_pro,deposit_amt,rent_amt,fur FROM post_pg where address_pro LIKE '%"+srch_text+"%' || land_mark LIKE '%"+srch_text+"%'");
             rs = stmt.executeQuery();
             while(rs.next()){
                 dd_kyc_utype.add(rs.getString("kyc_utype"));
                 dd_srch_type.add("PG");
                 dd_adate1.add(rs.getString("adate1"));
                 dd_btype.add("");
                 dd_bin.add("");
                 dd_name_num.add(rs.getString("name_num"));
                 dd_img1.add(rs.getString("img1"));
                 dd_img2.add(rs.getString("img2"));
                 dd_img3.add(rs.getString("img3"));
                 dd_img4.add(rs.getString("img4"));
                 dd_img5.add(rs.getString("img5"));
                 dd_img6.add(rs.getString("img6"));
                 dd_cstatus.add(rs.getString("cstatus"));
                 dd_id.add(rs.getString("id"));
                 dd_street.add(rs.getString("street_no"));
                 dd_address.add(rs.getString("address_pro"));
                 if(srch_type.equalsIgnoreCase("pg")){
                  dd_floor.add("");   
                 }
                 dd_dep.add(rs.getString("deposit_amt"));
                 dd_rent.add(rs.getString("rent_amt"));
                 
                 dd_area.add("");
                 stmt2 = con.prepareStatement("SELECT ftype FROM furnished_types WHERE id='"+rs.getString("fur")+"'");
                 rs2 = stmt2.executeQuery();
                 if(rs2.next()){
                     dd_fur.add(rs2.getString("ftype"));
                 }else{
                     dd_fur.add("");
                 }
                 dd_bhk.add("");
                 
                 stmt3 = con.prepareStatement("SELECT aname FROM area_types WHERE id='"+rs.getString("aname")+"'");
             rs3 = stmt3.executeQuery();
             if(rs3.next()){
                 dd_aname.add(rs3.getString("aname"));
             }else{
                 dd_aname.add("");
             }
             
             stmt3 = con.prepareStatement("SELECT cname FROM city_types WHERE id='"+rs.getString("city")+"'");
             rs3 = stmt3.executeQuery();
             if(rs3.next()){
                 dd_city.add(rs3.getString("cname"));
             }else{
                 dd_city.add("");
             }
             
             stmt3 = con.prepareStatement("SELECT sname FROM state_types where id='"+rs.getString("state")+"'");
             rs3 = stmt3.executeQuery();
             if(rs3.next()){
                 dd_state.add(rs3.getString("sname"));
             }else{
                 dd_state.add("");
             }
             }
             
             }
              
              
              
              if(srch_type.equalsIgnoreCase("offc")){
             stmt = con.prepareStatement("SELECT aname,city,state,kyc_utype,DATE_FORMAT(adate,'%d/%m/%Y')AS adate1,name_num,img1,img2,img3,img4,img5,img6,cstatus,id,street_no,address_pro,deposit_amt,rent_amt,fur FROM post_office where address_pro LIKE '%"+srch_text+"%' || land_mark LIKE '%"+srch_text+"%'");
             rs = stmt.executeQuery();
             while(rs.next()){
                 dd_kyc_utype.add(rs.getString("kyc_utype"));
                 dd_srch_type.add("OFFICE");
                 dd_adate1.add(rs.getString("adate1"));
                 dd_btype.add("");
                 dd_bin.add("");
                 dd_name_num.add(rs.getString("name_num"));
                 dd_img1.add(rs.getString("img1"));
                 dd_img2.add(rs.getString("img2"));
                 dd_img3.add(rs.getString("img3"));
                 dd_img4.add(rs.getString("img4"));
                 dd_img5.add(rs.getString("img5"));
                 dd_img6.add(rs.getString("img6"));
                 dd_cstatus.add(rs.getString("cstatus"));
                 dd_id.add(rs.getString("id"));
                 dd_street.add(rs.getString("street_no"));
                 dd_address.add(rs.getString("address_pro"));
                 if(srch_type.equalsIgnoreCase("pg")){
                  dd_floor.add("");   
                 }
                 dd_dep.add(rs.getString("deposit_amt"));
                 dd_rent.add(rs.getString("rent_amt"));
                 
                 dd_area.add("");
                 stmt2 = con.prepareStatement("SELECT ftype FROM furnished_types WHERE id='"+rs.getString("fur")+"'");
                 rs2 = stmt2.executeQuery();
                 if(rs2.next()){
                     dd_fur.add(rs2.getString("ftype"));
                 }else{
                     dd_fur.add("");
                 }
                 dd_bhk.add("");
                 
                 stmt3 = con.prepareStatement("SELECT aname FROM area_types WHERE id='"+rs.getString("aname")+"'");
             rs3 = stmt3.executeQuery();
             if(rs3.next()){
                 dd_aname.add(rs3.getString("aname"));
             }else{
                 dd_aname.add("");
             }
             
             stmt3 = con.prepareStatement("SELECT cname FROM city_types WHERE id='"+rs.getString("city")+"'");
             rs3 = stmt3.executeQuery();
             if(rs3.next()){
                 dd_city.add(rs3.getString("cname"));
             }else{
                 dd_city.add("");
             }
             
             stmt3 = con.prepareStatement("SELECT sname FROM state_types where id='"+rs.getString("state")+"'");
             rs3 = stmt3.executeQuery();
             if(rs3.next()){
                 dd_state.add(rs3.getString("sname"));
             }else{
                 dd_state.add("");
             }
             }
             
             }
              
              
              if(srch_type.equalsIgnoreCase("shop")){
             stmt = con.prepareStatement("SELECT aname,city,state,kyc_utype,DATE_FORMAT(adate,'%d/%m/%Y')AS adate1,name_num,img1,img2,img3,img4,img5,img6,cstatus,id,street_no,address_pro,deposit_amt,rent_amt,fur FROM post_shops where address_pro LIKE '%"+srch_text+"%' || land_mark LIKE '%"+srch_text+"%'");
             rs = stmt.executeQuery();
             while(rs.next()){
                 dd_kyc_utype.add(rs.getString("kyc_utype"));
                 dd_srch_type.add("SHOPS");
                 dd_adate1.add(rs.getString("adate1"));
                 dd_btype.add("");
                 dd_bin.add("");
                 dd_name_num.add(rs.getString("name_num"));
                 dd_img1.add(rs.getString("img1"));
                 dd_img2.add(rs.getString("img2"));
                 dd_img3.add(rs.getString("img3"));
                 dd_img4.add(rs.getString("img4"));
                 dd_img5.add(rs.getString("img5"));
                 dd_img6.add(rs.getString("img6"));
                 dd_cstatus.add(rs.getString("cstatus"));
                 dd_id.add(rs.getString("id"));
                 dd_street.add(rs.getString("street_no"));
                 dd_address.add(rs.getString("address_pro"));
                 if(srch_type.equalsIgnoreCase("pg")){
                  dd_floor.add("");   
                 }
                 dd_dep.add(rs.getString("deposit_amt"));
                 dd_rent.add(rs.getString("rent_amt"));
                 
                 dd_area.add("");
                 stmt2 = con.prepareStatement("SELECT ftype FROM furnished_types WHERE id='"+rs.getString("fur")+"'");
                 rs2 = stmt2.executeQuery();
                 if(rs2.next()){
                     dd_fur.add(rs2.getString("ftype"));
                 }else{
                     dd_fur.add("");
                 }
                 dd_bhk.add("");
                 
                 stmt3 = con.prepareStatement("SELECT aname FROM area_types WHERE id='"+rs.getString("aname")+"'");
             rs3 = stmt3.executeQuery();
             if(rs3.next()){
                 dd_aname.add(rs3.getString("aname"));
             }else{
                 dd_aname.add("");
             }
             
             stmt3 = con.prepareStatement("SELECT cname FROM city_types WHERE id='"+rs.getString("city")+"'");
             rs3 = stmt3.executeQuery();
             if(rs3.next()){
                 dd_city.add(rs3.getString("cname"));
             }else{
                 dd_city.add("");
             }
             
             stmt3 = con.prepareStatement("SELECT sname FROM state_types where id='"+rs.getString("state")+"'");
             rs3 = stmt3.executeQuery();
             if(rs3.next()){
                 dd_state.add(rs3.getString("sname"));
             }else{
                 dd_state.add("");
             }
                 
             }
             
             }
              
              
              if(srch_type.equalsIgnoreCase("ware")){
             stmt = con.prepareStatement("SELECT aname,city,state,kyc_utype,DATE_FORMAT(adate,'%d/%m/%Y')AS adate1,name_num,img1,img2,img3,img4,img5,img6,cstatus,id,street_no,address_pro,deposit_amt,rent_amt,fur FROM post_warehouse where address_pro LIKE '%"+srch_text+"%' || land_mark LIKE '%"+srch_text+"%'");
             rs = stmt.executeQuery();
             while(rs.next()){
                 dd_kyc_utype.add(rs.getString("kyc_utype"));
                 dd_srch_type.add("WAREHOUSE");
                 dd_adate1.add(rs.getString("adate1"));
                 dd_btype.add("");
                 dd_bin.add("");
                 dd_name_num.add(rs.getString("name_num"));
                 dd_img1.add(rs.getString("img1"));
                 dd_img2.add(rs.getString("img2"));
                 dd_img3.add(rs.getString("img3"));
                 dd_img4.add(rs.getString("img4"));
                 dd_img5.add(rs.getString("img5"));
                 dd_img6.add(rs.getString("img6"));
                 dd_cstatus.add(rs.getString("cstatus"));
                 dd_id.add(rs.getString("id"));
                 dd_street.add(rs.getString("street_no"));
                 dd_address.add(rs.getString("address_pro"));
                 if(srch_type.equalsIgnoreCase("pg")){
                  dd_floor.add("");   
                 }
                 dd_dep.add(rs.getString("deposit_amt"));
                 dd_rent.add(rs.getString("rent_amt"));
                 
                 dd_area.add("");
                 stmt2 = con.prepareStatement("SELECT ftype FROM furnished_types WHERE id='"+rs.getString("fur")+"'");
                 rs2 = stmt2.executeQuery();
                 if(rs2.next()){
                     dd_fur.add(rs2.getString("ftype"));
                 }else{
                     dd_fur.add("");
                 }
                 dd_bhk.add("");
                 
                 stmt3 = con.prepareStatement("SELECT aname FROM area_types WHERE id='"+rs.getString("aname")+"'");
             rs3 = stmt3.executeQuery();
             if(rs3.next()){
                 dd_aname.add(rs3.getString("aname"));
             }else{
                 dd_aname.add("");
             }
             
             stmt3 = con.prepareStatement("SELECT cname FROM city_types WHERE id='"+rs.getString("city")+"'");
             rs3 = stmt3.executeQuery();
             if(rs3.next()){
                 dd_city.add(rs3.getString("cname"));
             }else{
                 dd_city.add("");
             }
             
             stmt3 = con.prepareStatement("SELECT sname FROM state_types where id='"+rs.getString("state")+"'");
             rs3 = stmt3.executeQuery();
             if(rs3.next()){
                 dd_state.add(rs3.getString("sname"));
             }else{
                 dd_state.add("");
             }
                 
             }
             
             }
              
              
              if(srch_type.equalsIgnoreCase("special")){
             stmt = con.prepareStatement("SELECT aname,city,state,kyc_utype,DATE_FORMAT(adate,'%d/%m/%Y')AS adate1,area_build_type,area_build_in,name_num,img1,img2,img3,img4,img5,img6,cstatus,id,street_no,address_pro,floor,deposit_amt,rent_amt,area_build,fur FROM post_specials where address_pro LIKE '%"+srch_text+"%' || land_mark LIKE '%"+srch_text+"%'");
             rs = stmt.executeQuery();
             while(rs.next()){
                 dd_kyc_utype.add(rs.getString("kyc_utype"));
                 dd_srch_type.add("Specials");
                 dd_adate1.add(rs.getString("adate1"));
                 dd_btype.add(rs.getString("area_build_type"));
                 dd_bin.add(rs.getString("area_build_in"));
                 dd_name_num.add(rs.getString("name_num"));
                 dd_img1.add(rs.getString("img1"));
                 dd_img2.add(rs.getString("img2"));
                 dd_img3.add(rs.getString("img3"));
                 dd_img4.add(rs.getString("img4"));
                 dd_img5.add(rs.getString("img5"));
                 dd_img6.add(rs.getString("img6"));
                 dd_cstatus.add(rs.getString("cstatus"));
                 dd_id.add(rs.getString("id"));
                 dd_street.add(rs.getString("street_no"));
                 dd_address.add(rs.getString("address_pro"));
                 if(srch_type.equalsIgnoreCase("pg")){
                  dd_floor.add("");   
                 }else{
                 stmt1 = con.prepareStatement("SELECT floor FROM floor_types WHERE id='"+rs.getString("floor")+"'");
                 rs1 = stmt1.executeQuery();
                 if(rs1.next()){
                     dd_floor.add(rs1.getString("floor"));
                 }else{
                     dd_floor.add("");
                 }
                 }
                 dd_dep.add(rs.getString("deposit_amt"));
                 dd_rent.add(rs.getString("rent_amt"));
                 if(srch_type.equalsIgnoreCase("pg")){
                   dd_fur.add("");  
                   dd_area.add("");
                 }else{
                 dd_area.add(rs.getString("area_build"));
                 stmt2 = con.prepareStatement("SELECT ftype FROM furnished_types WHERE id='"+rs.getString("fur")+"'");
                 rs2 = stmt2.executeQuery();
                 if(rs2.next()){
                     dd_fur.add(rs2.getString("ftype"));
                 }else{
                     dd_fur.add("");
                 }
                  dd_bhk.add("");
                 
                 }
             }
             stmt3 = con.prepareStatement("SELECT aname FROM area_types WHERE id='"+rs.getString("aname")+"'");
             rs3 = stmt3.executeQuery();
             if(rs3.next()){
                 dd_aname.add(rs3.getString("aname"));
             }else{
                 dd_aname.add("");
             }
             
             stmt3 = con.prepareStatement("SELECT cname FROM city_types WHERE id='"+rs.getString("city")+"'");
             rs3 = stmt3.executeQuery();
             if(rs3.next()){
                 dd_city.add(rs3.getString("cname"));
             }else{
                 dd_city.add("");
             }
             
             stmt3 = con.prepareStatement("SELECT sname FROM state_types where id='"+rs.getString("state")+"'");
             rs3 = stmt3.executeQuery();
             if(rs3.next()){
                 dd_state.add(rs3.getString("sname"));
             }else{
                 dd_state.add("");
             }
             
             }
             
             session.setAttribute("dd_aname", dd_aname);
             session.setAttribute("dd_city", dd_city);
             session.setAttribute("dd_state", dd_state);
             session.setAttribute("dd_srch_type", dd_srch_type);
             session.setAttribute("dd_id", dd_id);
             session.setAttribute("dd_street", dd_street);
             session.setAttribute("dd_address", dd_address);
             session.setAttribute("dd_floor", dd_floor);
             session.setAttribute("dd_dep", dd_dep);
             session.setAttribute("dd_rent", dd_rent);
             session.setAttribute("dd_area", dd_area);
             session.setAttribute("dd_fur", dd_fur);
             session.setAttribute("dd_cstatus", dd_cstatus);
             session.setAttribute("dd_img1", dd_img1);
             session.setAttribute("dd_img2", dd_img2);
             session.setAttribute("dd_img3", dd_img3);
             session.setAttribute("dd_img4", dd_img4);
             session.setAttribute("dd_img5", dd_img5);
             session.setAttribute("dd_img6", dd_img6);
             session.setAttribute("dd_name_num", dd_name_num);
             session.setAttribute("dd_bhk", dd_bhk);
             session.setAttribute("dd_btype", dd_btype);
             session.setAttribute("dd_bin", dd_bin);
             session.setAttribute("dd_adate1", dd_adate1);
             session.setAttribute("dd_kyc_utype", dd_kyc_utype);

            RequestDispatcher rq = request.getRequestDispatcher("guest/search_results.jsp");
                rq.forward(request, response);
        } catch (Exception e) {
            throw new ServletException("Exception in  Servlet", e);
        } finally {
            if (out != null) {
                out.close();
            }
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {

                }
            }
        }

    }

    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        this.doGet(request, response);
    }
}
