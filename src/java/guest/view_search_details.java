/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package guest;


import java.io.IOException;
import java.io.OutputStream;
import java.util.Calendar;
import java.sql.*;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//@SuppressWarnings("deprecation")
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javax.servlet.*;
import javax.servlet.http.HttpSession;

public class view_search_details extends HttpServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String insert = "fail";
        Connection con = null;
        long connected;
        OutputStream out = null;
        PreparedStatement stmt, stmt1, stmt2, stmt3, stmt4, stmt5, stmt6, stmt7,stmt8,stmt9 = null;
        ResultSet rs, rs1, rs2, rs3, rs4, rs5, rs6,rs7,rs8,rs9 = null;
        ResourceBundle bundle = ResourceBundle.getBundle("SelectResource");
        HttpSession session = request.getSession();
        try {
            try {
                String url = bundle.getString("URL");
                String username = bundle.getString("username");
                String password = bundle.getString("password");
                Class.forName(bundle.getString("Driver"));
                con = DriverManager.getConnection(url, username, password);
            } catch (Exception e) {
                System.out.println("Database Connection Error" + e);
            }

            String currdate = "";
            Date date = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            String strDate = sdf.format(date);
            String time = new SimpleDateFormat("h:mm a").format(new Date());
            String dt = strDate + " " + time;
            int year = Calendar.getInstance().get(Calendar.YEAR);
            int yr = year % 100;
            String user = "";

            String id = request.getParameter("id");
            String stype = request.getParameter("stype");
            session.setAttribute("dd_stype", stype);
            if(stype.equalsIgnoreCase("Home")){
             
             String near_by = "",tenant_type = "";
             stmt = con.prepareStatement("SELECT rule_non_veg,rule_pets,rule_drink,rule_water,rule_elec,oter_details4,rent_amt,deposit_amt,nego,maintanece_amt,agreement_dur,info_leave,tenant_type,bhk,area_build,area_build_type,area_build_in,date_format(adate,'%d/%m/%Y')as adate1,near_by,name_num,street_no,land_mark,city,state,age_pro,address_pro,img1,img2,img3,img4,img5,img6,fur,floor,facings,parking,tenant_type,rent_amt,deposit_amt,nego FROM post_home where id='"+id+"'");
             rs = stmt.executeQuery();
             if(rs.next()){
                 
                 
                 near_by = rs.getString("near_by");
                 tenant_type = rs.getString("tenant_type");
                 
                 session.setAttribute("ss_rule_non_veg", rs.getString("rule_non_veg"));
                 session.setAttribute("ss_rule_pets", rs.getString("rule_pets"));
                 session.setAttribute("ss_rule_drink", rs.getString("rule_drink"));
                 session.setAttribute("ss_rule_water", rs.getString("rule_water"));
                 session.setAttribute("ss_rule_elec", rs.getString("rule_elec"));
                 session.setAttribute("ss_oter_details4", rs.getString("oter_details4"));
                 
                 session.setAttribute("ss_rent_amt", rs.getString("rent_amt"));
                 session.setAttribute("ss_deposit_amt", rs.getString("deposit_amt"));
                 session.setAttribute("ss_nego", rs.getString("nego"));
                 session.setAttribute("ss_maintanece_amt", rs.getString("maintanece_amt"));
                 session.setAttribute("ss_agreement_dur", rs.getString("agreement_dur"));
                 session.setAttribute("ss_info_leave", rs.getString("info_leave"));
                 
                 session.setAttribute("ss_area_build", rs.getString("area_build"));
                 session.setAttribute("ss_area_btype", rs.getString("area_build_type"));
                 session.setAttribute("ss_area_bin", rs.getString("area_build_in"));
                 session.setAttribute("ss_name_num", rs.getString("name_num"));
                 session.setAttribute("ss_adate1", rs.getString("adate1"));
                 session.setAttribute("ss_street", rs.getString("street_no"));
                 session.setAttribute("ss_lmark", rs.getString("land_mark"));
                 session.setAttribute("ss_city", rs.getString("city"));
                 session.setAttribute("ss_state", rs.getString("state"));
                 session.setAttribute("ss_age_pro", rs.getString("age_pro"));
                 session.setAttribute("ss_address_pro", rs.getString("address_pro"));
                 session.setAttribute("ss_img1", rs.getString("img1"));
                 session.setAttribute("ss_img2", rs.getString("img2"));
                 session.setAttribute("ss_img3", rs.getString("img3"));
                 session.setAttribute("ss_img4", rs.getString("img4"));
                 session.setAttribute("ss_img5", rs.getString("img5"));
                 session.setAttribute("ss_img6", rs.getString("img6"));
                 
                 session.setAttribute("ss_rent", rs.getString("rent_amt"));
                 session.setAttribute("ss_deposit", rs.getString("deposit_amt"));
                 session.setAttribute("ss_nego", rs.getString("nego"));
                 
                 stmt1 = con.prepareStatement("SELECT ttype FROM tenant_types WHERE id='"+rs.getString("tenant_type")+"'");
                 rs1 = stmt1.executeQuery();
                 if(rs1.next()){
                     session.setAttribute("ss_ttype", rs1.getString("ttype"));
                 }else{
                     session.setAttribute("ss_ttype", "");
                 }
                 
                 stmt1 = con.prepareStatement("SELECT bhk FROM bhk_types WHERE id='"+rs.getString("bhk")+"'");
                 rs1 = stmt1.executeQuery();
                 if(rs1.next()){
                     session.setAttribute("ss_bhk", rs1.getString("bhk"));
                 }else{
                     session.setAttribute("ss_bhk", "");
                 }
                 
                 stmt1 = con.prepareStatement("SELECT ftype FROM furnished_types WHERE id='"+rs.getString("fur")+"'");
                 rs1 = stmt1.executeQuery();
                 if(rs1.next()){
                     session.setAttribute("ss_fur", rs1.getString("ftype"));
                 }else{
                     session.setAttribute("ss_fur", "");
                 }
                 
                 stmt1 = con.prepareStatement("SELECT floor FROM floor_types WHERE id='"+rs.getString("floor")+"'");
                 rs1 = stmt1.executeQuery();
                 if(rs1.next()){
                     session.setAttribute("ss_floor", rs1.getString("floor"));
                 }
                 
                 stmt1 = con.prepareStatement("SELECT facings FROM facing_types WHERE id='"+rs.getString("facings")+"'");
                 rs1 = stmt1.executeQuery();
                 if(rs1.next()){
                     session.setAttribute("ss_facings", rs1.getString("facings"));
                 }
                 
                 session.setAttribute("ss_parking", rs.getString("parking"));
             }
             
             ArrayList<String> dd_near = new ArrayList<String>();
             for(int i=0;i<near_by.split(",").length;i++){
             stmt = con.prepareStatement("SELECT name FROM near_by WHERE id='"+near_by.split(",")[i]+"'");
             rs = stmt.executeQuery();
             if(rs.next()){
                 dd_near.add(rs.getString("name"));
             }
                     }
             session.setAttribute("dd_near", dd_near);
             
             
             ArrayList<String> dd_tenant = new ArrayList<String>();
             ArrayList<String> dd_tenant_class = new ArrayList<String>();
             for(int i=0;i<tenant_type.split(",").length;i++){
             stmt = con.prepareStatement("SELECT ttype,fclass FROM tenant_types WHERE id='"+tenant_type.split(",")[i]+"'");
             rs = stmt.executeQuery();
             if(rs.next()){
                 dd_tenant.add(rs.getString("ttype"));
                 dd_tenant_class.add(rs.getString("fclass"));
             }
                     }
             session.setAttribute("dd_tenant", dd_tenant);
             session.setAttribute("dd_tenant_class", dd_tenant_class);
             
             
             ArrayList<String> amn_det = new ArrayList<String>();
             ArrayList<String> amn_fa = new ArrayList<String>();
             
             stmt2 = con.prepareStatement("SELECT amn FROM home_amn WHERE apa_id='"+id+"'");
             rs2 = stmt2.executeQuery();
             while(rs2.next()){
                 stmt3 = con.prepareStatement("SELECT atype,fclass FROM amenities_type where id='"+rs2.getString("amn")+"'");
                 rs3 = stmt3.executeQuery();
                 if(rs3.next()){
                     amn_det.add(rs3.getString("atype"));
                     amn_fa.add(rs3.getString("fclass"));
                 }
             }
             
             session.setAttribute("amn_det", amn_det);
             session.setAttribute("amn_fa", amn_fa);
             
             ArrayList<String> ser_det = new ArrayList<String>();
             ArrayList<String> ser_fa = new ArrayList<String>();
             
             stmt2 = con.prepareStatement("SELECT ser FROM home_ser WHERE apa_id='"+id+"'");
             rs2 = stmt2.executeQuery();
             while(rs2.next()){
                 stmt3 = con.prepareStatement("SELECT stype,fclass FROM service_type where id='"+rs2.getString("ser")+"'");
                 rs3 = stmt3.executeQuery();
                 if(rs3.next()){
                     ser_det.add(rs3.getString("stype"));
                     ser_fa.add(rs3.getString("fclass"));
                 }
             }
             
             session.setAttribute("ser_det", ser_det);
             session.setAttribute("ser_fa", ser_fa);
             
            }
            
            
            if(stype.equalsIgnoreCase("Apartment")){
             
             String near_by = "",tenant_type = "";
             stmt = con.prepareStatement("SELECT rule_non_veg,rule_pets,rule_drink,rule_water,rule_elec,oter_details4,rent_amt,deposit_amt,nego,maintanece_amt,agreement_dur,info_leave,tenant_type,bhk,area_build,area_build_type,area_build_in,date_format(adate,'%d/%m/%Y')as adate1,near_by,name_num,street_no,land_mark,city,state,age_pro,address_pro,img1,img2,img3,img4,img5,img6,fur,floor,facings,parking,tenant_type,rent_amt,deposit_amt,nego FROM post_apartment where id='"+id+"'");
             rs = stmt.executeQuery();
             if(rs.next()){
                 near_by = rs.getString("near_by");
                 tenant_type = rs.getString("tenant_type");
                 
                 session.setAttribute("ss_rule_non_veg", rs.getString("rule_non_veg"));
                 session.setAttribute("ss_rule_pets", rs.getString("rule_pets"));
                 session.setAttribute("ss_rule_drink", rs.getString("rule_drink"));
                 session.setAttribute("ss_rule_water", rs.getString("rule_water"));
                 session.setAttribute("ss_rule_elec", rs.getString("rule_elec"));
                 session.setAttribute("ss_oter_details4", rs.getString("oter_details4"));
                 
                 session.setAttribute("ss_rent_amt", rs.getString("rent_amt"));
                 session.setAttribute("ss_deposit_amt", rs.getString("deposit_amt"));
                 session.setAttribute("ss_nego", rs.getString("nego"));
                 session.setAttribute("ss_maintanece_amt", rs.getString("maintanece_amt"));
                 session.setAttribute("ss_agreement_dur", rs.getString("agreement_dur"));
                 session.setAttribute("ss_info_leave", rs.getString("info_leave"));
                 
                 session.setAttribute("ss_area_build", rs.getString("area_build"));
                 session.setAttribute("ss_area_btype", rs.getString("area_build_type"));
                 session.setAttribute("ss_area_bin", rs.getString("area_build_in"));
                 session.setAttribute("ss_name_num", rs.getString("name_num"));
                 session.setAttribute("ss_adate1", rs.getString("adate1"));
                 session.setAttribute("ss_street", rs.getString("street_no"));
                 session.setAttribute("ss_lmark", rs.getString("land_mark"));
                 session.setAttribute("ss_city", rs.getString("city"));
                 session.setAttribute("ss_state", rs.getString("state"));
                 session.setAttribute("ss_age_pro", rs.getString("age_pro"));
                 session.setAttribute("ss_address_pro", rs.getString("address_pro"));
                 session.setAttribute("ss_img1", rs.getString("img1"));
                 session.setAttribute("ss_img2", rs.getString("img2"));
                 session.setAttribute("ss_img3", rs.getString("img3"));
                 session.setAttribute("ss_img4", rs.getString("img4"));
                 session.setAttribute("ss_img5", rs.getString("img5"));
                 session.setAttribute("ss_img6", rs.getString("img6"));
                 
                 session.setAttribute("ss_rent", rs.getString("rent_amt"));
                 session.setAttribute("ss_deposit", rs.getString("deposit_amt"));
                 session.setAttribute("ss_nego", rs.getString("nego"));
                 
                 stmt1 = con.prepareStatement("SELECT ttype FROM tenant_types WHERE id='"+rs.getString("tenant_type")+"'");
                 rs1 = stmt1.executeQuery();
                 if(rs1.next()){
                     session.setAttribute("ss_ttype", rs1.getString("ttype"));
                 }else{
                     session.setAttribute("ss_ttype", "");
                 }
                 
                 stmt1 = con.prepareStatement("SELECT bhk FROM bhk_types WHERE id='"+rs.getString("bhk")+"'");
                 rs1 = stmt1.executeQuery();
                 if(rs1.next()){
                     session.setAttribute("ss_bhk", rs1.getString("bhk"));
                 }else{
                     session.setAttribute("ss_bhk", "");
                 }
                 
                 stmt1 = con.prepareStatement("SELECT ftype FROM furnished_types WHERE id='"+rs.getString("fur")+"'");
                 rs1 = stmt1.executeQuery();
                 if(rs1.next()){
                     session.setAttribute("ss_fur", rs1.getString("ftype"));
                 }else{
                     session.setAttribute("ss_fur", "");
                 }
                 
                 stmt1 = con.prepareStatement("SELECT floor FROM floor_types WHERE id='"+rs.getString("floor")+"'");
                 rs1 = stmt1.executeQuery();
                 if(rs1.next()){
                     session.setAttribute("ss_floor", rs1.getString("floor"));
                 }
                 
                 stmt1 = con.prepareStatement("SELECT facings FROM facing_types WHERE id='"+rs.getString("facings")+"'");
                 rs1 = stmt1.executeQuery();
                 if(rs1.next()){
                     session.setAttribute("ss_facings", rs1.getString("facings"));
                 }
                 
                 session.setAttribute("ss_parking", rs.getString("parking"));
             }
             
             ArrayList<String> dd_near = new ArrayList<String>();
             for(int i=0;i<near_by.split(",").length;i++){
             stmt = con.prepareStatement("SELECT name FROM near_by WHERE id='"+near_by.split(",")[i]+"'");
             rs = stmt.executeQuery();
             if(rs.next()){
                 dd_near.add(rs.getString("name"));
             }
                     }
             session.setAttribute("dd_near", dd_near);
             
             
             ArrayList<String> dd_tenant = new ArrayList<String>();
             ArrayList<String> dd_tenant_class = new ArrayList<String>();
             for(int i=0;i<tenant_type.split(",").length;i++){
             stmt = con.prepareStatement("SELECT ttype,fclass FROM tenant_types WHERE id='"+tenant_type.split(",")[i]+"'");
             rs = stmt.executeQuery();
             if(rs.next()){
                 dd_tenant.add(rs.getString("ttype"));
                 dd_tenant_class.add(rs.getString("fclass"));
             }
                     }
             session.setAttribute("dd_tenant", dd_tenant);
             session.setAttribute("dd_tenant_class", dd_tenant_class);
             
             
             ArrayList<String> amn_det = new ArrayList<String>();
             ArrayList<String> amn_fa = new ArrayList<String>();
             
             stmt2 = con.prepareStatement("SELECT amn FROM apartment_amn WHERE apa_id='"+id+"'");
             rs2 = stmt2.executeQuery();
             while(rs2.next()){
                 stmt3 = con.prepareStatement("SELECT atype,fclass FROM amenities_type where id='"+rs2.getString("amn")+"'");
                 rs3 = stmt3.executeQuery();
                 if(rs3.next()){
                     amn_det.add(rs3.getString("atype"));
                     amn_fa.add(rs3.getString("fclass"));
                 }
             }
             
             session.setAttribute("amn_det", amn_det);
             session.setAttribute("amn_fa", amn_fa);
             
             ArrayList<String> ser_det = new ArrayList<String>();
             ArrayList<String> ser_fa = new ArrayList<String>();
             
             stmt2 = con.prepareStatement("SELECT ser FROM apartment_ser WHERE apa_id='"+id+"'");
             rs2 = stmt2.executeQuery();
             while(rs2.next()){
                 stmt3 = con.prepareStatement("SELECT stype,fclass FROM service_type where id='"+rs2.getString("ser")+"'");
                 rs3 = stmt3.executeQuery();
                 if(rs3.next()){
                     ser_det.add(rs3.getString("stype"));
                     ser_fa.add(rs3.getString("fclass"));
                 }
             }
             
             session.setAttribute("ser_det", ser_det);
             session.setAttribute("ser_fa", ser_fa);
             
            }
            
            
            if(stype.equalsIgnoreCase("PG")){
             
             String near_by = "",tenant_type = "";
             stmt = con.prepareStatement("SELECT food,food1,share,share1,rule_non_veg,rule_pets,rule_drink,rule_water,rule_elec,oter_details4,rent_amt,deposit_amt,nego,maintanece_amt,agreement_dur,info_leave,tenant_type,date_format(adate,'%d/%m/%Y')as adate1,near_by,name_num,street_no,land_mark,city,state,age_pro,address_pro,img1,img2,img3,img4,img5,img6,fur,parking,tenant_type,rent_amt,deposit_amt,nego FROM post_pg where id='"+id+"'");
             rs = stmt.executeQuery();
             if(rs.next()){
                 
                 session.setAttribute("ss_food", rs.getString("food"));
                 session.setAttribute("ss_food1", rs.getString("food1"));
                 session.setAttribute("ss_share", rs.getString("share"));
                 session.setAttribute("ss_share1", rs.getString("share1"));
                 near_by = rs.getString("near_by");
                 tenant_type = rs.getString("tenant_type");
                 
                 session.setAttribute("ss_rule_non_veg", rs.getString("rule_non_veg"));
                 session.setAttribute("ss_rule_pets", rs.getString("rule_pets"));
                 session.setAttribute("ss_rule_drink", rs.getString("rule_drink"));
                 session.setAttribute("ss_rule_water", rs.getString("rule_water"));
                 session.setAttribute("ss_rule_elec", rs.getString("rule_elec"));
                 session.setAttribute("ss_oter_details4", rs.getString("oter_details4"));
                 
                 session.setAttribute("ss_rent_amt", rs.getString("rent_amt"));
                 session.setAttribute("ss_deposit_amt", rs.getString("deposit_amt"));
                 session.setAttribute("ss_nego", rs.getString("nego"));
                 session.setAttribute("ss_maintanece_amt", rs.getString("maintanece_amt"));
                 session.setAttribute("ss_agreement_dur", rs.getString("agreement_dur"));
                 session.setAttribute("ss_info_leave", rs.getString("info_leave"));
                 
                 session.setAttribute("ss_area_build", "");
                 session.setAttribute("ss_area_btype", "");
                 session.setAttribute("ss_area_bin", "");
                 session.setAttribute("ss_name_num", rs.getString("name_num"));
                 session.setAttribute("ss_adate1", rs.getString("adate1"));
                 session.setAttribute("ss_street", rs.getString("street_no"));
                 session.setAttribute("ss_lmark", rs.getString("land_mark"));
                 session.setAttribute("ss_city", rs.getString("city"));
                 session.setAttribute("ss_state", rs.getString("state"));
                 session.setAttribute("ss_age_pro", rs.getString("age_pro"));
                 session.setAttribute("ss_address_pro", rs.getString("address_pro"));
                 session.setAttribute("ss_img1", rs.getString("img1"));
                 session.setAttribute("ss_img2", rs.getString("img2"));
                 session.setAttribute("ss_img3", rs.getString("img3"));
                 session.setAttribute("ss_img4", rs.getString("img4"));
                 session.setAttribute("ss_img5", rs.getString("img5"));
                 session.setAttribute("ss_img6", rs.getString("img6"));
                 
                 session.setAttribute("ss_rent", rs.getString("rent_amt"));
                 session.setAttribute("ss_deposit", rs.getString("deposit_amt"));
                 session.setAttribute("ss_nego", rs.getString("nego"));
                 
                 stmt1 = con.prepareStatement("SELECT ttype FROM tenant_types WHERE id='"+rs.getString("tenant_type")+"'");
                 rs1 = stmt1.executeQuery();
                 if(rs1.next()){
                     session.setAttribute("ss_ttype", rs1.getString("ttype"));
                 }else{
                     session.setAttribute("ss_ttype", "");
                 }
                 session.setAttribute("ss_bhk", "");
                 
                 
                 stmt1 = con.prepareStatement("SELECT ftype FROM furnished_types WHERE id='"+rs.getString("fur")+"'");
                 rs1 = stmt1.executeQuery();
                 if(rs1.next()){
                     session.setAttribute("ss_fur", rs1.getString("ftype"));
                 }else{
                     session.setAttribute("ss_fur", "");
                 }
                 
                 
                 
                 session.setAttribute("ss_parking", rs.getString("parking"));
             }
             
             ArrayList<String> dd_near = new ArrayList<String>();
             for(int i=0;i<near_by.split(",").length;i++){
             stmt = con.prepareStatement("SELECT name FROM near_by WHERE id='"+near_by.split(",")[i]+"'");
             rs = stmt.executeQuery();
             if(rs.next()){
                 dd_near.add(rs.getString("name"));
             }
                     }
             session.setAttribute("dd_near", dd_near);
             
             
             ArrayList<String> dd_tenant = new ArrayList<String>();
             ArrayList<String> dd_tenant_class = new ArrayList<String>();
             for(int i=0;i<tenant_type.split(",").length;i++){
             stmt = con.prepareStatement("SELECT ttype,fclass FROM tenant_types WHERE id='"+tenant_type.split(",")[i]+"'");
             rs = stmt.executeQuery();
             if(rs.next()){
                 dd_tenant.add(rs.getString("ttype"));
                 dd_tenant_class.add(rs.getString("fclass"));
             }
                     }
             session.setAttribute("dd_tenant", dd_tenant);
             session.setAttribute("dd_tenant_class", dd_tenant_class);
             
             
             ArrayList<String> amn_det = new ArrayList<String>();
             ArrayList<String> amn_fa = new ArrayList<String>();
             
             stmt2 = con.prepareStatement("SELECT amn FROM pg_amn WHERE apa_id='"+id+"'");
             rs2 = stmt2.executeQuery();
             while(rs2.next()){
                 stmt3 = con.prepareStatement("SELECT atype,fclass FROM amenities_type where id='"+rs2.getString("amn")+"'");
                 rs3 = stmt3.executeQuery();
                 if(rs3.next()){
                     amn_det.add(rs3.getString("atype"));
                     amn_fa.add(rs3.getString("fclass"));
                 }
             }
             
             session.setAttribute("amn_det", amn_det);
             session.setAttribute("amn_fa", amn_fa);
             
             ArrayList<String> ser_det = new ArrayList<String>();
             ArrayList<String> ser_fa = new ArrayList<String>();
             
             stmt2 = con.prepareStatement("SELECT ser FROM pg_ser WHERE apa_id='"+id+"'");
             rs2 = stmt2.executeQuery();
             while(rs2.next()){
                 stmt3 = con.prepareStatement("SELECT stype,fclass FROM service_type where id='"+rs2.getString("ser")+"'");
                 rs3 = stmt3.executeQuery();
                 if(rs3.next()){
                     ser_det.add(rs3.getString("stype"));
                     ser_fa.add(rs3.getString("fclass"));
                 }
             }
             
             session.setAttribute("ser_det", ser_det);
             session.setAttribute("ser_fa", ser_fa);
             
            }
            
            
            if(stype.equalsIgnoreCase("OFFICE")){
             
             String near_by = "",tenant_type = "";
             stmt = con.prepareStatement("SELECT facings,area_build,area_build_type,area_build_in,rule_non_veg,rule_pets,rule_drink,rule_water,rule_elec,oter_details4,rent_amt,deposit_amt,nego,maintanece_amt,agreement_dur,info_leave,tenant_type,date_format(adate,'%d/%m/%Y')as adate1,near_by,name_num,street_no,land_mark,city,state,age_pro,address_pro,img1,img2,img3,img4,img5,img6,fur,floor,parking,tenant_type,rent_amt,deposit_amt,nego FROM post_office where id='"+id+"'");
             rs = stmt.executeQuery();
             if(rs.next()){
                 
                  near_by = rs.getString("near_by");
                 tenant_type = rs.getString("tenant_type");
                 
                 session.setAttribute("ss_rule_non_veg", rs.getString("rule_non_veg"));
                 session.setAttribute("ss_rule_pets", rs.getString("rule_pets"));
                 session.setAttribute("ss_rule_drink", rs.getString("rule_drink"));
                 session.setAttribute("ss_rule_water", rs.getString("rule_water"));
                 session.setAttribute("ss_rule_elec", rs.getString("rule_elec"));
                 session.setAttribute("ss_oter_details4", rs.getString("oter_details4"));
                 
                 session.setAttribute("ss_rent_amt", rs.getString("rent_amt"));
                 session.setAttribute("ss_deposit_amt", rs.getString("deposit_amt"));
                 session.setAttribute("ss_nego", rs.getString("nego"));
                 session.setAttribute("ss_maintanece_amt", rs.getString("maintanece_amt"));
                 session.setAttribute("ss_agreement_dur", rs.getString("agreement_dur"));
                 session.setAttribute("ss_info_leave", rs.getString("info_leave"));
                 
                 session.setAttribute("ss_area_build", rs.getString("area_build"));
                 session.setAttribute("ss_area_btype", rs.getString("area_build_type"));
                 session.setAttribute("ss_area_bin", rs.getString("area_build_in"));
                 session.setAttribute("ss_name_num", rs.getString("name_num"));
                 session.setAttribute("ss_adate1", rs.getString("adate1"));
                 session.setAttribute("ss_street", rs.getString("street_no"));
                 session.setAttribute("ss_lmark", rs.getString("land_mark"));
                 session.setAttribute("ss_city", rs.getString("city"));
                 session.setAttribute("ss_state", rs.getString("state"));
                 session.setAttribute("ss_age_pro", rs.getString("age_pro"));
                 session.setAttribute("ss_address_pro", rs.getString("address_pro"));
                 session.setAttribute("ss_img1", rs.getString("img1"));
                 session.setAttribute("ss_img2", rs.getString("img2"));
                 session.setAttribute("ss_img3", rs.getString("img3"));
                 session.setAttribute("ss_img4", rs.getString("img4"));
                 session.setAttribute("ss_img5", rs.getString("img5"));
                 session.setAttribute("ss_img6", rs.getString("img6"));
                 
                 session.setAttribute("ss_rent", rs.getString("rent_amt"));
                 session.setAttribute("ss_deposit", rs.getString("deposit_amt"));
                 session.setAttribute("ss_nego", rs.getString("nego"));
                 
                 stmt1 = con.prepareStatement("SELECT ttype FROM tenant_types WHERE id='"+rs.getString("tenant_type")+"'");
                 rs1 = stmt1.executeQuery();
                 if(rs1.next()){
                     session.setAttribute("ss_ttype", rs1.getString("ttype"));
                 }else{
                     session.setAttribute("ss_ttype", "");
                 }
                 
                 session.setAttribute("ss_bhk", "");
                 
                 stmt1 = con.prepareStatement("SELECT ftype FROM furnished_types WHERE id='"+rs.getString("fur")+"'");
                 rs1 = stmt1.executeQuery();
                 if(rs1.next()){
                     session.setAttribute("ss_fur", rs1.getString("ftype"));
                 }else{
                     session.setAttribute("ss_fur", "");
                 }
                 
                 stmt1 = con.prepareStatement("SELECT floor FROM floor_types WHERE id='"+rs.getString("floor")+"'");
                 rs1 = stmt1.executeQuery();
                 if(rs1.next()){
                     session.setAttribute("ss_floor", rs1.getString("floor"));
                 }
                 
                 stmt1 = con.prepareStatement("SELECT facings FROM facing_types WHERE id='"+rs.getString("facings")+"'");
                 rs1 = stmt1.executeQuery();
                 if(rs1.next()){
                     session.setAttribute("ss_facings", rs1.getString("facings"));
                 }
                 
                 session.setAttribute("ss_parking", rs.getString("parking"));
             }
             
             ArrayList<String> dd_near = new ArrayList<String>();
             for(int i=0;i<near_by.split(",").length;i++){
             stmt = con.prepareStatement("SELECT name FROM near_by WHERE id='"+near_by.split(",")[i]+"'");
             rs = stmt.executeQuery();
             if(rs.next()){
                 dd_near.add(rs.getString("name"));
             }
                     }
             session.setAttribute("dd_near", dd_near);
             
             
             ArrayList<String> dd_tenant = new ArrayList<String>();
             ArrayList<String> dd_tenant_class = new ArrayList<String>();
             for(int i=0;i<tenant_type.split(",").length;i++){
             stmt = con.prepareStatement("SELECT ttype,fclass FROM tenant_types WHERE id='"+tenant_type.split(",")[i]+"'");
             rs = stmt.executeQuery();
             if(rs.next()){
                 dd_tenant.add(rs.getString("ttype"));
                 dd_tenant_class.add(rs.getString("fclass"));
             }
                     }
             session.setAttribute("dd_tenant", dd_tenant);
             session.setAttribute("dd_tenant_class", dd_tenant_class);
             
             
             ArrayList<String> amn_det = new ArrayList<String>();
             ArrayList<String> amn_fa = new ArrayList<String>();
             
             stmt2 = con.prepareStatement("SELECT amn FROM office_amn WHERE apa_id='"+id+"'");
             rs2 = stmt2.executeQuery();
             while(rs2.next()){
                 stmt3 = con.prepareStatement("SELECT atype,fclass FROM amenities_type where id='"+rs2.getString("amn")+"'");
                 rs3 = stmt3.executeQuery();
                 if(rs3.next()){
                     amn_det.add(rs3.getString("atype"));
                     amn_fa.add(rs3.getString("fclass"));
                 }
             }
             
             session.setAttribute("amn_det", amn_det);
             session.setAttribute("amn_fa", amn_fa);
             
             ArrayList<String> ser_det = new ArrayList<String>();
             ArrayList<String> ser_fa = new ArrayList<String>();
             
             stmt2 = con.prepareStatement("SELECT ser FROM office_ser WHERE apa_id='"+id+"'");
             rs2 = stmt2.executeQuery();
             while(rs2.next()){
                 stmt3 = con.prepareStatement("SELECT stype,fclass FROM service_type where id='"+rs2.getString("ser")+"'");
                 rs3 = stmt3.executeQuery();
                 if(rs3.next()){
                     ser_det.add(rs3.getString("stype"));
                     ser_fa.add(rs3.getString("fclass"));
                 }
             }
             
             session.setAttribute("ser_det", ser_det);
             session.setAttribute("ser_fa", ser_fa);
             
            }
            
            
            if(stype.equalsIgnoreCase("SHOPS")){
             
             String near_by = "",tenant_type = "";
             stmt = con.prepareStatement("SELECT facings,area_build,area_build_type,area_build_in,rule_non_veg,rule_pets,rule_drink,rule_water,rule_elec,oter_details4,rent_amt,deposit_amt,nego,maintanece_amt,agreement_dur,info_leave,tenant_type,date_format(adate,'%d/%m/%Y')as adate1,near_by,name_num,street_no,land_mark,city,state,age_pro,address_pro,img1,img2,img3,img4,img5,img6,fur,floor,parking,tenant_type,rent_amt,deposit_amt,nego FROM post_shops where id='"+id+"'");
             rs = stmt.executeQuery();
             if(rs.next()){
                 
                  near_by = rs.getString("near_by");
                 tenant_type = rs.getString("tenant_type");
                 
                 session.setAttribute("ss_rule_non_veg", rs.getString("rule_non_veg"));
                 session.setAttribute("ss_rule_pets", rs.getString("rule_pets"));
                 session.setAttribute("ss_rule_drink", rs.getString("rule_drink"));
                 session.setAttribute("ss_rule_water", rs.getString("rule_water"));
                 session.setAttribute("ss_rule_elec", rs.getString("rule_elec"));
                 session.setAttribute("ss_oter_details4", rs.getString("oter_details4"));
                 
                 session.setAttribute("ss_rent_amt", rs.getString("rent_amt"));
                 session.setAttribute("ss_deposit_amt", rs.getString("deposit_amt"));
                 session.setAttribute("ss_nego", rs.getString("nego"));
                 session.setAttribute("ss_maintanece_amt", rs.getString("maintanece_amt"));
                 session.setAttribute("ss_agreement_dur", rs.getString("agreement_dur"));
                 session.setAttribute("ss_info_leave", rs.getString("info_leave"));
                 
                 session.setAttribute("ss_area_build", rs.getString("area_build"));
                 session.setAttribute("ss_area_btype", rs.getString("area_build_type"));
                 session.setAttribute("ss_area_bin", rs.getString("area_build_in"));
                 session.setAttribute("ss_name_num", rs.getString("name_num"));
                 session.setAttribute("ss_adate1", rs.getString("adate1"));
                 session.setAttribute("ss_street", rs.getString("street_no"));
                 session.setAttribute("ss_lmark", rs.getString("land_mark"));
                 session.setAttribute("ss_city", rs.getString("city"));
                 session.setAttribute("ss_state", rs.getString("state"));
                 session.setAttribute("ss_age_pro", rs.getString("age_pro"));
                 session.setAttribute("ss_address_pro", rs.getString("address_pro"));
                 session.setAttribute("ss_img1", rs.getString("img1"));
                 session.setAttribute("ss_img2", rs.getString("img2"));
                 session.setAttribute("ss_img3", rs.getString("img3"));
                 session.setAttribute("ss_img4", rs.getString("img4"));
                 session.setAttribute("ss_img5", rs.getString("img5"));
                 session.setAttribute("ss_img6", rs.getString("img6"));
                 
                 session.setAttribute("ss_rent", rs.getString("rent_amt"));
                 session.setAttribute("ss_deposit", rs.getString("deposit_amt"));
                 session.setAttribute("ss_nego", rs.getString("nego"));
                 
                 stmt1 = con.prepareStatement("SELECT ttype FROM tenant_types WHERE id='"+rs.getString("tenant_type")+"'");
                 rs1 = stmt1.executeQuery();
                 if(rs1.next()){
                     session.setAttribute("ss_ttype", rs1.getString("ttype"));
                 }else{
                     session.setAttribute("ss_ttype", "");
                 }
                 
                 session.setAttribute("ss_bhk", "");
                 
                 stmt1 = con.prepareStatement("SELECT ftype FROM furnished_types WHERE id='"+rs.getString("fur")+"'");
                 rs1 = stmt1.executeQuery();
                 if(rs1.next()){
                     session.setAttribute("ss_fur", rs1.getString("ftype"));
                 }else{
                     session.setAttribute("ss_fur", "");
                 }
                 
                 stmt1 = con.prepareStatement("SELECT floor FROM floor_types WHERE id='"+rs.getString("floor")+"'");
                 rs1 = stmt1.executeQuery();
                 if(rs1.next()){
                     session.setAttribute("ss_floor", rs1.getString("floor"));
                 }
                 
                 stmt1 = con.prepareStatement("SELECT facings FROM facing_types WHERE id='"+rs.getString("facings")+"'");
                 rs1 = stmt1.executeQuery();
                 if(rs1.next()){
                     session.setAttribute("ss_facings", rs1.getString("facings"));
                 }
                 
                 session.setAttribute("ss_parking", rs.getString("parking"));
             }
             
             ArrayList<String> dd_near = new ArrayList<String>();
             for(int i=0;i<near_by.split(",").length;i++){
             stmt = con.prepareStatement("SELECT name FROM near_by WHERE id='"+near_by.split(",")[i]+"'");
             rs = stmt.executeQuery();
             if(rs.next()){
                 dd_near.add(rs.getString("name"));
             }
                     }
             session.setAttribute("dd_near", dd_near);
             
             
             ArrayList<String> dd_tenant = new ArrayList<String>();
             ArrayList<String> dd_tenant_class = new ArrayList<String>();
             for(int i=0;i<tenant_type.split(",").length;i++){
             stmt = con.prepareStatement("SELECT ttype,fclass FROM tenant_types WHERE id='"+tenant_type.split(",")[i]+"'");
             rs = stmt.executeQuery();
             if(rs.next()){
                 dd_tenant.add(rs.getString("ttype"));
                 dd_tenant_class.add(rs.getString("fclass"));
             }
                     }
             session.setAttribute("dd_tenant", dd_tenant);
             session.setAttribute("dd_tenant_class", dd_tenant_class);
             
             
             ArrayList<String> amn_det = new ArrayList<String>();
             ArrayList<String> amn_fa = new ArrayList<String>();
             
             stmt2 = con.prepareStatement("SELECT amn FROM shops_amn WHERE apa_id='"+id+"'");
             rs2 = stmt2.executeQuery();
             while(rs2.next()){
                 stmt3 = con.prepareStatement("SELECT atype,fclass FROM amenities_type where id='"+rs2.getString("amn")+"'");
                 rs3 = stmt3.executeQuery();
                 if(rs3.next()){
                     amn_det.add(rs3.getString("atype"));
                     amn_fa.add(rs3.getString("fclass"));
                 }
             }
             
             session.setAttribute("amn_det", amn_det);
             session.setAttribute("amn_fa", amn_fa);
             
             ArrayList<String> ser_det = new ArrayList<String>();
             ArrayList<String> ser_fa = new ArrayList<String>();
             
             stmt2 = con.prepareStatement("SELECT ser FROM shops_ser WHERE apa_id='"+id+"'");
             rs2 = stmt2.executeQuery();
             while(rs2.next()){
                 stmt3 = con.prepareStatement("SELECT stype,fclass FROM service_type where id='"+rs2.getString("ser")+"'");
                 rs3 = stmt3.executeQuery();
                 if(rs3.next()){
                     ser_det.add(rs3.getString("stype"));
                     ser_fa.add(rs3.getString("fclass"));
                 }
             }
             
             session.setAttribute("ser_det", ser_det);
             session.setAttribute("ser_fa", ser_fa);
             
            }
            
            
            if(stype.equalsIgnoreCase("WAREHOUSE")){
             
             String near_by = "",tenant_type = "";
             stmt = con.prepareStatement("SELECT facings,area_build,area_build_type,area_build_in,rule_non_veg,rule_pets,rule_drink,rule_water,rule_elec,oter_details4,rent_amt,deposit_amt,nego,maintanece_amt,agreement_dur,info_leave,tenant_type,date_format(adate,'%d/%m/%Y')as adate1,near_by,name_num,street_no,land_mark,city,state,age_pro,address_pro,img1,img2,img3,img4,img5,img6,fur,floor,parking,tenant_type,rent_amt,deposit_amt,nego FROM post_warehouse where id='"+id+"'");
             rs = stmt.executeQuery();
             if(rs.next()){
                 
                  near_by = rs.getString("near_by");
                 tenant_type = rs.getString("tenant_type");
                 
                 session.setAttribute("ss_rule_non_veg", rs.getString("rule_non_veg"));
                 session.setAttribute("ss_rule_pets", rs.getString("rule_pets"));
                 session.setAttribute("ss_rule_drink", rs.getString("rule_drink"));
                 session.setAttribute("ss_rule_water", rs.getString("rule_water"));
                 session.setAttribute("ss_rule_elec", rs.getString("rule_elec"));
                 session.setAttribute("ss_oter_details4", rs.getString("oter_details4"));
                 
                 session.setAttribute("ss_rent_amt", rs.getString("rent_amt"));
                 session.setAttribute("ss_deposit_amt", rs.getString("deposit_amt"));
                 session.setAttribute("ss_nego", rs.getString("nego"));
                 session.setAttribute("ss_maintanece_amt", rs.getString("maintanece_amt"));
                 session.setAttribute("ss_agreement_dur", rs.getString("agreement_dur"));
                 session.setAttribute("ss_info_leave", rs.getString("info_leave"));
                 
                 session.setAttribute("ss_area_build", rs.getString("area_build"));
                 session.setAttribute("ss_area_btype", rs.getString("area_build_type"));
                 session.setAttribute("ss_area_bin", rs.getString("area_build_in"));
                 session.setAttribute("ss_name_num", rs.getString("name_num"));
                 session.setAttribute("ss_adate1", rs.getString("adate1"));
                 session.setAttribute("ss_street", rs.getString("street_no"));
                 session.setAttribute("ss_lmark", rs.getString("land_mark"));
                 session.setAttribute("ss_city", rs.getString("city"));
                 session.setAttribute("ss_state", rs.getString("state"));
                 session.setAttribute("ss_age_pro", rs.getString("age_pro"));
                 session.setAttribute("ss_address_pro", rs.getString("address_pro"));
                 session.setAttribute("ss_img1", rs.getString("img1"));
                 session.setAttribute("ss_img2", rs.getString("img2"));
                 session.setAttribute("ss_img3", rs.getString("img3"));
                 session.setAttribute("ss_img4", rs.getString("img4"));
                 session.setAttribute("ss_img5", rs.getString("img5"));
                 session.setAttribute("ss_img6", rs.getString("img6"));
                 
                 session.setAttribute("ss_rent", rs.getString("rent_amt"));
                 session.setAttribute("ss_deposit", rs.getString("deposit_amt"));
                 session.setAttribute("ss_nego", rs.getString("nego"));
                 
                 stmt1 = con.prepareStatement("SELECT ttype FROM tenant_types WHERE id='"+rs.getString("tenant_type")+"'");
                 rs1 = stmt1.executeQuery();
                 if(rs1.next()){
                     session.setAttribute("ss_ttype", rs1.getString("ttype"));
                 }else{
                     session.setAttribute("ss_ttype", "");
                 }
                 
                 session.setAttribute("ss_bhk", "");
                 
                 stmt1 = con.prepareStatement("SELECT ftype FROM furnished_types WHERE id='"+rs.getString("fur")+"'");
                 rs1 = stmt1.executeQuery();
                 if(rs1.next()){
                     session.setAttribute("ss_fur", rs1.getString("ftype"));
                 }else{
                     session.setAttribute("ss_fur", "");
                 }
                 
                 stmt1 = con.prepareStatement("SELECT floor FROM floor_types WHERE id='"+rs.getString("floor")+"'");
                 rs1 = stmt1.executeQuery();
                 if(rs1.next()){
                     session.setAttribute("ss_floor", rs1.getString("floor"));
                 }
                 
                 stmt1 = con.prepareStatement("SELECT facings FROM facing_types WHERE id='"+rs.getString("facings")+"'");
                 rs1 = stmt1.executeQuery();
                 if(rs1.next()){
                     session.setAttribute("ss_facings", rs1.getString("facings"));
                 }
                 
                 session.setAttribute("ss_parking", rs.getString("parking"));
             }
             
             ArrayList<String> dd_near = new ArrayList<String>();
             for(int i=0;i<near_by.split(",").length;i++){
             stmt = con.prepareStatement("SELECT name FROM near_by WHERE id='"+near_by.split(",")[i]+"'");
             rs = stmt.executeQuery();
             if(rs.next()){
                 dd_near.add(rs.getString("name"));
             }
                     }
             session.setAttribute("dd_near", dd_near);
             
             
             ArrayList<String> dd_tenant = new ArrayList<String>();
             ArrayList<String> dd_tenant_class = new ArrayList<String>();
             for(int i=0;i<tenant_type.split(",").length;i++){
             stmt = con.prepareStatement("SELECT ttype,fclass FROM tenant_types WHERE id='"+tenant_type.split(",")[i]+"'");
             rs = stmt.executeQuery();
             if(rs.next()){
                 dd_tenant.add(rs.getString("ttype"));
                 dd_tenant_class.add(rs.getString("fclass"));
             }
                     }
             session.setAttribute("dd_tenant", dd_tenant);
             session.setAttribute("dd_tenant_class", dd_tenant_class);
             
             
             ArrayList<String> amn_det = new ArrayList<String>();
             ArrayList<String> amn_fa = new ArrayList<String>();
             
             stmt2 = con.prepareStatement("SELECT amn FROM warehouse_amn WHERE apa_id='"+id+"'");
             rs2 = stmt2.executeQuery();
             while(rs2.next()){
                 stmt3 = con.prepareStatement("SELECT atype,fclass FROM amenities_type where id='"+rs2.getString("amn")+"'");
                 rs3 = stmt3.executeQuery();
                 if(rs3.next()){
                     amn_det.add(rs3.getString("atype"));
                     amn_fa.add(rs3.getString("fclass"));
                 }
             }
             
             session.setAttribute("amn_det", amn_det);
             session.setAttribute("amn_fa", amn_fa);
             
             ArrayList<String> ser_det = new ArrayList<String>();
             ArrayList<String> ser_fa = new ArrayList<String>();
             
             stmt2 = con.prepareStatement("SELECT ser FROM warehouse_ser WHERE apa_id='"+id+"'");
             rs2 = stmt2.executeQuery();
             while(rs2.next()){
                 stmt3 = con.prepareStatement("SELECT stype,fclass FROM service_type where id='"+rs2.getString("ser")+"'");
                 rs3 = stmt3.executeQuery();
                 if(rs3.next()){
                     ser_det.add(rs3.getString("stype"));
                     ser_fa.add(rs3.getString("fclass"));
                 }
             }
             
             session.setAttribute("ser_det", ser_det);
             session.setAttribute("ser_fa", ser_fa);
             
            }
            
            
            
            if(stype.equalsIgnoreCase("Specials")){
             
             String near_by = "",tenant_type = "";
             stmt = con.prepareStatement("SELECT veg,nonveg,bothh,nrooms,rooms,hall,rtype,occu,rule_non_veg,rule_pets,rule_drink,rule_water,rule_elec,oter_details4,rent_amt,deposit_amt,nego,maintanece_amt,agreement_dur,info_leave,area_build,area_build_type,area_build_in,date_format(adate,'%d/%m/%Y')as adate1,near_by,name_num,street_no,land_mark,city,state,age_pro,address_pro,img1,img2,img3,img4,img5,img6,fur,floor,facings,parking,rent_amt,deposit_amt,nego FROM post_specials where id='"+id+"'");
             rs = stmt.executeQuery();
             if(rs.next()){
                 
                 session.setAttribute("ss_veg", rs.getString("veg"));
                 session.setAttribute("ss_nonveg", rs.getString("nonveg"));
                 session.setAttribute("ss_bothh", rs.getString("bothh"));
                 session.setAttribute("ss_nrooms", rs.getString("nrooms"));
                 session.setAttribute("ss_rooms", rs.getString("rooms"));
                 session.setAttribute("ss_hall", rs.getString("hall"));
                 
                 stmt1 = con.prepareStatement("SELECT rtype FROM rental_types where id='"+rs.getString("rtype")+"'");
                 rs1 = stmt1.executeQuery();
                 if(rs1.next()){
                     session.setAttribute("ss_rtype", rs1.getString("rtype"));
                 }
                 
                 session.setAttribute("ss_occu", rs.getString("occu"));
                 near_by = rs.getString("near_by");
                 tenant_type = "";
                 
                 session.setAttribute("ss_rule_non_veg", rs.getString("rule_non_veg"));
                 session.setAttribute("ss_rule_pets", rs.getString("rule_pets"));
                 session.setAttribute("ss_rule_drink", rs.getString("rule_drink"));
                 session.setAttribute("ss_rule_water", rs.getString("rule_water"));
                 session.setAttribute("ss_rule_elec", rs.getString("rule_elec"));
                 session.setAttribute("ss_oter_details4", rs.getString("oter_details4"));
                 
                 session.setAttribute("ss_rent_amt", rs.getString("rent_amt"));
                 session.setAttribute("ss_deposit_amt", rs.getString("deposit_amt"));
                 session.setAttribute("ss_nego", rs.getString("nego"));
                 session.setAttribute("ss_maintanece_amt", rs.getString("maintanece_amt"));
                 session.setAttribute("ss_agreement_dur", rs.getString("agreement_dur"));
                 session.setAttribute("ss_info_leave", rs.getString("info_leave"));
                 
                 session.setAttribute("ss_area_build", rs.getString("area_build"));
                 session.setAttribute("ss_area_btype", rs.getString("area_build_type"));
                 session.setAttribute("ss_area_bin", rs.getString("area_build_in"));
                 session.setAttribute("ss_name_num", rs.getString("name_num"));
                 session.setAttribute("ss_adate1", rs.getString("adate1"));
                 session.setAttribute("ss_street", rs.getString("street_no"));
                 session.setAttribute("ss_lmark", rs.getString("land_mark"));
                 session.setAttribute("ss_city", rs.getString("city"));
                 session.setAttribute("ss_state", rs.getString("state"));
                 session.setAttribute("ss_age_pro", rs.getString("age_pro"));
                 session.setAttribute("ss_address_pro", rs.getString("address_pro"));
                 session.setAttribute("ss_img1", rs.getString("img1"));
                 session.setAttribute("ss_img2", rs.getString("img2"));
                 session.setAttribute("ss_img3", rs.getString("img3"));
                 session.setAttribute("ss_img4", rs.getString("img4"));
                 session.setAttribute("ss_img5", rs.getString("img5"));
                 session.setAttribute("ss_img6", rs.getString("img6"));
                 
                 session.setAttribute("ss_rent", rs.getString("rent_amt"));
                 session.setAttribute("ss_deposit", rs.getString("deposit_amt"));
                 session.setAttribute("ss_nego", rs.getString("nego"));
                 
                session.setAttribute("ss_ttype", "");
                 
                 session.setAttribute("ss_bhk", "");
                 
                 stmt1 = con.prepareStatement("SELECT ftype FROM furnished_types WHERE id='"+rs.getString("fur")+"'");
                 rs1 = stmt1.executeQuery();
                 if(rs1.next()){
                     session.setAttribute("ss_fur", rs1.getString("ftype"));
                 }else{
                     session.setAttribute("ss_fur", "");
                 }
                 
                 stmt1 = con.prepareStatement("SELECT floor FROM floor_types WHERE id='"+rs.getString("floor")+"'");
                 rs1 = stmt1.executeQuery();
                 if(rs1.next()){
                     session.setAttribute("ss_floor", rs1.getString("floor"));
                 }
                 
                 stmt1 = con.prepareStatement("SELECT facings FROM facing_types WHERE id='"+rs.getString("facings")+"'");
                 rs1 = stmt1.executeQuery();
                 if(rs1.next()){
                     session.setAttribute("ss_facings", rs1.getString("facings"));
                 }
                 
                 session.setAttribute("ss_parking", rs.getString("parking"));
             }
             
             ArrayList<String> dd_near = new ArrayList<String>();
             for(int i=0;i<near_by.split(",").length;i++){
             stmt = con.prepareStatement("SELECT name FROM near_by WHERE id='"+near_by.split(",")[i]+"'");
             rs = stmt.executeQuery();
             if(rs.next()){
                 dd_near.add(rs.getString("name"));
             }
                     }
             session.setAttribute("dd_near", dd_near);
             
             
             ArrayList<String> dd_tenant = new ArrayList<String>();
             ArrayList<String> dd_tenant_class = new ArrayList<String>();
            
             session.setAttribute("dd_tenant", dd_tenant);
             session.setAttribute("dd_tenant_class", dd_tenant_class);
             
             
             ArrayList<String> amn_det = new ArrayList<String>();
             ArrayList<String> amn_fa = new ArrayList<String>();
             
             stmt2 = con.prepareStatement("SELECT amn FROM specials_amn WHERE apa_id='"+id+"'");
             rs2 = stmt2.executeQuery();
             while(rs2.next()){
                 stmt3 = con.prepareStatement("SELECT atype,fclass FROM amenities_type where id='"+rs2.getString("amn")+"'");
                 rs3 = stmt3.executeQuery();
                 if(rs3.next()){
                     amn_det.add(rs3.getString("atype"));
                     amn_fa.add(rs3.getString("fclass"));
                 }
             }
             
             session.setAttribute("amn_det", amn_det);
             session.setAttribute("amn_fa", amn_fa);
             
             ArrayList<String> ser_det = new ArrayList<String>();
             ArrayList<String> ser_fa = new ArrayList<String>();
             
             stmt2 = con.prepareStatement("SELECT ser FROM specials_ser WHERE apa_id='"+id+"'");
             rs2 = stmt2.executeQuery();
             while(rs2.next()){
                 stmt3 = con.prepareStatement("SELECT stype,fclass FROM service_type where id='"+rs2.getString("ser")+"'");
                 rs3 = stmt3.executeQuery();
                 if(rs3.next()){
                     ser_det.add(rs3.getString("stype"));
                     ser_fa.add(rs3.getString("fclass"));
                 }
             }
             
             session.setAttribute("ser_det", ser_det);
             session.setAttribute("ser_fa", ser_fa);
             
            }

            RequestDispatcher rq = request.getRequestDispatcher("guest/view_search_details.jsp");
                rq.forward(request, response);
        } catch (Exception e) {
            throw new ServletException("Exception in  Servlet", e);
        } finally {
            if (out != null) {
                out.close();
            }
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {

                }
            }
        }

    }

    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        this.doGet(request, response);
    }
}
