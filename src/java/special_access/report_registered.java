/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package special_access;


import java.io.IOException;
import java.io.OutputStream;
import java.util.Calendar;
import java.sql.*;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//@SuppressWarnings("deprecation")
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javax.servlet.*;
import javax.servlet.http.HttpSession;

public class report_registered extends HttpServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String insert = "fail";
        Connection con = null;
        long connected;
        OutputStream out = null;
        PreparedStatement stmt, stmt1, stmt2, stmt3, stmt4, stmt5, stmt6, stmt7,stmt8,stmt9 = null;
        ResultSet rs, rs1, rs2, rs3, rs4, rs5, rs6,rs7,rs8,rs9 = null;
        ResourceBundle bundle = ResourceBundle.getBundle("SelectResource");
        HttpSession session = request.getSession();
        try {
            try {
                String url = bundle.getString("URL");
                String username = bundle.getString("username");
                String password = bundle.getString("password");
                Class.forName(bundle.getString("Driver"));
                con = DriverManager.getConnection(url, username, password);
            } catch (Exception e) {
                System.out.println("Database Connection Error" + e);
            }

            String currdate = "";
            Date date = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            String strDate = sdf.format(date);
            String time = new SimpleDateFormat("h:mm a").format(new Date());
            String dt = strDate + " " + time;
            int year = Calendar.getInstance().get(Calendar.YEAR);
            int yr = year % 100;
            String user = "";

             if(session.getAttribute("user")!=null){
                user=session.getAttribute("user").toString();
                session.setAttribute("user", user);
            }else{
                 RequestDispatcher rq = request.getRequestDispatcher("index.jsp");
            rq.forward(request, response);
            }
             int home=0,apartment=0,pg =0,office =0,shops =0,ware=0,special=0;
            String cuser = "";
            stmt = con.prepareStatement("SELECT count(distinct cuser)as n,cuser FROM post_home WHERE payment_type='Free' group by cuser");
            rs = stmt.executeQuery();
            if(rs.next()){
                home = rs.getInt("n");
                cuser = rs.getString("cuser");
            }
            stmt = con.prepareStatement("SELECT count(distinct cuser)as n,cuser FROM post_apartment WHERE payment_type='Free' group by cuser");
            rs = stmt.executeQuery();
            if(rs.next()){
                if(cuser.equalsIgnoreCase(rs.getString("cuser"))){
                    
                }else{
                apartment = rs.getInt("n");
                }
            }
            stmt = con.prepareStatement("SELECT count(distinct cuser)as n,cuser FROM post_pg WHERE payment_type='Free' group by cuser");
            rs = stmt.executeQuery();
            if(rs.next()){
                if(cuser.equalsIgnoreCase(rs.getString("cuser"))){
                    
                }else{
                pg = rs.getInt("n");
                }
            }
            stmt = con.prepareStatement("SELECT count(distinct cuser)as n,cuser FROM post_office WHERE payment_type='Free' group by cuser");
            rs = stmt.executeQuery();
            if(rs.next()){
                if(cuser.equalsIgnoreCase(rs.getString("cuser"))){
                    
                }else{
                office = rs.getInt("n");
                }
            }
            stmt = con.prepareStatement("SELECT count(distinct cuser)as n,cuser FROM post_shops WHERE payment_type='Free' group by cuser");
            rs = stmt.executeQuery();
            if(rs.next()){
                if(cuser.equalsIgnoreCase(rs.getString("cuser"))){
                    
                }else{
                shops = rs.getInt("n");
                }
            }
            stmt = con.prepareStatement("SELECT count(distinct cuser)as n,cuser FROM post_warehouse WHERE payment_type='Free' group by cuser");
            rs = stmt.executeQuery();
            if(rs.next()){
                if(cuser.equalsIgnoreCase(rs.getString("cuser"))){
                    
                }else{
                ware = rs.getInt("n");
                }
            }
            stmt = con.prepareStatement("SELECT count(distinct cuser)as n,cuser FROM post_specials WHERE payment_type='Free' group by cuser");
            rs = stmt.executeQuery();
            if(rs.next()){
                if(cuser.equalsIgnoreCase(rs.getString("cuser"))){
                    
                }else{
                special = rs.getInt("n");
                }
            }
            
            int ttl = home+apartment+pg+office+shops+ware+special;
            session.setAttribute("ttl_free_users", Integer.toString(ttl));
            
            
            /*-------------------------------get all brokers posts--------------------------*/
            
            int brhome=0,brapartment=0,brpg =0,broffice =0,brshops =0,brware=0,brspecial=0;
            stmt = con.prepareStatement("SELECT count(id)as n FROM post_home WHERE kyc_utype='Broker'");
            rs = stmt.executeQuery();
            if(rs.next()){
                brhome = rs.getInt("n");
            }
            stmt = con.prepareStatement("SELECT count(id)as n FROM post_apartment WHERE kyc_utype='Broker'");
            rs = stmt.executeQuery();
            if(rs.next()){
                brapartment = rs.getInt("n");
            }
            stmt = con.prepareStatement("SELECT count(id)as n FROM post_pg WHERE kyc_utype='Broker'");
            rs = stmt.executeQuery();
            if(rs.next()){
                brpg = rs.getInt("n");
            }
            stmt = con.prepareStatement("SELECT count(id)as n FROM post_office WHERE kyc_utype='Broker'");
            rs = stmt.executeQuery();
            if(rs.next()){
                broffice = rs.getInt("n");
            }
            stmt = con.prepareStatement("SELECT count(id)as n FROM post_shops WHERE kyc_utype='Broker'");
            rs = stmt.executeQuery();
            if(rs.next()){
                brshops = rs.getInt("n");
            }
            stmt = con.prepareStatement("SELECT count(id)as n FROM post_warehouse WHERE kyc_utype='Broker'");
            rs = stmt.executeQuery();
            if(rs.next()){
                brware = rs.getInt("n");
            }
            stmt = con.prepareStatement("SELECT count(id)as n FROM post_specials WHERE kyc_utype='Broker'");
            rs = stmt.executeQuery();
            if(rs.next()){
                brspecial = rs.getInt("n");
            }
            
            int brttl = brhome+brapartment+brpg+broffice+brshops+brware+brspecial;
            session.setAttribute("ttl_broker_posts", Integer.toString(brttl));
            
            
            
            /*-------------------------------get all owner posts--------------------------*/
            
            int owhome=0,owapartment=0,owpg =0,owoffice =0,owshops =0,owware=0,owspecial=0;
            stmt = con.prepareStatement("SELECT count(id)as n FROM post_home WHERE kyc_utype='Owner'");
            rs = stmt.executeQuery();
            if(rs.next()){
                owhome = rs.getInt("n");
            }
            stmt = con.prepareStatement("SELECT count(id)as n FROM post_apartment WHERE kyc_utype='Owner'");
            rs = stmt.executeQuery();
            if(rs.next()){
                owapartment = rs.getInt("n");
            }
            stmt = con.prepareStatement("SELECT count(id)as n FROM post_pg WHERE kyc_utype='Owner'");
            rs = stmt.executeQuery();
            if(rs.next()){
                owpg = rs.getInt("n");
            }
            stmt = con.prepareStatement("SELECT count(id)as n FROM post_office WHERE kyc_utype='Owner'");
            rs = stmt.executeQuery();
            if(rs.next()){
                owoffice = rs.getInt("n");
            }
            stmt = con.prepareStatement("SELECT count(id)as n FROM post_shops WHERE kyc_utype='Owner'");
            rs = stmt.executeQuery();
            if(rs.next()){
                owshops = rs.getInt("n");
            }
            stmt = con.prepareStatement("SELECT count(id)as n FROM post_warehouse WHERE kyc_utype='Owner'");
            rs = stmt.executeQuery();
            if(rs.next()){
                owware = rs.getInt("n");
            }
            stmt = con.prepareStatement("SELECT count(id)as n FROM post_specials WHERE kyc_utype='Owner'");
            rs = stmt.executeQuery();
            if(rs.next()){
                owspecial = rs.getInt("n");
            }
            
            int owttl = owhome+owapartment+owpg+owoffice+owshops+owware+owspecial;
            session.setAttribute("ttl_owner_posts", Integer.toString(owttl));
            
            /*-------------------------------get all Builders posts--------------------------*/
            
            int blhome=0,blapartment=0,blpg =0,bloffice =0,blshops =0,blware=0,blspecial=0;
            stmt = con.prepareStatement("SELECT count(id)as n FROM post_home WHERE kyc_utype='Builder'");
            rs = stmt.executeQuery();
            if(rs.next()){
                blhome = rs.getInt("n");
            }
            stmt = con.prepareStatement("SELECT count(id)as n FROM post_apartment WHERE kyc_utype='Builder'");
            rs = stmt.executeQuery();
            if(rs.next()){
                blapartment = rs.getInt("n");
            }
            stmt = con.prepareStatement("SELECT count(id)as n FROM post_pg WHERE kyc_utype='Builder'");
            rs = stmt.executeQuery();
            if(rs.next()){
                blpg = rs.getInt("n");
            }
            stmt = con.prepareStatement("SELECT count(id)as n FROM post_office WHERE kyc_utype='Builder'");
            rs = stmt.executeQuery();
            if(rs.next()){
                bloffice = rs.getInt("n");
            }
            stmt = con.prepareStatement("SELECT count(id)as n FROM post_shops WHERE kyc_utype='Builder'");
            rs = stmt.executeQuery();
            if(rs.next()){
                blshops = rs.getInt("n");
            }
            stmt = con.prepareStatement("SELECT count(id)as n FROM post_warehouse WHERE kyc_utype='Builder'");
            rs = stmt.executeQuery();
            if(rs.next()){
                blware = rs.getInt("n");
            }
            stmt = con.prepareStatement("SELECT count(id)as n FROM post_specials WHERE kyc_utype='Builder'");
            rs = stmt.executeQuery();
            if(rs.next()){
                blspecial = rs.getInt("n");
            }
            
            int blttl = blhome+blapartment+blpg+bloffice+blshops+blware+blspecial;
            session.setAttribute("ttl_builder_posts", Integer.toString(blttl));

            RequestDispatcher rq = request.getRequestDispatcher("special_access/report_registered.jsp");
                rq.forward(request, response);
        } catch (Exception e) {
            throw new ServletException("Exception in  Servlet", e);
        } finally {
            if (out != null) {
                out.close();
            }
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {

                }
            }
        }

    }

    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        this.doGet(request, response);
    }
}
