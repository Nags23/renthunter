/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package special_access;


import java.io.IOException;
import java.io.OutputStream;
import java.util.Calendar;
import java.sql.*;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//@SuppressWarnings("deprecation")
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javax.servlet.*;
import javax.servlet.http.HttpSession;

public class free_users_details extends HttpServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String insert = "fail";
        Connection con = null;
        long connected;
        OutputStream out = null;
        PreparedStatement stmt, stmt1, stmt2, stmt3, stmt4, stmt5, stmt6, stmt7,stmt8,stmt9 = null;
        ResultSet rs, rs1, rs2, rs3, rs4, rs5, rs6,rs7,rs8,rs9 = null;
        ResourceBundle bundle = ResourceBundle.getBundle("SelectResource");
        HttpSession session = request.getSession();
        try {
            try {
                String url = bundle.getString("URL");
                String username = bundle.getString("username");
                String password = bundle.getString("password");
                Class.forName(bundle.getString("Driver"));
                con = DriverManager.getConnection(url, username, password);
            } catch (Exception e) {
                System.out.println("Database Connection Error" + e);
            }

            String currdate = "";
            Date date = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            String strDate = sdf.format(date);
            String time = new SimpleDateFormat("h:mm a").format(new Date());
            String dt = strDate + " " + time;
            int year = Calendar.getInstance().get(Calendar.YEAR);
            int yr = year % 100;
            String user = "";

             if(session.getAttribute("user")!=null){
                user=session.getAttribute("user").toString();
                session.setAttribute("user", user);
            }else{
                 RequestDispatcher rq = request.getRequestDispatcher("index.jsp");
            rq.forward(request, response);
            }

            String ttype = request.getParameter("dtype");
            // System.out.println("ttype is"+ttype);
             ArrayList<String> dd_fname = new ArrayList<String>();
             ArrayList<String> dd_email = new ArrayList<String>();
             ArrayList<String> dd_mob = new ArrayList<String>();
             ArrayList<String> dd_ldate = new ArrayList<String>();
             ArrayList<String> dd_cuser = new ArrayList<String>();
             ArrayList<String> dd_rdate = new ArrayList<String>();
             
             /*======================================Apartment Details===========================================*/
             stmt = con.prepareStatement("SELECT cuser FROM post_apartment where payment_type='"+ttype+"'");
             rs = stmt.executeQuery();
             while(rs.next()){
                 
                stmt1 = con.prepareStatement("SELECT firstname,email,mob,date_format(login_date,'%d/%m/%Y')as ldate,login_time,date_format(created_date,'%d/%m/%Y')as rdate FROM users where id='"+rs.getString("cuser")+"'");
                rs1 = stmt1.executeQuery();
                if(rs1.next()){
                    if(dd_cuser.contains(rs.getString("cuser"))){
                        
                    }else{
                    dd_cuser.add(rs.getString("cuser"));
                    dd_fname.add(rs1.getString("firstname"));
                    dd_email.add(rs1.getString("email"));
                    dd_mob.add(rs1.getString("mob"));
                    dd_ldate.add(rs1.getString("ldate")+" "+rs1.getString("login_time"));
                    dd_rdate.add(rs1.getString("rdate"));
                    }
                }else{
                    dd_fname.add("");
                    dd_email.add("");
                    dd_mob.add("");
                    dd_ldate.add("");
                    dd_rdate.add("");
                }
             }
             
             
             /*======================================Home Details===========================================*/
             stmt = con.prepareStatement("SELECT cuser FROM post_home where payment_type='"+ttype+"'");
             rs = stmt.executeQuery();
             while(rs.next()){
                 stmt1 = con.prepareStatement("SELECT firstname,email,mob,date_format(login_date,'%d/%m/%Y')as ldate,login_time,date_format(created_date,'%d/%m/%Y')as rdate FROM users where id='"+rs.getString("cuser")+"'");
                rs1 = stmt1.executeQuery();
                if(rs1.next()){if(dd_cuser.contains(rs.getString("cuser"))){
                        
                    }else{
                    dd_cuser.add(rs.getString("cuser"));
                    dd_fname.add(rs1.getString("firstname"));
                    dd_email.add(rs1.getString("email"));
                    dd_mob.add(rs1.getString("mob"));
                    dd_ldate.add(rs1.getString("ldate")+" "+rs1.getString("login_time"));
                    dd_rdate.add(rs1.getString("rdate"));
                }
                }else{
                    dd_fname.add("");
                    dd_email.add("");
                    dd_mob.add("");
                    dd_ldate.add("");
                    dd_rdate.add("");
                }
             }
             
             /*======================================PG Details===========================================*/
             stmt = con.prepareStatement("SELECT cuser FROM post_pg where payment_type='"+ttype+"'");
             rs = stmt.executeQuery();
             while(rs.next()){
                 stmt1 = con.prepareStatement("SELECT firstname,email,mob,date_format(login_date,'%d/%m/%Y')as ldate,login_time,date_format(created_date,'%d/%m/%Y')as rdate FROM users where id='"+rs.getString("cuser")+"'");
                rs1 = stmt1.executeQuery();
                if(rs1.next()){
                    if(dd_cuser.contains(rs.getString("cuser"))){
                        
                    }else{
                    dd_cuser.add(rs.getString("cuser"));
                    dd_fname.add(rs1.getString("firstname"));
                    dd_email.add(rs1.getString("email"));
                    dd_mob.add(rs1.getString("mob"));
                    dd_ldate.add(rs1.getString("ldate")+" "+rs1.getString("login_time"));
                    dd_rdate.add(rs1.getString("rdate"));
                    }
                }else{
                    dd_fname.add("");
                    dd_email.add("");
                    dd_mob.add("");
                    dd_ldate.add("");
                    dd_rdate.add("");
                }
             }
             
              /*======================================Office Details===========================================*/
             stmt = con.prepareStatement("SELECT cuser FROM post_office where payment_type='"+ttype+"'");
             rs = stmt.executeQuery();
             while(rs.next()){
                 stmt1 = con.prepareStatement("SELECT firstname,email,mob,date_format(login_date,'%d/%m/%Y')as ldate,login_time,date_format(created_date,'%d/%m/%Y')as rdate FROM users where id='"+rs.getString("cuser")+"'");
                rs1 = stmt1.executeQuery();
                if(rs1.next()){
                    if(dd_cuser.contains(rs.getString("cuser"))){
                        
                    }else{
                    dd_cuser.add(rs.getString("cuser"));
                    dd_fname.add(rs1.getString("firstname"));
                    dd_email.add(rs1.getString("email"));
                    dd_mob.add(rs1.getString("mob"));
                    dd_ldate.add(rs1.getString("ldate")+" "+rs1.getString("login_time"));
                    dd_rdate.add(rs1.getString("rdate"));
                    }
                }else{
                    dd_fname.add("");
                    dd_email.add("");
                    dd_mob.add("");
                    dd_ldate.add("");
                    dd_rdate.add("");
                }
             }
             
              /*======================================Shop Details===========================================*/
             stmt = con.prepareStatement("SELECT cuser FROM post_shops where payment_type='"+ttype+"'");
             rs = stmt.executeQuery();
             while(rs.next()){
                 stmt1 = con.prepareStatement("SELECT firstname,email,mob,date_format(login_date,'%d/%m/%Y')as ldate,login_time,date_format(created_date,'%d/%m/%Y')as rdate FROM users where id='"+rs.getString("cuser")+"'");
                rs1 = stmt1.executeQuery();
                if(rs1.next()){
                    if(dd_cuser.contains(rs.getString("cuser"))){
                        
                    }else{
                    dd_cuser.add(rs.getString("cuser"));
                    dd_fname.add(rs1.getString("firstname"));
                    dd_email.add(rs1.getString("email"));
                    dd_mob.add(rs1.getString("mob"));
                    dd_ldate.add(rs1.getString("ldate")+" "+rs1.getString("login_time"));
                    dd_rdate.add(rs1.getString("rdate"));
                    }
                }else{
                    dd_fname.add("");
                    dd_email.add("");
                    dd_mob.add("");
                    dd_ldate.add("");
                    dd_rdate.add("");
                }
             }
             
             /*======================================Warehouse Details===========================================*/
             stmt = con.prepareStatement("SELECT cuser FROM post_warehouse where payment_type='"+ttype+"'");
             rs = stmt.executeQuery();
             while(rs.next()){
                 stmt1 = con.prepareStatement("SELECT firstname,email,mob,date_format(login_date,'%d/%m/%Y')as ldate,login_time,date_format(created_date,'%d/%m/%Y')as rdate FROM users where id='"+rs.getString("cuser")+"'");
                rs1 = stmt1.executeQuery();
                if(rs1.next()){
                    if(dd_cuser.contains(rs.getString("cuser"))){
                        
                    }else{
                    dd_cuser.add(rs.getString("cuser"));
                    dd_fname.add(rs1.getString("firstname"));
                    dd_email.add(rs1.getString("email"));
                    dd_mob.add(rs1.getString("mob"));
                    dd_ldate.add(rs1.getString("ldate")+" "+rs1.getString("login_time"));
                    dd_rdate.add(rs1.getString("rdate"));
                    }
                }else{
                    dd_fname.add("");
                    dd_email.add("");
                    dd_mob.add("");
                    dd_ldate.add("");
                    dd_rdate.add("");
                }
                 
             }
             
             
             /*======================================Specials Details===========================================*/
             stmt = con.prepareStatement("SELECT cuser FROM post_specials where payment_type='"+ttype+"'");
             rs = stmt.executeQuery();
             while(rs.next()){
                stmt1 = con.prepareStatement("SELECT firstname,email,mob,date_format(login_date,'%d/%m/%Y')as ldate,login_time,date_format(created_date,'%d/%m/%Y')as rdate FROM users where id='"+rs.getString("cuser")+"'");
                rs1 = stmt1.executeQuery();
                if(rs1.next()){
                    if(dd_cuser.contains(rs.getString("cuser"))){
                        
                    }else{
                    dd_cuser.add(rs.getString("cuser"));
                    dd_fname.add(rs1.getString("firstname"));
                    dd_email.add(rs1.getString("email"));
                    dd_mob.add(rs1.getString("mob"));
                    dd_ldate.add(rs1.getString("ldate")+" "+rs1.getString("login_time"));
                    dd_rdate.add(rs1.getString("rdate"));
                    }
                }else{
                    dd_fname.add("");
                    dd_email.add("");
                    dd_mob.add("");
                    dd_ldate.add("");
                    dd_rdate.add("");
                }
             }
             
             session.setAttribute("dd_fname", dd_fname);
             session.setAttribute("dd_email", dd_email);
             session.setAttribute("dd_mob", dd_mob);
             session.setAttribute("dd_ldate", dd_ldate);
             session.setAttribute("dd_rdate", dd_rdate);
             
             //System.out.println("ddid is "+dd_id);
             
            RequestDispatcher rq = request.getRequestDispatcher("special_access/free_users_details.jsp");
                rq.forward(request, response);
        } catch (Exception e) {
            throw new ServletException("Exception in  Servlet", e);
        } finally {
            if (out != null) {
                out.close();
            }
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {

                }
            }
        }

    }

    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        this.doGet(request, response);
    }
}
