/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package post_property;


import java.io.IOException;
import java.io.OutputStream;
import java.util.Calendar;
import java.sql.*;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//@SuppressWarnings("deprecation")
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javax.servlet.*;
import javax.servlet.http.HttpSession;

public class post_shops extends HttpServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String insert = "fail";
        Connection con = null;
        long connected;
        OutputStream out = null;
        PreparedStatement stmt, stmt1, stmt2, stmt3, stmt4, stmt5, stmt6, stmt7,stmt8,stmt9 = null;
        ResultSet rs, rs1, rs2, rs3, rs4, rs5, rs6,rs7,rs8,rs9 = null;
        ResourceBundle bundle = ResourceBundle.getBundle("SelectResource");
        HttpSession session = request.getSession();
        try {
            try {
                String url = bundle.getString("URL");
                String username = bundle.getString("username");
                String password = bundle.getString("password");
                Class.forName(bundle.getString("Driver"));
                con = DriverManager.getConnection(url, username, password);
            } catch (Exception e) {
                System.out.println("Database Connection Error" + e);
            }

            String currdate = "";
            Date date = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            String strDate = sdf.format(date);
            String time = new SimpleDateFormat("h:mm a").format(new Date());
            String dt = strDate + " " + time;
            int year = Calendar.getInstance().get(Calendar.YEAR);
            int yr = year % 100;
            String user = "";

             if(session.getAttribute("user")!=null){
                user=session.getAttribute("user").toString();
                session.setAttribute("user", user);
            }else{
                 RequestDispatcher rq = request.getRequestDispatcher("index.jsp");
            rq.forward(request, response);
            }
             
             stmt = con.prepareStatement("SELECT mob,email,firstname FROM users where username='"+user+"'");
             rs = stmt.executeQuery();
             if(rs.next()){
                 session.setAttribute("uuu_mob", rs.getString("mob"));
                 session.setAttribute("uuu_email", rs.getString("email"));
                 session.setAttribute("uuu_firstname", rs.getString("firstname"));
             }
             
             ArrayList<String> dd_id = new ArrayList<String>();
             ArrayList<String> dd_sname = new ArrayList<String>();
             
             stmt = con.prepareStatement("select id,sname,disabled from state_types where disabled='n' order by id asc");
             rs = stmt.executeQuery();
             while(rs.next()){
                dd_id.add(rs.getString("id")); 
                dd_sname.add(rs.getString("sname"));
             }
             
             session.setAttribute("dd_id", dd_id);
             session.setAttribute("dd_sname", dd_sname);
             
             ArrayList<String> fur_id = new ArrayList<String>();
             ArrayList<String> fur_ftype = new ArrayList<String>();
             stmt1 = con.prepareStatement("SELECT id,ftype FROM furnished_types WHERE disabled='n' ORDER BY ftype asc");
             rs1 = stmt1.executeQuery();
             while(rs1.next()){
                 fur_id.add(rs1.getString("id"));
                 fur_ftype.add(rs1.getString("ftype"));
             }
             
             session.setAttribute("fur_id", fur_id);
             session.setAttribute("fur_ftype", fur_ftype);
             
             ArrayList<String> floor_id = new ArrayList<String>();
             ArrayList<String> floor_floor = new ArrayList<String>();
             
             stmt2 = con.prepareStatement("SELECT id,floor FROM floor_types WHERE disabled='n' ORDER BY id asc");
             rs2 = stmt2.executeQuery();
             while(rs2.next()){
                 floor_id.add(rs2.getString("id"));
                 floor_floor.add(rs2.getString("floor"));
             }
             
             session.setAttribute("floor_id", floor_id);
             session.setAttribute("floor_floor", floor_floor);
             
             ArrayList<String> facings_id = new ArrayList<String>();
             ArrayList<String> facings_facings = new ArrayList<String>();
             
             stmt3 = con.prepareStatement("SELECT id,facings FROM facing_types WHERE disabled='n' ORDER BY facings asc");
             rs3 = stmt3.executeQuery();
             while(rs3.next()){
                 facings_id.add(rs3.getString("id"));
                 facings_facings.add(rs3.getString("facings"));
             }
             
             session.setAttribute("facings_id", facings_id);
             session.setAttribute("facings_facings", facings_facings);
             
             ArrayList<String> ten_id = new ArrayList<String>();
             ArrayList<String> ten_ttype = new ArrayList<String>();
             ArrayList<String> ten_fclass = new ArrayList<String>();
             
             stmt4 = con.prepareStatement("SELECT id,ttype,fclass FROM tenant_types where ptype='Shops' || ptype='All' ORDER BY id asc");
             rs4 = stmt4.executeQuery();
             while(rs4.next()){
                 ten_id.add(rs4.getString("id"));
                 ten_ttype.add(rs4.getString("ttype"));
                 ten_fclass.add(rs4.getString("fclass"));
             }
             
             session.setAttribute("ten_id", ten_id);
             session.setAttribute("ten_ttype", ten_ttype);
             session.setAttribute("ten_fclass", ten_fclass);
             
             ArrayList<String> amn_id = new ArrayList<String>();
             ArrayList<String> amn_ttype = new ArrayList<String>();
             ArrayList<String> amn_fclass = new ArrayList<String>();
             stmt5 = con.prepareStatement("SELECT id,atype,fclass FROM amenities_type ORDER BY id asc");
             rs5 = stmt5.executeQuery();
             while(rs5.next()){
                 amn_id.add(rs5.getString("id"));
                 amn_ttype.add(rs5.getString("atype"));
                 amn_fclass.add(rs5.getString("fclass"));
             }
             
             session.setAttribute("amn_id", amn_id);
             session.setAttribute("amn_ttype", amn_ttype);
             session.setAttribute("amn_fclass", amn_fclass);
             
             ArrayList<String> ser_id = new ArrayList<String>();
             ArrayList<String> ser_ttype = new ArrayList<String>();
             ArrayList<String> ser_fclass = new ArrayList<String>();
             stmt5 = con.prepareStatement("SELECT id,stype,fclass FROM service_type ORDER BY id asc");
             rs5 = stmt5.executeQuery();
             while(rs5.next()){
                 ser_id.add(rs5.getString("id"));
                 ser_ttype.add(rs5.getString("stype"));
                 ser_fclass.add(rs5.getString("fclass"));
             }
             
             session.setAttribute("ser_id", ser_id);
             session.setAttribute("ser_ttype", ser_ttype);
             session.setAttribute("ser_fclass", ser_fclass);
             
             ArrayList<String> near_id = new ArrayList<String>();
             ArrayList<String> near_name = new ArrayList<String>();
             stmt5 = con.prepareStatement("SELECT id,name FROM near_by WHERE disabled='n' ORDER BY name");
             rs5 = stmt5.executeQuery();
             while(rs5.next()){
               near_id.add(rs5.getString("id"));
               near_name.add(rs5.getString("name"));
             }
             
             session.setAttribute("near_id", near_id);
             session.setAttribute("near_name", near_name);

            RequestDispatcher rq = request.getRequestDispatcher("post_property/post_shops.jsp");
                rq.forward(request, response);
        } catch (Exception e) {
            throw new ServletException("Exception in  Servlet", e);
        } finally {
            if (out != null) {
                out.close();
            }
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {

                }
            }
        }

    }

    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        this.doGet(request, response);
    }
}
