/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package post_property;

import java.io.File;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.ResourceBundle;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;

import org.apache.commons.fileupload.servlet.ServletFileUpload;

/**
 * Servlet implementation class FileUploadServlet
 */
public class office_space_process extends HttpServlet {

    private static final long serialVersionUID = 1L;

    Date date = new Date();
    String name = null;

    //public String UPLOAD_DIRECTORY = "";
    private String fileSavePath;
    private static final String UPLOAD_DIRECTORY = "form_pics";

    public void init() {
        fileSavePath = getServletContext().getRealPath("/") + File.separator + UPLOAD_DIRECTORY;/*save uploaded files to a 'Upload' directory in the web app*/

        if (!(new File(fileSavePath)).exists()) {
            (new File(fileSavePath)).mkdir();    // creates the directory if it does not exist        
        }
    }

    /**
     * @see HttpServlet#HttpServlet()
     */
    public office_space_process() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     * response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     * response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, java.io.IOException {
        // TODO Auto-generated method stub
        //process only if its multipart content
        Connection con = null;
        long connected;
        OutputStream out = null;
        PreparedStatement stmt = null, stmt1, stmt2, stmt3, stmt4, stmt5, stmt6 = null;
        ResultSet rs, rs1, rs2, rs3, rs4, rs5, rs6 = null;
        ResourceBundle bundle = ResourceBundle.getBundle("SelectResource");
        HttpSession session = request.getSession();
        try {
            String url = bundle.getString("URL");
            String username = bundle.getString("username");
            String password = bundle.getString("password");
            Class.forName(bundle.getString("Driver"));
            con = DriverManager.getConnection(url, username, password);
        } catch (Exception e) {
            System.out.println("Database Connection Error" + e);
        }

        int c = 0;
        String currdate = "";
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String strDate = sdf.format(date);
        String time = new SimpleDateFormat("h:mm a").format(new Date());
        String dt = strDate + " " + time;
        int year = Calendar.getInstance().get(Calendar.YEAR);
        int yr = year % 100;
        String user = "";

        if (session.getAttribute("user") != null) {
            user = session.getAttribute("user").toString();
            session.setAttribute("user", user);
        } else {
            RequestDispatcher rq = request.getRequestDispatcher("index.jsp");
            rq.forward(request, response);
        }

        String insert = "", ttl_files = "";
        int nn = 0;
        int rand = 0;
        //UPLOAD_DIRECTORY = getServletContext().getRealPath(UPLOAD_DIRECTORY);
        //System.out.println(UPLOAD_DIRECTORY);
        if (ServletFileUpload.isMultipartContent(request)) {

            try {

                List<FileItem> multiparts = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(request);

                      // System.out.println(getName());
                //random number generation
                String namenum="",street_no= "",address_pro="",land_mark="",city = "",state = "",country = "",aname = "",i1="",i2 ="",i3 ="",i4 ="",i5 = "",i6 = "",age_pro = "",oter_details = "";
                String tenant_type1="",area_build="",adate = "",fur="",floor=null,facings = null,oter_details1 = "",area_build_type = "",area_build_in = "";
                String parking = "",oter_details2 = "";
                String rent_amt = "",deposit_amt = "",nego = "",maintanece_amt = "",agreement_dur = "",info_leave = "",oter_details3 = "";
                String rule_pets = "",rule_non_veg = "",rule_drink = "",rule_water = "",rule_elec = "",oter_details4 = "";
                String payment_type = "";
                String kyc_time = "",kyc_utype = "",kyc_pin = "",kyc_add1 = "",kyc_email = "",kyc_mob = "",kyc_gender = "",kyc_name = "";
                String near_by1 = "";
                
                ArrayList<String> amn = new ArrayList<String>();
                ArrayList<String> ser = new ArrayList<String>();
                ArrayList<String> rules = new ArrayList<String>();
                
                ArrayList<String> near_by = new ArrayList<String>();
                ArrayList<String> tenant_type = new ArrayList<String>();
                ArrayList<String> temp_img = new ArrayList<String>();
                for (FileItem item : multiparts) {
                           // if(multiparts){
                    // FileItem item = null ;
                    if (item.getFieldName().equals("namenum")) {
                        namenum = item.getString();
                        
                    }
                    if (item.getFieldName().equals("street_no")) {
                        street_no = item.getString();
                       
                    }
                    if (item.getFieldName().equals("address_pro")) {
                        address_pro = item.getString();
                    }
                    
                    if (item.getFieldName().equals("land_mark")) {
                        land_mark = item.getString();
                    }
                    
                    if (item.getFieldName().equals("city")) {
                        city = item.getString();
                    }
                    
                    if (item.getFieldName().equals("state")) {
                        state = item.getString();
                    }
                    
                    if (item.getFieldName().equals("country")) {
                        country = item.getString();
                    }
                    
                    if (item.getFieldName().equals("aname")) {
                        aname = item.getString();
                    }
                    
                    if (item.getFieldName().equals("age_pro")) {
                        age_pro = item.getString();
                    }
                    
                   
                    if (item.getFieldName().equals("oter_details")) {
                        oter_details = item.getString();
                    }
                    /*----------------------------Section 2--------------------------------------*/
                    
                   
                    if (item.getFieldName().equals("area_build")) {
                        area_build = item.getString();
                    }
                    if (item.getFieldName().equals("area_build_type")) {
                        area_build_type = item.getString();
                    }
                    if (item.getFieldName().equals("area_build_in")) {
                        area_build_in = item.getString();
                    }
                    if (item.getFieldName().equals("fur")) {
                        fur = item.getString();
                    }
                    if (item.getFieldName().equals("floor")) {
                        floor = item.getString();
                    }
                    if(floor!=null && floor.length()>0){
                        
                    }else{
                        floor = null;
                    }
                    if (item.getFieldName().equals("facings")) {
                        facings = item.getString();
                    }
                    if(facings!=null && facings.length()>0){
                        
                    }else{
                        facings = null;
                    }
                    
                    if (item.getFieldName().equals("oter_details1")) {
                        oter_details1 = item.getString();
                    }
                    
                    if (item.getFieldName().equals("adate")) {
                        adate = item.getString();
                        adate = adate.split("/")[2]+"-"+adate.split("/")[1]+"-"+adate.split("/")[0];
                    }
                    /*----------------------------section 3-------------------------------------------*/
                    if (item.getFieldName().equals("parking")) {
                        parking = item.getString();
                    }
                    
                    if (item.getFieldName().equals("oter_details2")) {
                        oter_details2 = item.getString();
                    }
                    
                    /*-------------------------------section 4--------------------------*/
                    if (item.getFieldName().equals("rent_amt")) {
                        rent_amt = item.getString();
                    }
                    if (item.getFieldName().equals("deposit_amt")) {
                        deposit_amt = item.getString();
                    }
                    if (item.getFieldName().equals("nego")) {
                        nego = item.getString();
                    }
                    if (item.getFieldName().equals("maintanece_amt")) {
                        maintanece_amt = item.getString();
                    }
                    if (item.getFieldName().equals("agreement_dur")) {
                        agreement_dur = item.getString();
                    }
                    if (item.getFieldName().equals("info_leave")) {
                        info_leave = item.getString();
                    }
                   if (item.getFieldName().equals("oter_details3")) {
                        oter_details3 = item.getString();
                    }
                     
                    /*-----------------------------------section 5----------------------------------------*/
                    if (item.getFieldName().equals("rule_pets")) {
                        rule_pets = item.getString();
                    }
                    if (item.getFieldName().equals("rule_non_veg")) {
                        rule_non_veg = item.getString();
                    }
                    if (item.getFieldName().equals("rule_drink")) {
                        rule_drink = item.getString();
                    }
                    if (item.getFieldName().equals("rule_water")) {
                        rule_water = item.getString();
                    }
                    if (item.getFieldName().equals("rule_elec")) {
                        rule_elec = item.getString();
                    }
                    if (item.getFieldName().equals("oter_details4")) {
                        oter_details4 = item.getString();
                    }
                    
                    /*----------------------------section 7--------------------------------------*/
                    
                    if (item.getFieldName().equals("kyc_time")) {
                        kyc_time = item.getString();
                        
                    }
                
                if (item.getFieldName().equals("kyc_utype")) {
                        kyc_utype = item.getString();
                        
                    }
                if (item.getFieldName().equals("kyc_pin")) {
                        kyc_pin = item.getString();
                        
                    }
                if (item.getFieldName().equals("kyc_add1")) {
                        kyc_add1 = item.getString();
                        
                    }
                if (item.getFieldName().equals("kyc_email")) {
                        kyc_email = item.getString();
                        
                    }
                if (item.getFieldName().equals("kyc_mob")) {
                        kyc_mob = item.getString();
                        
                    }
                if (item.getFieldName().equals("kyc_gender")) {
                        kyc_gender = item.getString();
                        
                    }
                if (item.getFieldName().equals("kyc_name")) {
                        kyc_name = item.getString();
                        
                    }
                    
                    
                    /*----------------------------section 6----------------------------------*/
                    if (item.getFieldName().equals("payment_type")) {
                        payment_type = item.getString();
                    }
                    
                    
                    
                    if(item.getFieldName().equals("amn"))
        {   
             amn.add(item.getString());
        }
                     if(item.getFieldName().equals("ser"))
        {   
             ser.add(item.getString());
        }
                     if(item.getFieldName().equals("near_by"))
        {   
             near_by.add(item.getString());
        }
                       if(item.getFieldName().equals("tenant_type"))
        {   
             tenant_type.add(item.getString());
        }
                     
                

                }

                 /*------------Fetch New Images-------------------------------*/
                  try {
                        
                        for (FileItem item1 : multiparts) {
                       
                            nn++;

                            if (!item1.isFormField()) {
                                rand = (int) (Math.random() * 100000000);
                                String nam = user + "_" + strDate + rand;
                                String name = item1.getName();
//                                System.out.println(name);
                                File uploadedFile = new File(fileSavePath + File.separator + nam + name.substring(name.indexOf('.')));
//                                System.out.println("uploadedFile Path is :"+uploadedFile);
                                item1.write(uploadedFile);
                                ttl_files = ttl_files + (nam + name.substring(name.indexOf('.'))) + "nnnags123";
                                temp_img.add((nam + name.substring(name.indexOf('.'))));
                                
                                
                            }
                        }
                    } catch (Exception e) {
                        ttl_files = ttl_files + "" + "nnnags123";
                        temp_img.add("ww");
                    }
                 System.out.println("temp is "+temp_img);
                 
                 for(int ii=0;ii<temp_img.size();ii++){
                     if(ii==0 && temp_img.get(ii)!=null && temp_img.get(ii).length()>0 && !temp_img.get(ii).equalsIgnoreCase("ww")){
                         i1 = temp_img.get(ii);
                     }
                     
                     if(ii==1 && temp_img.get(ii)!=null && temp_img.get(ii).length()>0 && !temp_img.get(ii).equalsIgnoreCase("ww")){
                         i2 = temp_img.get(ii);
                     }
                     
                     if(ii==2 && temp_img.get(ii)!=null && temp_img.get(ii).length()>0 && !temp_img.get(ii).equalsIgnoreCase("ww")){
                         i3 = temp_img.get(ii);
                     }
                     
                     if(ii==3 && temp_img.get(ii)!=null && temp_img.get(ii).length()>0 && !temp_img.get(ii).equalsIgnoreCase("ww")){
                         i4 = temp_img.get(ii);
                     }
                     
                     if(ii==4 && temp_img.get(ii)!=null && temp_img.get(ii).length()>0 && !temp_img.get(ii).equalsIgnoreCase("ww")){
                         i5 = temp_img.get(ii);
                     }
                     
                     if(ii==5 && temp_img.get(ii)!=null && temp_img.get(ii).length()>0 && !temp_img.get(ii).equalsIgnoreCase("ww")){
                         i6 = temp_img.get(ii);
                     }
                 }
                 
                  
            try {

                int uid = 0;
                 stmt1 = con.prepareStatement("select id from users where username='"+user+"'");
                     rs1 = stmt1.executeQuery();
                     if(rs1.next()){
                         uid=rs1.getInt("id");
                     }
                     
                       
                    if(near_by.size()>0){
                     for(int ii=0;ii<near_by.size();ii++){
                         near_by1 = near_by1 + ","+near_by.get(ii);
                     }
                     near_by1 = near_by1.substring(1);
                    
                       }
                     for(int iii=0;iii<tenant_type.size();iii++){
                         tenant_type1 = tenant_type1 + ","+tenant_type.get(iii);
                     }
                     tenant_type1 = tenant_type1.substring(1);
                    
//                     System.out.println("uid is "+uid);

String doc_no = "";
                int counter = 0;
            stmt = con.prepareStatement("select max(counter)as n from post_office");
            rs = stmt.executeQuery();
            if (rs.next()) {
                counter = rs.getInt("n");
            }
            counter = counter + 1;
            String temp = "";
            if (counter < 10) {
                temp = "000" + counter;
            } else if (counter > 9 && counter < 100) {
                temp = "00" + counter;
            } else {
                temp = "0" + counter;
            }
            doc_no = "RHNTR" + year + "OF" + temp; 
                con.setAutoCommit(false);
             
               
                    stmt2 = con.prepareStatement("insert into post_office(name_num,street_no,address_pro,land_mark,city,state,country,age_pro,near_by,oter_details,tenant_type,area_build,adate,fur,floor,facings,oter_details1,parking,oter_details2,rent_amt,deposit_amt,nego,maintanece_amt,agreement_dur,info_leave,oter_details3,rule_non_veg,rule_pets,rule_drink,rule_water,rule_elec,oter_details4,payment_type,img1,img2,img3,img4,img5,img6,cdate,ctime,cuser,area_build_type,area_build_in,aname,kyc_name,kyc_gender,kyc_mob,kyc_email,kyc_add1,kyc_pin,kyc_utype,kyc_utime,counter,yrr,doc_no)values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
                    stmt2.setString(1, namenum);
                    stmt2.setString(2, street_no);
                    stmt2.setString(3, address_pro);
                    stmt2.setString(4, land_mark);
                    stmt2.setString(5, city);
                    stmt2.setString(6, state);
                    stmt2.setString(7, country);
                    stmt2.setString(8, age_pro);
                    stmt2.setString(9, near_by1);
                    stmt2.setString(10, oter_details);
                    stmt2.setString(11, tenant_type1);
                    stmt2.setString(12, area_build);
                    stmt2.setString(13, adate);
                    stmt2.setString(14, fur);
                    stmt2.setString(15, floor);
                    stmt2.setString(16, facings);
                    stmt2.setString(17, oter_details1);
                    stmt2.setString(18, parking);
                    stmt2.setString(19, oter_details2);
                    stmt2.setString(20, rent_amt);
                    stmt2.setString(21, deposit_amt);
                    stmt2.setString(22, nego);
                    stmt2.setString(23, maintanece_amt);
                    stmt2.setString(24, agreement_dur);
                    stmt2.setString(25, info_leave);
                    stmt2.setString(26, oter_details3);
                    stmt2.setString(27, rule_non_veg);
                    stmt2.setString(28, rule_pets);
                    stmt2.setString(29, rule_drink);  
                    stmt2.setString(30, rule_water);
                    stmt2.setString(31, rule_elec);
                    stmt2.setString(32, oter_details4);
                    stmt2.setString(33, payment_type);
                    stmt2.setString(34, i1);
                    stmt2.setString(35, i2);
                    stmt2.setString(36, i3);
                    stmt2.setString(37, i4);
                    stmt2.setString(38, i5);
                    stmt2.setString(39, i6);
                    stmt2.setString(40, strDate);
                    stmt2.setString(41, time);
                    stmt2.setInt(42, uid);
                    stmt2.setString(43, area_build_type);
                    stmt2.setString(44, area_build_in);
                    stmt2.setString(45, aname);
                    stmt2.setString(46, kyc_name);
                    stmt2.setString(47, kyc_gender);
                    stmt2.setString(48, kyc_mob);
                    stmt2.setString(49, kyc_email);
                    stmt2.setString(50, kyc_add1);
                    stmt2.setString(51, kyc_pin);
                    stmt2.setString(52, kyc_utype);
                    stmt2.setString(53, kyc_time);
                    stmt2.setInt(54, counter);
                    stmt2.setInt(55, year);
                    stmt2.setString(56, doc_no);
                    stmt2.executeUpdate();
                    
                    String apa_id = "";
                    
                    stmt3 = con.prepareStatement("select id from post_office where name_num='"+namenum+"' and cuser='"+uid+"' and cdate='"+strDate+"'");
                    rs3 = stmt3.executeQuery();
                    if(rs3.next()){
                        apa_id = rs3.getString("id");
                    }
                    
                    for(int i=0;i<amn.size();i++){
                      stmt4 = con.prepareStatement("insert into office_amn(apa_id,amn,cdate,ctime,cuser)values(?,?,?,?,?)");
                      stmt4.setString(1, apa_id);
                      stmt4.setString(2, amn.get(i));
                      stmt4.setString(3, strDate);
                      stmt4.setString(4, time);
                      stmt4.setInt(5, uid);
                      stmt4.executeUpdate();
                    }
                    
                    for(int i=0;i<ser.size();i++){
                      stmt4 = con.prepareStatement("insert into office_ser(apa_id,ser,cdate,ctime,cuser)values(?,?,?,?,?)");
                      stmt4.setString(1, apa_id);
                      stmt4.setString(2, ser.get(i));
                      stmt4.setString(3, strDate);
                      stmt4.setString(4, time);
                      stmt4.setInt(5, uid);
                      stmt4.executeUpdate();
                    }
                    
                   
                    con.commit();
                    insert="pass";
                     
                
                        } catch (Exception ex) {
                con.rollback();
                insert = "fail";
                System.out.println("the puot is "+ex);
                    
            }

               // insert = "pass";

            } catch (Exception ex) {
                insert = "fail";
                System.out.println("error is :" + ex);
                ex.printStackTrace();
                // request.setAttribute("message", "File Upload Failed due to " + ex);

            } finally {
                if (out != null) {
                    out.close();
                }
                if (con != null) {
                    try {
                        con.close();
                    } catch (SQLException ex) {

                        insert = "fail";
                        System.out.println("error is "+ex);

                    }
                }
            }

        } else {
            insert = "fail";
            request.setAttribute("message",
                    "Sorry this Servlet only handles file upload request");

        }

        session.setAttribute("insert", insert);

//        getServletContext().getRequestDispatcher("/salon_creation").forward(request, response);
        response.sendRedirect("office_space_temp");

    }

}
