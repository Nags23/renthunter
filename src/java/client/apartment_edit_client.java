/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package client;


import java.io.IOException;
import java.io.OutputStream;
import java.util.Calendar;
import java.sql.*;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//@SuppressWarnings("deprecation")
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javax.servlet.*;
import javax.servlet.http.HttpSession;

public class apartment_edit_client extends HttpServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String insert = "fail";
        Connection con = null;
        long connected;
        OutputStream out = null;
        PreparedStatement stmt, stmt1, stmt2, stmt3, stmt4, stmt5, stmt6, stmt7,stmt8,stmt9 = null;
        ResultSet rs, rs1, rs2, rs3, rs4, rs5, rs6,rs7,rs8,rs9 = null;
        ResourceBundle bundle = ResourceBundle.getBundle("SelectResource");
        HttpSession session = request.getSession();
        try {
            try {
                String url = bundle.getString("URL");
                String username = bundle.getString("username");
                String password = bundle.getString("password");
                Class.forName(bundle.getString("Driver"));
                con = DriverManager.getConnection(url, username, password);
            } catch (Exception e) {
                System.out.println("Database Connection Error" + e);
            }

            String currdate = "";
            Date date = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            String strDate = sdf.format(date);
            String time = new SimpleDateFormat("h:mm a").format(new Date());
            String dt = strDate + " " + time;
            int year = Calendar.getInstance().get(Calendar.YEAR);
            int yr = year % 100;
            String user = "";

             if(session.getAttribute("user")!=null){
                user=session.getAttribute("user").toString();
                session.setAttribute("user", user);
            }else{
                 RequestDispatcher rq = request.getRequestDispatcher("index.jsp");
            rq.forward(request, response);
            }
             
             stmt = con.prepareStatement("SELECT mob,email,firstname FROM users where username='"+user+"'");
             rs = stmt.executeQuery();
             if(rs.next()){
                 session.setAttribute("uuu_mob", rs.getString("mob"));
                 session.setAttribute("uuu_email", rs.getString("email"));
                 session.setAttribute("uuu_firstname", rs.getString("firstname"));
             }
             
             ArrayList<String> dd_id = new ArrayList<String>();
             ArrayList<String> dd_sname = new ArrayList<String>();
             
             stmt = con.prepareStatement("select id,sname,disabled from state_types where disabled='n' order by id asc");
             rs = stmt.executeQuery();
             while(rs.next()){
                dd_id.add(rs.getString("id")); 
                dd_sname.add(rs.getString("sname"));
             }
             
             session.setAttribute("dd_id", dd_id);
             session.setAttribute("dd_sname", dd_sname);
             
             ArrayList<String> bhk_id = new ArrayList<String>();
             ArrayList<String> bhk_bhk = new ArrayList<String>();
             
             stmt = con.prepareStatement("SELECT id,bhk FROM bhk_types WHERE disabled='n' ORDER BY bhk asc");
             rs = stmt.executeQuery();
             while(rs.next()){
                bhk_id.add(rs.getString("id")); 
                bhk_bhk.add(rs.getString("bhk"));
             }
             
             session.setAttribute("bhk_id", bhk_id);
             session.setAttribute("bhk_bhk", bhk_bhk);
             
             ArrayList<String> fur_id = new ArrayList<String>();
             ArrayList<String> fur_ftype = new ArrayList<String>();
             stmt1 = con.prepareStatement("SELECT id,ftype FROM furnished_types WHERE disabled='n' ORDER BY ftype asc");
             rs1 = stmt1.executeQuery();
             while(rs1.next()){
                 fur_id.add(rs1.getString("id"));
                 fur_ftype.add(rs1.getString("ftype"));
             }
             
             session.setAttribute("fur_id", fur_id);
             session.setAttribute("fur_ftype", fur_ftype);
             
             ArrayList<String> floor_id = new ArrayList<String>();
             ArrayList<String> floor_floor = new ArrayList<String>();
             
             stmt2 = con.prepareStatement("SELECT id,floor FROM floor_types WHERE disabled='n' ORDER BY id asc");
             rs2 = stmt2.executeQuery();
             while(rs2.next()){
                 floor_id.add(rs2.getString("id"));
                 floor_floor.add(rs2.getString("floor"));
             }
             
             session.setAttribute("floor_id", floor_id);
             session.setAttribute("floor_floor", floor_floor);
             
             ArrayList<String> facings_id = new ArrayList<String>();
             ArrayList<String> facings_facings = new ArrayList<String>();
             
             stmt3 = con.prepareStatement("SELECT id,facings FROM facing_types WHERE disabled='n' ORDER BY facings asc");
             rs3 = stmt3.executeQuery();
             while(rs3.next()){
                 facings_id.add(rs3.getString("id"));
                 facings_facings.add(rs3.getString("facings"));
             }
             
             session.setAttribute("facings_id", facings_id);
             session.setAttribute("facings_facings", facings_facings);
             
             ArrayList<String> ten_id = new ArrayList<String>();
             ArrayList<String> ten_ttype = new ArrayList<String>();
             ArrayList<String> ten_fclass = new ArrayList<String>();
             
             stmt4 = con.prepareStatement("SELECT id,ttype,fclass FROM tenant_types where ptype='Home' || ptype='All' ORDER BY id asc");
             rs4 = stmt4.executeQuery();
             while(rs4.next()){
                 ten_id.add(rs4.getString("id"));
                 ten_ttype.add(rs4.getString("ttype"));
                 ten_fclass.add(rs4.getString("fclass"));
             }
             
             session.setAttribute("ten_id", ten_id);
             session.setAttribute("ten_ttype", ten_ttype);
             session.setAttribute("ten_fclass", ten_fclass);
             
             ArrayList<String> amn_id = new ArrayList<String>();
             ArrayList<String> amn_ttype = new ArrayList<String>();
             ArrayList<String> amn_fclass = new ArrayList<String>();
             stmt5 = con.prepareStatement("SELECT id,atype,fclass FROM amenities_type ORDER BY id asc");
             rs5 = stmt5.executeQuery();
             while(rs5.next()){
                 amn_id.add(rs5.getString("id"));
                 amn_ttype.add(rs5.getString("atype"));
                 amn_fclass.add(rs5.getString("fclass"));
             }
             
             session.setAttribute("amn_id", amn_id);
             session.setAttribute("amn_ttype", amn_ttype);
             session.setAttribute("amn_fclass", amn_fclass);
             
             ArrayList<String> ser_id = new ArrayList<String>();
             ArrayList<String> ser_ttype = new ArrayList<String>();
             ArrayList<String> ser_fclass = new ArrayList<String>();
             stmt5 = con.prepareStatement("SELECT id,stype,fclass FROM service_type ORDER BY id asc");
             rs5 = stmt5.executeQuery();
             while(rs5.next()){
                 ser_id.add(rs5.getString("id"));
                 ser_ttype.add(rs5.getString("stype"));
                 ser_fclass.add(rs5.getString("fclass"));
             }
             
             session.setAttribute("ser_id", ser_id);
             session.setAttribute("ser_ttype", ser_ttype);
             session.setAttribute("ser_fclass", ser_fclass);
             
             ArrayList<String> near_id = new ArrayList<String>();
             ArrayList<String> near_name = new ArrayList<String>();
             stmt5 = con.prepareStatement("SELECT id,name FROM near_by WHERE disabled='n' ORDER BY name");
             rs5 = stmt5.executeQuery();
             while(rs5.next()){
               near_id.add(rs5.getString("id"));
               near_name.add(rs5.getString("name"));
             }
             
             session.setAttribute("near_id", near_id);
             session.setAttribute("near_name", near_name);
             
             if(request.getParameter("id")!=null){
                 String id = request.getParameter("id");
                 
                 /*---------------------------TAB 1-----------------------------*/
                 
                 ArrayList<String> vvv_city_id = new ArrayList<String>();
                 ArrayList<String> vvv_city_name = new ArrayList<String>();
                 
                 ArrayList<String> vvv_area_id = new ArrayList<String>();
                 ArrayList<String> vvv_area_name = new ArrayList<String>();
                 
                 
                 
                 stmt = con.prepareStatement("SELECT id,name_num,street_no,land_mark,age_pro,oter_details,city,state,aname,near_by FROM post_apartment where id='"+id+"'");
                 rs = stmt.executeQuery();
                 if(rs.next()){
                     session.setAttribute("vv_id", rs.getString("id"));
                     session.setAttribute("vv_name_num", rs.getString("name_num"));
                     session.setAttribute("vv_street_no", rs.getString("street_no"));
                     session.setAttribute("vv_land_mark", rs.getString("land_mark"));
                     session.setAttribute("vv_age_pro", rs.getString("age_pro"));
                     session.setAttribute("vv_oter_details", rs.getString("oter_details"));
                     stmt1 = con.prepareStatement("SELECT id,sname FROM state_types where id='"+rs.getString("state")+"'");
                     rs1 = stmt1.executeQuery();
                     if(rs1.next()){
                         session.setAttribute("vv_state_id", rs1.getString("id"));
                         session.setAttribute("vv_state", rs1.getString("sname"));
                     }else{
                         session.setAttribute("vv_state_id", "");
                         session.setAttribute("vv_state", "");
                     }
                     stmt2 = con.prepareStatement("SELECT id,cname FROM city_types WHERE id='"+rs.getString("city")+"'");
                     rs2 = stmt2.executeQuery();
                     if(rs2.next()){
                         session.setAttribute("vv_city_id", rs2.getString("id"));
                         session.setAttribute("vv_city_name", rs2.getString("cname"));
                     }else{
                         session.setAttribute("vv_city_id", "");
                         session.setAttribute("vv_city_name", ""); 
                     }
                     stmt2 = con.prepareStatement("select id,cname from city_types where sname='"+rs.getString("state")+"'");
                     rs2 = stmt2.executeQuery();
                     while(rs2.next()){
                       vvv_city_id.add(rs2.getString("id"));
                       vvv_city_name.add(rs2.getString("cname"));
                     }
                     
                     stmt2 = con.prepareStatement("select id,aname from area_types where sname='"+rs.getString("state")+"' and cname='"+rs.getString("city")+"'");
                     rs2 = stmt2.executeQuery();
                     while(rs2.next()){
                       vvv_area_id.add(rs2.getString("id"));
                       vvv_area_name.add(rs2.getString("aname"));
                     }
                     
                     stmt3 = con.prepareStatement("SELECT id,aname FROM area_types WHERE id='"+rs.getString("aname")+"'");
                     rs3 = stmt3.executeQuery();
                     if(rs3.next()){
                         session.setAttribute("vv_area_id", rs3.getString("id"));
                         session.setAttribute("vv_area_name", rs3.getString("aname"));
                     }else{
                         session.setAttribute("vv_area_id", "");
                         session.setAttribute("vv_area_name", ""); 
                     }
                     
                     session.setAttribute("vv_near_by", rs.getString("near_by"));
                 }
                 
                 session.setAttribute("vvv_city_id", vvv_city_id);
                 session.setAttribute("vvv_city_name", vvv_city_name);
                 session.setAttribute("vvv_area_id", vvv_area_id);
                 session.setAttribute("vvv_area_name", vvv_area_name);
                 
                 /*-----------------------------------------------------TAB2--------------------------------------------------------------*/
                 
                 stmt = con.prepareStatement("SELECT tenant_type,bhk,area_build,DATE_FORMAT(adate,'%d/%m/%Y')AS adate1,fur,floor,facings,oter_details1,area_build_type,area_build_in FROM post_apartment where id='"+id+"'");
                 rs = stmt.executeQuery();
                 if(rs.next()){
                     session.setAttribute("vvv_tenant_type", rs.getString("tenant_type"));
                     session.setAttribute("vvv_area", rs.getString("area_build"));
                     session.setAttribute("vvv_adate", rs.getString("adate1"));
                     session.setAttribute("vvv_oter_details1", rs.getString("oter_details1"));
                     session.setAttribute("vvv_area_build_type", rs.getString("area_build_type"));
                     session.setAttribute("vvv_area_build_in", rs.getString("area_build_in"));
                     stmt1 = con.prepareStatement("SELECT id,bhk FROM bhk_types WHERE id='"+rs.getString("bhk")+"'");
                     rs1 = stmt1.executeQuery();
                     if(rs1.next()){
                         session.setAttribute("vvv_bhk_id", rs1.getString("id"));
                         session.setAttribute("vvv_bhk", rs1.getString("bhk"));
                     }else{
                         session.setAttribute("vvv_bhk_id", "");
                         session.setAttribute("vvv_bhk", "");
                     }
                     
                     stmt2 = con.prepareStatement("SELECT id,ftype FROM furnished_types WHERE id='"+rs.getString("fur")+"'");
                     rs2 = stmt2.executeQuery();
                     if(rs2.next()){
                        session.setAttribute("vvv_fur_id", rs2.getString("id"));
                         session.setAttribute("vvv_fur", rs2.getString("ftype"));
                     }else{
                         session.setAttribute("vvv_fur_id", "");
                         session.setAttribute("vvv_fur", "");
                     }
                     
                     stmt3 = con.prepareStatement("SELECT id,floor FROM floor_types WHERE id='"+rs.getString("floor")+"'");
                     rs3 = stmt3.executeQuery();
                     if(rs3.next()){
                         session.setAttribute("vvv_floor_id", rs3.getString("id"));
                         session.setAttribute("vvv_floor", rs3.getString("floor"));
                     }else{
                         session.setAttribute("vvv_floor_id", "");
                         session.setAttribute("vvv_floor", "");
                     }
                     
                     stmt4 = con.prepareStatement("SELECT id,facings FROM facing_types WHERE id='"+rs.getString("facings")+"'");
                     rs4 = stmt4.executeQuery();
                     if(rs4.next()){
                       session.setAttribute("vvv_facings_id", rs4.getString("id"));
                         session.setAttribute("vvv_facings", rs4.getString("facings"));
                     }else{
                         session.setAttribute("vvv_facings_id", "");
                         session.setAttribute("vvv_facings", "");
                     }
                 }
                 
                 /*-----------------------------------------------TAB3--------------------------------------------*/
                 
                 stmt = con.prepareStatement("SELECT parking,oter_details2 FROM post_apartment where id='"+id+"'");
                 rs = stmt.executeQuery();
                 if(rs.next()){
                     session.setAttribute("vvv_parking", rs.getString("parking"));
                     session.setAttribute("vvv_oter_details2", rs.getString("oter_details2"));
                 }
                 String vvv_home_amn = "";
                 stmt1 = con.prepareStatement("SELECT amn FROM apartment_amn WHERE apa_id='"+id+"'");
                 rs1 = stmt1.executeQuery();
                 while(rs1.next()){
                    vvv_home_amn = vvv_home_amn + ","+rs1.getString("amn");
                 }
                 if(vvv_home_amn.length()>0){
                 vvv_home_amn = vvv_home_amn.substring(1);
                 }
                 session.setAttribute("vvv_home_amn", vvv_home_amn);
                 
                 String vvv_home_ser = "";
                 stmt2 = con.prepareStatement("SELECT ser FROM apartment_ser WHERE apa_id='"+id+"'");
                 rs2 = stmt2.executeQuery();
                 while(rs2.next()){
                     vvv_home_ser = vvv_home_ser + "," + rs2.getString("ser");
                 }
                 if(vvv_home_ser.length()>0){
                 vvv_home_ser = vvv_home_ser.substring(1);
                 }
                 session.setAttribute("vvv_home_ser", vvv_home_ser);
                 
                 
                 /*------------------------------------------TAB4------------------------------------*/
                 
                 stmt = con.prepareStatement("SELECT rent_amt,deposit_amt,nego,maintanece_amt,agreement_dur,info_leave,oter_details3 FROM post_apartment where id='"+id+"'");
                 rs = stmt.executeQuery();
                 if(rs.next()){
                   session.setAttribute("vv_rent_amt", rs.getString("rent_amt"));
                   session.setAttribute("vv_deposit_amt", rs.getString("deposit_amt"));
                   session.setAttribute("vv_nego", rs.getString("nego"));
                   session.setAttribute("vv_maintanece_amt", rs.getString("maintanece_amt"));
                   session.setAttribute("vv_agreement_dur", rs.getString("agreement_dur"));
                   session.setAttribute("vv_info_leave", rs.getString("info_leave"));
                   session.setAttribute("vv_oter_details3", rs.getString("oter_details3"));
                 }
                 
                 /*-----------------------------------------TAB5---------------------------------------------*/
                 
                 stmt = con.prepareStatement("SELECT rule_non_veg,rule_pets,rule_drink,rule_water,rule_elec,oter_details4 FROM post_apartment where id='"+id+"'");
                 rs = stmt.executeQuery();
                 if(rs.next()){
                     session.setAttribute("vv_rule_non_veg", rs.getString("rule_non_veg"));
                     session.setAttribute("vv_rule_pets", rs.getString("rule_pets"));
                     session.setAttribute("vv_rule_drink", rs.getString("rule_drink"));
                     session.setAttribute("vv_rule_water", rs.getString("rule_water"));
                     session.setAttribute("vv_rule_elec", rs.getString("rule_elec"));
                     session.setAttribute("vv_oter_details4", rs.getString("oter_details4"));
                 }
                 
                 /*-------------------------------------------TAB6-------------------------------------------------*/
                 
                 stmt = con.prepareStatement("SELECT kyc_name,kyc_gender,kyc_mob,kyc_email,kyc_add1,kyc_pin,kyc_utype,kyc_utime FROM post_apartment where id='"+id+"'");
                 rs = stmt.executeQuery();
                 if(rs.next()){
                     session.setAttribute("vv_kyc_name", rs.getString("kyc_name"));
                     session.setAttribute("vv_kyc_gender", rs.getString("kyc_gender"));
                     session.setAttribute("vv_kyc_mob", rs.getString("kyc_mob"));
                     session.setAttribute("vv_kyc_email", rs.getString("kyc_email"));
                     session.setAttribute("vv_kyc_add1", rs.getString("kyc_add1"));
                     session.setAttribute("vv_kyc_pin", rs.getString("kyc_pin"));
                     session.setAttribute("vv_kyc_utype", rs.getString("kyc_utype"));
                     session.setAttribute("vv_kyc_utime", rs.getString("kyc_utime"));
                 }
                 
             }
             
             RequestDispatcher rq = request.getRequestDispatcher("client/apartment_edit_client.jsp");
                rq.forward(request, response);
        } catch (Exception e) {
            throw new ServletException("Exception in  Servlet", e);
        } finally {
            if (out != null) {
                out.close();
            }
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {

                }
            }
        }

    }

    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        this.doGet(request, response);
    }
}
