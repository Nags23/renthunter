/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client;

import java.io.File;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.ResourceBundle;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;

import org.apache.commons.fileupload.servlet.ServletFileUpload;

/**
 * Servlet implementation class FileUploadServlet
 */
public class post_apartment_edit_process extends HttpServlet {

    private static final long serialVersionUID = 1L;

    Date date = new Date();
    String name = null;

    //public String UPLOAD_DIRECTORY = "";
    private String fileSavePath;
    private static final String UPLOAD_DIRECTORY = "form_pics";

    public void init() {
        fileSavePath = getServletContext().getRealPath("/") + File.separator + UPLOAD_DIRECTORY;/*save uploaded files to a 'Upload' directory in the web app*/

        if (!(new File(fileSavePath)).exists()) {
            (new File(fileSavePath)).mkdir();    // creates the directory if it does not exist        
        }
    }

    /**
     * @see HttpServlet#HttpServlet()
     */
    public post_apartment_edit_process() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     * response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     * response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, java.io.IOException {
        // TODO Auto-generated method stub
        //process only if its multipart content
        Connection con = null;
        long connected;
        OutputStream out = null;
        PreparedStatement stmt = null, stmt1, stmt2, stmt3, stmt4, stmt5, stmt6 = null;
        ResultSet rs, rs1, rs2, rs3, rs4, rs5, rs6 = null;
        ResourceBundle bundle = ResourceBundle.getBundle("SelectResource");
        HttpSession session = request.getSession();
        try {
            String url = bundle.getString("URL");
            String username = bundle.getString("username");
            String password = bundle.getString("password");
            Class.forName(bundle.getString("Driver"));
            con = DriverManager.getConnection(url, username, password);
        } catch (Exception e) {
            System.out.println("Database Connection Error" + e);
        }

        int c = 0;
        String currdate = "";
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String strDate = sdf.format(date);
        String time = new SimpleDateFormat("h:mm a").format(new Date());
        String dt = strDate + " " + time;
        int year = Calendar.getInstance().get(Calendar.YEAR);
        int yr = year % 100;
        String user = "";

        if (session.getAttribute("user") != null) {
            user = session.getAttribute("user").toString();
            session.setAttribute("user", user);
        } else {
            RequestDispatcher rq = request.getRequestDispatcher("index.jsp");
            rq.forward(request, response);
        }

        String insert = "", ttl_files = "";
        int nn = 0;
        int rand = 0;
        //UPLOAD_DIRECTORY = getServletContext().getRealPath(UPLOAD_DIRECTORY);
        //System.out.println(UPLOAD_DIRECTORY);
        if (ServletFileUpload.isMultipartContent(request)) {

            try {

                List<FileItem> multiparts = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(request);

                      // System.out.println(getName());
                //random number generation
                String namenum="",street_no= "",address_pro="",land_mark="",i1="",i2 ="",i3 ="",i4 ="",i5 = "",i6 = "",age_pro = "",id = "";
                String tenant_type="",bhk= "",area_build="",fur="",floor="",facings = "";
                String parking = "";
                String rent_amt = "",deposit_amt = "",nego = "",maintanece_amt = "",agreement_dur = "",info_leave = "";
                String rule_pets = "",rule_non_veg = "";
                String payment_type = "";
                String adate = "";
                
                ArrayList<String> amn = new ArrayList<String>();
                ArrayList<String> rules = new ArrayList<String>();
                
                for (FileItem item : multiparts) {
                           // if(multiparts){
                    // FileItem item = null ;
                    if (item.getFieldName().equals("id")) {
                        id = item.getString();
                        
                    }
                    
                    if (item.getFieldName().equals("namenum")) {
                        namenum = item.getString();
                        
                    }
                    if (item.getFieldName().equals("street_no")) {
                        street_no = item.getString();
                       
                    }
                    if (item.getFieldName().equals("address_pro")) {
                        address_pro = item.getString();
                    }
                    
                    if (item.getFieldName().equals("land_mark")) {
                        land_mark = item.getString();
                    }
                    
                    if (item.getFieldName().equals("age_pro")) {
                        age_pro = item.getString();
                    }
                    
                    if (item.getFieldName().equals("tenant_type")) {
                        tenant_type = item.getString();
                    }
                    if (item.getFieldName().equals("bhk")) {
                        bhk = item.getString();
                    }
                    if (item.getFieldName().equals("area_build")) {
                        area_build = item.getString();
                    }
                    if (item.getFieldName().equals("fur")) {
                        fur = item.getString();
                    }
                    if (item.getFieldName().equals("floor")) {
                        floor = item.getString();
                    }
                    if (item.getFieldName().equals("facings")) {
                        facings = item.getString();
                    }
                    
                    
                    if (item.getFieldName().equals("parking")) {
                        parking = item.getString();
                    }
                    
                    
                    if (item.getFieldName().equals("rent_amt")) {
                        rent_amt = item.getString();
                    }
                    if (item.getFieldName().equals("deposit_amt")) {
                        deposit_amt = item.getString();
                    }
                    if (item.getFieldName().equals("nego")) {
                        nego = item.getString();
                    }
                    if (item.getFieldName().equals("maintanece_amt")) {
                        maintanece_amt = item.getString();
                    }
                    if (item.getFieldName().equals("agreement_dur")) {
                        agreement_dur = item.getString();
                    }
                    if (item.getFieldName().equals("info_leave")) {
                        info_leave = item.getString();
                    }
                   
                     
                    
                    if (item.getFieldName().equals("rule_pets")) {
                        rule_pets = item.getString();
                    }
                    if (item.getFieldName().equals("rule_non_veg")) {
                        rule_non_veg = item.getString();
                    }
                    
                    
                    if (item.getFieldName().equals("payment_type")) {
                        payment_type = item.getString();
                    }
                    
                    if (item.getFieldName().equals("adate")) {
                        adate = item.getString();
                    }
                    
                    if(item.getFieldName().equals("amn"))
        {   
             amn.add(item.getString());
        }
                    if(item.getFieldName().equals("rules"))
        {   
             rules.add(item.getString());
        }
                   
                    /*------------Fetch New Images-------------------------------*/
                    try {
                        
                        for (FileItem item1 : multiparts) {
                            nn++;

                            if (!item1.isFormField()) {
                                rand = (int) (Math.random() * 100000000);
                                String nam = user + "_" + strDate + rand;
                                String name = item1.getName();
//                                System.out.println(name);
                                File uploadedFile = new File(fileSavePath + File.separator + nam + name.substring(name.indexOf('.')));
//                                System.out.println("uploadedFile Path is :"+uploadedFile);
                                item1.write(uploadedFile);
                                ttl_files = ttl_files + nam + name.substring(name.indexOf('.')) + "nnnags123";
                                
                               
                            }
                        }
                    } catch (Exception e) {
                        ttl_files = ttl_files + "" + "nnnags123";
                    }
                    System.out.println("total files are :" + ttl_files);

                    try {
                        if(ttl_files.split("nnnags123")[0]!=null && ttl_files.split("nnnags123")[0].length()>0){
                        i1 = ttl_files.split("nnnags123")[0];
                        }else{
                            i1 = "";
                        }
                          if(ttl_files.split("nnnags123")[1]!=null && ttl_files.split("nnnags123")[1].length()>0){
                        i2 = ttl_files.split("nnnags123")[2];
                        }else{
                            i2 = "";
                        }
                           if(ttl_files.split("nnnags123")[2]!=null && ttl_files.split("nnnags123")[2].length()>0){
                        i3 = ttl_files.split("nnnags123")[2];
                        }else{
                            i3 = "";
                        }
                              if(ttl_files.split("nnnags123")[3]!=null && ttl_files.split("nnnags123")[3].length()>0){
                        i4 = ttl_files.split("nnnags123")[3];
                        }else{
                            i4 = "";
                        }
                              if(ttl_files.split("nnnags123")[3]!=null && ttl_files.split("nnnags123")[3].length()>0){
                        i5 = ttl_files.split("nnnags123")[3];
                        }else{
                            i5 = "";
                        }
                              if(ttl_files.split("nnnags123")[3]!=null && ttl_files.split("nnnags123")[3].length()>0){
                        i6 = ttl_files.split("nnnags123")[3];
                        }else{
                            i6 = "";
                        }
                    } catch (Exception e) {
                        i1 = "";i2 = "";i3 = "";i4 = "";i5 = "";i6 = "";
                    }

                }

                 
                  
            try {

                int uid = 0;
                 stmt1 = con.prepareStatement("select id from users where username='"+user+"'");
                     rs1 = stmt1.executeQuery();
                     if(rs1.next()){
                         uid=rs1.getInt("id");
                     }
//                     System.out.println("uid is "+uid);
                con.setAutoCommit(false);
             
               
                    stmt2 = con.prepareStatement("update post_apartments set name_num='"+namenum+"',street_no='"+street_no+"',address_pro='"+address_pro+"',land_mark='"+land_mark+"',age_pro='"+age_pro+"',tenant_type='"+tenant_type+"',bhk='"+bhk+"',area_build='"+area_build+"',fur='"+fur+"',floor='"+floor+"',facings='"+facings+"',parking='"+parking+"',amn='',rent_amt='"+rent_amt+"',deposit_amt='"+deposit_amt+"',nego='"+nego+"',maintanece_amt='"+maintanece_amt+"',agreement_dur='"+agreement_dur+"',info_leave='"+info_leave+"',rules='',rule_pets='"+rule_pets+"',rule_non_veg='"+rule_non_veg+"',payment_type='"+payment_type+"',img1='"+i1+"',img2='"+i2+"',img3='"+i3+"',img4='"+i4+"',adate='"+adate+"',img5='"+i5+"',img6='"+i6+"'");
                    stmt2.executeUpdate();
                    
                    String apa_id = "";
                    
                   stmt3 = con.prepareStatement("delete from apartment_amn where apa_id='"+id+"'");
                   stmt3.executeUpdate();
                    
                    for(int i=0;i<amn.size();i++){
                      stmt4 = con.prepareStatement("insert into apartment_amn(apa_id,amn,cdate,ctime,cuser)values(?,?,?,?,?)");
                      stmt4.setString(1, id);
                      stmt4.setString(2, amn.get(i));
                      stmt4.setString(3, strDate);
                      stmt4.setString(4, time);
                      stmt4.setInt(5, uid);
                      stmt4.executeUpdate();
                    }
                    
                   stmt3 = con.prepareStatement("delete from apartment_rules where apa_id='"+id+"'");
                   stmt3.executeUpdate();
                    
                    for(int i=0;i<rules.size();i++){
                      stmt5 = con.prepareStatement("insert into apartment_rules(apa_id,rules,cdate,ctime,cuser)values(?,?,?,?,?)");
                      stmt5.setString(1, id);
                      stmt5.setString(2, rules.get(i));
                      stmt5.setString(3, strDate);
                      stmt5.setString(4, time);
                      stmt5.setInt(5, uid);
                      stmt5.executeUpdate();
                    }
                    
                   
                    con.commit();
                    insert="pass";
                     
                
                        } catch (Exception ex) {
                con.rollback();
                insert = "fail";
                System.out.println("the puot is "+ex);
                    
            }

               // insert = "pass";

            } catch (Exception ex) {
                insert = "fail";
                System.out.println("error is :" + ex);
                ex.printStackTrace();
                // request.setAttribute("message", "File Upload Failed due to " + ex);

            } finally {
                if (out != null) {
                    out.close();
                }
                if (con != null) {
                    try {
                        con.close();
                    } catch (SQLException ex) {

                        insert = "fail";
                        System.out.println("error is "+ex);

                    }
                }
            }

        } else {
            insert = "fail";
            request.setAttribute("message",
                    "Sorry this Servlet only handles file upload request");

        }

        session.setAttribute("insert", insert);

//        getServletContext().getRequestDispatcher("/salon_creation").forward(request, response);
        response.sendRedirect("post_apartment_edit_temp");

    }

}
