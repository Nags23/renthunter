<%-- 
    Document   : register
    Created on : 3 Dec, 2019, 8:05:13 PM
    Author     : Nandini
--%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">    
    <title>Home Property | Register</title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
    
    
    <!-- Font awesome -->
    <link href="css/font-awesome.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="css/bootstrap.css" rel="stylesheet">   
    <!-- slick slider -->
    <link rel="stylesheet" type="text/css" href="css/slick.css">
    <!-- price picker slider -->
    <link rel="stylesheet" type="text/css" href="css/nouislider.css">
    <!-- Fancybox slider -->
    <link rel="stylesheet" href="css/jquery.fancybox.css" type="text/css" media="screen" /> 
    <!-- Theme color -->
    <link id="switcher" href="css/theme-color/orange-theme.css" rel="stylesheet">     

    <!-- Main style sheet -->
    <link href="css/style-main.css" rel="stylesheet">    

   
    <!-- Google Font -->
    <link href='https://fonts.googleapis.com/css?family=Vollkorn' rel='stylesheet' type='text/css'>    
    <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  <script type="text/javascript">
            function allowpositivenumber(e) {
                var charCode = (e.which) ? e.which : event.keyCode;

                if ((charCode < 46 || charCode > 57)) {
                    return false;
                }
                return true;
            }
        </script>
        
        <script language="javascript" type="text/javascript">
            var xmlHttp
            var xmlHttp
            function check_user_duplicate(str) {
                
                if(document.getElementById("mob").value !=null && document.getElementById("mob").value.length==10){
                    
                if (typeof XMLHttpRequest != "undefined") {
                    xmlHttp = new XMLHttpRequest();
                }
                else if (window.ActiveXObject) {
                    xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                if (xmlHttp == null) {
                    alert("Browser does not support XMLHTTP Request")
                    return;
                }
                
                var url = "fetch_existing_user";
                url += "?count=" + encodeURIComponent(document.getElementById("name").value+ "nagsnn" + document.getElementById("mob").value);
                xmlHttp.onreadystatechange = stateChange3;
                xmlHttp.open("GET", url, true);
                xmlHttp.send(null);
            }
            }

            function stateChange3() {
                if (xmlHttp.readyState == 4 || xmlHttp.readyState == "complete") {
            //alert(xmlHttp.responseText);        
            if(xmlHttp.responseText.trim().split("nagsnn")[0]=="dup"){
                document.getElementById("user_dup").innerHTML = "User Already Registered with this Mobile No";
                document.getElementById("mob").value = "";
                document.getElementById("mob").focus();
            }else{
                document.getElementById("user_dup").innerHTML = "";
                document.getElementById("aa_otp").hidden = false;
                document.getElementById("otp").focus();
                document.getElementById("otp").value = xmlHttp.responseText.trim().split("nagsnn")[1];
            }
                }
            }
        </script>
        
        <script language="javascript" type="text/javascript">
            var xmlHttp
            var xmlHttp
            function check_otp(e) {
                
//                 var charCode = (e.which) ? e.which : event.keyCode;
//
//                if ((charCode < 46 || charCode > 57)) {
//                    return false;
//                }
//                if(document.getElementById("otp").value.length==4 ){
//                    return false;
//                }
                if (typeof XMLHttpRequest != "undefined") {
                    xmlHttp = new XMLHttpRequest();
                }
                else if (window.ActiveXObject) {
                    xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                if (xmlHttp == null) {
                    alert("Browser does not support XMLHTTP Request")
                    return;
                }
                if(document.getElementById("otp").value.length>2){
                var url = "check_otp";
                url += "?count=" + encodeURIComponent(document.getElementById("mob").value + "nagsnn" + document.getElementById("otp").value);
                xmlHttp.onreadystatechange = stateChangeotp;
                xmlHttp.open("GET", url, true);
                xmlHttp.send(null);
            }
        }

            function stateChangeotp() {
                if (xmlHttp.readyState == 4 || xmlHttp.readyState == "complete") {
                    //alert(xmlHttp.responseText);        
                    if (xmlHttp.responseText.trim() == "err") {
                        document.getElementById("otp_msg").innerHTML = "Invalid OTP";
                        document.getElementById("otp").value = "";
//                        document.getElementById("reg_reg").disabled = true;
                    } else {

//                        document.getElementById("reg_reg").disabled = false;
                        document.getElementById("otp_msg").innerHTML = "";
                    }
                }
            }
        </script>
        
        <script type="text/javascript">
            function compare_pwd(){
                if(document.getElementById("pwd").value!=document.getElementById("cpwd").value){
                    document.getElementById("msg_msg").innerHTML = "Confirm password Not Matching with Password";
                    document.getElementById("cpwd").value = "";
                    document.getElementById("cpwd").focus();
                    return false;
                }else{
                    document.getElementById("msg_msg").innerHTML = "";
                }
            }
        </script>
        
        <script type="text/javascript">
            function form_validate(){
                if(document.getElementById("name").value==""){
                    document.getElementById("a_name").hidden = false;
                    document.getElementById("name").focus();
                    return false;
                }else{
                    document.getElementById("a_name").hidden = true;
                }
                
                if(document.getElementById("mob").value=="" || document.getElementById("mob").value.length<10){
                    document.getElementById("a_mob").hidden = false;
                    document.getElementById("mob").focus();
                    return false;
                }else{
                    document.getElementById("a_mob").hidden = true;
                }
                
                  if (document.getElementById("email").value == "") {
                    document.getElementById("email").focus();
                    document.getElementById("a_email").hidden = false;
                    return false;
              
                } else {
                    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
                    var address = document.getElementById("email").value;
                    if (reg.test(address) == false) {

                        document.getElementById("a_email").hidden = false;
                        document.getElementById("email").focus();
                        return false;
                    }else{
                        document.getElementById("a_email").hidden = true;
                    }
                }
                
                if(document.getElementById("pwd").value==""){
                    document.getElementById("a_pwd").hidden = false;
                    document.getElementById("pwd").focus();
                    return false;
                }else{
                    
                    document.getElementById("a_pwd").hidden = true;
                }
                
                if(document.getElementById("cpwd").value==""){
                    document.getElementById("a_cpwd").hidden = false;
                    document.getElementById("cpwd").focus();
                    return false;
                }else{
                    document.getElementById("a_cpwd").hidden = true;
                }
                
                if(document.getElementById("pwd").value!=document.getElementById("cpwd").value){
                    document.getElementById("msg_msg").innerHTML = "Confirm password Not Matching with Password";
                    document.getElementById("cpwd").value = "";
                    document.getElementById("cpwd").focus();
                    return false;
                }else{
                    document.getElementById("msg_msg").innerHTML = "";
                }
                
                document.register.action = "register_process";
            }
        </script>
        
        <script>
            function validatePassword(password) {
                
                // Do not show anything when the length of password is zero.
                if (password.length === 0) {
                    document.getElementById("msg").innerHTML = "";
                    return;
                }
                // Create an array and push all possible values that you want in password
                var matchedCase = new Array();
                matchedCase.push("[$@$!%*#?&]"); // Special Charector
                matchedCase.push("[A-Z]");      // Uppercase Alpabates
                matchedCase.push("[0-9]");      // Numbers
                matchedCase.push("[a-z]");     // Lowercase Alphabates

                // Check the conditions
                var ctr = 0;
                for (var i = 0; i < matchedCase.length; i++) {
                    if (new RegExp(matchedCase[i]).test(password)) {
                        ctr++;
                    }
                }
                // Display it
                var color = "";
                var strength = "";
                switch (ctr) {
                    case 0:
                    case 1:
                    case 2:
                        strength = "Very Weak";
                        color = "red";
                        break;
                    case 3:
                        strength = "Medium";
                        color = "orange";
                        break;
                    case 4:
                        strength = "Strong";
                        color = "green";
                        break;
                }
                document.getElementById("msg").innerHTML = strength;
                document.getElementById("msg").style.color = color;
                
                var re = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$/;
                
                    if(re.test(document.getElementById("pwd").value)==false){
                        document.getElementById("a_pwd").hidden = false;
                    }else{
                        document.getElementById("a_pwd").hidden = true;
                    }
            }
        </script>

  </head>
  <body>   
  <section id="aa-signin">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="aa-signin-area">
            <div class="aa-signin-form">
              <div class="aa-signin-form-title">
                <a class="aa-property-home" href="home">Property Home</a>
                <h4>Create your account and Stay with us</h4>
              </div>
              <form class="contactform" name="register" method="post">                                                 
                <div class="aa-single-field">
                  <label for="name">Name <span class="required">*</span></label>
                  <input type="text"  aria-required="true" name="name" id="name" placeholder="Name">
                </div>
                  <span style="color:red" id="a_name" hidden> Please Enter Your Name</span>
                  <div class="aa-single-field">
                  <label for="mobile-no">Mobile No <span class="required">*</span></label>
                  <input type="text" name="mob" id="mob" maxlength="10" placeholder="Mobile No" onkeypress="return allowpositivenumber(event);" onblur="return check_user_duplicate();"> 
                </div>
                  <span style="color:red" id="a_mob" hidden> Please Enter Valid 10 Digit Mobile No</span>
                  <span style="color:red" id="user_dup"></span>
                  <div id="aa_otp" hidden>
                  <div class="aa-single-field">
                      <label for="mobile-no">OTP <span class="required">*</span></label>
                            <input   type="text" placeholder="OTP" name="otp" autocomplete="off" id="otp" onblur="return check_otp();" maxlength="4" onblur="return check_otp();"  onkeypress="return allowpositivenumber(event);">
                            <span style="color:red" id="a_otp" hidden> Please Enter Valid OTP</span>
                            <span id="otp_msg" style="color:red"></span>
                            
                        </div>
                  </div>
                  
                <div class="aa-single-field">
                  <label for="email">Email <span class="required">*</span></label>
                  <input type="email"  aria-required="true" name="email" id="email">
                </div>
                  <span style="color:red" id="a_email" hidden> Please Enter Valid Email Id</span>
                <div class="aa-single-field">
                  <label for="password">Password <span class="required">*</span><span id="msg"></span></label>
                  <input type="password" name="pwd" id="pwd" maxlength="10" onblur="return compare_pwd();" onkeyup="validatePassword(this.value);"> 
                </div>
                  <span style="color:red" id="a_pwd" hidden> Please Enter Password(Minimum Length 6 to Maximum 16. 1 Uppercase 1 Special Character 1 Numeric</span>
                <div class="aa-single-field">
                  <label for="confirm-password">Confirm Password <span class="required">*</span></label>
                  <input type="password" name="cpwd" id="cpwd" mexlength="10" onblur="return compare_pwd();"> 
                </div>
                  <span style="color:red" id="a_cpwd" hidden> Please Enter Confirm Password</span>
                  <span style="color:red" id="msg_msg"></span>
                                                    <br>
                  
                <div class="aa-single-submit">
                  <input type="submit" value="Create Account" name="submit" onclick="return form_validate();"> 
                  <br>
                  <a href="signin" title="Click to Sign In">Back to Sign In</a>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section> 


  <!-- jQuery library -->
  <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> -->
  <script src="js/jquery.min.js"></script>   
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="js/bootstrap.js"></script>   
  <!-- slick slider -->
  <script type="text/javascript" src="js/slick.js"></script>
  <!-- Price picker slider -->
  <script type="text/javascript" src="js/nouislider.js"></script>
   <!-- mixit slider -->
  <script type="text/javascript" src="js/jquery.mixitup.js"></script>
  <!-- Add fancyBox -->        
  <script type="text/javascript" src="js/jquery.fancybox.pack.js"></script>
  <!-- Custom js -->
  <script src="js/custom.js"></script> 
  
  </body>
</html>