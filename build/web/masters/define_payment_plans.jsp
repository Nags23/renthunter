<%-- 
    Document   : define_payment_plans
    Created on : 7 Dec, 2019, 5:30:04 PM
    Author     : Nandini
--%>



<%@page import="java.util.List"%>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Home Property | Define Payment Plans</title>

        <!-- Favicon -->
        <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">

        <!-- Font awesome -->
        <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/jquery.steps.css">
        <link href="css/font-awesome.css" rel="stylesheet">
        <!-- Bootstrap -->
        <link href="css/bootstrap.css" rel="stylesheet">
        <!-- slick slider -->
        <link rel="stylesheet" type="text/css" href="css/slick.css">
        <!-- price picker slider -->
        <link rel="stylesheet" type="text/css" href="css/nouislider.css">
        <!-- Fancybox slider -->
        <link rel="stylesheet" href="css/jquery.fancybox.css" type="text/css" media="screen" />
        <!-- Theme color -->
        <link id="switcher" href="css/theme-color/orange-theme.css" rel="stylesheet">

        <!-- Main style sheet -->
        <link href="css/style-main.css" rel="stylesheet">
        <link href="css/materialize.min.css" rel="stylesheet">

        <!-- Google Font -->
        <link href='https://fonts.googleapis.com/css?family=Vollkorn' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>


        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
          <![endif]-->



        <script type="text/javascript">
            function allowpositivenumber(e) {
                var charCode = (e.which) ? e.which : event.keyCode;

                if ((charCode < 46 || charCode > 57)) {
                    return false;
                }
                return true;
            }
        </script>

        <script type="text/javascript">
            function form_validate() {
                if (document.getElementById("pname").value == "") {
                    document.getElementById("a_pname").hidden = false;
                    document.getElementById("pname").focus();
                    return false;
                } else {
                    document.getElementById("a_pname").hidden = true;
                }
                
                if (document.getElementById("cost").value == "") {
                    document.getElementById("a_cost").hidden = false;
                    document.getElementById("cost").focus();
                    return false;
                } else {
                    document.getElementById("a_cost").hidden = true;
                }
                
                if (document.getElementById("mon").value == "") {
                    document.getElementById("a_mon").hidden = false;
                    document.getElementById("mon").focus();
                    return false;
                } else {
                    document.getElementById("a_mon").hidden = true;
                }
                
                if (document.getElementById("fea1").value == "") {
                    document.getElementById("a_fea1").hidden = false;
                    document.getElementById("fea1").focus();
                    return false;
                } else {
                    document.getElementById("a_fea1").hidden = true;
                }
                
               
              

                var nn = confirm("Are you sure to update details?");
                if(nn==true){
                document.masters.action = "define_plans_process";
            }else{
                return false;
            }
            }
        </script>
         <script type="text/javascript">
            function del_con(){
                var nn = confirm("Are you sure to delete this entry?");
                if(nn==true){
                    return true;
                }else{
                    return false;
                }
            }
        </script>
        
        <script type="text/javascript">
            function edit_det(n,id){
               
                var ful_string = n;
//                alert(ful_string.split("nagsnn")[4]);
                document.getElementById("id").value=ful_string.split("nagsnn")[0];
                document.getElementById("pname").value=ful_string.split("nagsnn")[1];
                document.getElementById("cost").value=ful_string.split("nagsnn")[2];
                document.getElementById("mon").value=ful_string.split("nagsnn")[3];
                document.getElementById("fea1").value=ful_string.split("nagsnn")[4];
                document.getElementById("fea2").value=ful_string.split("nagsnn")[5];
                document.getElementById("fea3").value=ful_string.split("nagsnn")[6];
                document.getElementById("fea4").value=ful_string.split("nagsnn")[7];
                if(ful_string.split("nagsnn")[8].trim()=="n"){
                    
                    document.getElementById("chk").checked=true;
                }else{
                   
                    document.getElementById("chk").checked=false;
                    }
                
               
                window.scroll(0,0);
            }
        </script>

    </head>

    <body>
        <!-- Pre Loader -->
        <div id="aa-preloader-area" style="display: none;">
            <div class="pulse"></div>
        </div>
        <!-- SCROLL TOP BUTTON -->
        <a class="scrollToTop" href="#"><i class="fa fa-angle-double-up"></i></a>
        <!-- END SCROLL TOP BUTTON -->


        <!-- Start header section -->
        <%@include file="../header.jsp" %>
        <!-- End header section -->
        <!-- Start menu section -->
        <div class="body-hold">

            <!-- End menu section -->


            <form name="masters" method="post>"
                  <!-- Start Proerty header  -->
                  <section class="parenthold">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 user-in">
                                <div class="row">

                                    <div class="col-md-12">
                                        <div class="tab-content card mt-0 p-25">
                                            <div id="menu0" class="tab-pane fade in active">
                                                <%
                                                    if (session.getAttribute("insert") != null) {
                                                %>


                                                <%
                                                    if (session.getAttribute("insert").equals("pass")) {
                                                %>
                                                <div class="alert alert-block alert-success col-lg-12 ">
                                                    <button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">�</span></button>
                                                    <strong> Details Updated Successfully.</strong> 


                                                </div>
                                                <%} else if (session.getAttribute("insert").equals("fail")) {%>
                                                <div class="alert alert-block alert-danger col-lg-12 ">
                                                    <button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">�</span></button>
                                                    <strong> Creation Failed Please Try Again.</strong> 
                                                </div>
                                                <%} else if (session.getAttribute("insert").equals("exists")) {%>
                                                <div class="alert alert-block alert-success col-lg-12 ">
                                                    <button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">�</span></button>
                                                    <strong>  Already Exists in Database.</strong>
                                                </div>
                                                <%} else if (session.getAttribute("insert").equals("delpass")) {%>
                                                <div class="alert alert-block alert-success col-lg-12 ">
                                                    <button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">�</span></button>
                                                    <strong>  Deleted Successfully from all Records</strong>
                                                </div>
                                                <%} else if (session.getAttribute("insert").equals("delfail")) {%>
                                                <div class="alert alert-block alert-success col-lg-12 ">
                                                    <button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">�</span></button>
                                                    <strong>Deletion Failed.</strong>
                                                </div>
                                                <%} else if (session.getAttribute("insert").equals("updatepass")) {%>
                                                <div class="alert alert-block alert-success col-lg-12 ">
                                                    <button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">�</span></button>
                                                    <strong>  Updated Successfully</strong>
                                                </div>
                                                <%} else if (session.getAttribute("insert").equals("updatefail")) {%>
                                                <div class="alert alert-block alert-success col-lg-12 ">
                                                    <button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">�</span></button>
                                                    <strong>Update Failed Pls Try Again</strong>
                                                </div>

                                                <%}%>
                                                <%
                                                        session.removeAttribute("insert");
                                                    }%>
                                                <h3>Define Payment Plans</h3>
                                                <div class="row">
                                                    <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                                        <div class="input-field">
                                                            <input type="text" name="pname" id="pname" maxlength="30"  placeholder="Enter Plan Name">
                                                            <input type="hidden" id="id" name="id">
                                                            <label for="u_name">Plan Name
                                                                <span class="required">*</span></label>
                                                        </div>
                                                        <span style="color:red" id="a_pname" hidden> Please Enter Plan Name</span>
                                                    </div>
                                                    
                                                    <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                                        <div class="input-field">
                                                            <input type="text" name="cost" id="cost" maxlength="20"  placeholder="Enter Cost" onkeypress="return allowpositivenumber(event);">
                                                            <label for="u_name">Cost
                                                                <span class="required">*</span></label>
                                                        </div>
                                                        <span style="color:red" id="a_cost" hidden> Please Enter Cost</span>
                                                    </div>
                                                    
                                                    <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                                        <div class="input-field">
                                                            <input type="text" name="mon" id="mon" maxlength="10"  placeholder="Enter Duration">
                                                            <label for="u_name">Duration(In Months)
                                                                <span class="required">*</span></label>
                                                        </div>
                                                        <span style="color:red" id="a_mon" hidden> Please Enter Duration</span>
                                                    </div>
                                                    
                                                    
                                                    
                                                    
                                                    <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                                        <div class="aa-single-field">
                                                            <label class="checkbox">Click to Enable
                    <input type="checkbox" formControlName="petsAllowed" name="chk" id="chk" value="n" checked>
                    <span class="checkmark"></span>
                  </label>                                                        
                                                        </div> 
                                                    </div> 
                                                </div>
                                                    
                                                    <div class="row">
                                                        
                                                        <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                                        <div class="input-field">
                                                            <input type="text" name="fea1" id="fea1" maxlength="150"  placeholder="Enter Feature1">
                                                            <label for="u_name">Feature1
                                                                <span class="required">*</span></label>
                                                        </div>
                                                        <span style="color:red" id="a_fea1" hidden> Please Enter Feature1</span>
                                                    </div>
                                                        
                                                        <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                                        <div class="input-field">
                                                            <input type="text" name="fea2" id="fea2" maxlength="150"  placeholder="Enter Feature2">
                                                            <label for="u_name">Feature2
                                                                </label>
                                                        </div>
                                                    </div>
                                                        
                                                        <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                                        <div class="input-field">
                                                            <input type="text" name="fea3" id="fea3" maxlength="150"  placeholder="Enter Feature3">
                                                            <label for="u_name">Feature3
                                                                <span class="required">*</span></label>
                                                        </div>
                                                    </div>
                                                        
                                                        <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                                        <div class="input-field">
                                                            <input type="text" name="fea4" id="fea4" maxlength="150"  placeholder="Enter Feature4">
                                                            <label for="u_name">Feature4
                                                                <span class="required">*</span></label>
                                                        </div>
                                                    </div>
                                                        
                                                        
                                                    
                                                        
                                                        
                                                </div> 

                                                <button class="default-btn" type="submit" onclick="return form_validate();">Update Amenities Types</button>
                                            </div>


                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                                                
                                                <section class="parenthold">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 user-in">
                                <div class="row">

                                    <div class="col-md-12">
                                        <div class="tab-content card mt-0 p-25">
                                            <div id="menu0" class="tab-pane fade in active">
                                                
                                                <h3>Payment Plan Details</h3>
                                                <div id="datagrid" style="overflow:auto; overflow-x: scroll; overflow-y:hidden; width:100%; height:130%">
                                    <table width="100%" class="display table-bordered" id="example2" cellspacing="0" border="1">
                                        <thead>
                                            <tr class="headings" style="color:#fff;background-color: #000">
                                                  <th>Slno</th>
                                                <th>Edit</th>
                                                <th>Delete</th>
                                                <th>Plane Name</th>
                                                <th>Cost</th>
                                                <th>Duration(In Months)</th>
                                                <th>Feature1</th>
                                                <th>Feature2</th>
                                                <th>Feature3</th>
                                                <th>Feature4</th>
                                                <th>Enable/Disable</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                             <%

                            if (session.getAttribute("dd_id") != null) {
                                List<String> dd_id = (List<String>) session.getAttribute("dd_id");
                                List<String> dd_pname = (List<String>) session.getAttribute("dd_pname");
                                List<String> dd_cost = (List<String>) session.getAttribute("dd_cost");
                                List<String> dd_mon = (List<String>) session.getAttribute("dd_mon");
                                List<String> dd_fea1 = (List<String>) session.getAttribute("dd_fea1");
                                List<String> dd_fea2 = (List<String>) session.getAttribute("dd_fea2");
                                List<String> dd_fea3 = (List<String>) session.getAttribute("dd_fea3");
                                List<String> dd_fea4 = (List<String>) session.getAttribute("dd_fea4");
                                List<String> dd_dis = (List<String>) session.getAttribute("dd_dis");
                        %>
                        <%
                             for (int i = 0; i < dd_id.size(); i++) {
                        %>
                                            <tr>
                                                <td><%=i+1%></td>
                                                <td>
                                                   
                 <label class="radiobtn mr-4 mb-0">
                    <input type="radio" name="parking" formControlName="parking"  id="<%=dd_id.get(i)%>" value="<%=dd_id.get(i)%>nagsnn<%=dd_pname.get(i)%>nagsnn<%=dd_cost.get(i)%>nagsnn<%=dd_mon.get(i)%>nagsnn<%=dd_fea1.get(i)%>nagsnn<%=dd_fea2.get(i)%>nagsnn<%=dd_fea3.get(i)%>nagsnn<%=dd_fea4.get(i)%>nagsnn<%=dd_dis.get(i)%>" onclick="return edit_det(this.value,this.id);">Edit
                    <span class="checkmark"></span>
                  </label>
                                                </td>
                                                <td><a href="plan_del?id=<%=dd_id.get(i)%>" title="Click to Delete" onclick="return del_con();"><i class="fa fa-trash" style="color:red"></i></a></td>
                                                
                                                
                                                <td><%=dd_pname.get(i)%></td>
                                                <td><%=dd_cost.get(i)%></td>
                                                <td><%=dd_mon.get(i)%></td>
                                                <td><%=dd_fea1.get(i)%></td>
                                                <td><%=dd_fea2.get(i)%></td>
                                                <td><%=dd_fea3.get(i)%></td>
                                                <td><%=dd_fea4.get(i)%></td>
                                                <td>
                                                    <%
                                                        if(dd_dis.get(i).equalsIgnoreCase("n")){
                                                    %>
                                                    <p style="color:green">Enabled</p>
                                                    <%}else{%>
                                                    <p style="color:red">Disabled</p>
                                                    <%}%>
                                                </td>
                                                 
                                               
                                            </tr>
                                            <%}
                             session.removeAttribute("dd_id");
                             session.removeAttribute("dd_username");
                             session.removeAttribute("dd_pwd");
                             session.removeAttribute("dd_firstname");
                             session.removeAttribute("dd_email");
                             session.removeAttribute("dd_mob");
                             session.removeAttribute("dd_type");
                             session.removeAttribute("dd_dis");
                            
                            }
                            %>
                                        </tbody>

                                    </table>
                                </div>

                                            </div>


                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </form>
            <!-- End Proerty header  -->

        </div>
        <!-- Footer -->
        <%@include file="../footer.jsp" %>
        <!-- / Footer -->

        <!-- jQuery library -->
        <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> -->
        <script src="js/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="js/bootstrap.js"></script>
        <!-- slick slider -->
        <script type="text/javascript" src="js/slick.js"></script>
        <!-- Price picker slider -->
        <script type="text/javascript" src="js/nouislider.js"></script>
        <!-- mixit slider -->
        <script type="text/javascript" src="js/jquery.mixitup.js"></script>
        <!-- Add fancyBox -->
        <script type="text/javascript" src="js/jquery.fancybox.pack.js"></script>
        <script src="js/materialize.min.js"></script>
        <!-- Custom js -->
        <script src="js/custom.js"></script>
        <script>
            $(document).ready(function () {
                $('.body-hold').css('min-height', $(window).height() - 208);
                // Comma, not colon ----^
            });
            $(window).resize(function () {
                $('.body-hold').css('min-height', $(window).height() - 208);
                // Comma, not colon ----^
            });

            $("#example-basic").steps({
                headerTag: "h3",
                bodyTag: "section",
                transitionEffect: "slideLeft",
                autoFocus: true
            });

        </script>
        
        
<link rel="stylesheet" type="text/css" href="media/css/demo_page.css" />
<link rel="stylesheet" type="text/css" href="media/css/demo_table_jui.css" />
<link rel="stylesheet" type="text/css" href="examples/examples_support/themes/smoothness/jquery-ui-1.8.4.custom.css" />
<link rel="stylesheet" type="text/css" href="media/css/TableTools_JUI.css" />



        <%--<script type="text/javascript" charset="utf-8" src="media/js/jquery.js"></script>--%>
        <script type="text/javascript" charset="utf-8" src="media/js/jquery.dataTables.js"></script>
        <script type="text/javascript" charset="utf-8" src="media/js/ZeroClipboard.js"></script>
        <script type="text/javascript" charset="utf-8" src="media/js/TableTools.js"></script>
        <script type="text/javascript" charset="utf-8">
                $(document).ready( function () {
                        $('#example2').dataTable( {
                                "bJQueryUI": true,
                                "sPaginationType": "full_numbers",
                                 "sDom": 'T<"clear"><"H"lfr>t<"F"ip>',
//                                 "iDisplayLength": -1,
                                "oTableTools": {
                                        "aButtons": [
                                            // "xls"
                                                //"copy", "csv", "xls", "pdf","print"//,
                                                //{
                                                //	"sExtends":    "collection",
                                                //	"sButtonText": "Save",
                                                //	"aButtons":    [ "csv", "xls", "pdf" ]
                                                //}
                                        ]
                                }
                        } );
                        
                        
                        
                } );
        </script>
        
        <script>
       

        $(document).ready(function () {
            $('select').formSelect();
        });

    </script>
    </body>

</html>