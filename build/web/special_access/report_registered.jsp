<%-- 
    Document   : report_registered
    Created on : 12 Jul, 2020, 4:00:03 PM
    Author     : Nandini
--%>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Home Property | Registered Property Report</title>

  <!-- Favicon -->
  <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">

  <!-- Font awesome -->
  <link href="css/font-awesome.css" rel="stylesheet">
  <!-- Bootstrap -->
  <link href="css/bootstrap.css" rel="stylesheet">
  <!-- slick slider -->
  <link rel="stylesheet" type="text/css" href="css/slick.css">
  <!-- price picker slider -->
  <link rel="stylesheet" type="text/css" href="css/nouislider.css">
  <!-- Fancybox slider -->
  <link rel="stylesheet" href="css/jquery.fancybox.css" type="text/css" media="screen" />
  <!-- Theme color -->
  <link id="switcher" href="css/theme-color/orange-theme.css" rel="stylesheet">

  <!-- Main style sheet -->
  <link href="css/style-main.css" rel="stylesheet">
  <link href="css/materialize.min.css" rel="stylesheet">

  <!-- Google Font -->
  <link href='https://fonts.googleapis.com/css?family=Vollkorn' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>


  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->


</head>

<body>
  <!-- Pre Loader -->
  <div id="aa-preloader-area" style="display: none;">
    <div class="pulse"></div>
  </div>
  <!-- SCROLL TOP BUTTON -->
  <a class="scrollToTop" href="#"><i class="fa fa-angle-double-up"></i></a>
  <!-- END SCROLL TOP BUTTON -->


  <!-- Start header section -->
  <%@include file="../header.jsp" %>
  <!-- End header section -->
  <!-- Start menu section -->
  <div class="body-hold">
    
    <!-- End menu section -->

    <!-- Start Proerty header  -->
    <%
        String ttl_free_users = "",ttl_broker_posts = "",ttl_owner_posts = "",ttl_builder_posts = "";
        
        if(session.getAttribute("ttl_free_users")!=null){
            ttl_free_users = session.getAttribute("ttl_free_users").toString();
            session.removeAttribute("ttl_free_users");
        }
        if(session.getAttribute("ttl_broker_posts")!=null){
            ttl_broker_posts = session.getAttribute("ttl_broker_posts").toString();
            session.removeAttribute("ttl_broker_posts");
        }
        if(session.getAttribute("ttl_owner_posts")!=null){
            ttl_owner_posts = session.getAttribute("ttl_owner_posts").toString();
            session.removeAttribute("ttl_owner_posts");
        }
        if(session.getAttribute("ttl_builder_posts")!=null){
            ttl_builder_posts = session.getAttribute("ttl_builder_posts").toString();
            session.removeAttribute("ttl_builder_posts");
        }
    %>
    <section class="parenthold">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <table class="common-table" cellspacing="0" cellpadding="0" border="0">
              <thead>
                <tr>
                  <th>Report on Registered Property</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>
                    Number of free users
                  </td>
                  <td><a href="free_users_details?dtype=Free" title="Click to View Details"><%=ttl_free_users%></a></td>
                </tr>
                <tr>
                  <td>
                    Number of paid users
                  </td>
                  <td>-</td>
                </tr>
                <tr>
                  <td>
                   Number property posted by brokers
                  </td>
                  <td><a href="report_registered_details?dtype=Broker" title="Click to View Details"><%=ttl_broker_posts%></a></td>
                </tr>
                <tr>
                  <td> Number of property posted by owners
                  </td>
                  <td><a href="report_registered_details?dtype=Owner" title="Click to View Details"><%=ttl_owner_posts%></a></td>
                </tr>
                <tr>
                  <td> Number of property posted by builders
                  </td>
                  <td><a href="report_registered_details?dtype=Builder" title="Click to View Details"><%=ttl_builder_posts%></a></td>
                </tr>
                
              </tbody>
            </table>
          </div>
        </div>
        
        
      </div>
    </section>

    <!-- End Proerty header  -->

  </div>
  <!-- Footer -->
  <%@include file="../footer.jsp" %>
  <!-- / Footer -->

  <!-- jQuery library -->
  <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> -->
  <script src="js/jquery.min.js"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="js/bootstrap.js"></script>
  <!-- slick slider -->
  <script type="text/javascript" src="js/slick.js"></script>
  <!-- Price picker slider -->
  <script type="text/javascript" src="js/nouislider.js"></script>
  <!-- mixit slider -->
  <script type="text/javascript" src="js/jquery.mixitup.js"></script>
  <!-- Add fancyBox -->
  <script type="text/javascript" src="js/jquery.fancybox.pack.js"></script>
  <script src="js/materialize.min.js"></script>
  <!-- Custom js -->
  <script src="js/custom.js"></script>
  <script>
    $(document).ready(function () {
      $('.body-hold').css('min-height', $(window).height() - 208);
      // Comma, not colon ----^
    });
    $(window).resize(function () {
      $('.body-hold').css('min-height', $(window).height() - 208);
      // Comma, not colon ----^
    });

    $("#example-basic").steps({
      headerTag: "h3",
      bodyTag: "section",
      transitionEffect: "slideLeft",
      autoFocus: true
    });

  </script>
</body>

</html>