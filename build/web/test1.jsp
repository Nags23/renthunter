<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Home Property | 404</title>

  <!-- Favicon -->
  <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">

  <!-- Font awesome -->
  <link href="css/font-awesome.css" rel="stylesheet">
  <!-- Bootstrap -->
  <link href="css/bootstrap.css" rel="stylesheet">
  <!-- slick slider -->
  <link rel="stylesheet" type="text/css" href="css/slick.css">
  <!-- price picker slider -->
  <link rel="stylesheet" type="text/css" href="css/nouislider.css">
  <!-- Fancybox slider -->
  <link rel="stylesheet" href="css/jquery.fancybox.css" type="text/css" media="screen" />
  <!-- Theme color -->
  <link id="switcher" href="css/theme-color/orange-theme.css" rel="stylesheet">

  <!-- Main style sheet -->
  <link href="css/style-main.css" rel="stylesheet">
  <link href="css/materialize.min.css" rel="stylesheet">

  <!-- Google Font -->
  <link href='https://fonts.googleapis.com/css?family=Vollkorn' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>


  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->


</head>

<body>
  <!-- Pre Loader -->
  <div id="aa-preloader-area">
    <div class="pulse"></div>
  </div>
  <!-- SCROLL TOP BUTTON -->
  <a class="scrollToTop" href="#"><i class="fa fa-angle-double-up"></i></a>
  <!-- END SCROLL TOP BUTTON -->


  <!-- Start header section -->
  <header id="aa-header">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="aa-header-area">
            <div class="row">
              <div class="col-md-6 col-sm-6 col-xs-6">
                <div class="aa-header-left">
                  <div class="aa-telephone-no">
                    <span class="fa fa-phone"></span>
                    76100 65100
                  </div>
                  <div class="aa-email hidden-xs">
                    <abbr class="" href="mailTo:contactus@renthunter.in">
                      <span class="fa fa-envelope-o"></span> contactus@renthunter.in
                      </a>
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-sm-6 col-xs-6">
                <div class="aa-header-right">
                  <a href="register.html" class="aa-register">Register</a>
                  <a href="signin.html" class="aa-login">Login</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </header>
  <!-- End header section -->
  <!-- Start menu section -->
  <div class="body-hold">
    <section id="aa-menu-area">
      <nav class="navbar navbar-default main-navbar" role="navigation">
        <div class="container">
          <div class="navbar-header">
            <!-- FOR MOBILE VIEW COLLAPSED BUTTON -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
              aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <!-- LOGO -->
            <!-- Text based logo -->
            <a class="navbar-brand aa-logo" href="index.html"> Rent<span>Hunter.in</span></a>
            <!-- Image based logo -->
            <!-- <a class="navbar-brand aa-logo-img" href="index.html"><img src="img/logo.png" alt="logo"></a> -->
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul id="top-menu" class="nav navbar-nav navbar-right aa-main-nav">
              <li class="active"><a href="index.html">HOME</a></li>
              <li><a href="index.html">PRICING PLAN</a></li>
              <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="properties.html">POST PROPERTIES <span
                    class="caret"></span></a>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="properties.html">Home</a></li>
                  <li><a href="properties-detail.html">Paying Guests</a></li>
                  <li><a href="properties-detail.html">Appartment</a></li>
                  <li><a href="properties.html">Office Space</a></li>
                  <li><a href="properties-detail.html">Shops</a></li>
                  <li><a href="properties-detail.html">Warehouse / Godown </a></li>
                  <li><a href="properties-detail.html">Party Hall </a></li>
                </ul>
              </li>
              <li><a href="contact.html">CONTACT</a></li>
              <li><a href="404.html">ABOUT US</a></li>
              <li><a href="gallery.html">GALLERY</a></li>
              <!-- <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="blog-archive.html">BLOG <span class="caret"></span></a>
                <ul class="dropdown-menu" role="menu">                
                  <li><a href="blog-archive.html">BLOG</a></li>
                  <li><a href="blog-single.html">BLOG DETAILS</a></li>                                            
                </ul>
              </li> -->


            </ul>
          </div>
          <!--/.nav-collapse -->
        </div>
      </nav>
    </section>
    <!-- End menu section -->

    <!-- Start Proerty header  -->
    <section class="parenthold">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-3">
                <div class="search-filter smooth-scroll">
                  <ul class="nav nav-tabs">
                    <li>
                      <div class="clearfix">
                        <div class="pull-left">
                          <label class="lbl-main">Basic Filter</label>
                        </div>
                        <div class="pull-right">
                          <a class="clear-text" routerlink="#" ng-reflect-router-link="#" href="/search-page/%23"><i
                              class="fa fa-undo"></i> Clear all</a>
                        </div>
                      </div>
                    </li>
                  </ul>
                  <div>
                    <form novalidate="" class=" ">
                      <div class="filterhold">
                        <div class="basic-filter" id="filter-home">
						
						
						<label class="lbl-list"></label>                     
                          <label class="mr-15">
						  <input name="tenants" type="checkbox" class="filled-in">
                          <span >Show Only With Photos</span>
						  </label><label class="mr-15">

                          <label class="lbl-list"> Tenant type</label>                     
                          <label class="mr-15">                            
                            <input name="tenants" type="checkbox" class="filled-in">
                            <span >Bachelors</span>
                          </label><label class="mr-15">
                            
                            <input name="tenants" type="checkbox" class="filled-in">
                            <span >Family</span>
                          </label><label class="mr-15">
                            
                            <input name="tenants" type="checkbox" class="filled-in">
                            <span >Any</span>
                          </label>

                          <div class="clear"></div>
						  
						  <label class="lbl-list">Property You Are Looking</label>
                          <label class="mr-15">                            
                            <input name="bhkLength" type="checkbox" class="filled-in">
                            <span >Independent Villa</span>
							</label><label class="mr-15">
							<input name="bhkLength" type="checkbox" class="filled-in">
                            <span >Apartment</span>
							</label><label class="mr-15">
							<input name="bhkLength" type="checkbox" class="filled-in">
                            <span >Gated Community</span>
							</label><label class="mr-15">
						  

                          <label class="lbl-list">Select BHK</label>
                          <label class="mr-15">                            
                            <input name="bhkLength" type="checkbox" class="filled-in">
                            <span >1 RK</span>
                          </label><label class="mr-15">                           
                            <input name="bhkLength" type="checkbox" class="filled-in">
                            <span >1 BHK</span>
                          </label><label class="mr-15">                          
                            <input name="bhkLength" type="checkbox"class="filled-in">
                            <span >2 BHK</span>
                          </label><label class="mr-15">                            
                            <input name="bhkLength" type="checkbox"  class="filled-in">
                            <span >3 BHK</span>
                          </label><label class="mr-15">                           
                            <input name="bhkLength" type="checkbox" class="filled-in">
                            <span >4 BHK</span>
                          </label><label class="mr-15">                            
                            <input name="bhkLength" type="checkbox"  class="filled-in">
                            <span >4+ BHK</span>
                          </label>
                          <label class="lbl-list">Furnishing</label>
                          <label class="mr-15">
                           
                            <input name="furnished" type="checkbox" class="filled-in">
                            <span > Unfurnished</span>
                          </label><label class="mr-15">
                            
                            <input name="furnished" type="checkbox"   class="filled-in">
                            <span >Semi Furnished</span>
                          </label><label class="mr-15">
                            
                            <input name="furnished" type="checkbox"   class="filled-in">
                            <span >Fully Furnished</span>
                          </label>

                        </div>
                        <div class="advancebtn" id="headingOne1" role="tab">
                          <a aria-controls="collapseOne1" aria-expanded="true" class="advance-filter"
                            data-parent="#accordionEx1" data-toggle="collapse" href="#collapseOne1"
                            target="filter-home">
                            Advanced Filters <i aria-hidden="true" class="fa fa-angle-double-up pull-right"></i>
                          </a>
                        </div>

                        <div class="advance-filter-hold" id="filter-home-advance-panel">

                          <div aria-multiselectable="true" class="accordion md-accordion" id="accordionEx1"
                            role="tablist">
                            <div aria-labelledby="headingOne1" class="collapse" data-parent="#accordionEx1"
                              id="collapseOne1" role="tabpanel">
                              <div class="pb-3">
                                <label class="lbl-list">Floor</label>
                                <label class="mr-15">
                                  
                                  <input name="floors" type="checkbox"  class="filled-in">
                                  <span >Ground</span>
                                </label><label class="mr-15">
                                  
                                  <input name="floors" type="checkbox"   class="filled-in">
                                  <span >First</span>
                                </label><label class="mr-15">
                                  
                                  <input name="floors" type="checkbox"   class="filled-in">
                                  <span >Second</span>
                                </label><label class="mr-15">
                                  
                                  <input name="floors" type="checkbox"  class="filled-in">
                                  <span >Third</span>
                                </label><label class="mr-15">
                                  
                                  <input name="floors" type="checkbox"   class="filled-in">
                                  <span >Fourth</span>
                                </label><label class="mr-15">
                                  
                                  <input name="floors" type="checkbox"    class="filled-in">
                                  <span >Fifth</span>
                                </label><label class="mr-15">
                                  
                                  <input name="floors" type="checkbox"   class="filled-in">
                                  <span >Any</span>
                                </label>

                                <div class="clear"></div>

                                <label class="lbl-list">Amenties</label>

                                <label class="mr-15">
                                  
                                  <input name="amenities" type="checkbox"  class="filled-in">
                                  <span >TV</span>
                                </label><label class="mr-15">
                                  
                                  <input name="amenities" type="checkbox"  class="filled-in">
                                  <span >FAN</span>
                                </label><label class="mr-15">
                                  
                                  <input name="amenities" type="checkbox"  class="filled-in">
                                  <span >Gyser</span>
                                </label><label class="mr-15">
                                  
                                  <input name="amenities" type="checkbox"  class="filled-in">
                                  <span >Power Backup</span>
                                </label>

                                <div class="clear"></div>

                                <label class="lbl-list checkbox">
                                  <input name="securityGuard" type="checkbox"  class="filled-in">
                                  <span >Wardrobe</span>
                                </label>

                                <div class="clear"></div>

                                <label class="lbl-list">Parking</label>
                                <label class="mr-15">
                                  
                                  <input name="parking" type="checkbox"  class="filled-in">
                                  <span >Bike</span>
                                </label><label class="mr-15">
                                  
                                  <input name="parking" type="checkbox"  class="filled-in">
                                  <span >Car</span>
                                </label>
                                <div class="clear"></div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
          </div>
          <div class="col-md-7 search-list">
            <div class="card bar-left-success">
              <div class="ribbon-hold green darken-1">
                <span>Ready to move</span>
              </div>
              <div class="property">
                <div class="card-body d-flex ">
                  <div class="prop-img">
                    <img src="img/item/5.jpg" alt="img">
                  </div>
                  <div class="prop-details">
                    <div class="list-card-title">
                      <span class="header">
                          <a href="#" target="_blank"> 
                            3 BHK In Vidyaranyapura                             
                            <small><i class="fa fa-external-link">  </i></small></a>
                      </span>
                      <small class="block">
                        <i class="fa fa-map-marker"></i>Independent House, Ams Layout Near Nativity Church
                      </small>
                    </div>
                    <div class="list-detail my-2">
                      <div class="block-1">
                        <small>Deposite</small>
                        <span class="d-block"> 250,000</span>
                      </div>
                      <div class="block-1">
                        <small>Rent</small>
                        <span class="d-block"> 15,000</span>
                      </div>
                      <div class="block-1">
                          <small>Plot area</small>
                          <span class="d-block">1200 Sq.Ft</span>
                        </div>
                      <div class="block-1">
                        <small>Furnishing</small>
                        <span class="d-block">Semi  furnished</span>
                      </div>
                    </div>
                    <div class="list-action mt-15 text-right">
                      <button type="button" class="btn btn-common mb-0 ml-0 pull-left">Contact owner</button>
                      <a href="#" class="shortlist"> <i class="fa fa-heart-o"></i></a>
                      <!-- <a href="#" class="shortlist"> <i class="fa fa-heart"></i></a> -->
                      <a href="#" class="shortlist"> <i class="fa fa-share-alt"></i></a>
                      <small class="btn-text">1 Month ago</small>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="card bar-left-success">
              <div class="ribbon-hold green darken-1">
                <span>Ready to move</span>
              </div>
              <div class="property">
                <div class="card-body d-flex ">
                  <div class="prop-img">
                    <img src="img/item/5.jpg" alt="img">
                  </div>
                  <div class="prop-details">
                    <div class="list-card-title">
                      <span class="header">
                          <a href="#" target="_blank"> 
                            3 BHK In Vidyaranyapura                             
                            <small><i class="fa fa-external-link">  </i></small></a>
                      </span>
                      <small class="block">
                        <i class="fa fa-map-marker"></i>Independent House, Ams Layout Near Nativity Church
                      </small>
                    </div>
                    <div class="list-detail my-2">
                      <div class="block-1">
                        <small>Deposite</small>
                        <span class="d-block"> 250,000</span>
                      </div>
                      <div class="block-1">
                        <small>Rent</small>
                        <span class="d-block"> 15,000</span>
                      </div>
                      <div class="block-1">
                          <small>Plot area</small>
                          <span class="d-block">1200 Sq.Ft</span>
                        </div>
                      <div class="block-1">
                        <small>Furnishing</small>
                        <span class="d-block">Semi  furnished</span>
                      </div>
                    </div>
                    <div class="list-action mt-15 text-right">
                      <button type="button" class="btn btn-common mb-0 ml-0 pull-left">Contact owner</button>
                      <a href="#" class="shortlist"> <i class="fa fa-heart-o"></i></a>
                      <!-- <a href="#" class="shortlist"> <i class="fa fa-heart"></i></a> -->
                      <a href="#" class="shortlist"> <i class="fa fa-share-alt"></i></a>
                      <small class="btn-text">1 Month ago</small>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-2">

          </div>
        </div>
      </div>
    </section>
    <!-- End Proerty header  -->

  </div>
  <!-- Footer -->
  <footer id="aa-footer">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="aa-footer-area">
            <div class="row">
              <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="aa-footer-left">
                  <p>Designed by <a rel="nofollow" href="http://www.markups.io/">MarkUps.io</a></p>
                </div>
              </div>
              <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="aa-footer-middle">
                  <a href="#"><i class="fa fa-facebook"></i></a>
                  <a href="#"><i class="fa fa-twitter"></i></a>
                  <a href="#"><i class="fa fa-google-plus"></i></a>
                  <a href="#"><i class="fa fa-youtube"></i></a>
                </div>
              </div>
              <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="aa-footer-right">
                  <a href="#">Home</a>
                  <a href="#">Support</a>
                  <a href="#">License</a>
                  <a href="#">FAQ</a>
                  <a href="#">Privacy & Term</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </footer>
  <!-- / Footer -->

  <!-- jQuery library -->
  <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> -->
  <script src="js/jquery.min.js"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="js/bootstrap.js"></script>
  <!-- slick slider -->
  <script type="text/javascript" src="js/slick.js"></script>
  <!-- Price picker slider -->
  <script type="text/javascript" src="js/nouislider.js"></script>
  <!-- mixit slider -->
  <script type="text/javascript" src="js/jquery.mixitup.js"></script>
  <!-- Add fancyBox -->
  <script type="text/javascript" src="js/jquery.fancybox.pack.js"></script>
  <script src="js/materialize.min.js"></script>
  <!-- Custom js -->
  <script src="js/custom.js"></script>
  <script>
    $(document).ready(function () {
      $('.body-hold').css('min-height', $(window).height() - 198);
      // Comma, not colon ----^
    });
    $(window).resize(function () {
      $('.body-hold').css('min-height', $(window).height() - 198);
      // Comma, not colon ----^
    });

    $("#example-basic").steps({
      headerTag: "h3",
      bodyTag: "section",
      transitionEffect: "slideLeft",
      autoFocus: true
    });

  </script>
</body>

</html>