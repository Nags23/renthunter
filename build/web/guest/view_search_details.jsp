<%-- 
    Document   : view_search_details
    Created on : 19 Jan, 2020, 8:37:53 AM
    Author     : Nandini
--%>
<%@page import="java.util.List"%>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Home Property | Properties Details</title>

  <!-- Favicon -->
  <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">


  <!-- Font awesome -->
  <link href="css/font-awesome.css" rel="stylesheet">
  <!-- Bootstrap -->
  <link href="css/bootstrap.css" rel="stylesheet">
  <!-- slick slider -->
  <link rel="stylesheet" type="text/css" href="css/slick.css">
  <!-- price picker slider -->
  <link rel="stylesheet" type="text/css" href="css/nouislider.css">
  <!-- Fancybox slider -->
  <link rel="stylesheet" href="css/jquery.fancybox.css" type="text/css" media="screen" />
  <!-- Theme color -->
  <link id="switcher" href="css/theme-color/orange-theme.css" rel="stylesheet">

  <!-- Main style sheet -->
  <link href="css/style-main.css" rel="stylesheet">
  <link id="switcher" href="../../css/theme-color/orange-theme.css" rel="stylesheet">
  <link href="css/materialize.min.css" rel="stylesheet">
  <script src="js/materialize.min.js"></script>
  <!-- Google Font -->
  <link href='https://fonts.googleapis.com/css?family=Vollkorn' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>


  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->


</head>

<body class="aa-price-range">
  <!-- Pre Loader -->
  <div id="aa-preloader-area">
    <div class="pulse"></div>
  </div>
  <!-- SCROLL TOP BUTTON -->
  <a class="scrollToTop" href="#"><i class="fa fa-angle-double-up"></i></a>
  <!-- END SCROLL TOP BUTTON -->


  <!-- Start header section -->
  <%@include file="../header.jsp" %>
  <!-- End header section -->
  <!-- Start menu section -->
  
  <!-- End menu section -->
  <!-- Start Properties  -->
  <%
      try{
  %>
  
  
  <%
      String ss_rent = "",ss_deposit = "",ss_nego = "";
      
      if(session.getAttribute("ss_rent")!=null){
          ss_rent = session.getAttribute("ss_rent").toString();
          session.removeAttribute("ss_rent");
      }
      
      if(session.getAttribute("ss_deposit")!=null){
          ss_deposit = session.getAttribute("ss_deposit").toString();
          session.removeAttribute("ss_deposit");
      }
      
      if(session.getAttribute("ss_nego")!=null){
          ss_nego = session.getAttribute("ss_nego").toString();
          session.removeAttribute("ss_nego");
      }
      String ss_ttype = "",ss_fur = "";
      if(session.getAttribute("ss_ttype")!=null){
          ss_ttype = session.getAttribute("ss_ttype").toString();
          session.removeAttribute("ss_ttype");
      }
      if(session.getAttribute("ss_fur")!=null){
          ss_fur = session.getAttribute("ss_fur").toString();
          session.removeAttribute("ss_fur");
      }
      String ss_floor = "",ss_facings = "",ss_parking = "";
      if(session.getAttribute("ss_floor")!=null){
          ss_floor = session.getAttribute("ss_floor").toString();
          session.removeAttribute("ss_floor");
      }
      if(session.getAttribute("ss_facings")!=null){
          ss_facings = session.getAttribute("ss_facings").toString();
          session.removeAttribute("ss_facings");
      }
      if(session.getAttribute("ss_parking")!=null){
          ss_parking = session.getAttribute("ss_parking").toString();
          session.removeAttribute("ss_parking");
      }
      String ss_address_pro = "";
      if(session.getAttribute("ss_address_pro")!=null){
          ss_address_pro = session.getAttribute("ss_address_pro").toString();
          session.removeAttribute("ss_address_pro");
      }
  %>
  
  <%
      String ss_img1 = "",ss_img2 = "",ss_img3 = "",ss_img4 = "",ss_img5 = "",ss_img6 = "";
      String img1 = "",img2 = "",img3 = "",img4 = "",img5 = "",img6 = "";
      if(session.getAttribute("ss_img1")!=null){
          ss_img1 = session.getAttribute("ss_img1").toString();
          session.removeAttribute("ss_img1");
          if(ss_img1!=null && ss_img1.length()>0){
              img1  = "form_pics"+"/"+ss_img1;
          }else{
              img1 = "img/no-image.png";
          }
      }
      
      if(session.getAttribute("ss_img2")!=null){
          ss_img2 = session.getAttribute("ss_img2").toString();
          session.removeAttribute("ss_img2");
          if(ss_img2!=null && ss_img2.length()>0){
              img2  = "form_pics"+"/"+ss_img2;
          }else{
              img2 = "img/no-image.png";
          }
      }
      
      if(session.getAttribute("ss_img3")!=null){
          ss_img3 = session.getAttribute("ss_img3").toString();
          session.removeAttribute("ss_img3");
          if(ss_img3!=null && ss_img3.length()>0){
              img3  = "form_pics"+"/"+ss_img3;
          }else{
              img3 = "img/no-image.png";
          }
      }
      
      if(session.getAttribute("ss_img4")!=null){
          ss_img4 = session.getAttribute("ss_img4").toString();
          session.removeAttribute("ss_img4");
          if(ss_img4!=null && ss_img4.length()>0){
              img4  = "form_pics"+"/"+ss_img4;
          }else{
              img4 = "img/no-image.png";
          }
      }
      
      if(session.getAttribute("ss_img5")!=null){
          ss_img5 = session.getAttribute("ss_img5").toString();
          session.removeAttribute("ss_img5");
          if(ss_img5!=null && ss_img5.length()>0){
              img5  = "form_pics"+"/"+ss_img5;
          }else{
              img5 = "img/no-image.png";
          }
      }
      
      if(session.getAttribute("ss_img6")!=null){
          ss_img6 = session.getAttribute("ss_img6").toString();
          session.removeAttribute("ss_img6");
          if(ss_img6!=null && ss_img6.length()>0){
              img6  = "form_pics"+"/"+ss_img6;
          }else{
              img6 = "img/no-image.png";
          }
      }
      String dd_stype = "";
      if(session.getAttribute("dd_stype")!=null){
          dd_stype = session.getAttribute("dd_stype").toString();
          session.removeAttribute("dd_stype");
      }
      
      String ss_food = "",ss_food1 = "",ss_share = "",ss_share1 = "";
      
      if(session.getAttribute("ss_food")!=null){
          ss_food = session.getAttribute("ss_food").toString();
          session.removeAttribute("ss_food");
      }
      if(session.getAttribute("ss_food1")!=null){
          ss_food1 = session.getAttribute("ss_food1").toString();
          session.removeAttribute("ss_food1");
      }
      
    if(session.getAttribute("ss_share")!=null){
          ss_share = session.getAttribute("ss_share").toString();
          session.removeAttribute("ss_share");
      }
      if(session.getAttribute("ss_share1")!=null){
          ss_share1 = session.getAttribute("ss_share1").toString();
          session.removeAttribute("ss_share1");
      }
      
    String ss_occu = "",ss_rtype = "";

    if(session.getAttribute("ss_occu")!=null){
        ss_occu = session.getAttribute("ss_occu").toString();
        session.removeAttribute("ss_occu");
    }
    if(session.getAttribute("ss_rtype")!=null){
        ss_rtype = session.getAttribute("ss_rtype").toString();
        session.removeAttribute("ss_rtype");
    }
  %>
  <input type="hidden" name="addr" id="addr" value="<%=ss_address_pro%>">
  <section id="aa-properties">
    <div class="container ">
      <div class="row">
        <div class="col-md-6">
          <div class="aa-properties-content">
            <!-- Start properties content body -->
            <div class="aa-properties-details">
              <div class="aa-properties-details-img">
                <img src="<%=img1%>" alt="img">
                <img src="<%=img2%>" alt="img">
                <img src="<%=img3%>" alt="img">
                <img src="<%=img4%>" alt="img">
                <img src="<%=img5%>" alt="img">
                <img src="<%=img6%>" alt="img">
              </div>
              <%
                  String ss_name_num = "",ss_street = "",ss_lmark = "",ss_city = "",ss_state = "",ss_age_pro = "";
                  if(session.getAttribute("ss_name_num")!=null){
                      ss_name_num = session.getAttribute("ss_name_num").toString();
                      session.removeAttribute("ss_name_num");
                  }
                  if(session.getAttribute("ss_street")!=null){
                      ss_street = session.getAttribute("ss_street").toString();
                      session.removeAttribute("ss_street");
                  }
                  if(session.getAttribute("ss_lmark")!=null){
                      ss_lmark = session.getAttribute("ss_lmark").toString();
                      session.removeAttribute("ss_lmark");
                  }
                  if(session.getAttribute("ss_city")!=null){
                      ss_city = session.getAttribute("ss_city").toString();
                      session.removeAttribute("ss_city");
                  }
                  if(session.getAttribute("ss_state")!=null){
                      ss_state = session.getAttribute("ss_state").toString();
                      session.removeAttribute("ss_state");
                  }
                  if(session.getAttribute("ss_age_pro")!=null){
                      ss_age_pro = session.getAttribute("ss_age_pro").toString();
                      session.removeAttribute("ss_age_pro");
                  }
              %>
              <div class="aa-properties-info">
                  <div class="card parenthold mt-30">
                      <h6><%=dd_stype%> Details</h6>
                  <div class="list-detail ">
                    
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-6 form-group">
                            <small><u>Name /No of House</u></small>
                        <span class="d-block"> <%=ss_name_num%></span>
                        </div>
                          
                          <div class="col-md-6 col-sm-6 col-xs-6 form-group">
                              <small><u>Street</u></small>
                        <span class="d-block"> <%=ss_street%></span>
                        </div>
                       
                    </div>
                        <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-6 form-group">
                            <small><u>Address</u></small>
                        <span class="d-block"> <%=ss_address_pro%></span>
                        </div>
                          
                          <div class="col-md-6 col-sm-6 col-xs-6 form-group">
                              <small><u>Landmark</u></small>
                        <span class="d-block"> <%=ss_lmark%></span>
                        </div>
                       
                    </div>
                        
                        <div class="row">
                        <div class="col-md-4 col-sm-4 col-xs-4 form-group">
                            <small><u>City</u></small>
                        <span class="d-block"> <%=ss_city%></span>
                        </div>
                          
                          <div class="col-md-4 col-sm-4 col-xs-4 form-group">
                              <small><u>State</u></small>
                        <span class="d-block"> <%=ss_state%></span>
                        </div>
                        
                        <div class="col-md-4 col-sm-4 col-xs-4 form-group">
                            <small><u>Age of Property</u></small>
                        <span class="d-block"> <%=ss_age_pro%></span>
                        </div>
                       
                    </div>
                        
                        <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                            <small><u>Near By</u></small>
                            
                        <div class="aa-tag for-sale">
                             <%

                            if (session.getAttribute("dd_near") != null) {
                                List<String> dd_near = (List<String>) session.getAttribute("dd_near");
                        %>
                        <%
                             for (int i = 0; i < dd_near.size(); i++) {
                        %>
                        <i class="fa fa-map-marker" style="color: #FF5733"></i> <%=dd_near.get(i)%>
                        <%}%>
                        <%
                            session.removeAttribute("dd_near");
                            }
                        %>
                        
                        </div>
                        
                        </div>
                          
                        
                       
                    </div>
                    
                    
                  </div>
                  
                </div>
                  
                  
                  
                
                
               

               
              </div>
              <!-- Properties social share -->

            </div>
          </div>
        </div>
        <!-- Start properties sidebar -->
        <div class="col-md-6"> 
            <div id="map"></div>
            <div class="card  mt-30 parenthold">
                    <div class="col-md-12 col-sm-12 col-xs-">
                        <h4  class="section-head">Contact owner</h4>
                    </div>              
                      <div class="row">
                          <div class="col-md-6 col-md-sm-12 col-xs-12">
                              <div class="input-field mt-0">
                                  <input id="name" type="text" class="validate">
                                  <label for="name" class="">Name <span class="required">*</span></label>
                              </div>
                          </div>
                          <div class="col-md-6 col-md-sm-12 col-xs-12">
                              <div class="input-field mt-0">
                                  <input id="number" type="text" class="validate">
                                  <label for="number" class="">Mobile <span class="required">*</span></label>
                              </div>
                          </div>
                      </div>
                      <div class="col-md-12 col-md-sm-12 col-xs-12">
                          <div class="input-field mt-0">
                              <input id="mail-id" type="text" class="validate">
                              <label for="mail-id" class="">Email ID <span class="required">*</span></label>
                          </div>
                      </div>
                
                <div class="col-md-12 col-md-sm-12 col-xs-12">
                          <div class="input-field mt-0">
                              <input id="mail-id" type="text" class="validate">
                              <label for="mail-id" class="">Leave a Message <span class="required">*</span></label>
                          </div>
                      </div>
                      <div>
                          <button _ngcontent-c2="" class="btn btn-common right mt-3 waves-effect waves-light">Submit</button>
                      </div>
                  </div>

        </div>
      </div>
                        
                        <%
                            String ss_adate1 = "",ss_area_build = "",ss_area_btype = "",ss_area_bin = "",ss_bhk = "";
                            if(session.getAttribute("ss_adate1")!=null){
                                ss_adate1 = session.getAttribute("ss_adate1").toString();
                                session.removeAttribute("ss_adate1");
                            }
                            if(session.getAttribute("ss_area_build")!=null){
                                ss_area_build = session.getAttribute("ss_area_build").toString();
                                session.removeAttribute("ss_area_build");
                            }
                            if(session.getAttribute("ss_area_btype")!=null){
                                ss_area_btype = session.getAttribute("ss_area_btype").toString();
                                session.removeAttribute("ss_area_btype");
                            }
                            if(session.getAttribute("ss_area_bin")!=null){
                                ss_area_bin = session.getAttribute("ss_area_bin").toString();
                                session.removeAttribute("ss_area_bin");
                            }
                            if(session.getAttribute("ss_bhk")!=null){
                                ss_bhk = session.getAttribute("ss_bhk").toString();
                                session.removeAttribute("ss_bhk");
                            }
                            
                            String ss_rent_amt = "",ss_deposit_amt = "",ss_maintanece_amt = "",ss_agreement_dur = "",ss_info_leave = "";
                            if(session.getAttribute("ss_rent_amt")!=null){
                                ss_rent_amt = session.getAttribute("ss_rent_amt").toString();
                                session.removeAttribute("ss_rent_amt");
                            }
                            if(session.getAttribute("ss_deposit_amt")!=null){
                                ss_deposit_amt = session.getAttribute("ss_deposit_amt").toString();
                                session.removeAttribute("ss_deposit_amt");
                            }
                            if(session.getAttribute("ss_maintanece_amt")!=null){
                                ss_maintanece_amt = session.getAttribute("ss_maintanece_amt").toString();
                                session.removeAttribute("ss_maintanece_amt");
                            }
                            if(session.getAttribute("ss_agreement_dur")!=null){
                                ss_agreement_dur = session.getAttribute("ss_agreement_dur").toString();
                                session.removeAttribute("ss_agreement_dur");
                            }
                            if(session.getAttribute("ss_info_leave")!=null){
                                ss_info_leave = session.getAttribute("ss_info_leave").toString();
                                session.removeAttribute("ss_info_leave");
                            }
                           
                        %>
                            <div class="card parenthold">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <h6>Rent Details</h6>
                        <div class="row">
                            <div class="col-md-4 col-sm-4 col-xs-4 form-group">
                              <small><u>Available From</u></small>
                        <span class="d-block"> <%=ss_adate1%></span>
                        </div>
                        <%
                                if(!dd_stype.equalsIgnoreCase("OFFICE") && !dd_stype.equalsIgnoreCase("SHOPS") && !dd_stype.equalsIgnoreCase("WAREHOUSE") && !dd_stype.equalsIgnoreCase("Specials")){
                            %>
                        <div class="col-md-8 col-sm-8 col-xs-8 form-group">
                             <small><u>Tenant Preferred</u></small>
                             <br>
                        <span class="d-block" >
                             <%

                            if (session.getAttribute("dd_tenant") != null) {
                                List<String> dd_tenant = (List<String>) session.getAttribute("dd_tenant");
                                List<String> dd_tenant_class = (List<String>) session.getAttribute("dd_tenant_class");
                        %>
                        <%
                             for (int i = 0; i < dd_tenant.size(); i++) {
                        %>
                         <%=dd_tenant.get(i)%>
                        <%}%>
                        <%
                            session.removeAttribute("dd_tenant");
                            session.removeAttribute("dd_tenant_class");
                            }
                        %>
                        
                        </span>
                        </div>
                          <%}%>
                          
                       
                    </div>
                          <%
                          if(!dd_stype.equalsIgnoreCase("PG")){
                      %>
                        <div class="row">
                            <%
                                if(!dd_stype.equalsIgnoreCase("OFFICE") && !dd_stype.equalsIgnoreCase("SHOPS") && !dd_stype.equalsIgnoreCase("WAREHOUSE") && !dd_stype.equalsIgnoreCase("Specials")){
                            %>
                        <div class="col-md-4 col-sm-4 col-xs-4 form-group">
                            <small><u>BHK</u></small>
                        <span class="d-block"> <%=ss_bhk%></span>
                        </div>
                          <%}%>
                          <%
                          if(dd_stype.equalsIgnoreCase("Specials")){
                      %>
                      <div class="col-md-4 col-sm-4 col-xs-4 form-group">
                            <small><u>Type</u></small>
                        <span class="d-block"> <%=ss_rtype%></span>
                        </div>
                        
                        <div class="col-md-4 col-sm-4 col-xs-4 form-group">
                            <small><u>Occupancy</u></small>
                        <span class="d-block"> <%=ss_occu%></span>
                        </div>
                      
                      <%}%>
                          <div class="col-md-4 col-sm-4 col-xs-4 form-group">
                              <small><u>Area</u></small>
                        <span class="d-block"> <%=ss_area_build%> <%=ss_area_bin%> <%=ss_area_btype%></span>
                        </div>
                       
                    </div>
                        <%}%>
                        <%
                          if(dd_stype.equalsIgnoreCase("Specials")){
                      %>
                        <%
                            String ss_veg = "",ss_nonveg = "",ss_bothh = "",ss_nrooms = "",ss_rooms = "",ss_hall = "";
                            
                            if(session.getAttribute("ss_veg")!=null){
                                ss_hall = session.getAttribute("ss_hall").toString();
                                session.removeAttribute("ss_hall");
                            }
                            if(session.getAttribute("ss_nonveg")!=null){
                                ss_nonveg = session.getAttribute("ss_nonveg").toString();
                                session.removeAttribute("ss_nonveg");
                            }
                            if(session.getAttribute("ss_bothh")!=null){
                                ss_bothh = session.getAttribute("ss_bothh").toString();
                                session.removeAttribute("ss_bothh");
                            }
                            if(session.getAttribute("ss_nrooms")!=null){
                                ss_nrooms = session.getAttribute("ss_nrooms").toString();
                                session.removeAttribute("ss_nrooms");
                            }
                            if(session.getAttribute("ss_rooms")!=null){
                                ss_rooms = session.getAttribute("ss_rooms").toString();
                                session.removeAttribute("ss_rooms");
                            }
                            if(session.getAttribute("ss_hall")!=null){
                                ss_hall = session.getAttribute("ss_hall").toString();
                                session.removeAttribute("ss_hall");
                            }
                        %>
                        
                        <div class="row">
                          <div class="col-md-4 col-sm-4 col-xs-4 form-group">
                            <small><u>No of Rooms</u></small>
                        <span class="d-block"> <%=ss_nrooms%></span>
                        </div> 
                        
                        <div class="col-md-4 col-sm-4 col-xs-4 form-group">
                            <small><u>AC</u></small>
                            <%
                               if(ss_rooms.equalsIgnoreCase("y")){
                            %>
                        <span class="d-block"> Rooms</span>
                        <%}%>&nbsp;&nbsp;
                        <%
                               if(ss_rooms.equalsIgnoreCase("y")){
                            %>
                        <span class="d-block"> Hall</span>
                        <%}%>
                        </div> 
                        
                        <div class="col-md-4 col-sm-4 col-xs-4 form-group">
                            <small><u>Food Type</u></small>
                            <%
                               if(ss_veg.equalsIgnoreCase("y")){
                            %>
                        <span class="d-block"> Veg</span>
                        <%}%>&nbsp;&nbsp;
                        <%
                               if(ss_nonveg.equalsIgnoreCase("y")){
                            %>
                        <span class="d-block"> Non-veg</span>
                        <%}%>
                        <%
                               if(ss_bothh.equalsIgnoreCase("y")){
                            %>
                        <span class="d-block"> Both</span>
                        <%}%>
                        </div> 
                        
                        
                        </div>
                        <%}%>
                        <div class="row">
                        <div class="col-md-4 col-sm-4 col-xs-4 form-group">
                            <small><u>Furnishing</u></small>
                        <span class="d-block"> <%=ss_fur%></span>
                        </div>
                           <%
                          if(dd_stype.equalsIgnoreCase("PG")){
                      %>
                          <div class="col-md-4 col-sm-4 col-xs-4 form-group">
                            <small><u>Food Service</u></small>
                            <%
                                if(ss_food.equalsIgnoreCase("y")){
                            %>
                        <span class="d-block"> With Food</span>
                        <%}%>
                        <%
                                if(ss_food1.equalsIgnoreCase("y")){
                            %>
                        <span class="d-block"> Without Food</span>
                        <%}%>
                        </div>
                        
                        <div class="col-md-4 col-sm-4 col-xs-4 form-group">
                            <small><u>Sharing</u></small>
                        <%
                                if(ss_share.equalsIgnoreCase("y")){
                            %>
                        <span class="d-block"> With Sharing</span>
                        <%}%>
                        <%
                                if(ss_share1.equalsIgnoreCase("y")){
                            %>
                        <span class="d-block"> Without Sharing</span>
                        <%}%>
                        </div>
                       <%}else{%>
                       
                       <div class="col-md-4 col-sm-4 col-xs-4 form-group">
                            <small><u>Floor</u></small>
                        <span class="d-block"> <%=ss_floor%></span>
                        </div>
                        
                        <div class="col-md-4 col-sm-4 col-xs-4 form-group">
                            <small><u>Facings</u></small>
                        <span class="d-block"> <%=ss_facings%></span>
                        </div>
                       
                       
                       <%}%>
                    </div>
                        <hr>
                        <div class="row">
                        <div class="col-md-4 col-sm-4 col-xs-4 form-group">
                            <small><u>Rent Amount</u></small>
                        <span class="d-block"> Rs.<%=ss_rent_amt%></span>
                        </div>
                          
                          <div class="col-md-4 col-sm-4 col-xs-4 form-group">
                              <%
                          if(dd_stype.equalsIgnoreCase("PG")){
                      %>
                            <small><u>Advance Amount</u></small>
                            <%
                                }else if(dd_stype.equalsIgnoreCase("Specials")){
                            %>
                            <small><u>Advance Booking Charges</u></small>
                            <%}else{%>
                            <small><u>Deposit Amount</u></small>
                            <%}%>
                        <span class="d-block"> Rs.<%=ss_deposit_amt%></span>
                        </div>
                        
                        <div class="col-md-4 col-sm-4 col-xs-4 form-group">
                            <small><u>Negotiable?</u></small>
                        <span class="d-block"> <%
                                if(ss_nego.equalsIgnoreCase("y")){
                            %>
                        Yes
                        <%}else{%>
                        No
                        <%}%></span>
                        </div>
                       
                    </div>
                        
                        <div class="row">
                        <div class="col-md-4 col-sm-4 col-xs-4 form-group">
                            <%
                          if(dd_stype.equalsIgnoreCase("Specials")){
                      %>
                      <small><u>Cancellation Charge</u></small>
                      <%}else{%>
                            <small><u>Maintenence Charge</u></small>
                            <%}%>
                        <span class="d-block"> Rs.<%=ss_maintanece_amt%></span>
                        </div>
                          
                          <div class="col-md-4 col-sm-4 col-xs-4 form-group">
                               <%
                          if(dd_stype.equalsIgnoreCase("PG")){
                      %>
                            <small><u>Rent Without Food </u></small>
                            <%}else if(dd_stype.equalsIgnoreCase("Specials")){%>
                            <small><u>Deduction if Canceled </u></small>
                            <%}else{%>
                            <small><u>Agreement Duration </u></small>
                            <%}%>
                            <%
                          if(dd_stype.equalsIgnoreCase("PG")){
                      %>
                         <span class="d-block">Rs.<%=ss_agreement_dur%></span>  
                         <%}else if(dd_stype.equalsIgnoreCase("Specials")){%>
                         <span class="d-block">Rs.<%=ss_agreement_dur%></span>  
                            <%}else{%>
                        <span class="d-block"><%=ss_agreement_dur%> Months</span>
                        <%}%>
                        </div>
                        <%
                          if(!dd_stype.equalsIgnoreCase("Specials")){
                      %>
                        <div class="col-md-4 col-sm-4 col-xs-4 form-group">
                            <small><u>Intimation before leaving</u></small>
                        <span class="d-block"> <%=ss_info_leave%> Months</span>
                        </div>
                       <%}%>
                    </div>
                    </div>
                        
                        
                        
                        <div class="col-md-6 col-sm-6 col-xs-6">
                             <h6>Amenities</h6>
                        <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-6 form-group">
                            <small><u>Parking Available</u></small>
                        <span class="d-block"> 
                            <%
                                if(ss_parking.equalsIgnoreCase("y")){
                            %>
                        Yes
                        <%}else{%>
                        No
                        <%}%>
                        </span>
                        </div>
                          
                          
                       
                    </div>
                        
                        <div class="row">
                           
                        <div class="col-md-12 col-md-sm-12 col-xs-12">
                                <p class="comment-form-email">
                                    <label for="email" class="d-block">Available Amenities </label>
						
                                             <%

                            if (session.getAttribute("amn_det") != null) {
                                List<String> amn_det = (List<String>) session.getAttribute("amn_det");
                                List<String> amn_fa = (List<String>) session.getAttribute("amn_fa");
                        %>
                        <%
                             for (int i = 0; i < amn_det.size(); i++) {
                        %>
					<div class="col-md-6 col-md-sm-6 col-xs-6"> 
                        <label class="fa <%=amn_fa.get(i)%>">
                                        <input type="checkbox" class="with-gap" name="amn" checked/>
                                       
                                        <span><%=amn_det.get(i)%></span>
                                    </label>
                                        </div>
                                    <%}%>
                                    <%
                                        session.removeAttribute("amn_det");
                                        session.removeAttribute("amn_fa");
                                        }
                                    %>
									
					
                                </p>
                            </div>
                        
                          
                       
                    </div>
                                    <hr>
                                    <div class="row">
                           
                        <div class="col-md-12 col-md-sm-12 col-xs-12">
                                <p class="comment-form-email">
                                    <label for="email" class="d-block">Available Services </label>
						
                                             <%

                            if (session.getAttribute("ser_det") != null) {
                                List<String> ser_det = (List<String>) session.getAttribute("ser_det");
                                List<String> ser_fa = (List<String>) session.getAttribute("ser_fa");
                        %>
                        <%
                             for (int i = 0; i < ser_det.size(); i++) {
                        %>
					<div class="col-md-6 col-md-sm-6 col-xs-6"> 
                        <label class="fa <%=ser_fa.get(i)%>">
                                        <input type="checkbox" class="with-gap" name="amn" checked/>
                                       
                                        <span><%=ser_det.get(i)%></span>
                                    </label>
                                        </div>
                                    <%}%>
                                    <%
                                        session.removeAttribute("ser_det");
                                        session.removeAttribute("ser_fa");
                                        }
                                    %>
									
					
                                </p>
                            </div>
                        
                          
                       
                    </div>
                                    <hr>
                                    <%
                                        String ss_rule_non_veg = "",ss_rule_pets = "",ss_rule_drink = "",ss_rule_water = "",ss_oter_details4 = "",ss_rule_elec = "";
                                        if(session.getAttribute("ss_rule_non_veg")!=null){
                                            ss_rule_non_veg = session.getAttribute("ss_rule_non_veg").toString();
                                            session.removeAttribute("ss_rule_non_veg");
                                        }
                                        if(session.getAttribute("ss_rule_pets")!=null){
                                            ss_rule_pets = session.getAttribute("ss_rule_pets").toString();
                                            session.removeAttribute("ss_rule_pets");
                                        }
                                        if(session.getAttribute("ss_rule_drink")!=null){
                                            ss_rule_drink = session.getAttribute("ss_rule_drink").toString();
                                            session.removeAttribute("ss_rule_drink");
                                        }
                                        if(session.getAttribute("ss_rule_water")!=null){
                                            ss_rule_water = session.getAttribute("ss_rule_water").toString();
                                            session.removeAttribute("ss_rule_water");
                                        }
                                        if(session.getAttribute("ss_oter_details4")!=null){
                                            ss_oter_details4 = session.getAttribute("ss_oter_details4").toString();
                                            session.removeAttribute("ss_oter_details4");
                                        }
                                        if(session.getAttribute("ss_rule_elec")!=null){
                                            ss_rule_elec = session.getAttribute("ss_rule_elec").toString();
                                            session.removeAttribute("ss_rule_elec");
                                        }
                                    %>
                                    
                                    <div class="col-md-12 col-md-sm-12 col-xs-12">
                                        <label for="email" class="d-block">Rules/Conditions </label>
                                <div class="row">
                                    <div class="col-md-6 col-md-sm-12 col-xs-12"> 
                                            <label>
                                                <%
                                                    if(ss_rule_non_veg.equalsIgnoreCase("y")){
                                                %>
                                                    <input type="checkbox" class="filled-in" name="rule_non_veg" id="rule_non_veg" value="y" disabled checked="checked" />
                                                    <%}else{%>
                                                    <input type="checkbox" class="filled-in" name="rule_non_veg" id="rule_non_veg" disabled />
                                                    <%}%>
                                                    <span>Only For Vegetarians </span>
                                                </label>
                                    </div>
                                        <div class="col-md-6 col-md-sm-12 col-xs-12"> 
                                                <label>
                                                    <%
                                                    if(ss_rule_pets.equalsIgnoreCase("y")){
                                                %>
                                                    <input type="checkbox" class="filled-in" name="rule_pets" id="rule_pets" value="y" checked="checked" disabled />
                                                   <%}else{%>
                                                   <input type="checkbox" class="filled-in" name="rule_pets" id="rule_pets" disabled />
                                                   <%}%>
                                                    <span>Pets Not Allowed </span>
                                                </label>
                                        </div>
                                        <div class="col-md-6 col-md-sm-12 col-xs-12"> 
                                        <label>
                                             <%
                                                    if(ss_rule_drink.equalsIgnoreCase("y")){
                                                %>
                                                    <input type="checkbox" class="filled-in" name="rule_drink" id="rule_drink" value="y" checked="checked" disabled />
                                                    <%}else{%>
                                                    <input type="checkbox" class="filled-in" name="rule_drink" id="rule_drink" disabled />
                                                    <%}%>
                                                    <span>No Drinking Smoking</span>
                                                </label>
                                    </div>
                                    <div class="col-md-6 col-md-sm-12 col-xs-12"> 
                                        <label>
                                            <%
                                                    if(ss_rule_water.equalsIgnoreCase("y")){
                                                %>
                                                    <input type="checkbox" class="filled-in" name="rule_water" id="rule_water" value="y" checked="checked" disabled />
                                                    <%}else{%>
                                                    <input type="checkbox" class="filled-in" name="rule_water" id="rule_water" disabled />
                                                    <%}%>
                                                    <span>Water Charges Extra</span>
                                                </label>
                                        <label>
                                    </div>
                                        <div class="col-md-6 col-md-sm-12 col-xs-12"> 
                                            <label>
                                                <%
                                                    if(ss_rule_elec.equalsIgnoreCase("y")){
                                                %>
                                                    <input type="checkbox" class="filled-in" name="rule_elec" id="rule_elec" value="y" checked="checked" disabled/>
                                                    <%}else{%>
                                                    <input type="checkbox" class="filled-in" name="rule_elec" id="rule_elec" disabled />
                                                    <%}%>
                                                    <span>Electricity Charges Extra</span>
                                                </label>
                                    </div>
                                                    <br>
                                                    <%=ss_oter_details4%>
                                    </div>
                                </div>
                    </div>
                    </div>
                        </div>
    </div>
  </section>
  <!-- / Properties  -->
<%
    }catch(Exception e){
            
            }
%>
  <!-- Footer -->
  <%@include file="../footer.jsp" %>
  <!-- / Footer -->

  <!-- jQuery library -->
  <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> -->
  <script src="js/jquery.min.js"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="js/bootstrap.js"></script>
  <!-- slick slider -->
  <script type="text/javascript" src="js/slick.js"></script>
  <!-- Price picker slider -->
  <script type="text/javascript" src="js/nouislider.js"></script>
  <!-- mixit slider -->
  <script type="text/javascript" src="js/jquery.mixitup.js"></script>
  <!-- Add fancyBox -->
  <script type="text/javascript" src="js/jquery.fancybox.pack.js"></script>
  <!-- Custom js -->
  <script src="js/custom.js"></script>
  
  <style>
      #map {
        height: 300px;
        width: 100%;
       }
    </style>
  <script>
      function initMap() {
          
        var map = new google.maps.Map(document.getElementById('map'), {zoom: 19});
        var geocoder = new google.maps.Geocoder;
        geocoder.geocode({'address': document.getElementById("addr").value}, function(results, status) {
          if (status === 'OK') {
            map.setCenter(results[0].geometry.location);
            new google.maps.Marker({
              map: map,
              position: results[0].geometry.location
            });
          } else {
            window.alert('Geocode was not successful for the following reason: ' +
                status);
          }
        });
      }
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCZLiZff4EUU1YkSPIpxZdADSJCiarUAd4&callback=initMap">
        </script>

</body>

</html>