<%-- 
    Document   : change_password
    Created on : 3 Dec, 2019, 8:46:27 PM
    Author     : Nandini
--%>

<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Home Property | Change Password</title>

        <!-- Favicon -->
        <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">

        <!-- Font awesome -->
        <link href="css/font-awesome.css" rel="stylesheet">
        <!-- Bootstrap -->
        <link href="css/bootstrap.css" rel="stylesheet">
        <!-- slick slider -->
        <link rel="stylesheet" type="text/css" href="css/slick.css">
        <!-- price picker slider -->
        <link rel="stylesheet" type="text/css" href="css/nouislider.css">
        <!-- Fancybox slider -->
        <link rel="stylesheet" href="css/jquery.fancybox.css" type="text/css" media="screen" />
        <!-- Theme color -->
        <link id="switcher" href="css/theme-color/orange-theme.css" rel="stylesheet">

        <!-- Main style sheet -->
        <link href="css/style-main.css" rel="stylesheet">
        <link href="css/materialize.min.css" rel="stylesheet">

        <!-- Google Font -->
        <link href='https://fonts.googleapis.com/css?family=Vollkorn' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>


        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
          <![endif]-->

        <script type="text/javascript">
                                              function form_validate(){
                                                   if (document.getElementById("opwd").value == "") {
                    //alert("Kindly Enter Old Password");
                    document.getElementById("e_pwd").hidden=false;
                    document.getElementById("opwd").focus();
                    return false;
                }
                if (document.getElementById("oopwd").value != document.getElementById("opwd").value) {
                    //alert( document.getElementById("opwd").value);
                    document.getElementById("msg_msg").innerHTML="Invalid Existing Password";
                    document.getElementById("opwd").value = "";
                    document.getElementById("opwd").focus();
                    return false;
                }else{
                    document.getElementById("msg_msg").innerHTML=""; 
                }
                if (document.getElementById("npwd").value == "") {
                   // alert("Kindly Enter New Password");
                    document.getElementById("n_pwd").hidden=false;
                    document.getElementById("npwd").focus();
                    return false;
                }

                if (document.getElementById("cpwd").value == "") {
//                    alert("Kindly Enter Confirm Password");
                     document.getElementById("c_pwd").hidden=false;
                    document.getElementById("cpwd").focus();
                    return false;
                }

                if (document.getElementById("npwd").value != document.getElementById("cpwd").value) {
//                    alert("New Password and Confirm Password Not Matching");
                    document.getElementById("msg_msg").innerHTML="New Password and Confirm Password Not Matching";
                    document.getElementById("cpwd").value = "";
                    document.getElementById("cpwd").focus();
                    return false;
                }else{
                      document.getElementById("msg_msg").innerHTML="";
                }

                if (document.getElementById("npwd").value == document.getElementById("opwd").value) {
                    //alert("Your New Password is Same as Your Older Password");
                    document.getElementById("msg_msg").innerHTML="Your New Password is Same as Your Older Password";
                    document.getElementById("npwd").value = "";
                    document.getElementById("npwd").focus();
                    return false;
                }else{
                    document.getElementById("msg_msg").innerHTML="";
                }


                var nn = confirm("Are You Sure to Change Password?");
                if (nn == true) {
                    document.ch_password.action = "change_password_process";
                } else {
                    return false;
                }
                                              }
                                          </script>
                                          
                                          <script type="text/javascript">
            function allowpositivenumber(e) {
                var charCode = (e.which) ? e.which : event.keyCode;

                if ((charCode < 46 || charCode > 57)) {
                    return false;
                }
                return true;
            }
        </script>
        
        <script type="text/javascript">
            function fill_pwd(){
                 if(document.getElementById("opwd").value!="" && document.getElementById("opwd").value.length>0){
                   document.getElementById("e_pwd").hidden=true;
//                   document.getElementById("mborder").style.border = "thin solid transparent";
               }else{
                   document.getElementById("e_pwd").hidden=false;
//                   document.getElementById("mborder").style.border = "thin solid #FF0000";
               }
               
                if(document.getElementById("opwd").value!="" && document.getElementById("opwd").value.length>0){
                 if (document.getElementById("oopwd").value != document.getElementById("opwd").value) {
                    //alert( document.getElementById("opwd").value);
                    document.getElementById("msg_msg").innerHTML="Invalid Existing Password";
                    document.getElementById("opwd").value = "";
                    document.getElementById("opwd").focus();
                    return false;
                }else{
                    document.getElementById("msg_msg").innerHTML=""; 
                }
            }
            }
        </script>
        
        <script type="text/javascript">
            function fill_pwd1(){
                 if(document.getElementById("npwd").value!="" && document.getElementById("npwd").value.length>0){
                   document.getElementById("n_pwd").hidden=true;
//                   document.getElementById("mborder").style.border = "thin solid transparent";
               }else{
                   document.getElementById("n_pwd").hidden=false;
//                   document.getElementById("mborder").style.border = "thin solid #FF0000";
               }
               
                if(document.getElementById("npwd").value!="" && document.getElementById("npwd").value.length>0){
                if (document.getElementById("npwd").value == document.getElementById("opwd").value) {
                    //alert("Your New Password is Same as Your Older Password");
                    document.getElementById("msg_msg").innerHTML="Your New Password is Same as Your Older Password";
                    document.getElementById("npwd").value = "";
                    document.getElementById("npwd").focus();
                    return false;
                }else{
                    document.getElementById("msg_msg").innerHTML="";
                }
            }
            }
        </script>
        
        <script type="text/javascript">
            function fill_pwd2(){
                 if(document.getElementById("cpwd").value!="" && document.getElementById("cpwd").value.length>0){
                   document.getElementById("c_pwd").hidden=true;
//                   document.getElementById("mborder").style.border = "thin solid transparent";
               }else{
                   document.getElementById("c_pwd").hidden=false;
//                   document.getElementById("mborder").style.border = "thin solid #FF0000";
               }
               
                if(document.getElementById("cpwd").value!="" && document.getElementById("cpwd").value.length>0){
                if (document.getElementById("npwd").value != document.getElementById("cpwd").value) {
//                    alert("New Password and Confirm Password Not Matching");
                    document.getElementById("msg_msg").innerHTML="New Password and Confirm Password Not Matching";
                    document.getElementById("cpwd").value = "";
                    document.getElementById("cpwd").focus();
                    return false;
                }else{
                      document.getElementById("msg_msg").innerHTML="";
                }
            }
            }
        </script>

    </head>

    <body>
        <!-- Pre Loader -->
        <div id="aa-preloader-area" style="display: none;">
            <div class="pulse"></div>
        </div>
        <!-- SCROLL TOP BUTTON -->
        <a class="scrollToTop" href="#"><i class="fa fa-angle-double-up"></i></a>
        <!-- END SCROLL TOP BUTTON -->


        <!-- Start header section -->
        <%@include file="../header.jsp" %>
        <!-- End header section -->
        <!-- Start menu section -->
        <div class="body-hold">

            <!-- End menu section -->

            <%
                                            String cpwd = "";
                                            if (session.getAttribute("cpwd") != null) {
                                                cpwd = session.getAttribute("cpwd").toString();
                                                session.removeAttribute("cpwd");
                                            }
                                        %>
            <form name="ch_password" method="post>"
            <!-- Start Proerty header  -->
            <section class="parenthold">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 user-in">
                            <div class="row">

                                <div class="col-md-12">
                                    <div class="tab-content card mt-0 p-25">
                                        <div id="menu0" class="tab-pane fade in active">
                                            <%    
                           if (session.getAttribute("insert") != null) {
                          %>
                                                                    
                                                                    
                    <%  
                           if (session.getAttribute("insert").equals("pass")) {
                    %>
                        <div class="alert alert-block alert-success col-lg-12 ">
                    <button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">�</span></button>
                    <strong> Details Updated Successfully.</strong> 
                   
                    
                  </div>
                        <%} else if (session.getAttribute("insert").equals("fail")) {%>
                        <div class="alert alert-block alert-danger col-lg-12 ">
                    <button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">�</span></button>
                                <strong> Creation Failed Please Try Again.</strong> 
                            </div>
                        <%} else if (session.getAttribute("insert").equals("exists")) {%>
                        <div class="alert alert-block alert-success col-lg-12 ">
                    <button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">�</span></button>
                                <strong>  Already Exists in Database.</strong>
                            </div>
                         <%} else if (session.getAttribute("insert").equals("delpass")) {%>
                         <div class="alert alert-block alert-success col-lg-12 ">
                    <button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">�</span></button>
                                <strong>  Deleted Successfully from all Records</strong>
                            </div>
                         <%} else if (session.getAttribute("insert").equals("delfail")) {%>
                         <div class="alert alert-block alert-success col-lg-12 ">
                    <button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">�</span></button>
                    <strong>Deletion Failed.</strong>
                            </div>
                          <%}
                   else if (session.getAttribute("insert").equals("updatepass")) {%>
                   <div class="alert alert-block alert-success col-lg-12 ">
                    <button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">�</span></button>
                    <strong>  Updated Successfully</strong>
                            </div>
                    <%}
                   else if (session.getAttribute("insert").equals("updatefail")) {%>
                   <div class="alert alert-block alert-success col-lg-12 ">
                    <button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">�</span></button>
                    <strong>Update Failed Pls Try Again</strong>
                            </div>
                   
                    <%}%>
                    <%
                            session.removeAttribute("insert");
                        }%>
                                            <h3>Change Password</h3>
                                             
                                            <div class="formhold">
                                                <div class="input-field">
                                                    <input type="password" name="opwd" id="opwd" onblur="return fill_pwd();" tabindex="1"  placeholder="Existing Password">
                                                    <input type="hidden" id="oopwd" name="oopwd" value="<%=cpwd%>" >
                                                    <label for="u_name">Existing Password 
                                                        <span class="required">*</span></label>
                                                </div>
                                                    <span id="e_pwd" style="color:red" hidden>Please Enter Existing Password</span>
                                                <div class="input-field">
                                                    <input type="password" placeholder="Confirm Password" name="npwd" tabindex="2" id="npwd" maxlength="10" onblur="return fill_pwd1();">
                                                    <label for="u_email">New Password 
                                                        <span class="required">*</span></label>
                                                </div>
                                                    <span style="color:red" id="n_pwd" hidden> Please Enter New Password</span>
                                                <div class="input-field">
                                                    <input type="password" placeholder="Confirm Password"  name="cpwd" id="cpwd" tabindex="3" maxlength="20" onblur="return fill_pwd2();">
                                                    <label for="u_number">Confirm Password 
                                                        <span class="required">*</span></label>
                                                </div>
                                                    <span style="color:red" id="c_pwd" hidden>Please Repeat Your New Password </span>
                                                    
                                                    <span style="color:red" id="msg_msg"></span>
                                                    <br>
                                                    <button class="default-btn" type="submit" onclick="return form_validate();">Update Password</button>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </form>
            <!-- End Proerty header  -->

        </div>
        <!-- Footer -->
        <%@include file="../footer.jsp" %>
        <!-- / Footer -->

        <!-- jQuery library -->
        <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> -->
        <script src="js/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="js/bootstrap.js"></script>
        <!-- slick slider -->
        <script type="text/javascript" src="js/slick.js"></script>
        <!-- Price picker slider -->
        <script type="text/javascript" src="js/nouislider.js"></script>
        <!-- mixit slider -->
        <script type="text/javascript" src="js/jquery.mixitup.js"></script>
        <!-- Add fancyBox -->
        <script type="text/javascript" src="js/jquery.fancybox.pack.js"></script>
        <script src="js/materialize.min.js"></script>
        <!-- Custom js -->
        <script src="js/custom.js"></script>
        <script>
            $(document).ready(function () {
                $('.body-hold').css('min-height', $(window).height() - 208);
                // Comma, not colon ----^
            });
            $(window).resize(function () {
                $('.body-hold').css('min-height', $(window).height() - 208);
                // Comma, not colon ----^
            });

            $("#example-basic").steps({
                headerTag: "h3",
                bodyTag: "section",
                transitionEffect: "slideLeft",
                autoFocus: true
            });

        </script>
    </body>

</html>