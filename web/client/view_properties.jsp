<%-- 
    Document   : view_properties
    Created on : 13 Jan, 2020, 4:58:45 PM
    Author     : Nandini
--%>
<%@page import="java.util.List"%>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Home Property | View Properties</title>

  <!-- Favicon -->
  <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">

  <!-- Font awesome -->
  <link href="css/font-awesome.css" rel="stylesheet">
  <!-- Bootstrap -->
  <link href="css/bootstrap.css" rel="stylesheet">
  <!-- slick slider -->
  <link rel="stylesheet" type="text/css" href="css/slick.css">
  <!-- price picker slider -->
  <link rel="stylesheet" type="text/css" href="css/nouislider.css">
  <!-- Fancybox slider -->
  <link rel="stylesheet" href="css/jquery.fancybox.css" type="text/css" media="screen" />
  <!-- Theme color -->
  <link id="switcher" href="css/theme-color/orange-theme.css" rel="stylesheet">

  <!-- Main style sheet -->
  <link href="css/style-main.css" rel="stylesheet">
  <link href="css/materialize.min.css" rel="stylesheet">

  <!-- Google Font -->
  <link href='https://fonts.googleapis.com/css?family=Vollkorn' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>


  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->


</head>

<body>
  <!-- Pre Loader -->
  <div id="aa-preloader-area">
    <div class="pulse"></div>
  </div>
  <!-- SCROLL TOP BUTTON -->
  <a class="scrollToTop" href="#"><i class="fa fa-angle-double-up"></i></a>
  <!-- END SCROLL TOP BUTTON -->


  <!-- Start header section -->
  <%@include file="../header.jsp" %>
  <!-- End header section -->
  <!-- Start menu section -->
  <div class="body-hold">
    
    <!-- End menu section -->

    <!-- Start Proerty header  -->
    <section class="parenthold">
      <div class="container-fluid">
        <div class="row">
<!--          <div class="col-md-3">
                <div class="search-filter smooth-scroll">
                  <ul class="nav nav-tabs">
                    <li>
                      <div class="clearfix">
                        <div class="pull-left">
                          <label class="lbl-main">Basic Filter</label>
                        </div>
                        <div class="pull-right">
                          <a class="clear-text" routerlink="#" ng-reflect-router-link="#" href="/search-page/%23"><i
                              class="fa fa-undo"></i> Clear all</a>
                        </div>
                      </div>
                    </li>
                  </ul>
                  <div>
                    <form novalidate="" class=" ">
                      <div class="filterhold">
                        <div class="basic-filter" id="filter-home">
						
						
						<label class="lbl-list"></label>                     
                          <label class="mr-15">
						  <input name="tenants" type="checkbox" class="filled-in">
                          <span >Show Only With Photos</span>
						  </label><label class="mr-15">

                          <label class="lbl-list"> Tenant type</label>                     
                          <label class="mr-15">                            
                            <input name="tenants" type="checkbox" class="filled-in">
                            <span >Bachelors</span>
                          </label><label class="mr-15">
                            
                            <input name="tenants" type="checkbox" class="filled-in">
                            <span >Family</span>
                          </label><label class="mr-15">
                            
                            <input name="tenants" type="checkbox" class="filled-in">
                            <span >Any</span>
                          </label>

                          <div class="clear"></div>
						  
						  <label class="lbl-list">Property You Are Looking</label>
                          <label class="mr-15">                            
                            <input name="bhkLength" type="checkbox" class="filled-in">
                            <span >Independent Villa</span>
							</label><label class="mr-15">
							<input name="bhkLength" type="checkbox" class="filled-in">
                            <span >Apartment</span>
							</label><label class="mr-15">
							<input name="bhkLength" type="checkbox" class="filled-in">
                            <span >Gated Community</span>
							</label><label class="mr-15">
						  

                          <label class="lbl-list">Select BHK</label>
                          <label class="mr-15">                            
                            <input name="bhkLength" type="checkbox" class="filled-in">
                            <span >1 RK</span>
                          </label><label class="mr-15">                           
                            <input name="bhkLength" type="checkbox" class="filled-in">
                            <span >1 BHK</span>
                          </label><label class="mr-15">                          
                            <input name="bhkLength" type="checkbox"class="filled-in">
                            <span >2 BHK</span>
                          </label><label class="mr-15">                            
                            <input name="bhkLength" type="checkbox"  class="filled-in">
                            <span >3 BHK</span>
                          </label><label class="mr-15">                           
                            <input name="bhkLength" type="checkbox" class="filled-in">
                            <span >4 BHK</span>
                          </label><label class="mr-15">                            
                            <input name="bhkLength" type="checkbox"  class="filled-in">
                            <span >4+ BHK</span>
                          </label>
                          <label class="lbl-list">Furnishing</label>
                          <label class="mr-15">
                           
                            <input name="furnished" type="checkbox" class="filled-in">
                            <span > Unfurnished</span>
                          </label><label class="mr-15">
                            
                            <input name="furnished" type="checkbox"   class="filled-in">
                            <span >Semi Furnished</span>
                          </label><label class="mr-15">
                            
                            <input name="furnished" type="checkbox"   class="filled-in">
                            <span >Fully Furnished</span>
                          </label>

                        </div>
                        <div class="advancebtn" id="headingOne1" role="tab">
                          <a aria-controls="collapseOne1" aria-expanded="true" class="advance-filter"
                            data-parent="#accordionEx1" data-toggle="collapse" href="#collapseOne1"
                            target="filter-home">
                            Advanced Filters <i aria-hidden="true" class="fa fa-angle-double-up pull-right"></i>
                          </a>
                        </div>

                        <div class="advance-filter-hold" id="filter-home-advance-panel">

                          <div aria-multiselectable="true" class="accordion md-accordion" id="accordionEx1"
                            role="tablist">
                            <div aria-labelledby="headingOne1" class="collapse" data-parent="#accordionEx1"
                              id="collapseOne1" role="tabpanel">
                              <div class="pb-3">
                                <label class="lbl-list">Floor</label>
                                <label class="mr-15">
                                  
                                  <input name="floors" type="checkbox"  class="filled-in">
                                  <span >Ground</span>
                                </label><label class="mr-15">
                                  
                                  <input name="floors" type="checkbox"   class="filled-in">
                                  <span >First</span>
                                </label><label class="mr-15">
                                  
                                  <input name="floors" type="checkbox"   class="filled-in">
                                  <span >Second</span>
                                </label><label class="mr-15">
                                  
                                  <input name="floors" type="checkbox"  class="filled-in">
                                  <span >Third</span>
                                </label><label class="mr-15">
                                  
                                  <input name="floors" type="checkbox"   class="filled-in">
                                  <span >Fourth</span>
                                </label><label class="mr-15">
                                  
                                  <input name="floors" type="checkbox"    class="filled-in">
                                  <span >Fifth</span>
                                </label><label class="mr-15">
                                  
                                  <input name="floors" type="checkbox"   class="filled-in">
                                  <span >Any</span>
                                </label>

                                <div class="clear"></div>

                                <label class="lbl-list">Amenties</label>

                                <label class="mr-15">
                                  
                                  <input name="amenities" type="checkbox"  class="filled-in">
                                  <span >TV</span>
                                </label><label class="mr-15">
                                  
                                  <input name="amenities" type="checkbox"  class="filled-in">
                                  <span >FAN</span>
                                </label><label class="mr-15">
                                  
                                  <input name="amenities" type="checkbox"  class="filled-in">
                                  <span >Gyser</span>
                                </label><label class="mr-15">
                                  
                                  <input name="amenities" type="checkbox"  class="filled-in">
                                  <span >Power Backup</span>
                                </label>

                                <div class="clear"></div>

                                <label class="lbl-list checkbox">
                                  <input name="securityGuard" type="checkbox"  class="filled-in">
                                  <span >Wardrobe</span>
                                </label>

                                <div class="clear"></div>

                                <label class="lbl-list">Parking</label>
                                <label class="mr-15">
                                  
                                  <input name="parking" type="checkbox"  class="filled-in">
                                  <span >Bike</span>
                                </label><label class="mr-15">
                                  
                                  <input name="parking" type="checkbox"  class="filled-in">
                                  <span >Car</span>
                                </label>
                                <div class="clear"></div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
          </div>-->
<%
                                                    if (session.getAttribute("insert") != null) {
                                                %>


                                                <%
                                                    if (session.getAttribute("insert").equals("pass")) {
                                                %>
                                                <div class="alert alert-block alert-success col-lg-12 ">
                                                    <button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">�</span></button>
                                                    <strong> Details Updated Successfully.</strong> 


                                                </div>
                                                <%} else if (session.getAttribute("insert").equals("fail")) {%>
                                                <div class="alert alert-block alert-danger col-lg-12 ">
                                                    <button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">�</span></button>
                                                    <strong> Creation Failed Please Try Again.</strong> 
                                                </div>
                                                <%} else if (session.getAttribute("insert").equals("exists")) {%>
                                                <div class="alert alert-block alert-success col-lg-12 ">
                                                    <button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">�</span></button>
                                                    <strong>  Already Exists in Database.</strong>
                                                </div>
                                                <%} else if (session.getAttribute("insert").equals("delpass")) {%>
                                                <div class="alert alert-block alert-success col-lg-12 ">
                                                    <button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">�</span></button>
                                                    <strong>  Deleted Successfully from all Records</strong>
                                                </div>
                                                <%} else if (session.getAttribute("insert").equals("delfail")) {%>
                                                <div class="alert alert-block alert-success col-lg-12 ">
                                                    <button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">�</span></button>
                                                    <strong>Deletion Failed.</strong>
                                                </div>
                                                <%} else if (session.getAttribute("insert").equals("updatepass")) {%>
                                                <div class="alert alert-block alert-success col-lg-12 ">
                                                    <button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">�</span></button>
                                                    <strong>  Updated Successfully</strong>
                                                </div>
                                                <%} else if (session.getAttribute("insert").equals("updatefail")) {%>
                                                <div class="alert alert-block alert-success col-lg-12 ">
                                                    <button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">�</span></button>
                                                    <strong>Update Failed Pls Try Again</strong>
                                                </div>

                                                <%}%>
                                                <%
                                                        session.removeAttribute("insert");
                                                    }%>
          <div class="col-md-12 search-list">
              <%
                  if(session.getAttribute("dd_id")!=null){
                      List<String> dd_id = (List<String>) session.getAttribute("dd_id");
                      List<String> dd_street = (List<String>) session.getAttribute("dd_street");
                      List<String> dd_address = (List<String>) session.getAttribute("dd_address");
                      List<String> dd_floor = (List<String>) session.getAttribute("dd_floor");
                      List<String> dd_dep = (List<String>) session.getAttribute("dd_dep");
                      List<String> dd_rent = (List<String>) session.getAttribute("dd_rent");
                      List<String> dd_area = (List<String>) session.getAttribute("dd_area");
                      List<String> dd_fur = (List<String>) session.getAttribute("dd_fur");
                      List<String> dd_cstatus = (List<String>) session.getAttribute("dd_cstatus");
                      List<String> dd_img = (List<String>) session.getAttribute("dd_img");
                      
                      for(int i=0;i<dd_id.size();i++){
                          String img = "";
                          if(dd_img.get(i)!=null && dd_img.get(i).length()>0){
                              img = "form_pics"+"/"+dd_img.get(i);
                          }else{
                              img = "img/no-image.png";
                          }
                  
              %>
            <div class="card bar-left-success">
              <div class="ribbon-hold green darken-1">
                <span><%=dd_cstatus.get(i)%></span>
              </div>
              <div class="property">
                <div class="card-body d-flex ">
                  <div class="prop-img">
                    <img src="<%=img%>" alt="img" width="200">
                  </div>
                  <div class="prop-details">
                    <div class="list-card-title">
                      <span class="header">
                          <a href="#"> 
                            <%=dd_floor.get(i)%> In <%=dd_street.get(i)%>                             
                            <small><i class="fa fa-external-link">  </i></small></a>
                      </span>
                      <small class="block">
                        <i class="fa fa-map-marker"></i><%=dd_address.get(i)%>
                      </small>
                    </div>
                    <div class="list-detail my-2">
                      <div class="block-1">
                        <small>Deposit</small>
                        <span class="d-block"> <%=dd_dep.get(i)%></span>
                      </div>
                      <div class="block-1">
                        <small>Rent</small>
                        <span class="d-block"> <%=dd_rent.get(i)%></span>
                      </div>
                      <div class="block-1">
                          <small>Plot area</small>
                          <span class="d-block"><%=dd_area.get(i)%> Sq.Ft</span>
                        </div>
                      <div class="block-1">
                        <small>Furnishing</small>
                        <span class="d-block"><%=dd_fur.get(i)%></span>
                      </div>
                    </div>
                    <div class="list-action mt-15 text-right">
                      
                      <a href="apartment_edit_client?id=<%=dd_id.get(i)%>" title="Click to Edit Details" class="btn btn-common mb-0 ml-0 pull-left">Edit Details</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <%
                }
                session.removeAttribute("dd_id");
                session.removeAttribute("dd_street");
                session.removeAttribute("dd_address");
                session.removeAttribute("dd_floor");
                session.removeAttribute("dd_dep");
                session.removeAttribute("dd_rent");
                session.removeAttribute("dd_area");
                session.removeAttribute("dd_fur");
                session.removeAttribute("dd_cstatus");
                session.removeAttribute("dd_img");
                }
            %>
          </div>
          <div class="col-md-2">

          </div>
        </div>
      </div>
    </section>
    <!-- End Proerty header  -->

  </div>
  <!-- Footer -->
  <%@include file="../footer.jsp" %>
  <!-- / Footer -->

  <!-- jQuery library -->
  <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> -->
  <script src="js/jquery.min.js"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="js/bootstrap.js"></script>
  <!-- slick slider -->
  <script type="text/javascript" src="js/slick.js"></script>
  <!-- Price picker slider -->
  <script type="text/javascript" src="js/nouislider.js"></script>
  <!-- mixit slider -->
  <script type="text/javascript" src="js/jquery.mixitup.js"></script>
  <!-- Add fancyBox -->
  <script type="text/javascript" src="js/jquery.fancybox.pack.js"></script>
  <script src="js/materialize.min.js"></script>
  <!-- Custom js -->
  <script src="js/custom.js"></script>
  <script>
    $(document).ready(function () {
      $('.body-hold').css('min-height', $(window).height() - 198);
      // Comma, not colon ----^
    });
    $(window).resize(function () {
      $('.body-hold').css('min-height', $(window).height() - 198);
      // Comma, not colon ----^
    });

    $("#example-basic").steps({
      headerTag: "h3",
      bodyTag: "section",
      transitionEffect: "slideLeft",
      autoFocus: true
    });

  </script>
</body>

</html>