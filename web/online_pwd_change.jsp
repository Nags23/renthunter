<%-- 
    Document   : online_pwd_change
    Created on : 5 Jul, 2020, 1:50:44 PM
    Author     : Nandini
--%>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">    
    <title>Home Property | Password Change</title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
    
    
    <!-- Font awesome -->
    <link href="css/font-awesome.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="css/bootstrap.css" rel="stylesheet">   
    <!-- slick slider -->
    <link rel="stylesheet" type="text/css" href="css/slick.css">
    <!-- price picker slider -->
    <link rel="stylesheet" type="text/css" href="css/nouislider.css">
    <!-- Fancybox slider -->
    <link rel="stylesheet" href="css/jquery.fancybox.css" type="text/css" media="screen" /> 
    <!-- Theme color -->
    <link id="switcher" href="css/theme-color/orange-theme.css" rel="stylesheet">     

    <!-- Main style sheet -->
    <link href="css/style-main.css" rel="stylesheet">    

   
    <!-- Google Font -->
    <link href='https://fonts.googleapis.com/css?family=Vollkorn' rel='stylesheet' type='text/css'>    
    <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script type="text/javascript">
                                              function form_validate(){
                                                   
                if (document.getElementById("npwd").value == "") {
                   // alert("Kindly Enter New Password");
                    document.getElementById("n_pwd").hidden=false;
                    document.getElementById("npwd").focus();
                    return false;
                }

                if (document.getElementById("cpwd").value == "") {
//                    alert("Kindly Enter Confirm Password");
                     document.getElementById("c_pwd").hidden=false;
                    document.getElementById("cpwd").focus();
                    return false;
                }

                if (document.getElementById("npwd").value != document.getElementById("cpwd").value) {
//                    alert("New Password and Confirm Password Not Matching");
                    document.getElementById("msg_msg").innerHTML="New Password and Confirm Password Not Matching";
                    document.getElementById("cpwd").value = "";
                    document.getElementById("cpwd").focus();
                    return false;
                }else{
                      document.getElementById("msg_msg").innerHTML="";
                }

              
                    document.signin.action = "online_pwd_process";
               
                                              }
                                          </script>
                                          
                                          <script type="text/javascript">
            function allowpositivenumber(e) {
                var charCode = (e.which) ? e.which : event.keyCode;

                if ((charCode < 46 || charCode > 57)) {
                    return false;
                }
                return true;
            }
        </script>
        
        
        
        <script type="text/javascript">
            function fill_pwd1(){
                 if(document.getElementById("npwd").value!="" && document.getElementById("npwd").value.length>0){
                   document.getElementById("n_pwd").hidden=true;
//                   document.getElementById("mborder").style.border = "thin solid transparent";
               }else{
                   document.getElementById("n_pwd").hidden=false;
//                   document.getElementById("mborder").style.border = "thin solid #FF0000";
               }
               
                
            }
        </script>
        
        <script type="text/javascript">
            function fill_pwd2(){
                 if(document.getElementById("cpwd").value!="" && document.getElementById("cpwd").value.length>0){
                   document.getElementById("c_pwd").hidden=true;
//                   document.getElementById("mborder").style.border = "thin solid transparent";
               }else{
                   document.getElementById("c_pwd").hidden=false;
//                   document.getElementById("mborder").style.border = "thin solid #FF0000";
               }
               
                if(document.getElementById("cpwd").value!="" && document.getElementById("cpwd").value.length>0){
                if (document.getElementById("npwd").value != document.getElementById("cpwd").value) {
//                    alert("New Password and Confirm Password Not Matching");
                    document.getElementById("msg_msg").innerHTML="New Password and Confirm Password Not Matching";
                    document.getElementById("cpwd").value = "";
                    document.getElementById("cpwd").focus();
                    return false;
                }else{
                      document.getElementById("msg_msg").innerHTML="";
                }
            }
            }
        </script>

  </head>
  <body>
  <section id="aa-signin">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="aa-signin-area">
            <div class="aa-signin-form">
              <div class="aa-signin-form-title">
                <!--<a class="aa-property-home" href="home">Property Home</a>-->
                <h4>Change Password</h4>
                 <%
                           String mob = request.getParameter("mob");
                  %> 
              </div>
              <form class="contactform" name="signin" method="post">                                                 
                <div class="aa-single-field">
                  <label for="email">Mobile No <span class="required">*</span></label>
                  <input type="hidden" name="mob" id="mob" value="<%=mob%>">
                  <input type="password"  aria-required="true"  placeholder="Password" name="npwd" id="npwd" autofocus maxlength="10" onblur="return fill_pwd1();">
                  <span style="color:red" id="n_pwd" hidden> Please Enter New Password</span>
                </div>
                <div class="aa-single-field">
                  <label for="password">Password <span class="required">*</span></label>
                  <input type="password" placeholder="Confirm Password" name="cpwd" id="cpwd" maxlength="20" onblur="return fill_pwd2();"> 
                  <span style="color:red" id="c_pwd" hidden>Please Repeat Your New Password </span>
                </div>
                  <span style="color:red" id="msg_msg"></span>
                
                <div class="aa-single-submit">
                    <input type="submit" value="Update Password" class="aa-browse-btn" name="submit" onclick="return form_validate();">  
                  <p>Don't Have a Account Yet? <a href="register">CREATE NOW!</a></p>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  
  <!-- jQuery library -->
  <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> -->
  <script src="js/jquery.min.js"></script>   
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="js/bootstrap.js"></script>   
  <!-- slick slider -->
  <script type="text/javascript" src="js/slick.js"></script>
  <!-- Price picker slider -->
  <script type="text/javascript" src="js/nouislider.js"></script>
   <!-- mixit slider -->
  <script type="text/javascript" src="js/jquery.mixitup.js"></script>
  <!-- Add fancyBox -->        
  <script type="text/javascript" src="js/jquery.fancybox.pack.js"></script>
  <!-- Custom js -->
  <script src="js/custom.js"></script> 
  
  </body>
</html>