<%-- 
    Document   : search_results
    Created on : 19 Jan, 2020, 8:26:57 AM
    Author     : Nandini
--%>

<%@page import="java.util.List"%>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Home Property | Search Results</title>

  <!-- Favicon -->
  <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">

  <!-- Font awesome -->
  <link href="css/font-awesome.css" rel="stylesheet">
  <!-- Bootstrap -->
  <link href="css/bootstrap.css" rel="stylesheet">
  <!-- slick slider -->
  <link rel="stylesheet" type="text/css" href="css/slick.css">
  <!-- price picker slider -->
  <link rel="stylesheet" type="text/css" href="css/nouislider.css">
  <!-- Fancybox slider -->
  <link rel="stylesheet" href="css/jquery.fancybox.css" type="text/css" media="screen" />
  <!-- Theme color -->
  <link id="switcher" href="css/theme-color/orange-theme.css" rel="stylesheet">

  <!-- Main style sheet -->
  <link href="css/style-main.css" rel="stylesheet">
  <link id="switcher" href="../../css/theme-color/orange-theme.css" rel="stylesheet">
  <link href="css/materialize.min.css" rel="stylesheet">

  <!-- Google Font -->
  <link href='https://fonts.googleapis.com/css?family=Vollkorn' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>


  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->


</head>

<body>
  <!-- Pre Loader -->
  <div id="aa-preloader-area">
    <div class="pulse"></div>
  </div>
  <!-- SCROLL TOP BUTTON -->
  <a class="scrollToTop" href="#"><i class="fa fa-angle-double-up"></i></a>
  <!-- END SCROLL TOP BUTTON -->


  <!-- Start header section -->
  <%@include file="../header.jsp" %>
  <!-- End header section -->
  <!-- Start menu section -->
  <div class="body-hold">
    
    <!-- End menu section -->

    <!-- Start Proerty header  -->
    <section class="parenthold">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-3">
                <div class="search-filter smooth-scroll">
                  <ul class="nav nav-tabs">
                    <li>
                      <div class="clearfix">
                        <div class="pull-left">
                          <label class="lbl-main">Basic Filter</label>
                        </div>
                        <div class="pull-right">
                          <a class="clear-text" routerlink="#" ng-reflect-router-link="#" href="/search-page/%23"><i
                              class="fa fa-undo"></i> Clear all</a>
                        </div>
                      </div>
                    </li>
                  </ul>
                  <div>
                    <form novalidate="" class=" ">
                      <div class="filterhold">
                        <div class="basic-filter" id="filter-home">
						
						
						<label class="lbl-list"></label>                     
                          <label class="mr-15">
						  <input name="tenants" type="checkbox" class="filled-in">
                          <span >Show Only With Photos</span>
						  </label><label class="mr-15">

                          <label class="lbl-list"> Tenant type</label>                     
                          <label class="mr-15">                            
                            <input name="tenants" type="checkbox" class="filled-in">
                            <span >Bachelors</span>
                          </label><label class="mr-15">
                            
                            <input name="tenants" type="checkbox" class="filled-in">
                            <span >Family</span>
                          </label><label class="mr-15">
                            
                            <input name="tenants" type="checkbox" class="filled-in">
                            <span >Any</span>
                          </label>

                          <div class="clear"></div>
						  
						  <label class="lbl-list">Property You Are Looking</label>
                          <label class="mr-15">                            
                            <input name="bhkLength" type="checkbox" class="filled-in">
                            <span >Independent Villa</span>
							</label><label class="mr-15">
							<input name="bhkLength" type="checkbox" class="filled-in">
                            <span >Apartment</span>
							</label><label class="mr-15">
							<input name="bhkLength" type="checkbox" class="filled-in">
                            <span >Gated Community</span>
							</label><label class="mr-15">
						  

                          <label class="lbl-list">Select BHK</label>
                          <label class="mr-15">                            
                            <input name="bhkLength" type="checkbox" class="filled-in">
                            <span >1 RK</span>
                          </label><label class="mr-15">                           
                            <input name="bhkLength" type="checkbox" class="filled-in">
                            <span >1 BHK</span>
                          </label><label class="mr-15">                          
                            <input name="bhkLength" type="checkbox"class="filled-in">
                            <span >2 BHK</span>
                          </label><label class="mr-15">                            
                            <input name="bhkLength" type="checkbox"  class="filled-in">
                            <span >3 BHK</span>
                          </label><label class="mr-15">                           
                            <input name="bhkLength" type="checkbox" class="filled-in">
                            <span >4 BHK</span>
                          </label><label class="mr-15">                            
                            <input name="bhkLength" type="checkbox"  class="filled-in">
                            <span >4+ BHK</span>
                          </label>
                          <label class="lbl-list">Furnishing</label>
                          <label class="mr-15">
                           
                            <input name="furnished" type="checkbox" class="filled-in">
                            <span > Unfurnished</span>
                          </label><label class="mr-15">
                            
                            <input name="furnished" type="checkbox"   class="filled-in">
                            <span >Semi Furnished</span>
                          </label><label class="mr-15">
                            
                            <input name="furnished" type="checkbox"   class="filled-in">
                            <span >Fully Furnished</span>
                          </label>

                        </div>
                        <div class="advancebtn" id="headingOne1" role="tab">
                          <a aria-controls="collapseOne1" aria-expanded="true" class="advance-filter"
                            data-parent="#accordionEx1" data-toggle="collapse" href="#collapseOne1"
                            target="filter-home">
                            Advanced Filters <i aria-hidden="true" class="fa fa-angle-double-up pull-right"></i>
                          </a>
                        </div>

                        <div class="advance-filter-hold" id="filter-home-advance-panel">

                          <div aria-multiselectable="true" class="accordion md-accordion" id="accordionEx1"
                            role="tablist">
                            <div aria-labelledby="headingOne1" class="collapse" data-parent="#accordionEx1"
                              id="collapseOne1" role="tabpanel">
                              <div class="pb-3">
                                <label class="lbl-list">Floor</label>
                                <label class="mr-15">
                                  
                                  <input name="floors" type="checkbox"  class="filled-in">
                                  <span >Ground</span>
                                </label><label class="mr-15">
                                  
                                  <input name="floors" type="checkbox"   class="filled-in">
                                  <span >First</span>
                                </label><label class="mr-15">
                                  
                                  <input name="floors" type="checkbox"   class="filled-in">
                                  <span >Second</span>
                                </label><label class="mr-15">
                                  
                                  <input name="floors" type="checkbox"  class="filled-in">
                                  <span >Third</span>
                                </label><label class="mr-15">
                                  
                                  <input name="floors" type="checkbox"   class="filled-in">
                                  <span >Fourth</span>
                                </label><label class="mr-15">
                                  
                                  <input name="floors" type="checkbox"    class="filled-in">
                                  <span >Fifth</span>
                                </label><label class="mr-15">
                                  
                                  <input name="floors" type="checkbox"   class="filled-in">
                                  <span >Any</span>
                                </label>

                                <div class="clear"></div>

                                <label class="lbl-list">Amenties</label>

                                <label class="mr-15">
                                  
                                  <input name="amenities" type="checkbox"  class="filled-in">
                                  <span >TV</span>
                                </label><label class="mr-15">
                                  
                                  <input name="amenities" type="checkbox"  class="filled-in">
                                  <span >FAN</span>
                                </label><label class="mr-15">
                                  
                                  <input name="amenities" type="checkbox"  class="filled-in">
                                  <span >Gyser</span>
                                </label><label class="mr-15">
                                  
                                  <input name="amenities" type="checkbox"  class="filled-in">
                                  <span >Power Backup</span>
                                </label>

                                <div class="clear"></div>

                                <label class="lbl-list checkbox">
                                  <input name="securityGuard" type="checkbox"  class="filled-in">
                                  <span >Wardrobe</span>
                                </label>

                                <div class="clear"></div>

                                <label class="lbl-list">Parking</label>
                                <label class="mr-15">
                                  
                                  <input name="parking" type="checkbox"  class="filled-in">
                                  <span >Bike</span>
                                </label><label class="mr-15">
                                  
                                  <input name="parking" type="checkbox"  class="filled-in">
                                  <span >Car</span>
                                </label>
                                <div class="clear"></div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
          </div>

          <div class="col-md-9 search-list">
              <%
                  if(session.getAttribute("dd_id")!=null){
                      List<String> dd_id = (List<String>) session.getAttribute("dd_id");
                      List<String> dd_street = (List<String>) session.getAttribute("dd_street");
                      List<String> dd_address = (List<String>) session.getAttribute("dd_address");
                      List<String> dd_floor = (List<String>) session.getAttribute("dd_floor");
                      List<String> dd_dep = (List<String>) session.getAttribute("dd_dep");
                      List<String> dd_rent = (List<String>) session.getAttribute("dd_rent");
                      List<String> dd_area = (List<String>) session.getAttribute("dd_area");
                      List<String> dd_fur = (List<String>) session.getAttribute("dd_fur");
                      List<String> dd_cstatus = (List<String>) session.getAttribute("dd_cstatus");
                      List<String> dd_img1 = (List<String>) session.getAttribute("dd_img1");
                      List<String> dd_img2 = (List<String>) session.getAttribute("dd_img2");
                      List<String> dd_img3 = (List<String>) session.getAttribute("dd_img3");
                      List<String> dd_img4 = (List<String>) session.getAttribute("dd_img4");
                      List<String> dd_img5 = (List<String>) session.getAttribute("dd_img5");
                      List<String> dd_img6 = (List<String>) session.getAttribute("dd_img6");
                      List<String> dd_name_num = (List<String>) session.getAttribute("dd_name_num");
                      List<String> dd_bhk = (List<String>) session.getAttribute("dd_bhk");
                      List<String> dd_btype = (List<String>) session.getAttribute("dd_btype");
                      List<String> dd_bin = (List<String>) session.getAttribute("dd_bin");
                      List<String> dd_adate1 = (List<String>) session.getAttribute("dd_adate1");
                      List<String> dd_srch_type = (List<String>) session.getAttribute("dd_srch_type");
                      List<String> dd_kyc_utype = (List<String>) session.getAttribute("dd_kyc_utype");
                      
                      for(int i=0;i<dd_id.size();i++){
                          String img1 = "",img2 = "",img3 = "",img4 = "",img5 = "",img6 = "";
                          if(dd_img1.get(i)!=null && dd_img1.get(i).length()>0){
                              img1 = "form_pics"+"/"+dd_img1.get(i);
                          }else{
                              img1 = "img/no-image.png";
                          }
                          
                        if(dd_img2.get(i)!=null && dd_img2.get(i).length()>0){
                              img2 = "form_pics"+"/"+dd_img2.get(i);
                          }else{
                              img2 = "img/no-image.png";
                          }
                          
                          if(dd_img3.get(i)!=null && dd_img3.get(i).length()>0){
                              img3 = "form_pics"+"/"+dd_img3.get(i);
                          }else{
                              img3 = "img/no-image.png";
                          }
                          
                          if(dd_img4.get(i)!=null && dd_img4.get(i).length()>0){
                              img4 = "form_pics"+"/"+dd_img4.get(i);
                          }else{
                              img4 = "img/no-image.png";
                          }
                          
                          if(dd_img5.get(i)!=null && dd_img5.get(i).length()>0){
                              img5 = "form_pics"+"/"+dd_img5.get(i);
                          }else{
                              img5 = "img/no-image.png";
                          }
                          
                          if(dd_img6.get(i)!=null && dd_img6.get(i).length()>0){
                              img6 = "form_pics"+"/"+dd_img6.get(i);
                          }else{
                              img6 = "img/no-image.png";
                          }
                  
              %>
            <div class="card bar-left-success">
              <div class="ribbon-hold green darken-1">
                <span>Available From : <%=dd_adate1.get(i)%></span>
              </div>
              <div class="ribbon-hold green darken-1 pull-left">
                <span>Posted By : <%=dd_kyc_utype.get(i)%></span>
              </div>
              <div class="property">
                <div class="card-body d-flex ">
                  <div class="aa-properties-details-img">
                <img src="<%=img1%>" style="width: 298px;height:230px" alt="img">
                <img src="<%=img2%>" style="width: 298px;height:230px" alt="img">
                <img src="<%=img3%>" style="width: 298px;height:230px" alt="img">
                <img src="<%=img4%>" style="width: 298px;height:230px" alt="img">
                <img src="<%=img5%>" style="width: 298px;height:230px" alt="img">
                <img src="<%=img6%>" style="width: 298px;height:230px" alt="img">
              </div>
                  <div class="prop-details">
                    <div class="list-card-title">
                      <span class="header">
                          <a href="#"> 
                            <%=dd_bhk.get(i)%> <br>In <%=dd_street.get(i)%>                             
                          </a>
                      </span>
                      <small class="block">
                        <i class="fa fa-map-marker"></i><%=dd_address.get(i)%>
                      </small>
                    </div>
                      <%
                          if(dd_srch_type.get(i).equalsIgnoreCase("PG")){
                      %>
                      <div class="row">
                          <div class="col-md-6 col-sm-6 col-xs-6 form-group">
                           <small>Rent</small>
                        <span class="d-block"> Rs.<%=dd_rent.get(i)%></span>
                        </div>
                        
                        <div class="col-md-6 col-sm-6 col-xs-6 form-group">
                           <small>Advance</small>
                        <span class="d-block"> Rs.<%=dd_dep.get(i)%></span>
                        </div>
                          
                          
                       
                    </div>
                      <%}else{%>
                      <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-6 form-group">
                           <small>Deposit</small>
                        <span class="d-block"> Rs.<%=dd_dep.get(i)%></span>
                        </div>
                          
                          <div class="col-md-6 col-sm-6 col-xs-6 form-group">
                           <small>Rent</small>
                        <span class="d-block"> Rs.<%=dd_rent.get(i)%></span>
                        </div>
                       
                    </div>
                        <%}%>
                        
                        <%
                          if(dd_srch_type.get(i).equalsIgnoreCase("PG") || dd_srch_type.get(i).equalsIgnoreCase("OFFICE") || dd_srch_type.get(i).equalsIgnoreCase("SHOPS") || dd_srch_type.get(i).equalsIgnoreCase("WAREHOUSE")){
                      %>
                     
                      <div class="row">
                        
                          
                          <div class="col-md-6 col-sm-6 col-xs-6 form-group">
                           <small>Furnishing</small>
                        <span class="d-block"><%=dd_fur.get(i)%></span>
                        </div>
                       
                    </div>
                      
                      <%}else{%>
                        <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-6 form-group">
                           <small>Plot area</small>
                          <span class="d-block"><%=dd_area.get(i)%> <%=dd_bin.get(i)%> <%=dd_btype.get(i)%></span>
                        </div>
                          
                          <div class="col-md-6 col-sm-6 col-xs-6 form-group">
                           <small>Furnishing</small>
                        <span class="d-block"><%=dd_fur.get(i)%></span>
                        </div>
                       
                    </div>
                        <%}%>
                    
                    <div class="list-action mt-15 text-right">
                     
                      <a href="view_search_details?id=<%=dd_id.get(i)%>&stype=<%=dd_srch_type.get(i)%>" title="Click to View Details" class="btn btn-common mb-0 ml-0 pull-left">View Details</a>
                    </div>
                    
                    
                  </div>
                </div>
              </div>
                    <br><br>
                    <div class="row">
                        <div class="col-md-4 col-sm-4 col-xs-4 form-group">
                            <a href="#" title="Save Propery" class="shortlist" style="color:#ff871c"> <i class="fa fa-home"></i></a>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-4 form-group">
                            <center>
                            <a href="#" title="Complaint about Propery" class="shortlist" style="color:#ff871c"> <i class="fa fa-flag"></i></a>
                            </center>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-4 form-group">
                            <a href="#" title="Share Details" class="shortlist pull-right" style="color:#ff871c"> <i class="fa fa-share"></i></a>
                        </div>
                    </div>
            </div>
            <%
                }
                session.removeAttribute("dd_id");
                session.removeAttribute("dd_street");
                session.removeAttribute("dd_address");
                session.removeAttribute("dd_floor");
                session.removeAttribute("dd_dep");
                session.removeAttribute("dd_rent");
                session.removeAttribute("dd_area");
                session.removeAttribute("dd_fur");
                session.removeAttribute("dd_cstatus");
                session.removeAttribute("dd_img");
                session.removeAttribute("dd_name_num");
                session.removeAttribute("dd_bhk");
                session.removeAttribute("dd_bin");
                session.removeAttribute("dd_btype");
                session.removeAttribute("dd_adate1");
                session.removeAttribute("dd_srch_type");
                }
            %>
          </div>
          <div class="col-md-2">

          </div>
        </div>
      </div>
    </section>
    <!-- End Proerty header  -->

  </div>
  <!-- Footer -->
  <%@include file="../footer.jsp" %>
  <!-- / Footer -->

  <!-- jQuery library -->
  <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> -->
  <script src="js/jquery.min.js"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="js/bootstrap.js"></script>
  <!-- slick slider -->
  <script type="text/javascript" src="js/slick.js"></script>
  <!-- Price picker slider -->
  <script type="text/javascript" src="js/nouislider.js"></script>
  <!-- mixit slider -->
  <script type="text/javascript" src="js/jquery.mixitup.js"></script>
  <!-- Add fancyBox -->
  <script type="text/javascript" src="js/jquery.fancybox.pack.js"></script>
  <script src="js/materialize.min.js"></script>
  <!-- Custom js -->
  <script src="js/custom.js"></script>
  <script>
    $(document).ready(function () {
      $('.body-hold').css('min-height', $(window).height() - 198);
      // Comma, not colon ----^
    });
    $(window).resize(function () {
      $('.body-hold').css('min-height', $(window).height() - 198);
      // Comma, not colon ----^
    });

    $("#example-basic").steps({
      headerTag: "h3",
      bodyTag: "section",
      transitionEffect: "slideLeft",
      autoFocus: true
    });

  </script>
</body>

</html>
