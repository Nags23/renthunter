<%-- 
    Document   : add_areas
    Created on : 25 Apr, 2020, 11:44:51 AM
    Author     : Nandini
--%>

<%@page import="java.util.List"%>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Home Property | Add Areas</title>

        <!-- Favicon -->
        <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">

        <!-- Font awesome -->
        <link href="css/font-awesome.css" rel="stylesheet">
        <!-- Bootstrap -->
        <link href="css/bootstrap.css" rel="stylesheet">
        <!-- slick slider -->
        <link rel="stylesheet" type="text/css" href="css/slick.css">
        <!-- price picker slider -->
        <link rel="stylesheet" type="text/css" href="css/nouislider.css">
        <!-- Fancybox slider -->
        <link rel="stylesheet" href="css/jquery.fancybox.css" type="text/css" media="screen" />
        <!-- Theme color -->
        <link id="switcher" href="css/theme-color/orange-theme.css" rel="stylesheet">

        <!-- Main style sheet -->
        <link href="css/style-main.css" rel="stylesheet">
        <link href="css/materialize.min.css" rel="stylesheet">

        <!-- Google Font -->
        <link href='https://fonts.googleapis.com/css?family=Vollkorn' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>


        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
          <![endif]-->



        <script type="text/javascript">
            function allowpositivenumber(e) {
                var charCode = (e.which) ? e.which : event.keyCode;

                if ((charCode < 46 || charCode > 57)) {
                    return false;
                }
                return true;
            }
        </script>

        <script type="text/javascript">
            function form_validate() {
                if (document.getElementById("sname").value == "") {
                    document.getElementById("a_sname").hidden = false;
                    document.getElementById("sname").focus();
                    return false;
                } else {
                    document.getElementById("a_sname").hidden = true;
                }
                
                if (document.getElementById("cname").value == "") {
                    document.getElementById("a_cname").hidden = false;
                    document.getElementById("cname").focus();
                    return false;
                } else {
                    document.getElementById("a_cname").hidden = true;
                }
                
                if (document.getElementById("aname").value == "") {
                    document.getElementById("a_aname").hidden = false;
                    document.getElementById("aname").focus();
                    return false;
                } else {
                    document.getElementById("a_aname").hidden = true;
                }
                
              

                var nn = confirm("Are you sure to update details?");
                if(nn==true){
                document.masters.action = "add_areas_process";
            }else{
                return false;
            }
            }
        </script>
         <script type="text/javascript">
            function del_con(){
                var nn = confirm("Are you sure to delete this entry?");
                if(nn==true){
                    return true;
                }else{
                    return false;
                }
            }
        </script>
        
        <script language="javascript" type="text/javascript">
            var xmlHttp
            var xmlHttp
            function fill_city(str) {
                if (typeof XMLHttpRequest != "undefined") {
                    xmlHttp = new XMLHttpRequest();
                }
                else if (window.ActiveXObject) {
                    xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                if (xmlHttp == null) {
                    alert("Browser does not support XMLHTTP Request")
                    return;
                }
                var url = "city_fetch";
                url += "?count=" + encodeURIComponent(document.getElementById("sname").value);
                xmlHttp.onreadystatechange = stateChange3;
                xmlHttp.open("GET", url, true);
                xmlHttp.send(null);
            }

            function stateChange3() {
                if (xmlHttp.readyState == 4 || xmlHttp.readyState == "complete") {
            //alert(xmlHttp.responseText);     
            //document.getElementById("cname").hidden = true;
            //document.getElementById("cc").innerHTML = xmlHttp.responseText.trim();
            //document.getElementById("load").innerHTML = "<div class='col-md-3 col-sm-12 col-xs-12 form-group'><div class='input-field'><select name='cname' id='cname' class='form-control'><option value=''></option></select><label for='u_name'>City Name<span class='required'>*</span></label></div><span style='color:red' id='a_cname' hidden> Please Enter City Name</span></div>";
            document.getElementById("cname").innerHTML  = xmlHttp.responseText;
            }
        }
        </script>
        
        <script type="text/javascript">
            function edit_det(n,id){
               
                var ful_string = n;
                //alert(n);
                document.getElementById("id").value=ful_string.split("nagsnn")[0];
                document.getElementById("sname").value=ful_string.split("nagsnn")[1];
                document.getElementById("sname").text=ful_string.split("nagsnn")[2];
                
                document.getElementById("cname").value=ful_string.split("nagsnn")[3];
                document.getElementById("cname").text=ful_string.split("nagsnn")[4];
                document.getElementById("aname").value=ful_string.split("nagsnn")[5];
                return fill_city("a");
                if(ful_string.split("nagsnn")[6].trim()=="n"){
                    
                    document.getElementById("chk").checked=true;
                }else{
                   
                    document.getElementById("chk").checked=false;
                    }
                
               
                window.scroll(0,0);
            }
        </script>
        
        <script type="text/javascript">
            function form_validate_upload_1() {
                 if(document.getElementById("file2").value==""){
                    document.getElementById("a_file2").hidden = false;
                    return false;
                }else{
                    document.getElementById("a_file2").hidden = true;
                }
                 var n=document.getElementById("file2").value;
                 var nn=n.split(".");
                 //alert(nn[1]);
                 if(nn[1]=="csv"){
                     //document.getElementById("file").value = "";
                     var nn = confirm("Are You Sure to Upload?");
                     if(nn == true){
                    document.nnn.action  = "upload_areas";
                     }else{
                         return false;
                     }
                 }else{
                     alert("Please Choose CSV Sheet to Upload");
                     return false;
                 }
            }
        </script> 
        
        <script type="text/javascript">
            function proceed_1() {
              document.nnn.action = "download_areas.jsp";
                
            }
        </script>
        
        

    </head>

    <body>
        <!-- Pre Loader -->
        <div id="aa-preloader-area" style="display: none;">
            <div class="pulse"></div>
        </div>
        <!-- SCROLL TOP BUTTON -->
        <a class="scrollToTop" href="#"><i class="fa fa-angle-double-up"></i></a>
        <!-- END SCROLL TOP BUTTON -->


        <!-- Start header section -->
        <%@include file="../header.jsp" %>
        <!-- End header section -->
        <!-- Start menu section -->
        <div class="body-hold">

            <!-- End menu section -->


            <form name="masters" method="post>"
                  <!-- Start Proerty header  -->
                  <section class="parenthold">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 user-in">
                                <div class="row">

                                    <div class="col-md-12">
                                        <div class="tab-content card mt-0 p-25">
                                            <div id="menu0" class="tab-pane fade in active">
                                                <%
                                                    if (session.getAttribute("insert") != null) {
                                                %>


                                                <%
                                                    if (session.getAttribute("insert").equals("pass")) {
                                                %>
                                                <div class="alert alert-block alert-success col-lg-12 ">
                                                    <button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">�</span></button>
                                                    <strong> Details Updated Successfully.</strong> 


                                                </div>
                                                <%} else if (session.getAttribute("insert").equals("fail")) {%>
                                                <div class="alert alert-block alert-danger col-lg-12 ">
                                                    <button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">�</span></button>
                                                    <strong> Creation Failed Please Try Again.</strong> 
                                                </div>
                                                <%} else if (session.getAttribute("insert").equals("exists")) {%>
                                                <div class="alert alert-block alert-success col-lg-12 ">
                                                    <button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">�</span></button>
                                                    <strong>  Already Exists in Database.</strong>
                                                </div>
                                                <%} else if (session.getAttribute("insert").equals("delpass")) {%>
                                                <div class="alert alert-block alert-success col-lg-12 ">
                                                    <button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">�</span></button>
                                                    <strong>  Deleted Successfully from all Records</strong>
                                                </div>
                                                <%} else if (session.getAttribute("insert").equals("delfail")) {%>
                                                <div class="alert alert-block alert-success col-lg-12 ">
                                                    <button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">�</span></button>
                                                    <strong>Deletion Failed.</strong>
                                                </div>
                                                <%} else if (session.getAttribute("insert").equals("updatepass")) {%>
                                                <div class="alert alert-block alert-success col-lg-12 ">
                                                    <button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">�</span></button>
                                                    <strong>  Updated Successfully</strong>
                                                </div>
                                                <%} else if (session.getAttribute("insert").equals("updatefail")) {%>
                                                <div class="alert alert-block alert-success col-lg-12 ">
                                                    <button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">�</span></button>
                                                    <strong>Update Failed Pls Try Again</strong>
                                                </div>

                                                <%}%>
                                                <%
                                                        session.removeAttribute("insert");
                                                    }%>
                                                <h3>Add Areas</h3>
                                                <div class="row">
                                                    <div class="col-md-3 col-sm-12 col-xs-12 form-group default-select">
                                        <!--<div class="input-field">-->
                                        <input type="hidden" name="id" id="id">
                                        <select name="sname" id="sname" class="form-control" onchange="return fill_city();" style="    border-width: 0 0 1px 0 !important;box-shadow: none;border-radius: 0 !important;padding: 10px 10px;height: 46px;border-color: #585858;margin-top: 14px;">
                                              
                                         <option selected></option>
                                         <%

                            if (session.getAttribute("dd_id") != null) {
                                List<String> dd_id = (List<String>) session.getAttribute("dd_id");
                                List<String> dd_sname = (List<String>) session.getAttribute("dd_sname");
                        %>
                        <%
                             for (int i = 0; i < dd_id.size(); i++) {
                        %>
                        <option value="<%=dd_id.get(i)%>"><%=dd_sname.get(i)%></option>
                        <%}%>
                        <%
                            session.removeAttribute("dd_id");
                            session.removeAttribute("dd_sname");
                            }
                        %>
                                    </select>
                                    <label>Select State <span class="required">*</span></label>
                                        <!--</div>-->
                                        <span style="color:red" id="a_sname" hidden> Please Select State</span>
                                    </div>
                                                    
                                    <div class="col-md-3 col-sm-12 col-xs-12 form-group default-select">
                                        <!--<div class="input-field">-->
                                            <select name="cname" id="cname" class="form-control" style="    border-width: 0 0 1px 0 !important;
    box-shadow: none;
    border-radius: 0 !important;
    padding: 10px 10px;
    height: 46px;
    border-color: #585858;
    margin-top: 14px;">
                                                <option value="">Select City Name</option>
                                            </select>
                                            <label for="u_name">Select City<span class="required">*</span></label>
                                        <!--</div>-->
                                        <span style="color:red" id="a_cname" hidden> Please Enter City Name</span>
                                    </div>
                                                    
                                                    <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                                        <div class="input-field">
                                                            <input type="text" name="aname" id="aname" maxlength="75"  placeholder="Enter Area Name">
                                                            <label for="u_name" style="top:0">Area Name
                                                                <span class="required">*</span></label>
                                                        </div>
                                                        <span style="color:red" id="a_aname" hidden> Please Enter Area Name</span>
                                                    </div>
                                                    

                                                   <div class="col-md-3 col-sm-12 col-xs-12 form-group">
                                                        <div class="aa-single-field">
                                                           <label class="checkbox">Click to Enable
                    <input type="checkbox" formControlName="petsAllowed" name="chk" id="chk" value="n" checked>
                    <span class="checkmark"></span>
                  </label>                                                       
                                                        </div> 
                                                    </div> 
                                                </div> 

                                                <button class="default-btn" type="submit" onclick="return form_validate();">Update Area</button>
                                            </div>


                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
        </form>
                                    
                                    <form name="nnn" method="post" enctype="multipart/form-data">
                                                     <section class="parenthold">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 user-in">
                                <div class="row">

                                    <div class="col-md-12">
                                        <div class="tab-content card mt-0 p-25">
                                            <div id="menu0" class="tab-pane fade in active">
                                               
                                                <h3>Upload Areas</h3>
                                                <div class="row">
                                                    <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                                                        <label for="exampleInputEmail1">Choose File</label><br>
                                                       <input type="file" name="file2" id="file2" class="form-control">
                                                        <span style="color:red" id="a_file2" hidden> Please Select CSV File</span>
                                                       
                                                    </div>
                                                    
                                                    <div class="col-md-6">
                                                        <label for="exampleInputEmail1">Get Upload Format</label><br>
                                                        <button type="submit" class="btn btn-danger"  onclick="return proceed_1();">Get File</button><br>
                                                        <span style="color:red;font-weight: bold">Download File and delete all contents except header(Delete Special Characters if found)then fill products and upload.</span>
                                                    </div>

                                                    
                                                </div> 

                                                <button class="default-btn" type="submit" onclick="return form_validate_upload_1();">Upload File</button>
                                            </div>


                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                                                 </form>
                                                
                                                <section class="parenthold">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 user-in">
                                <div class="row">

                                    <div class="col-md-12">
                                        <div class="tab-content card mt-0 p-25">
                                            <div id="menu0" class="tab-pane fade in active">
                                                
                                                <h3>Area Details</h3>
                                                <div id="datagrid" style="overflow:auto; overflow-x: scroll; overflow-y:hidden; width:100%; height:130%">
                                    <table width="100%" class="display table-bordered" id="example2" cellspacing="0" border="1">
                                        <thead>
                                            <tr class="headings" style="color:#fff;background-color: #000">
                                                  <th>Slno</th>
                                                <th>Edit</th>
                                                <th>Delete</th>
                                                <th>State Name</th>
                                                <th>City Name</th>
                                                <th>Area Name</th>
                                                <th>Enable/Disable</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                             <%

                            if (session.getAttribute("ss_id") != null) {
                                List<String> ss_id = (List<String>) session.getAttribute("ss_id");
                                List<String> ss_sid = (List<String>) session.getAttribute("ss_sid");
                                List<String> ss_sname = (List<String>) session.getAttribute("ss_sname");
                                List<String> ss_cid = (List<String>) session.getAttribute("ss_cid");
                                List<String> ss_cname = (List<String>) session.getAttribute("ss_cname");
                                List<String> ss_aname = (List<String>) session.getAttribute("ss_aname");
                                List<String> ss_disabled = (List<String>) session.getAttribute("ss_disabled");
                        %>
                        <%
                             for (int i = 0; i < ss_id.size(); i++) {
                        %>
                                            <tr>
                                                <td><%=i+1%></td>
                                                <td>
                                                   
                 <label class="radiobtn mr-4 mb-0">
                    <input type="radio" name="parking" formControlName="parking"  id="<%=ss_id.get(i)%>" value="<%=ss_id.get(i)%>nagsnn<%=ss_sid.get(i)%>nagsnn<%=ss_sname.get(i)%>nagsnn<%=ss_cid.get(i)%>nagsnn<%=ss_cname.get(i)%>nagsnn<%=ss_aname.get(i)%>nagsnn<%=ss_disabled.get(i)%>" onclick="return edit_det(this.value,this.id);">Edit
                    <span class="checkmark"></span>
                  </label>
                                                </td>
                                                <td><a href="add_area_del?id=<%=ss_id.get(i)%>" title="Click to Delete" onclick="return del_con();"><i class="fa fa-trash" style="color:red"></i></a></td>
                                                
                                                
                                                <td><%=ss_sname.get(i)%></td>
                                                <td><%=ss_cname.get(i)%></td>
                                                <td><%=ss_aname.get(i)%></td>
                                                <td>
                                                    <%
                                                        if(ss_disabled.get(i).equalsIgnoreCase("n")){
                                                    %>
                                                    <p style="color:green">Enabled</p>
                                                    <%}else{%>
                                                    <p style="color:red">Disabled</p>
                                                    <%}%>
                                                </td>
                                                 
                                               
                                            </tr>
                                            <%}
                             session.removeAttribute("ss_id");
                             session.removeAttribute("ss_sid");
                             session.removeAttribute("ss_sname");
session.removeAttribute("ss_cid");
session.removeAttribute("ss_cname");
session.removeAttribute("ss_aname");
session.removeAttribute("ss_disabled");
                            
                            }
                            %>
                                        </tbody>

                                    </table>
                                </div>

                                            </div>


                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </form>
            <!-- End Proerty header  -->

        </div>
        <!-- Footer -->
        <%@include file="../footer.jsp" %>
        <!-- / Footer -->

        <!-- jQuery library -->
        <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> -->
        <script src="js/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="js/bootstrap.js"></script>
        <!-- slick slider -->
        <script type="text/javascript" src="js/slick.js"></script>
        <!-- Price picker slider -->
        <script type="text/javascript" src="js/nouislider.js"></script>
        <!-- mixit slider -->
        <script type="text/javascript" src="js/jquery.mixitup.js"></script>
        <!-- Add fancyBox -->
        <script type="text/javascript" src="js/jquery.fancybox.pack.js"></script>
        <script src="js/materialize.min.js"></script>
        <!-- Custom js -->
        <script src="js/custom.js"></script>
        <script>
            $(document).ready(function () {
                $('.body-hold').css('min-height', $(window).height() - 208);
                // Comma, not colon ----^
            });
            $(window).resize(function () {
                $('.body-hold').css('min-height', $(window).height() - 208);
                // Comma, not colon ----^
            });

            $("#example-basic").steps({
                headerTag: "h3",
                bodyTag: "section",
                transitionEffect: "slideLeft",
                autoFocus: true
            });

        </script>
        
        
<link rel="stylesheet" type="text/css" href="media/css/demo_page.css" />
<link rel="stylesheet" type="text/css" href="media/css/demo_table_jui.css" />
<link rel="stylesheet" type="text/css" href="examples/examples_support/themes/smoothness/jquery-ui-1.8.4.custom.css" />
<link rel="stylesheet" type="text/css" href="media/css/TableTools_JUI.css" />



        <%--<script type="text/javascript" charset="utf-8" src="media/js/jquery.js"></script>--%>
        <script type="text/javascript" charset="utf-8" src="media/js/jquery.dataTables.js"></script>
        <script type="text/javascript" charset="utf-8" src="media/js/ZeroClipboard.js"></script>
        <script type="text/javascript" charset="utf-8" src="media/js/TableTools.js"></script>
        <script type="text/javascript" charset="utf-8">
                $(document).ready( function () {
                        $('#example2').dataTable( {
                                "bJQueryUI": true,
                                "sPaginationType": "full_numbers",
                                 "sDom": 'T<"clear"><"H"lfr>t<"F"ip>',
//                                 "iDisplayLength": -1,
                                "oTableTools": {
                                        "aButtons": [
                                            // "xls"
                                                //"copy", "csv", "xls", "pdf","print"//,
                                                //{
                                                //	"sExtends":    "collection",
                                                //	"sButtonText": "Save",
                                                //	"aButtons":    [ "csv", "xls", "pdf" ]
                                                //}
                                        ]
                                }
                        } );
                        
                        
                        
                } );
        </script>
        
        <script>
       

        $(document).ready(function () {
            $('select').formSelect();
        });

    </script>
    </body>

</html>
