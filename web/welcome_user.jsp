<%-- 
    Document   : welcome_user
    Created on : 3 Dec, 2019, 8:38:09 PM
    Author     : Nandini
--%>
<%@page import="java.util.List"%>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Home Property | Dashboard</title>

  <!-- Favicon -->
  <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">

  <!-- Font awesome -->
  <link href="css/font-awesome.css" rel="stylesheet">
  <!-- Bootstrap -->
  <link href="css/bootstrap.css" rel="stylesheet">
  <!-- slick slider -->
  <link rel="stylesheet" type="text/css" href="css/slick.css">
  <!-- price picker slider -->
  <link rel="stylesheet" type="text/css" href="css/nouislider.css">
  <!-- Fancybox slider -->
  <link rel="stylesheet" href="css/jquery.fancybox.css" type="text/css" media="screen" />
  <!-- Theme color -->
  <link id="switcher" href="css/theme-color/orange-theme.css" rel="stylesheet">

  <!-- Main style sheet -->
  <link href="css/style-main.css" rel="stylesheet">
  <link href="css/materialize.min.css" rel="stylesheet">

  <!-- Google Font -->
  <link href='https://fonts.googleapis.com/css?family=Vollkorn' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>


  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  <script type="text/javascript">
            function allowpositivenumber(e) {
                var charCode = (e.which) ? e.which : event.keyCode;

                if ((charCode < 46 || charCode > 57)) {
                    return false;
                }
                return true;
            }
        </script>
        
  
  <script type="text/javascript">
      function form_validate_profile(){
          if(document.getElementById("u_name").value==""){
              document.getElementById("a_u_name").hidden = false;
              document.getElementById("u_name").focus();
              return false;
          }else{
              document.getElementById("a_u_name").hidden = true;
          }
          
          if(document.getElementById("u_email").value==""){
              document.getElementById("a_u_email").hidden = false;
              document.getElementById("u_email").focus();
              return false;
          }else{
              document.getElementById("a_u_email").hidden = true;
          }
          
          if(document.getElementById("u_number").value==""){
              document.getElementById("a_u_number").hidden = false;
              document.getElementById("u_number").focus();
              return false;
          }else{
              document.getElementById("a_u_number").hidden = true;
          }
          
          document.renthunter_dash.action = "profile_update_process";
      }
  </script>

</head>

<body>
  <!-- Pre Loader -->
  <div id="aa-preloader-area" style="display: none;">
    <div class="pulse"></div>
  </div>
  <!-- SCROLL TOP BUTTON -->
  <a class="scrollToTop" href="#"><i class="fa fa-angle-double-up"></i></a>
  <!-- END SCROLL TOP BUTTON -->


  <!-- Start header section -->
  <%@include file="header.jsp" %>
  <!-- End header section -->
  <!-- Start menu section -->
  <div class="body-hold">
    
    <!-- End menu section -->

    <!-- Start Proerty header  -->
    <section class="parenthold">
      <div class="container">
        <div class="row">
          <div class="col-md-12 user-in">
            <div class="row">
              <div class="col-md-3">
                <div class="card w-100 mt-0">

                
                  <ul class="nav nav-tabs">
                      <li class="active"><a data-toggle="tab" href="#menu0">Home</a></li>
                      <li><a data-toggle="tab" href="#menu1">My Properties</a></li>
                      <li><a data-toggle="tab" href="#menu2">My Plan</a></li>
                      <li><a data-toggle="tab" href="#menu3">My Offers</a></li>
                      <li><a data-toggle="tab" href="#menu4">My Rewards</a></li>
              
                      
                      <li><a data-toggle="tab" href="#menu5">Invite and earn</a></li>
                      <li><a data-toggle="tab" href="#menu6">Chat with us</a></li>
                    </ul>
                  </div>
              </div>
                <%
                    String dash_fname = "",dash_email = "",dash_mob = "";
                    if(session.getAttribute("dash_fname")!=null){
                        dash_fname = session.getAttribute("dash_fname").toString();
                        session.removeAttribute("dash_fname");
                    }
                    if(session.getAttribute("dash_email")!=null){
                        dash_email = session.getAttribute("dash_email").toString();
                        session.removeAttribute("dash_email");
                    }
                    if(session.getAttribute("dash_mob")!=null){
                        dash_mob = session.getAttribute("dash_mob").toString();
                        session.removeAttribute("dash_mob");
                    }
                %>
              <div class="col-md-9">
                  
                  <div class="tab-content card mt-0 p-25">
                      <div id="menu0" class="tab-pane fade in active">
                        <h3>Edit your profile</h3>
                        <%
                                                    if (session.getAttribute("insert") != null) {
                                                %>


                                                <%
                                                    if (session.getAttribute("insert").equals("pass")) {
                                                %>
                                                <div class="alert alert-block alert-success col-lg-12 ">
                                                    <button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">�</span></button>
                                                    <strong> Details Updated Successfully.</strong> 


                                                </div>
                                                <%} else if (session.getAttribute("insert").equals("fail")) {%>
                                                <div class="alert alert-block alert-danger col-lg-12 ">
                                                    <button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">�</span></button>
                                                    <strong> Creation Failed Please Try Again.</strong> 
                                                </div>
                                                <%} else if (session.getAttribute("insert").equals("exists")) {%>
                                                <div class="alert alert-block alert-success col-lg-12 ">
                                                    <button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">�</span></button>
                                                    <strong>  Already Exists in Database.</strong>
                                                </div>
                                                <%} else if (session.getAttribute("insert").equals("delpass")) {%>
                                                <div class="alert alert-block alert-success col-lg-12 ">
                                                    <button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">�</span></button>
                                                    <strong>  Deleted Successfully from all Records</strong>
                                                </div>
                                                <%} else if (session.getAttribute("insert").equals("delfail")) {%>
                                                <div class="alert alert-block alert-success col-lg-12 ">
                                                    <button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">�</span></button>
                                                    <strong>Deletion Failed.</strong>
                                                </div>
                                                <%} else if (session.getAttribute("insert").equals("updatepass")) {%>
                                                <div class="alert alert-block alert-success col-lg-12 ">
                                                    <button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">�</span></button>
                                                    <strong>  Updated Successfully</strong>
                                                </div>
                                                <%} else if (session.getAttribute("insert").equals("updatefail")) {%>
                                                <div class="alert alert-block alert-success col-lg-12 ">
                                                    <button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">�</span></button>
                                                    <strong>Update Failed Pls Try Again</strong>
                                                </div>

                                                <%}%>
                                                <%
                                                        session.removeAttribute("insert");
                                                    }%>
                        <form name="renthunter_dash" method="post">
                        <div class="formhold">
                            <div class="input-field">
                                <input id="u_name" name="u_name" type="text" class="validate" value="<%=dash_fname%>" maxlength="50">
                                <label for="u_name">Name <span
                                        class="required">*</span></label>
                            </div>
                                <span id="a_u_name" style="color:red" hidden>Please Enter Your Name</span>
                            <div class="input-field">
                                <input id="u_email" name="u_email" type="email" class="validate" value="<%=dash_email%>" maxlength="50">
                                <label for="u_email">Email <span
                                        class="required">*</span></label>
                            </div>
                                <span id="a_u_email" style="color:red" hidden>Please Enter Email Id</span>
                            <div class="input-field">
                                <input id="u_number" name="u_number" type="tel" class="validate" value="<%=dash_mob%>" redonly maxlength="10" onkeypress="return allowpositivenumber(event);">
                                <label for="u_number">Mobile Number <span
                                        class="required">*</span></label>
                            </div>
                                <span id="a_u_number" style="color:red" hidden>Please Enter Mobile No</span>
                                <button class="default-btn" type="submit" onclick="return form_validate_profile();">Save profile</button>
                        </div>
                        </form>
                      </div>
                      <div id="menu1" class="tab-pane fade">
                        <h3>My Properties</h3>
                        <p>
                        <%
                  if(session.getAttribute("dd_id")!=null){
                      List<String> dd_id = (List<String>) session.getAttribute("dd_id");
                      List<String> dd_street = (List<String>) session.getAttribute("dd_street");
                      List<String> dd_address = (List<String>) session.getAttribute("dd_address");
                      List<String> dd_floor = (List<String>) session.getAttribute("dd_floor");
                      List<String> dd_dep = (List<String>) session.getAttribute("dd_dep");
                      List<String> dd_rent = (List<String>) session.getAttribute("dd_rent");
                      List<String> dd_area = (List<String>) session.getAttribute("dd_area");
                      List<String> dd_fur = (List<String>) session.getAttribute("dd_fur");
                      List<String> dd_cstatus = (List<String>) session.getAttribute("dd_cstatus");
                      List<String> dd_img = (List<String>) session.getAttribute("dd_img");
                      
                      for(int i=0;i<dd_id.size();i++){
                          String img = "";
                          if(dd_img.get(i)!=null && dd_img.get(i).length()>0){
                              img = "form_pics"+"/"+dd_img.get(i);
                          }else{
                              img = "img/no-image.png";
                          }
                  
              %>
            <div class="card bar-left-success">
              <div class="ribbon-hold green darken-1">
                <span><%=dd_cstatus.get(i)%></span>
              </div>
              <div class="property">
                <div class="card-body d-flex ">
                  <div class="prop-img">
                    <img src="<%=img%>" alt="img" width="200">
                  </div>
                  <div class="prop-details">
                    <div class="list-card-title">
                      <span class="header">
                          <a href="#"> 
                            <%=dd_floor.get(i)%> In <%=dd_street.get(i)%>                             
                            <small><i class="fa fa-external-link">  </i></small></a>
                      </span>
                      <small class="block">
                        <i class="fa fa-map-marker"></i><%=dd_address.get(i)%>
                      </small>
                    </div>
                    <div class="list-detail my-2">
                      <div class="block-1">
                        <small>Deposit</small>
                        <span class="d-block"> <%=dd_dep.get(i)%></span>
                      </div>
                      <div class="block-1">
                        <small>Rent</small>
                        <span class="d-block"> <%=dd_rent.get(i)%></span>
                      </div>
                      <div class="block-1">
                          <small>Plot area</small>
                          <span class="d-block"><%=dd_area.get(i)%> Sq.Ft</span>
                        </div>
                      <div class="block-1">
                        <small>Furnishing</small>
                        <span class="d-block"><%=dd_fur.get(i)%></span>
                      </div>
                    </div>
                    <div class="list-action mt-15 text-right">
                      <%
                          if(dd_cstatus.get(i).equalsIgnoreCase("Home")){
                      %>
                      <a href="home_edit_client?id=<%=dd_id.get(i)%>" title="Click to Edit Details" class="btn btn-common mb-0 ml-0 pull-left">Edit Details</a>
                      <%}%>
                       <%
                          if(dd_cstatus.get(i).equalsIgnoreCase("Apartment")){
                      %>
                      <a href="apartment_edit_client?id=<%=dd_id.get(i)%>" title="Click to Edit Details" class="btn btn-common mb-0 ml-0 pull-left">Edit Details</a>
                   <%}%>
                   <%
                          if(dd_cstatus.get(i).equalsIgnoreCase("PG")){
                      %>
                      <a href="pg_edit_client?id=<%=dd_id.get(i)%>" title="Click to Edit Details" class="btn btn-common mb-0 ml-0 pull-left">Edit Details</a>
                   <%}%>
                   <%
                          if(dd_cstatus.get(i).equalsIgnoreCase("Office")){
                      %>
                      <a href="office_edit_client?id=<%=dd_id.get(i)%>" title="Click to Edit Details" class="btn btn-common mb-0 ml-0 pull-left">Edit Details</a>
                   <%}%>
                   <%
                          if(dd_cstatus.get(i).equalsIgnoreCase("Shop")){
                      %>
                      <a href="shop_edit_client?id=<%=dd_id.get(i)%>" title="Click to Edit Details" class="btn btn-common mb-0 ml-0 pull-left">Edit Details</a>
                   <%}%>
                   <%
                          if(dd_cstatus.get(i).equalsIgnoreCase("Warehouse")){
                      %>
                      <a href="warehouse_edit_client?id=<%=dd_id.get(i)%>" title="Click to Edit Details" class="btn btn-common mb-0 ml-0 pull-left">Edit Details</a>
                   <%}%>
                   <%
                          if(dd_cstatus.get(i).equalsIgnoreCase("Specials")){
                      %>
                      <a href="specials_edit_client?id=<%=dd_id.get(i)%>" title="Click to Edit Details" class="btn btn-common mb-0 ml-0 pull-left">Edit Details</a>
                   <%}%>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <%
                }
                session.removeAttribute("dd_id");
                session.removeAttribute("dd_street");
                session.removeAttribute("dd_address");
                session.removeAttribute("dd_floor");
                session.removeAttribute("dd_dep");
                session.removeAttribute("dd_rent");
                session.removeAttribute("dd_area");
                session.removeAttribute("dd_fur");
                session.removeAttribute("dd_cstatus");
                session.removeAttribute("dd_img");
                }
            %>
                      </p>
                      </div>
                      <div id="menu2" class="tab-pane fade">
                        <h3>Menu 2</h3>
                        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>
                      </div>
                      <div id="menu3" class="tab-pane fade">
                        <h3>Menu 3</h3>
                        <p>Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                      </div>
                      <div id="menu4" class="tab-pane fade">
                          <h3>Menu 1</h3>
                          <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                        </div>
                        <div id="menu5" class="tab-pane fade">
                          <h3>Menu 2</h3>
                          <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>
                        </div>
                        <div id="menu6" class="tab-pane fade">
                          <h3>Menu 3</h3>
                          <p>Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                        </div>
                        <div id="menu7" class="tab-pane fade">
                            <h3>Menu 2</h3>
                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>
                          </div>
                          <div id="menu8" class="tab-pane fade">
                            <h3>Menu 3</h3>
                            <p>Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                          </div>
                    </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- End Proerty header  -->

  </div>
  <!-- Footer -->
  <%@include file="footer.jsp" %>
  <!-- / Footer -->

  <!-- jQuery library -->
  <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> -->
  <script src="js/jquery.min.js"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="js/bootstrap.js"></script>
  <!-- slick slider -->
  <script type="text/javascript" src="js/slick.js"></script>
  <!-- Price picker slider -->
  <script type="text/javascript" src="js/nouislider.js"></script>
  <!-- mixit slider -->
  <script type="text/javascript" src="js/jquery.mixitup.js"></script>
  <!-- Add fancyBox -->
  <script type="text/javascript" src="js/jquery.fancybox.pack.js"></script>
  <script src="js/materialize.min.js"></script>
  <!-- Custom js -->
  <script src="js/custom.js"></script>
  <script>
    $(document).ready(function () {
      $('.body-hold').css('min-height', $(window).height() - 208);
      // Comma, not colon ----^
    });
    $(window).resize(function () {
      $('.body-hold').css('min-height', $(window).height() - 208);
      // Comma, not colon ----^
    });

    $("#example-basic").steps({
      headerTag: "h3",
      bodyTag: "section",
      transitionEffect: "slideLeft",
      autoFocus: true
    });

  </script>
</body>

</html>