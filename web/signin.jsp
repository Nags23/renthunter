<%-- 
    Document   : signin
    Created on : 3 Dec, 2019, 8:16:03 PM
    Author     : Nandini
--%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">    
    <title>Home Property | Signin</title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
    
    
    <!-- Font awesome -->
    <link href="css/font-awesome.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="css/bootstrap.css" rel="stylesheet">   
    <!-- slick slider -->
    <link rel="stylesheet" type="text/css" href="css/slick.css">
    <!-- price picker slider -->
    <link rel="stylesheet" type="text/css" href="css/nouislider.css">
    <!-- Fancybox slider -->
    <link rel="stylesheet" href="css/jquery.fancybox.css" type="text/css" media="screen" /> 
    <!-- Theme color -->
    <link id="switcher" href="css/theme-color/orange-theme.css" rel="stylesheet">     

    <!-- Main style sheet -->
    <link href="css/style-main.css" rel="stylesheet">    

   
    <!-- Google Font -->
    <link href='https://fonts.googleapis.com/css?family=Vollkorn' rel='stylesheet' type='text/css'>    
    <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script type="text/javascript">
        function form_validate(){
            if(document.getElementById("username").value==""){
                document.getElementById("a_username").hidden = false;
                document.getElementById("username").focus();
                return false;
            }else{
                document.getElementById("a_username").hidden = true;
            }
            
            if(document.getElementById("pwd").value==""){
                document.getElementById("a_pwd").hidden = false;
                document.getElementById("pwd").focus();
                return false;
            }else{
                document.getElementById("a_pwd").hidden = true;
            }
            
            document.signin.action = "login";
        }
    </script>
    
    <script type="text/javascript">
             var xmlHttp
            var xmlHttp
    function forgot_pwd(){
                 if (typeof XMLHttpRequest != "undefined") {
                    xmlHttp = new XMLHttpRequest();
                }
                else if (window.ActiveXObject) {
                    xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                if (xmlHttp == null) {
                    alert("Browser does not support XMLHTTP Request")
                    return;
                }
                  if(document.getElementById("username").value==""){
                    //alert("in if");
                     document.getElementById("a_username").hidden=false;
                    document.getElementById("username").focus();
                    return false;
                }else{
                   document.getElementById("a_username").hidden=true; 
                   document.getElementById("pwd_msg").innerHTML = "Please Wait..."
                }
                var url = "forgot_pwd";
                url += "?count=" + encodeURIComponent( document.getElementById("username").value);
                xmlHttp.onreadystatechange = stateChangepwd;
                xmlHttp.open("GET", url, true);
                xmlHttp.send(null); 
            }
            
            function stateChangepwd() {
                if (xmlHttp.readyState == 4 || xmlHttp.readyState == "complete") {
                    //alert(xmlHttp.responseText);        
                    if (xmlHttp.responseText.trim() == "done") {
                        document.getElementById("pwd_msg").innerHTML = "Reset Password Link sent to your Email Id";
                       
                    } 
                }
            }
        </script>

  </head>
  <body>
  <section id="aa-signin">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="aa-signin-area">
            <div class="aa-signin-form">
              <div class="aa-signin-form-title">
                <!--<a class="aa-property-home" href="home">Property Home</a>-->
                <h4>Sign in to your account</h4>
                 <%
                            if(session.getAttribute("pwdinsert")!=null){
                        %>
                        <h2 style="color:green">Password Updated Successfully</h2>
                        <%
                                session.removeAttribute("pwdinsert");
                            }%>
                        <span id="pwd_msg" style="color:green"></span>
              </div>
              <form class="contactform" name="signin" method="post">                                                 
                <div class="aa-single-field">
                  <label for="email">Mobile No <span class="required">*</span></label>
                  <input type="text"  aria-required="true"  name="username" id="username" placeholder="Mobile No" autocomplete="off" autofocus>
                  <span id="a_username" style="color:red" hidden>Please Enter Mobile No</span>
                </div>
                <div class="aa-single-field">
                  <label for="password">Password <span class="required">*</span></label>
                  <input type="password" name="pwd" id="pwd"> 
                  <span id="a_pwd" style="color:red" hidden>Please Enter Password</span>
                </div>
                <div class="aa-single-field">
                <label>
                  <input type="checkbox"> Remember me
                </label>  
                    
                    <label class="pull-right">
                        <a href="#!" onclick="return forgot_pwd();" class="link">Forgot Password?</a>
                </label> 
                </div> 
                <div class="aa-single-submit">
                    <input type="submit" value="Sign In" class="aa-browse-btn" name="submit" onclick="return form_validate();">  
                  <p>Don't Have a Account Yet? <a href="register">CREATE NOW!</a></p>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  
  <!-- jQuery library -->
  <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> -->
  <script src="js/jquery.min.js"></script>   
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="js/bootstrap.js"></script>   
  <!-- slick slider -->
  <script type="text/javascript" src="js/slick.js"></script>
  <!-- Price picker slider -->
  <script type="text/javascript" src="js/nouislider.js"></script>
   <!-- mixit slider -->
  <script type="text/javascript" src="js/jquery.mixitup.js"></script>
  <!-- Add fancyBox -->        
  <script type="text/javascript" src="js/jquery.fancybox.pack.js"></script>
  <!-- Custom js -->
  <script src="js/custom.js"></script> 
  
  </body>
</html>