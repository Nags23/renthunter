<%-- 
    Document   : post_shops
    Created on : 25 Dec, 2019, 12:04:10 PM
    Author     : Nandini
--%>


<%@page import="java.util.List"%>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Home Property | Post Properties(Shops)</title>

        <!-- Favicon -->
        <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">
    <link rel="stylesheet" href="apartment_css/css/normalize.css">
    <link rel="stylesheet" href="apartment_css/css/main.css">
    <link rel="stylesheet" href="apartment_css/css/jquery.steps.css">
    <link id="switcher" href="css/theme-color/orange-theme.css" rel="stylesheet">
    <link href="css/style-main.css" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    <link href="css/bootstrap.css" rel="stylesheet">
    <script src="lib/modernizr-2.6.2.min.js"></script>
    <script src="lib/jquery-1.9.1.min.js"></script>
    <script src="lib/jquery.cookie-1.3.1.js"></script>
    <script src="build/jquery.steps.js"></script>

    <!-- Compiled and minified CSS -->
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css"> -->
    <link href="css/materialize.min.css" rel="stylesheet">

    <!-- Compiled and minified JavaScript -->
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script> -->
    <script src="js/materialize.min.js"></script>
    
    <script type="text/javascript">
            function allowpositivenumber(e) {
                var charCode = (e.which) ? e.which : event.keyCode;

                if ((charCode < 46 || charCode > 57)) {
                    return false;
                }
                return true;
            }
        </script>
        
    <script type="text/javascript">
        function form_validate(){
            
            
               // return true;
              //  document.apartment.action = "post_apartment_process";
        }
    </script>
    
     <script type="text/javascript">
        function form_validate_final(){
            
                var nn = confirm("Are you sure to update details?");
                if(nn==true){
//                document.apartment.action = "post_apartment_process";
                document.forms[0].submit();
            }else{
                return false;
            }
        }
    </script>
    
     <script>
function textCounter(field,field2,maxlimit)
{
 var countfield = document.getElementById(field2);
 if ( field.value.length > maxlimit ) {
  field.value = field.value.substring( 0, maxlimit );
  return false;
 } else {
  countfield.innerHTML = maxlimit - field.value.length + " Remaining Characters";
 }
}
</script>

<script type="text/javascript">
    function insRow()
{

 var x=document.getElementById('POITable');
    var cells=document.getElementById('POITable').getElementsByTagName("tr");
   
   
    if(cells.length<=2){
        
        if(document.getElementById("rules").value==""){
            document.getElementById("rules").focus();
            alert("Please Enter Rules");
            return false;
        }
    }else{
       
        for(var i=0;i<(document.apartment.rules.length);i++){
       //  alert(document.nsquare.slno[i].value);
            if(document.apartment.rules[i].value=="" ){
            document.apartment.rules[i].focus();
            alert("Please Enter Rules");
            return false;
        }
        
        }
    }
     $('#POITable tbody').append(
         '<tr align="left"><td><div class="input-field col s6"><textarea id="cus_rule" class="materialize-textarea" name="rules" id="rules" ></textarea><label for="cus_rule">Customize your rules</label></div></td><td><div class="input-field col s6"><a href="#" onclick="insRow(this); return false;" style="text-decoration: none" class="w3-button w3-xlarge w3-circle w3-teal">+</a>&nbsp;&nbsp;<a href="#" onclick="deleteRow(this); return false;" style="text-decoration: none" class="w3-button w3-xlarge w3-circle w3-dan">x</a></div></td></tr>'
                );
}
</script>

<script type="text/javascript">
            function deleteRow(row)
{
    var x=document.getElementById('POITable');
    var cells=document.getElementById('POITable').getElementsByTagName("tr");
    
    if(cells.length==2){
        
        alert("Cannot Remove...Atleast One Entry is Mandatory");
        return false;
    }else{
    var i=row.parentNode.parentNode.rowIndex;
   document.getElementById('POITable').deleteRow(i);
    }
}
</script>

<style>
    .w3-teal, .w3-hover-teal:hover {
    color: #fff!important;
    background-color: green!important;
}

.w3-dan, .w3-hover-teal:hover {
    color: #fff!important;
    background-color: red!important;
}

.w3-xlarge {
    font-size: 16px!important;
}
.w3-circle {
    border-radius: 18%;
}
    .w3-btn, .w3-button {
    border: none;
    display: inline-block;
    padding: 8px 16px;
    vertical-align: middle;
    overflow: hidden;
    text-decoration: none;
    color: inherit;
    background-color: inherit;
    text-align: center;
    cursor: pointer;
    white-space: nowrap;
}
</style>


<script language="javascript" type="text/javascript">
            var xmlHttp
            var xmlHttp
            function fill_city(str) {
                if (typeof XMLHttpRequest != "undefined") {
                    xmlHttp = new XMLHttpRequest();
                }
                else if (window.ActiveXObject) {
                    xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                if (xmlHttp == null) {
                    alert("Browser does not support XMLHTTP Request")
                    return;
                }
                var url = "city_fetch";
                url += "?count=" + encodeURIComponent(document.getElementById("state").value);
                xmlHttp.onreadystatechange = stateChange3;
                xmlHttp.open("GET", url, true);
                xmlHttp.send(null);
            }

            function stateChange3() {
                if (xmlHttp.readyState == 4 || xmlHttp.readyState == "complete") {
            //alert(xmlHttp.responseText);     
            //document.getElementById("cname").hidden = true;
            //document.getElementById("cc").innerHTML = xmlHttp.responseText.trim();
            //document.getElementById("load").innerHTML = "<div class='col-md-3 col-sm-12 col-xs-12 form-group'><div class='input-field'><select name='cname' id='cname' class='form-control'><option value=''></option></select><label for='u_name'>City Name<span class='required'>*</span></label></div><span style='color:red' id='a_cname' hidden> Please Enter City Name</span></div>";
            document.getElementById("city").innerHTML  = xmlHttp.responseText;
            }
        }
        </script>
        
        
        <script language="javascript" type="text/javascript">
            var xmlHttp
            var xmlHttp
            function fill_area(str) {
                if (typeof XMLHttpRequest != "undefined") {
                    xmlHttp = new XMLHttpRequest();
                }
                else if (window.ActiveXObject) {
                    xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                if (xmlHttp == null) {
                    alert("Browser does not support XMLHTTP Request")
                    return;
                }
                var url = "area_fetch";
                url += "?count=" + encodeURIComponent(document.getElementById("state").value)+"nagsnn"+encodeURIComponent(document.getElementById("city").value);
                xmlHttp.onreadystatechange = stateChange3area;
                xmlHttp.open("GET", url, true);
                xmlHttp.send(null);
            }

            function stateChange3area() {
                if (xmlHttp.readyState == 4 || xmlHttp.readyState == "complete") {
            //alert(xmlHttp.responseText);     
            //document.getElementById("cname").hidden = true;
            //document.getElementById("cc").innerHTML = xmlHttp.responseText.trim();
            //document.getElementById("load").innerHTML = "<div class='col-md-3 col-sm-12 col-xs-12 form-group'><div class='input-field'><select name='cname' id='cname' class='form-control'><option value=''></option></select><label for='u_name'>City Name<span class='required'>*</span></label></div><span style='color:red' id='a_cname' hidden> Please Enter City Name</span></div>";
            document.getElementById("aname").innerHTML  = xmlHttp.responseText;
            }
        }
        </script>
        
        <script type="text/javascript">
            function fill_kyc_add(){
               // document.getElementById("kyc_add1").value = document.getElementById("namenum").value+" "+document.getElementById("street_no").value+" "+document.getElementById("aname").text+" "+document.getElementById("city").text+" "+document.getElementById("state").text;
                   document.getElementById("kyc_add1").value = document.getElementById("namenum").value+" "+document.getElementById("street_no").value;
    }
        </script>

</head>

<body class="aa-price-range">
    <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

    <!-- SCROLL TOP BUTTON -->
    <a class="scrollToTop" href="#"><i class="fa fa-angle-double-up"></i></a>
    <!-- END SCROLL TOP BUTTON -->

    <!-- Start header section -->
    <%@include file="../header.jsp" %>
    <!-- End header section -->

    <!-- Start menu section -->
    
    <!-- End menu section -->
    <div class="parenthold grey-bg">
        <div class="container">
            <div class="card white-bg">
                <div class="content">
                    <script>
                        $(function () {
                            $("#wizard").steps({
                                headerTag: "h2",
                                bodyTag: "section",
                                transitionEffect: "slideLeft",
                                stepsOrientation: "vertical"
                            });
                        });
                    </script>
                    <%
                                                    if (session.getAttribute("insert") != null) {
                                                %>


                                                <%
                                                    if (session.getAttribute("insert").equals("pass")) {
                                                %>
                                                <div class="alert alert-block alert-success col-lg-12 ">
                                                    <button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">�</span></button>
                                                    <strong> Details Updated Successfully.</strong> 


                                                </div>
                                                <%} else if (session.getAttribute("insert").equals("fail")) {%>
                                                <div class="alert alert-block alert-danger col-lg-12 ">
                                                    <button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">�</span></button>
                                                    <strong> Creation Failed Please Try Again.</strong> 
                                                </div>
                                                <%} else if (session.getAttribute("insert").equals("exists")) {%>
                                                <div class="alert alert-block alert-success col-lg-12 ">
                                                    <button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">�</span></button>
                                                    <strong>  Already Exists in Database.</strong>
                                                </div>
                                                <%} else if (session.getAttribute("insert").equals("delpass")) {%>
                                                <div class="alert alert-block alert-success col-lg-12 ">
                                                    <button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">�</span></button>
                                                    <strong>  Deleted Successfully from all Records</strong>
                                                </div>
                                                <%} else if (session.getAttribute("insert").equals("delfail")) {%>
                                                <div class="alert alert-block alert-success col-lg-12 ">
                                                    <button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">�</span></button>
                                                    <strong>Deletion Failed.</strong>
                                                </div>
                                                <%} else if (session.getAttribute("insert").equals("updatepass")) {%>
                                                <div class="alert alert-block alert-success col-lg-12 ">
                                                    <button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">�</span></button>
                                                    <strong>  Updated Successfully</strong>
                                                </div>
                                                <%} else if (session.getAttribute("insert").equals("updatefail")) {%>
                                                <div class="alert alert-block alert-success col-lg-12 ">
                                                    <button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">�</span></button>
                                                    <strong>Update Failed Pls Try Again</strong>
                                                </div>

                                                <%}%>
                                                <%
                                                        session.removeAttribute("insert");
                                                    }%>
<form name="apartment" id="apartment-form" method="post" action="post_shops_process" enctype="multipart/form-data">
                    <div id="wizard" class="aa-form">
                        <h2><i class="fa fa-home" aria-hidden="true"></i>Shop Details</h2>
                        
                        <section>
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="row">
                                    <div class="col-md-6 col-md-sm-12 col-xs-12">
                                        <div class="input-field">
                                            <input id="namenum" name="namenum" type="text" maxlength="75" class="validate" >
                                            <input type="hidden" name="fr_nn_type" id="fr_nn_type" value="SHOP_SUB">
                                            <label for="namenum">Name or Number of Property <span
                                                    class="required">*</span></label>
                                        </div>
                                        <span style="color:red" id="a_namenum" hidden> Please Enter Name or Number of Shop</span>
                                    </div>
                                    <div class="col-md-6 col-md-sm-12 col-xs-12">
                                        <div class="input-field ">
                                            <input id="street_no" name="street_no" maxlength="50" type="text" class="validate">
                                            <label for="street_no">Street <span class="required">*</span></label>
                                        </div>
                                        <span style="color:red" id="a_street_no" hidden> Please Enter Street No</span>
                                    </div>
                                </div>
                                    <div class="col-md-3 col-md-sm-12 col-xs-12">
                                        <div class="input-field ">
                                            <input id="country" name="country" maxlength="200" value="India" readonly type="text" class="validate">
                                            <label for="country">Country<span class="required">*</span></label>
                                        </div>
                                        <span style="color:red" id="a_country" hidden> Please Enter Country</span>
                                    </div>
                                    
                                    <div class="col-md-3 col-sm-12 col-xs-12 form-group default-select">
                                        <!--<div class="input-field">-->
                                        
                                        <select name="state" id="state" class="form-control" onchange="return fill_city();" style="    border-width: 0 0 1px 0 !important;box-shadow: none;border-radius: 0 !important;padding: 10px 10px;height: 46px;border-color: #585858;margin-top: 14px;">
                                              
                                         <option selected></option>
                                         <%

                            if (session.getAttribute("dd_id") != null) {
                                List<String> dd_id = (List<String>) session.getAttribute("dd_id");
                                List<String> dd_sname = (List<String>) session.getAttribute("dd_sname");
                        %>
                        <%
                             for (int i = 0; i < dd_id.size(); i++) {
                        %>
                        <option value="<%=dd_id.get(i)%>"><%=dd_sname.get(i)%></option>
                        <%}%>
                        <%
                            session.removeAttribute("dd_id");
                            session.removeAttribute("dd_sname");
                            }
                        %>
                                    </select>
                                    <label>Select State <span class="required">*</span></label>
                                        <!--</div>-->
                                        <span style="color:red" id="a_state" hidden> Please Select State</span>
                                    </div>
                                    
                                     <div class="col-md-3 col-sm-12 col-xs-12 form-group default-select">
                                        <!--<div class="input-field">-->
                                            <select name="city" id="city" class="form-control" onchange="return fill_area();" style="    border-width: 0 0 1px 0 !important;
    box-shadow: none;
    border-radius: 0 !important;
    padding: 10px 10px;
    height: 46px;
    border-color: #585858;
    margin-top: 14px;">
                                                <option value="">Select City Name</option>
                                            </select>
                                            <label for="u_name">Select City<span class="required">*</span></label>
                                        <!--</div>-->
                                        <span style="color:red" id="a_city" hidden> Please Select City Name</span>
                                    </div>
                                    
                                    <div class="col-md-3 col-sm-12 col-xs-12 form-group default-select">
                                        <!--<div class="input-field">-->
                                            <select name="aname" id="aname" class="form-control" style="    border-width: 0 0 1px 0 !important;
    box-shadow: none;
    border-radius: 0 !important;
    padding: 10px 10px;
    height: 46px;
    border-color: #585858;
    margin-top: 14px;">
                                                <option value="">Select Area Name</option>
                                            </select>
                                            <label for="u_name">Select Area<span class="required">*</span></label>
                                        <!--</div>-->
                                        <span style="color:red" id="a_aname" hidden> Please Select Area</span>
                                    </div>
                                    
                                    
                                    
                                    
<!--                                    <div class="col-md-12 col-md-sm-12 col-xs-12">
                                        <div class="input-field ">
                                            <input id="autocomplete" onFocus="geolocate()" name="address_pro" onblur="return fill_kyc_add();" maxlength="200" type="text" class="validate">
                                            <label for="autocomplete">Address of property <span class="required">*</span></label>
                                            <span class="note-small">Note: for accurate result, select address from dropdown</span>
                                        </div>
                                        <span style="color:red" id="a_autocomplete" hidden> Please Enter Address of Property</span>
                                    </div>-->
                                    <div class="col-md-12 col-md-sm-12 col-xs-12">
                                        <div class="input-field ">
                                            <input id="land_mark" name="land_mark" maxlength="200" type="text" class="validate">
                                            <label for="land_mark">Land Mark <span class="required">*</span></label>
                                        </div>
                                        <span style="color:red" id="a_land_mark" hidden> Please Enter Land Mark</span>
                                    </div>
                                    
                                    
                                    
                                    <div class="col-md-6 col-md-sm-12 col-xs-12">
                                        <div class="input-field ">
                                            <select name="age_pro" id="age_pro">
                                                <option selected></option>
                                                <option>Less than 1 Year</option>
                                                <option>1 Year</option>
                                                <option>2 Years</option>
                                                <option>3 Years</option>
                                                <option>4 Years</option>
                                                <option>5 Years</option>
                                                <option>6 Years</option>
                                                <option>7 Years</option>
                                                <option>8 Years</option>
                                                <option>9 Years</option>
                                                <option>10 Years</option>
                                                <option>Greater than 10 years</option>
                                            </select>
                                            <label for="age_pro">Age of property(In Years) <span class="required">*</span></label>
                                        </div>
                                        <span style="color:red" id="a_age_pro" hidden> Please Select Age of Property</span>
                                    </div>
                                    <div class="col-md-6 col-md-sm-12 col-xs-12">
                                        <label for="email" class="d-block">Near By</label>
                                        <%

                            if (session.getAttribute("near_id") != null) {
                                List<String> near_id = (List<String>) session.getAttribute("near_id");
                                List<String> near_name = (List<String>) session.getAttribute("near_name");
                        %>
                        <%
                             for (int i = 0; i < near_id.size(); i++) {
                        %>
					 <label>
                                        <input type="checkbox" class="with-gap" name="near_by" id="<%=near_id.get(i)%>" value="<%=near_id.get(i)%>" />
                                        
                                        <span><%=near_name.get(i)%></span>
                                    </label>
                                    <%}%>
                                    <%
                                        session.removeAttribute("near_id");
                                        session.removeAttribute("near_name");
                                        }
                                    %>
                                <span style="color:red" id="a_near_by" hidden> Please Select Near By Option</span>
                            </div>
                                    <div class="col-md-12 col-md-sm-12 col-xs-12">
                                        <div class="input-field ">
                                            <textarea id="oter_details" name="oter_details" maxlength="250" style="height: 80px"></textarea>
                                            <label for="oter_details">Specify Other Details(If Any)</label>
                                        </div>
                                        
                                    </div>

                                </div>
                            </div>
                        </section>

                        <h2> <i class="fa fa-keyboard-o" aria-hidden="true"></i>Rent Details</h2>
                        <section>
                            <div class="col-md-14 col-md-sm-14 col-xs-14">
                                <p class="comment-form-email">
                                    <label for="email" class="d-block">Type<span
                                            class="required">*</span></label>
                                            <%
                                                  if (session.getAttribute("ten_id") != null) {
                                List<String> ten_id = (List<String>) session.getAttribute("ten_id");
                                List<String> ten_ttype = (List<String>) session.getAttribute("ten_ttype");
                                List<String> ten_fclass = (List<String>) session.getAttribute("ten_fclass");
                                for (int i = 0; i < ten_id.size(); i++) {
                                            %>
                                    <label class="ico <%=ten_fclass.get(i)%>">
                                        <input type="checkbox" class="with-gap" name="tenant_type" id="<%=ten_ttype.get(i)%>" value="<%=ten_id.get(i)%>" />
                                        <span><%=ten_ttype.get(i)%> </span>
                                    </label>
                                    <%}%>
                                    <%
                                        session.removeAttribute("ten_id");
                                        session.removeAttribute("ten_ttype");
                                        session.removeAttribute("ten_fclass");
                                        }
                                    %>
                                    
                                </p>
						
				<span style="color:red" id="a_tenant" hidden> Please Select Preferred Tenant Type</span>				
                            </div>
                            
                            <div class="col-md-3 col-md-sm-12 col-xs-12">
                                <div class="input-field ">
                                    <input id="area_build" name="area_build" type="text" class="validate" onkeypress="return allowpositivenumber(event);">
                                    
                                    <label for="area_build">Area <span class="required">*</span></label>
                                </div>
                                <span style="color:red" id="a_area_build" hidden> Please Enter Area</span>
                            </div>
                                    <div class="col-md-3 col-md-sm-12 col-xs-12">
                                <div class="input-field ">
                                    <select name="area_build_type" id="area_build_type">
                                        <option selected></option>
                                        <option>Built-Up</option>
                                        <option>Carpet</option>
                                        <option>Total</option>
                                    </select>
                                    <label for="area_build_type">Area type <span class="required">*</span></label>
                                </div>
                                <span style="color:red" id="a_area_build_type" hidden> Please Select Area Type</span>
                            </div>
                                    
                                    <div class="col-md-3 col-md-sm-12 col-xs-12">
                                <div class="input-field ">
                                    <select name="area_build_in" id="area_build_in">
                                        <option selected></option>
                                        <option>Sqft</option>
                                        <option>Yard</option>
                                        <option>Acre</option>
                                    </select>
                                    
                                    <label for="area_build_type">In <span class="required">*</span></label>
                                </div>
                                <span style="color:red" id="a_area_build_in" hidden> Please Select Type</span>
                            </div>
                                    
                                    <div class="col-md-3 col-md-sm-12 col-xs-12">
                                <div class="input-field ">
                                    <input id="adate" name="adate" type="text" class="validate" readonly>
                                    <label for="adate">Available From<span class="required">*</span></label>
                                </div>
                                <span style="color:red" id="a_adate" hidden> Please Select Available Date</span>
                            </div>
                            <div class="col-md-4 col-md-sm-12 col-xs-12">
                                <div class="input-field ">
                                    <select name="fur" id="fur">
                                <option selected></option>
                                         <%

                            if (session.getAttribute("fur_id") != null) {
                                List<String> fur_id = (List<String>) session.getAttribute("fur_id");
                                List<String> fur_ftype = (List<String>) session.getAttribute("fur_ftype");
                        %>
                        <%
                             for (int i = 0; i < fur_id.size(); i++) {
                        %>
                        <option value="<%=fur_id.get(i)%>"><%=fur_ftype.get(i)%></option>
                        <%}%>
                        <%
                            session.removeAttribute("fur_id");
                            session.removeAttribute("fur_ftype");
                            }
                        %>
                                    </select>
									 <label>Furnishing <span class="required">*</span></label>
                                    
                                </div>
                                    <span style="color:red" id="a_fur" hidden> Please Select Furniture Type</span>
                            </div>
                            <div class="col-md-4 col-md-sm-12 col-xs-12">
                                <div class="input-field ">
                                    <select name="floor" id="floor">
					<option selected></option>
                                         <%

                            if (session.getAttribute("floor_id") != null) {
                                List<String> floor_id = (List<String>) session.getAttribute("floor_id");
                                List<String> floor_floor = (List<String>) session.getAttribute("floor_floor");
                        %>
                        <%
                             for (int i = 0; i < floor_id.size(); i++) {
                        %>
                        <option value="<%=floor_id.get(i)%>"><%=floor_floor.get(i)%></option>
                        <%}%>
                        <%
                            session.removeAttribute("floor_id");
                            session.removeAttribute("floor_floor");
                            }
                        %>
                                    </select>
                                    <label>Floor</label>
                                </div>
                                    <span style="color:red" id="a_floor" hidden> Please Select Floor Type</span>
                            </div>
                            <div class="col-md-4 col-md-sm-12 col-xs-12">
                                <div class="input-field ">
                                    <select name="facings" id="facings">
                                       <option selected></option>
                                         <%

                            if (session.getAttribute("facings_id") != null) {
                                List<String> facings_id = (List<String>) session.getAttribute("facings_id");
                                List<String> facings_facings = (List<String>) session.getAttribute("facings_facings");
                        %>
                        <%
                             for (int i = 0; i < facings_id.size(); i++) {
                        %>
                        <option value="<%=facings_id.get(i)%>"><%=facings_facings.get(i)%></option>
                        <%}%>
                        <%
                            session.removeAttribute("facings_id");
                            session.removeAttribute("facings_facings");
                            }
                        %>
                                    </select>
                                    <label>Facing</label>
                                </div>
                                    <span style="color:red" id="a_facings" hidden> Please Select Facing Type</span>
                            </div>
                                    
                                    <div class="col-md-12 col-md-sm-12 col-xs-12">
                                        <div class="input-field ">
                                            <textarea id="oter_details1" name="oter_details1" maxlength="250" style="height: 80px"></textarea>
                                            <label for="oter_details1">Specify Other Details(If Any)</label>
                                        </div>
                                        
                                    </div>
                        </section>

                        <h2><i class="fa fa-certificate" aria-hidden="true"></i> Amenities</h2>
                        <section>
                            <div class="col-md-12 col-md-sm-12 col-xs-12">
                                <p class="comment-form-email">
                                    <label for="email" class="d-block">Parking <span class="required">*</span></label>
                                    <label>
                                        <input class="with-gap" name="parking" id="parking_yes" value="y" checked type="radio" />
                                        <span>Yes </span>
                                    </label>
                                    <label>
                                        <input class="with-gap" name="parking" id="parking_no" value="n" type="radio" />
                                        <span>No </span>
                                    </label>
                                </p>
                            </div>
                            
                            <div class="col-md-6 col-md-sm-12 col-xs-12">
                                <p class="comment-form-email">
                                    <label for="email" class="d-block">Available Amenities </label>
						
                                             <%

                            if (session.getAttribute("amn_id") != null) {
                                List<String> amn_id = (List<String>) session.getAttribute("amn_id");
                                List<String> amn_ttype = (List<String>) session.getAttribute("amn_ttype");
                                List<String> amn_fclass = (List<String>) session.getAttribute("amn_fclass");
                        %>
                        <%
                             for (int i = 0; i < amn_id.size(); i++) {
                        %>
					<div class="col-md-6 col-md-sm-6 col-xs-6"> 
                        <label class="fa <%=amn_fclass.get(i)%>">
                                        <input type="checkbox" class="with-gap" name="amn" id="<%=amn_id.get(i)%>" value="<%=amn_id.get(i)%>" />
                                       
                                        <span><%=amn_ttype.get(i)%></span>
                                    </label>
                                        </div>
                                    <%}%>
                                    <%
                                        session.removeAttribute("amn_id");
                                        session.removeAttribute("amn_ttype");
                                        session.removeAttribute("amn_fclass");
                                        }
                                    %>
									
					
                                </p>
                            </div>
                            <div class="col-md-6 col-md-sm-12 col-xs-12">
                                <p class="comment-form-email">
                                    <label for="email" class="d-block">Available Services </label>
						
                                             <%

                            if (session.getAttribute("ser_id") != null) {
                                List<String> ser_id = (List<String>) session.getAttribute("ser_id");
                                List<String> ser_ttype = (List<String>) session.getAttribute("ser_ttype");
                                List<String> ser_fclass = (List<String>) session.getAttribute("ser_fclass");
                        %>
                        <%
                             for (int i = 0; i < ser_id.size(); i++) {
                        %>
                        <div class="col-md-6 col-md-sm-6 col-xs-6">
					 <label class="fa <%=ser_fclass.get(i)%>">
                                        <input type="checkbox" class="with-gap" name="ser" id="<%=ser_id.get(i)%>" value="<%=ser_id.get(i)%>" />
                                       
                                        <span><%=ser_ttype.get(i)%></span>
                                    </label>
                        </div>
                                    <%}%>
                                    <%
                                        session.removeAttribute("ser_id");
                                        session.removeAttribute("ser_ttype");
                                        session.removeAttribute("ser_fclass");
                                        }
                                    %>
									
					
                                </p>
                            </div>
                                    <div class="col-md-12 col-md-sm-12 col-xs-12">
                                        <div class="input-field ">
                                            <textarea id="oter_details2" name="oter_details2" maxlength="250" style="height: 80px"></textarea>
                                            <label for="oter_details2">Specify Other Details(If Any)</label>
                                        </div>
                                        
                                    </div>
                            
                        </section>
                                    
                                    

                        <h2><i class="fa fa-inr" aria-hidden="true"></i>Pricing</h2>
                        <section>
                            <div class="row">
                                <div class="col-md-12 col-md-sm-12 col-xs-12">
                                        <div class="col-md-4 col-md-sm-12 col-xs-12">
                                                <div class="input-field">
                                                    <i class="fa fa-inr  prefix"></i>
                                                    <input id="rent_amt" name="rent_amt" type="text" class="validate" onkeypress="return allowpositivenumber(event);">
                                                    <label for="rent_amt">Rent amount <span class="required">*</span></label>
                                                    <span class="note-small pull-right">Month</span>
                                                </div>
                                            <span style="color:red" id="a_rent_amt" hidden> Please Enter Rent Amount</span>
                                            </div>
                                            <div class="col-md-4 col-md-sm-12 col-xs-12">
                                                <div class="input-field">
                                                    <i class="fa fa-inr  prefix"></i>
                                                    <input id="deposit_amt" name="deposit_amt" type="text" class="validate" onkeypress="return allowpositivenumber(event);">
                                                    <label for="deposit_amt">Deposit <span class="required">*</span></label>
                                                </div>
                                                <span style="color:red" id="a_deposit_amt" hidden> Please Enter Deposit Amount</span>
                                            </div>
                
                                            <div class="col-md-4 col-md-sm-12 col-xs-12">
                                                <p class="comment-form-email">
                                                    <label for="email" class="d-block">Negotiable<span class="required">*</span></label>
                                                    <label>
                                                        <input class="with-gap" name="nego" id="nego" value="y" type="radio"  />
                                                        <span>Yes</span>
                                                    </label>
                                                    <label>
                                                        <input class="with-gap" name="nego" id="nego" value="n" type="radio" checked />
                                                        <span>No</span>
                                                    </label>
                                                </p>
                                            </div>
                                </div>
                            </div>
                            <div class="row">
                                    <div class="col-md-12 col-md-sm-12 col-xs-12">
                                            <div class="col-md-4 col-md-sm-12 col-xs-12">
                                                    <div class="input-field">
                                                        <i class="fa fa-inr  prefix"></i>
                                                        <input id="maintanece_amt" name="maintanece_amt" type="text" class="validate" onkeypress="return allowpositivenumber(event);">
                                                        <label for="maintanece_amt">Advance </label>
                                                        <span class="note-small pull-right"></span>
                                                    </div>
                                                <span style="color:red" id="a_maintanece_amt" hidden> Please Enter Maintenance Amount</span>
                                                </div>
                                                <div class="col-md-4 col-md-sm-12 col-xs-12">
                                                    <div class="input-field">
                                                        
                                                        <input id="agreement_dur" name="agreement_dur" type="text" class="validate" onkeypress="return allowpositivenumber(event);">
                                                        <label for="agreement_dur">Agreement Duration </label>
                                                        <span class="note-small pull-right">Month</span>
                                                    </div>
                                                    <span style="color:red" id="a_agreement_dur" hidden> Please Enter Agreement Duration</span>
                                                </div>
                                                <div class="col-md-4 col-md-sm-12 col-xs-12">
                                                    <div class="input-field">
                                                        
                                                        <input id="info_leave" name="info_leave" type="text" class="validate" onkeypress="return allowpositivenumber(event);">
                                                        <label for="info_leave">Intimation Before Leaving </label>
                                                        <span class="note-small pull-right">Month</span>
                                                    </div>
                                                    <span style="color:red" id="a_info_leave" hidden> Please Enter Intimation Before Leaving</span>
                                                </div>
                                    </div>
                                </div>
                            <div class="row">
                                <div class="col-md-12 col-md-sm-12 col-xs-12">
                                        <div class="input-field ">
                                            <textarea id="oter_details3" name="oter_details3" maxlength="250" style="height: 80px"></textarea>
                                            <label for="oter_details3">Specify Other Details(If Any)</label>
                                        </div>
                                        
                                    </div>
                            </div>


                        </section>
                        <h2><i class="fa fa-file-image-o" aria-hidden="true"></i>Gallery</h2>
                        <section>

                                      <div class="input-field">
                <label class="active">Photos(Drop images or click in the below area to add images)</label>
                <div class="input-images-1" style="padding-top: .5rem;"></div>
                <center>
                <div class="upload-text"><i class="fa fa-camera">Upload Images</i><span>Drag &amp; Drop files here or click in the border to browse</span></div>
                </center>
                                      </div>
                            
                        </section>
                        <h2><i class="fa fa-list-ul" aria-hidden="true"></i>Rules</h2>
                        <section>
                            <div class="col-md-12 col-md-sm-12 col-xs-12">
                                <div class="row">
                                    <div class="col-md-6 col-md-sm-12 col-xs-12"> 
                                            <label>
                                                    <input type="checkbox" class="filled-in" name="rule_non_veg" id="rule_non_veg" value="y" checked="checked" />
                                                    <span>Only For Vegetarians </span>
                                                </label>
                                    </div>
                                        <div class="col-md-6 col-md-sm-12 col-xs-12"> 
                                                <label>
                                                    <input type="checkbox" class="filled-in" name="rule_pets" id="rule_pets" value="y" checked="checked" />
                                                    <span>Pets Not Allowed </span>
                                                </label>
                                        </div>
                                        <div class="col-md-6 col-md-sm-12 col-xs-12"> 
                                        <label>
                                                    <input type="checkbox" class="filled-in" name="rule_drink" id="rule_drink" value="y" checked="checked" />
                                                    <span>No Drinking Smoking</span>
                                                </label>
                                    </div>
                                    <div class="col-md-6 col-md-sm-12 col-xs-12"> 
                                        <label>
                                                    <input type="checkbox" class="filled-in" name="rule_water" id="rule_water" value="y" checked="checked" />
                                                    <span>Water Charges Extra</span>
                                                </label>
                                        <label>
                                    </div>
                                        <div class="col-md-6 col-md-sm-12 col-xs-12"> 
                                            <label>
                                                    <input type="checkbox" class="filled-in" name="rule_elec" id="rule_elec" value="y" checked="checked" />
                                                    <span>Electricity Charges Extra</span>
                                                </label>
                                    </div>
                                    </div>
                                </div>
<div class="row">
    <div class="col-md-12 col-md-sm-12 col-xs-12">
                                        <div class="input-field ">
                                            <textarea id="oter_details4" name="oter_details4" maxlength="250" style="height: 80px"></textarea>
                                            <label for="oter_details4">Specify Other Details(If Any)</label>
                                        </div>
                                        
                                    </div>
</div>
                                
                            
                        </section>
                        
                        <%
                            String uuu_mob = "",uuu_email = "",uuu_firstname = "";
                            if(session.getAttribute("uuu_mob")!=null){
                                uuu_mob = session.getAttribute("uuu_mob").toString();
                                session.removeAttribute("uuu_mob");
                            }
                            if(session.getAttribute("uuu_email")!=null){
                                uuu_email = session.getAttribute("uuu_email").toString();
                                session.removeAttribute("uuu_email");
                            }
                            if(session.getAttribute("uuu_firstname")!=null){
                                uuu_firstname = session.getAttribute("uuu_firstname").toString();
                                session.removeAttribute("uuu_firstname");
                            }
                        %>
                        <h2><i class="fa fa-user" aria-hidden="true"></i>KYC</h2>
                        <section>
                            <div class="col-md-12 col-md-sm-12 col-xs-12">
                                <div class="row">
                                    
                                    <div class="col-md-3 col-md-sm-12 col-xs-12">
                                        <div class="input-field ">
                                            <input id="kyc_name" name="kyc_name" value="<%=uuu_firstname%>" maxlength="50" type="text" class="validate">
                                            <label for="kyc_name">Your Name<span class="required">*</span></label>
                                        </div>
                                        <span style="color:red" id="a_kyc_name" hidden> Please Enter Your Name</span>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-12 form-group default-select">
                                        <!--<div class="input-field">-->
                                        
                                        <select name="kyc_gender" id="kyc_gender" class="form-control" style="    border-width: 0 0 1px 0 !important;box-shadow: none;border-radius: 0 !important;padding: 10px 10px;height: 46px;border-color: #585858;margin-top: 14px;">
                                              
                                         <option selected></option>
                                         <option>Male</option>
                                         <option>Female</option>
                                         <option>Others</option>
                                    </select>
                                    <label>Select Gender <span class="required">*</span></label>
                                        <!--</div>-->
                                        <span style="color:red" id="a_kyc_gender" hidden> Please Select Gender</span>
                                    </div>
                                    <div class="col-md-3 col-md-sm-12 col-xs-12">
                                        <div class="input-field ">
                                            <input id="kyc_mob" name="kyc_mob" value="<%=uuu_mob%>" maxlength="10" onkeypress="return allowpositivenumber(event);" type="text" class="validate">
                                            <label for="country">Mobile No<span class="required">*</span></label>
                                        </div>
                                        <span style="color:red" id="a_kyc_mob" hidden> Please Enter Mobile No</span>
                                    </div>  
                                    
                                    <div class="col-md-3 col-md-sm-12 col-xs-12">
                                        <div class="input-field ">
                                            <input id="kyc_email" name="kyc_email" value="<%=uuu_email%>" maxlength="75"  type="text" class="validate">
                                            <label for="country">Email Id</label>
                                        </div>
                                        
                                    </div>  
                                    
                                    <div class="col-md-3 col-md-sm-12 col-xs-12">
                                        <div class="input-field ">
                                            <input id="kyc_add1" name="kyc_add1" maxlength="200"  type="text" class="validate">
                                            <label for="country">Permanent Address<span class="required">*</span></label>
                                        </div>
                                        <span style="color:red" id="a_kyc_add1" hidden> Please Enter Address</span>
                                    </div> 
                                    
                                    <div class="col-md-3 col-md-sm-12 col-xs-12">
                                        <div class="input-field ">
                                            <input id="kyc_pin" name="kyc_pin" maxlength="6" onkeypress="return allowpositivenumber(event);"  type="text" class="validate">
                                            <label for="country">Pin Code<span class="required">*</span></label>
                                        </div>
                                        <span style="color:red" id="a_kyc_pin" hidden> Please Enter 6 Digits Pin Code</span>
                                    </div>
                                    
                                    <div class="col-md-3 col-sm-12 col-xs-12 form-group default-select">
                                        <!--<div class="input-field">-->
                                        
                                        <select name="kyc_utype" id="kyc_utype" class="form-control" style="    border-width: 0 0 1px 0 !important;box-shadow: none;border-radius: 0 !important;padding: 10px 10px;height: 46px;border-color: #585858;margin-top: 14px;">
                                              
                                         <option selected></option>
                                         <option>Owner</option>
                                         <option>Broker</option>
                                         <option>Builder</option>
                                    </select>
                                    <label>Are You <span class="required">*</span></label>
                                        <!--</div>-->
                                        <span style="color:red" id="a_kyc_utype" hidden> Please Select Type</span>
                                    </div>
                                    
                                    
                                    <div class="col-md-3 col-sm-12 col-xs-12 form-group default-select">
                                        <!--<div class="input-field">-->
                                        
                                        <select name="kyc_time" id="kyc_time" class="form-control" style="    border-width: 0 0 1px 0 !important;box-shadow: none;border-radius: 0 !important;padding: 10px 10px;height: 46px;border-color: #585858;margin-top: 14px;">
                                              
                                         <option selected></option>
                                         <option>Monday to Friday</option>
                                         <option>All Days</option>
                                         <option>Week Ends</option>
                                    </select>
                                    <label>Availability to Connect <span class="required">*</span></label>
                                        <!--</div>-->
                                        <span style="color:red" id="a_kyc_time" hidden> Please Select Availability Slot</span>
                                    </div>
                                        
                                    
                                        
                                    </div>
                                </div>

                                
                            
                        </section>
                                            
                                            
                        <h2><i class="fa fa-money" aria-hidden="true"></i>Payment</h2>
                        <section>
                            <div class="row">
                                <div class="col-lg-3 col-md-6 col-xs-12">
                                    <div class="pricing-table text-center">
                                        <div class="table-header">
                                            <h3>Guest Users</h3>
                                        </div>

                                        <div class="plan">
                                            <h3 class="price">Free</h3>
                                            <p class="period">30 Days</p>
                                        </div>

                                        <div class="plan-info">
                                            <p>
                                                <strong><i class="lni-check-box"></i> Email Promotion to Tenants</strong>
                                            </p>
                                            <p>
                                                <strong><i class="lni-check-box"></i>Expert Property</strong>
                                            </p>
                                            <label class="radiobtn mr-4 mb-0">
                    <input type="radio" name="payment_type" value="Free" formControlName="parking"  checked>
                    <span class="checkmark"></span>
                  </label>
                                            <div class="button-area">
                                                <a class="btn btn-common btn-lg" href="#">Get Started Now</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="col-lg-3 col-md-6 col-xs-12">
                                        <div class="pricing-table  text-center">
                                            <div class="table-header">
                                                <h3>Normal Plan</h3>
                                            </div>
                                            <div class="plan">
                                                <h3 class="price">&#8377;600</h3>
                                                <p class="period">3 Months</p>
                                            </div>
    
                                            <div class="plan-info">
                                                    <p>
                                                            <strong><i class="lni-check-box"></i> Email Promotion to Tenants</strong>
                                                        </p>
                                                <p>
                                                    <strong><i class="lni-check-box"></i> Top search visibility</strong>
                                                </p>
                                                <p>
                                                    <strong><i class="lni-check-box"></i> Expert Property</strong>
                                                </p>
                                                <label class="radiobtn mr-4 mb-0">
                    <input type="radio" name="parking" formControlName="parking"  disabled>
                    <span class="checkmark"></span>
                  </label>
                                                <div class="button-area">
                                                    <a class="btn btn-common btn-lg" href="#">Get Started Now</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                <div class="col-lg-3 col-md-6 col-xs-12">
                                    <div class="pricing-table text-center">
                                        <div class="table-header">
                                            <h3>Special Plan</h3>
                                        </div>
                                        <div class="plan">
                                            <h3 class="price">&#8377;1000</h3>
                                            <p class="period">6 Months</p>
                                        </div>
                                        <div class="plan-info">
                                                <p>
                                                        <strong><i class="lni-check-box"></i> Email Promotion to Tenants</strong>
                                                    </p>
                                            <p>
                                                <strong><i class="lni-check-box"></i> Photoshoot</strong>
                                            </p>
                                            <p>
                                                <strong><i class="lni-check-box"></i> Top search visibility</strong>
                                            </p>
                                            <p>
                                                <strong><i class="lni-check-box"></i> Expert Property </strong>
                                            </p>
                                            <label class="radiobtn mr-4 mb-0">
                    <input type="radio" name="parking" formControlName="parking"  disabled>
                    <span class="checkmark"></span>
                  </label>
                                            <div class="button-area">
                                                <a class="btn btn-common btn-lg" href="#">Get Started Now</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-6 col-xs-12">
                                        <div class="pricing-table  text-center">
                                            <div class="table-header">
                                                <h3>Rent Hunter Super User</h3>
                                            </div>
                                            <div class="plan">
                                                <h3 class="price">&#8377;2000</h3>
                                                <p class="period">1 Year</p>
                                            </div>
    
                                            <div class="plan-info">
                                                    <p>
                                                            <strong><i class="lni-check-box"></i> Email Promotion to Tenants</strong>
                                                        </p>
                                                <p>
                                                    <strong><i class="lni-check-box"></i> Photoshoot</strong>
                                                </p>
                                                <p>
                                                    <strong><i class="lni-check-box"></i> Top search visibility</strong>
                                                </p>
                                                <p>
                                                    <strong><i class="lni-check-box"></i>Expert Property </strong>
                                                </p>
                                                <label class="radiobtn mr-4 mb-0">
                    <input type="radio" name="parking" formControlName="parking"  disabled>
                    <span class="checkmark"></span>
                  </label>
                                                <div class="button-area">
                                                    <input type="submit" class="btn btn-common btn-lg" value="Get Started Now" disabled>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                        </section>
                        
                    </div>
                                    </form>
                </div>
            </div>
        </div>
    </div>
                                    
                                   
  <!-- jQuery library -->
  <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> -->
  <!--<script src="js/jquery.min.js"></script>-->
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="js/bootstrap.js"></script>
  <!-- slick slider -->
  <script type="text/javascript" src="js/slick.js"></script>
  <!-- Price picker slider -->
  <script type="text/javascript" src="js/nouislider.js"></script>
  <!-- mixit slider -->
  <script type="text/javascript" src="js/jquery.mixitup.js"></script>
  <!-- Add fancyBox -->
  <script type="text/javascript" src="js/jquery.fancybox.pack.js"></script>
  <!--<script src="js/materialize.min.js"></script>-->
  <!-- Custom js -->
  <script src="js/custom.js"></script>
    <script>
        $(document).ready(function () {
            $height = $(window).height() - 370;
            $('.wizard > .content > .body').height($height);
            $(window).resize(function () {
                $height = $(window).height() - 370;
                $('.wizard > .content > .body').height($height);
            });
        });
        document.addEventListener('DOMContentLoaded', function () {
            var elems = document.querySelectorAll('select');
            var instances = M.FormSelect.init(elems, options);
        });

        // Or with jQuery

        $(document).ready(function () {
            $('select').formSelect();
        });

    </script>
    
    
        
        <script type="text/javascript">
           function readURL1(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah1')
                    .attr('src', e.target.result)
                    .width(150)
                    .height(150);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
        </script>
        
        <script type="text/javascript">
           function readURL2(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah2')
                    .attr('src', e.target.result)
                    .width(150)
                    .height(150);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
        </script>
        
        <script type="text/javascript">
           function readURL3(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah3')
                    .attr('src', e.target.result)
                    .width(150)
                    .height(150);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
        </script>
        
        <script type="text/javascript">
           function readURL4(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah4')
                    .attr('src', e.target.result)
                    .width(150)
                    .height(150);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
        </script>
        
        <script type="text/javascript">
           function readURL5(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah5')
                    .attr('src', e.target.result)
                    .width(150)
                    .height(150);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
        </script>
        
        <script type="text/javascript">
           function readURL6(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah6')
                    .attr('src', e.target.result)
                    .width(150)
                    .height(150);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
        </script>
        
        
        
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <!--<script src="https://code.jquery.com/jquery-1.12.4.js"></script>-->
  <script src="dates/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#adate" ).datepicker({
        minDate :0
    });
  } );
  </script>
  
  <script type="text/javascript">
      function previewImages() {

  var $preview = $('#preview').empty();
  if (this.files) $.each(this.files, readAndPreview);

  function readAndPreview(i, file) {
    
    if (!/\.(jpe?g|png|gif)$/i.test(file.name)){
      return alert(file.name +" is not an image");
    } // else...
    
    var reader = new FileReader();

    $(reader).on("load", function() {
      $preview.append($("<img/>", {src:this.result, height:100}));
    });

    reader.readAsDataURL(file);
    
  }

}

$('#file-input').on("change", previewImages);
  </script>
        
  <link rel="stylesheet" href="dist/image-uploader.min.css">
  <script type="text/javascript" src="dist/image-uploader.min.js"></script>

<script>
    $(function () {

        $('.input-images-1').imageUploader();

        let preloaded = [
            {id: 1, src: 'https://picsum.photos/500/500?random=1'},
            {id: 2, src: 'https://picsum.photos/500/500?random=2'},
            {id: 3, src: 'https://picsum.photos/500/500?random=3'},
            {id: 4, src: 'https://picsum.photos/500/500?random=4'},
            {id: 5, src: 'https://picsum.photos/500/500?random=5'},
            {id: 6, src: 'https://picsum.photos/500/500?random=6'},
        ];

        $('.input-images-2').imageUploader({
            preloaded: preloaded,
            imagesInputName: 'photos',
            preloadedInputName: 'old'
        });

        $('form').on('submit', function (event) {

            // Stop propagation
            event.preventDefault();
            event.stopPropagation();

            // Get some vars
            let $form = $(this),
                $modal = $('.modal');

            // Set name and description
            $modal.find('#display-name span').text($form.find('input[id^="name"]').val());
            $modal.find('#display-description span').text($form.find('input[id^="description"]').val());

            // Get the input file
            let $inputImages = $form.find('input[name^="images"]');
            if (!$inputImages.length) {
                $inputImages = $form.find('input[name^="photos"]')
            }

            // Get the new files names
            let $fileNames = $('<ul>');
            for (let file of $inputImages.prop('files')) {
                $('<li>', {text: file.name}).appendTo($fileNames);
            }

            // Set the new files names
            $modal.find('#display-new-images').html($fileNames.html());

            // Get the preloaded inputs
            let $inputPreloaded = $form.find('input[name^="old"]');
            if ($inputPreloaded.length) {

                // Get the ids
                let $preloadedIds = $('<ul>');
                for (let iP of $inputPreloaded) {
                    $('<li>', {text: '#' + iP.value}).appendTo($preloadedIds);
                }

                // Show the preloadede info and set the list of ids
                $modal.find('#display-preloaded-images').show().html($preloadedIds.html());

            } else {

                // Hide the preloaded info
                $modal.find('#display-preloaded-images').hide();

            }

            // Show the modal
            $modal.css('visibility', 'visible');
        });

        // Input and label handler
        $('input').on('focus', function () {
            $(this).parent().find('label').addClass('active')
        }).on('blur', function () {
            if ($(this).val() == '') {
                $(this).parent().find('label').removeClass('active');
            }
        });

        // Sticky menu
        let $nav = $('nav'),
            $header = $('header'),
            offset = 4 * parseFloat($('body').css('font-size')),
            scrollTop = $(this).scrollTop();

        // Initial verification
        setNav();

        // Bind scroll
        $(window).on('scroll', function () {
            scrollTop = $(this).scrollTop();
            // Update nav
            setNav();
        });

        function setNav() {
            if (scrollTop > $header.outerHeight()) {
                $nav.css({position: 'fixed', 'top': offset});
            } else {
                $nav.css({position: '', 'top': ''});
            }
        }
    });
</script>
 
    <script>
// This sample uses the Autocomplete widget to help the user select a
// place, then it retrieves the address components associated with that
// place, and then it populates the form fields with those details.
// This sample requires the Places library. Include the libraries=places
// parameter when you first load the API. For example:
// <script
// src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

var placeSearch, autocomplete;

var componentForm = {
  street_number: 'short_name',
  route: 'long_name',
  locality: 'long_name',
  administrative_area_level_1: 'short_name',
  country: 'long_name',
  postal_code: 'short_name'
};

function initAutocomplete() {
  // Create the autocomplete object, restricting the search predictions to
  // geographical location types.
  autocomplete = new google.maps.places.Autocomplete(
      document.getElementById('autocomplete'), {types: ['geocode']});

  // Avoid paying for data that you don't need by restricting the set of
  // place fields that are returned to just the address components.
  autocomplete.setFields(['address_component']);

  // When the user selects an address from the drop-down, populate the
  // address fields in the form.
  autocomplete.addListener('place_changed', fillInAddress);
}

function fillInAddress() {
  // Get the place details from the autocomplete object.
  var place = autocomplete.getPlace();

  for (var component in componentForm) {
    document.getElementById(component).value = '';
    document.getElementById(component).disabled = false;
  }

  // Get each component of the address from the place details,
  // and then fill-in the corresponding field on the form.
  for (var i = 0; i < place.address_components.length; i++) {
    var addressType = place.address_components[i].types[0];
    if (componentForm[addressType]) {
      var val = place.address_components[i][componentForm[addressType]];
      document.getElementById(addressType).value = val;
    }
  }
}

// Bias the autocomplete object to the user's geographical location,
// as supplied by the browser's 'navigator.geolocation' object.
function geolocate() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var geolocation = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      };
      var circle = new google.maps.Circle(
          {center: geolocation, radius: position.coords.accuracy});
      autocomplete.setBounds(circle.getBounds());
    });
  }
}
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCZLiZff4EUU1YkSPIpxZdADSJCiarUAd4&libraries=places&callback=initAutocomplete" async defer></script>
        
       

</body>

</html>
