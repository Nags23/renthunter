<%-- 
    Document   : welcome_portal
    Created on : 3 Dec, 2019, 8:29:41 PM
    Author     : Nandini
--%>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Home Property | Dashboard</title>

  <!-- Favicon -->
  <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">

  <!-- Font awesome -->
  <link href="css/font-awesome.css" rel="stylesheet">
  <!-- Bootstrap -->
  <link href="css/bootstrap.css" rel="stylesheet">
  <!-- slick slider -->
  <link rel="stylesheet" type="text/css" href="css/slick.css">
  <!-- price picker slider -->
  <link rel="stylesheet" type="text/css" href="css/nouislider.css">
  <!-- Fancybox slider -->
  <link rel="stylesheet" href="css/jquery.fancybox.css" type="text/css" media="screen" />
  <!-- Theme color -->
  <link id="switcher" href="css/theme-color/orange-theme.css" rel="stylesheet">

  <!-- Main style sheet -->
  <link href="css/style-main.css" rel="stylesheet">
  <link href="css/materialize.min.css" rel="stylesheet">

  <!-- Google Font -->
  <link href='https://fonts.googleapis.com/css?family=Vollkorn' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>


  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->


</head>

<body>
  <!-- Pre Loader -->
  <div id="aa-preloader-area" style="display: none;">
    <div class="pulse"></div>
  </div>
  <!-- SCROLL TOP BUTTON -->
  <a class="scrollToTop" href="#"><i class="fa fa-angle-double-up"></i></a>
  <!-- END SCROLL TOP BUTTON -->


  <!-- Start header section -->
  <%@include file="header.jsp" %>
  <!-- End header section -->
  <!-- Start menu section -->
  <div class="body-hold">
    
    <!-- End menu section -->

    <!-- Start Proerty header  -->
    <section class="parenthold">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <table class="common-table" cellspacing="0" cellpadding="0" border="0">
              <thead>
                <tr>
                  <th>VISITORS DETAILS</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>
                    Total number of visitors
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td>
                    Total Number of times visited
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td>
                   Total number of clicks
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td> Time spent on site
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td> Daily report
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td>Weekly report
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td> Monthly report
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td>Quarterly report
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td>Number of sign in users</td>
                  <td></td>
                </tr>
                <tr>
                  <td>
                    Number of people shown interest</td>
                  <td></td>
                </tr>
                <tr>
                  <td>
                    Number of people requested for owner contact</td>
                  <td></td>
                </tr>
                <tr>
                  <td>
                    Number of people got the property through renthunter</td>
                  <td></td>
                </tr>
                <tr>
                  <td>
                    Website visitor details</td>
                  <td></td>
                </tr>
                <tr>
                  <td>
                    Number of people looking for rent</td>
                  <td></td>
                </tr>
                <tr>
                  <td>
                    Number of people looking for lease</td>
                  <td></td>
                </tr>
                <tr>
                  <td>
                    Number of people looking for buy</td>
                  <td></td>
                </tr>
                <tr>
                  <td>
                    FAQ from visitors (tenants)</td>
                  <td></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <table class="common-table" cellspacing="0" cellpadding="0" border="0">
              <thead>
                <tr>
                  <th>REGISTRATION DETAILS</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Total Number of Home registered
                  </td>
                  <td></td>
                </tr>
				<tr>
                  <td>
                    Total Number of PG registered</td>
                  <td></td>
                </tr>
				<tr>
                  <td>
                    Total Number of Flats/Apartments registered</td>
                  <td></td>
                </tr>
				<tr>
                  <td>
                    Total Number of Office Space registered</td>
                  <td></td>
                </tr>
				<tr>
                  <td>
                    Total Number of Shops registered</td>
                  <td></td>
                </tr>
				<tr>
                  <td>
                    Total Number of Warehouse/Godown registration</td>
                  <td></td>
                </tr>
				<tr>
                  <td>
                    Total Number of Hall registered</td>
                  <td></td>
                </tr>
				
				
                <tr>
                  <td>
                    Number of free users</td>
                  <td></td>
                </tr>
                <tr>
                  <td>
                    Number of paid users(each sec of chosen plan separately) </td>
                  <td></td>
                </tr>
                <tr>
                  <td>
                    Number of people looking for sale</td>
                  <td></td>
                </tr>
                <tr>
                  <td>
                    Number property posted by brokers</td>
                  <td></td>
                </tr>
                <tr>
                  <td>
                    Number of property posted by owners</td>
                  <td></td>
                </tr>
                <tr>
                  <td>
                    Number of property posted by builders</td>
                  <td></td>
                </tr>
                <tr>
                  <td>
                    Number of 1 bhk houses registered Area wise</td>
                  <td></td>
                </tr>
                <tr>
                  <td>
                    Number of 2 bhk houses registered Area wise</td>
                  <td></td>
                </tr>
                <tr>
                  <td>
                    Number of 3 bhk houses registered Area wise</td>
                  <td></td>
                </tr>
                <tr>
                  <td>
                    Number of 4 bhk houses registered Area wise</td>
                  <td></td>
                </tr>
                <tr>
                  <td>
                    Number of 4+ bhk houses registered Area wise</td>
                  <td></td>
                </tr>
                <tr>
                  <td>
                    Number of 1rk houses registered Area wise</td>
                  <td></td>
                </tr>
                <tr>
                  <td>
                    Number of studio apartments registered Area wise</td>
                  <td></td>
                </tr>
                <tr>
                  <td>
                    Number of independent houses registered Area wise</td>
                  <td></td>
                </tr>
                <tr>
                  <td>
                    Number of independent villa registered Area wise</td>
                  <td></td>
                </tr>
                <tr>
                  <td>
                    Number of pg registered Area wise</td>
                  <td></td>
                </tr>
                <tr>
                  <td>
                    Number of office space registered Area wise</td>
                  <td></td>
                </tr>
                <tr>
                  <td>
                    Number of Shops registered Area wise</td>
                  <td></td>
                </tr>
                <tr>
                  <td>
                    Number of warehouses\godown registered Area wise.</td>
                  <td></td>
                </tr>
                <tr>
                  <td>
                    FAQ from owners.</td>
                  <td></td>
                </tr>

                <tr>
                  <td>
                    Property posted date.</td>
                  <td></td>
                </tr>


              </tbody>
            </table>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <table class="common-table" cellspacing="0" cellpadding="0" border="0">
              <thead>
                <tr>
                  <th>INCOME GENERATED SOURCES AND PROFIT LOSS</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Total income by PG registrations in rental
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td>
                   Total income by home registrations in rental</td>
                  <td></td>
                </tr>
                <tr>
                  <td>
                   Total income by apartments registrations in rental</td>
                  <td></td>
                </tr>
                <tr>
                  <td>
                    Total income by shops registrations in rental</td>
                  <td></td>
                </tr>
                <tr>
                  <td>
                     Total income by office spent registrations in rental</td>
                  <td></td>
                </tr>

                <tr>
                  <td>
                    Total income by warehouse\godown registrations in rental</td>
                  <td></td>
                </tr>
                <tr>
                  <td>
                     total income by PG registrations in Lease</td>
                  <td></td>
                </tr>
                <tr>
                  <td>
                     Total income by PG registrations in Lease</td>
                  <td></td>
                </tr>
                <tr>
                  <td>
                    Total income by home registrations in lease</td>
                  <td></td>
                </tr>
                <tr>
                  <td>
                     Total income by apartments registrations in lease</td>
                  <td></td>
                </tr>
                <tr>
                  <td>
                    Total income by shops registrations in lease</td>
                  <td></td>
                </tr>
                <tr>
                  <td>
                     Total income by office spent registrations in lease</td>
                  <td></td>
                </tr>
                <tr>
                  <td>
                    Total income by warehouse\godown registrations lease </td>
                  <td></td>
                </tr>
                <tr>
                  <td>
                     Total income by PG registrations in buy and sale</td>
                  <td></td>
                </tr>

                <tr>
                  <td>
                     Total income by PG registrations in buy and sale</td>
                  <td></td>
                </tr>
                <tr>
                  <td>
                     Total income by home registrations in buy and sale</td>
                  <td></td>
                </tr>
                <tr>
                  <td>
                    Total income by apartments registrations in buy and sale</td>
                  <td></td>
                </tr>
                <tr>
                  <td>
                    Total income by shops registrations in buy and sale</td>
                  <td></td>
                </tr>
                <tr>
                  <td>
                     Total income by office spent registrations in buy and sale</td>
                  <td></td>
                </tr>
                <tr>
                  <td>
                    Total income by warehouse\godown registrations buy and sale </td>
                  <td></td>
                </tr>
                <tr>
                  <td>
                    Total income generated from interior designers </td>
                  <td></td>
                </tr>
                <tr>
                  <td>
                     Total income generated from insurance company/agent.</td>
                  <td></td>
                </tr>
                <tr>
                  <td>
                    Total income generated from movers and packers.</td>
                  <td></td>
                </tr>
                <tr>
                  <td>
                    Total income generated from home appliances agencies.</td>
                  <td></td>
                </tr>
                <tr>
                  <td>
                     Total income generated from plumber and other services</td>
                  <td></td>
                </tr>
                <tr>
                  <td>
                     Total income generated from </td>
                  <td></td>
                </tr>
                <tr>
                  <td>
                    Total income of all the sections</td>
                  <td></td>
                </tr>
                <tr>
                  <td>
                     Company money spent in 4 months </td>
                  <td></td>
                </tr>
                <tr>
                  <td>
                    Profit generated in 4 months</td>
                  <td></td>
                </tr>
                <tr>
                  <td>
                    Loss generated in 4 months</td>
                  <td></td>
                </tr>


              </tbody>
            </table>
          </div>
        </div>
      </div>
    </section>

    <!-- End Proerty header  -->

  </div>
  <!-- Footer -->
  <%@include file="footer.jsp" %>
  <!-- / Footer -->

  <!-- jQuery library -->
  <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> -->
  <script src="js/jquery.min.js"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="js/bootstrap.js"></script>
  <!-- slick slider -->
  <script type="text/javascript" src="js/slick.js"></script>
  <!-- Price picker slider -->
  <script type="text/javascript" src="js/nouislider.js"></script>
  <!-- mixit slider -->
  <script type="text/javascript" src="js/jquery.mixitup.js"></script>
  <!-- Add fancyBox -->
  <script type="text/javascript" src="js/jquery.fancybox.pack.js"></script>
  <script src="js/materialize.min.js"></script>
  <!-- Custom js -->
  <script src="js/custom.js"></script>
  <script>
    $(document).ready(function () {
      $('.body-hold').css('min-height', $(window).height() - 208);
      // Comma, not colon ----^
    });
    $(window).resize(function () {
      $('.body-hold').css('min-height', $(window).height() - 208);
      // Comma, not colon ----^
    });

    $("#example-basic").steps({
      headerTag: "h3",
      bodyTag: "section",
      transitionEffect: "slideLeft",
      autoFocus: true
    });

  </script>
</body>

</html>