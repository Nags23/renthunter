<%-- 
    Document   : report_registered_count
    Created on : 12 Jul, 2020, 4:35:57 PM
    Author     : Nandini
--%>


<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Home Property | Registered Property Report</title>

  <!-- Favicon -->
  <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">

  <!-- Font awesome -->
  <link href="css/font-awesome.css" rel="stylesheet">
  <!-- Bootstrap -->
  <link href="css/bootstrap.css" rel="stylesheet">
  <!-- slick slider -->
  <link rel="stylesheet" type="text/css" href="css/slick.css">
  <!-- price picker slider -->
  <link rel="stylesheet" type="text/css" href="css/nouislider.css">
  <!-- Fancybox slider -->
  <link rel="stylesheet" href="css/jquery.fancybox.css" type="text/css" media="screen" />
  <!-- Theme color -->
  <link id="switcher" href="css/theme-color/orange-theme.css" rel="stylesheet">

  <!-- Main style sheet -->
  <link href="css/style-main.css" rel="stylesheet">
  <link href="css/materialize.min.css" rel="stylesheet">

  <!-- Google Font -->
  <link href='https://fonts.googleapis.com/css?family=Vollkorn' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>


  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->


</head>

<body>
  <!-- Pre Loader -->
  <div id="aa-preloader-area" style="display: none;">
    <div class="pulse"></div>
  </div>
  <!-- SCROLL TOP BUTTON -->
  <a class="scrollToTop" href="#"><i class="fa fa-angle-double-up"></i></a>
  <!-- END SCROLL TOP BUTTON -->


  <!-- Start header section -->
  <%@include file="../header.jsp" %>
  <!-- End header section -->
  <!-- Start menu section -->
  <div class="body-hold">
    
    <!-- End menu section -->

    <!-- Start Proerty header  -->
    <%
        String ttl_home = "0",ttl_apart = "0",ttl_pg = "0",ttl_offc = "0",ttl_shop = "0",ttl_spcl = "0",ttl_ware = "0";
        
        if(session.getAttribute("ttl_home")!=null){
            ttl_home = session.getAttribute("ttl_home").toString();
            session.removeAttribute("ttl_home");
        }
        if(session.getAttribute("ttl_apart")!=null){
            ttl_apart = session.getAttribute("ttl_apart").toString();
            session.removeAttribute("ttl_apart");
        }
        if(session.getAttribute("ttl_pg")!=null){
            ttl_pg = session.getAttribute("ttl_pg").toString();
            session.removeAttribute("ttl_pg");
        }
        if(session.getAttribute("ttl_offc")!=null){
            ttl_offc = session.getAttribute("ttl_offc").toString();
            session.removeAttribute("ttl_offc");
        }
        if(session.getAttribute("ttl_shop")!=null){
            ttl_shop = session.getAttribute("ttl_shop").toString();
            session.removeAttribute("ttl_shop");
        }
        if(session.getAttribute("ttl_spcl")!=null){
            ttl_spcl = session.getAttribute("ttl_spcl").toString();
            session.removeAttribute("ttl_spcl");
        }
        if(session.getAttribute("ttl_ware")!=null){
            ttl_ware = session.getAttribute("ttl_ware").toString();
            session.removeAttribute("ttl_ware");
        }
    %>
    <section class="parenthold">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <table class="common-table" cellspacing="0" cellpadding="0" border="0">
              <thead>
                <tr>
                  <th>Report on Property Count</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>
                    Total Number of Home registered
                  </td>
                  <td><a href="report_regsitered_count_details?ttype=Home" title="Click to view details"><%=ttl_home%></a></td>
                </tr>
                <tr>
                  <td>
                    Total Number of PG registered
                  </td>
                  <td><a href="report_regsitered_count_details?ttype=PG" title="Click to view details"><%=ttl_pg%></a></td>
                </tr>
                <tr>
                  <td>
                   Total Number of Flats/Apartments registered
                  </td>
                  <td><a href="report_regsitered_count_details?ttype=Apartment" title="Click to view details"><%=ttl_apart%></a></td>
                </tr>
                <tr>
                  <td>Total Number of Office Space registered</td>
                  <td><a href="report_regsitered_count_details?ttype=Office" title="Click to view details"><%=ttl_offc%></a></td>
                </tr>
                <tr>
                  <td>Total Number of Shops registered</td>
                  <td><a href="report_regsitered_count_details?ttype=Shops" title="Click to view details"><%=ttl_shop%></a></td>
                </tr>
                <tr>
                  <td>Total Number of Warehouse/Godown registration</td>
                  <td><a href="report_regsitered_count_details?ttype=Warehouse" title="Click to view details"><%=ttl_ware%></a></td>
                </tr>
                <tr>
                  <td>Total Number of Special rentals</td>
                  <td><a href="report_regsitered_count_details?ttype=Specials" title="Click to view details"><%=ttl_spcl%></a></td>
                </tr>
                
              </tbody>
            </table>
          </div>
        </div>
        
        
      </div>
    </section>

    <!-- End Proerty header  -->

  </div>
  <!-- Footer -->
  <%@include file="../footer.jsp" %>
  <!-- / Footer -->

  <!-- jQuery library -->
  <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> -->
  <script src="js/jquery.min.js"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="js/bootstrap.js"></script>
  <!-- slick slider -->
  <script type="text/javascript" src="js/slick.js"></script>
  <!-- Price picker slider -->
  <script type="text/javascript" src="js/nouislider.js"></script>
  <!-- mixit slider -->
  <script type="text/javascript" src="js/jquery.mixitup.js"></script>
  <!-- Add fancyBox -->
  <script type="text/javascript" src="js/jquery.fancybox.pack.js"></script>
  <script src="js/materialize.min.js"></script>
  <!-- Custom js -->
  <script src="js/custom.js"></script>
  <script>
    $(document).ready(function () {
      $('.body-hold').css('min-height', $(window).height() - 208);
      // Comma, not colon ----^
    });
    $(window).resize(function () {
      $('.body-hold').css('min-height', $(window).height() - 208);
      // Comma, not colon ----^
    });

    $("#example-basic").steps({
      headerTag: "h3",
      bodyTag: "section",
      transitionEffect: "slideLeft",
      autoFocus: true
    });

  </script>
</body>

</html>