<%-- 
    Document   : report_regsitered_count_details
    Created on : 12 Jul, 2020, 8:47:28 PM
    Author     : Nandini
--%>


<%@page import="java.util.List"%>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Home Property | Search Results</title>

  <!-- Favicon -->
  <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">

  <!-- Font awesome -->
  <link href="css/font-awesome.css" rel="stylesheet">
  <!-- Bootstrap -->
  <link href="css/bootstrap.css" rel="stylesheet">
  <!-- slick slider -->
  <link rel="stylesheet" type="text/css" href="css/slick.css">
  <!-- price picker slider -->
  <link rel="stylesheet" type="text/css" href="css/nouislider.css">
  <!-- Fancybox slider -->
  <link rel="stylesheet" href="css/jquery.fancybox.css" type="text/css" media="screen" />
  <!-- Theme color -->
  <link id="switcher" href="css/theme-color/orange-theme.css" rel="stylesheet">

  <!-- Main style sheet -->
  <link href="css/style-main.css" rel="stylesheet">
  <link id="switcher" href="../../css/theme-color/orange-theme.css" rel="stylesheet">
  <link href="css/materialize.min.css" rel="stylesheet">

  <!-- Google Font -->
  <link href='https://fonts.googleapis.com/css?family=Vollkorn' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>


  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->


</head>

<body>
  <!-- Pre Loader -->
  <div id="aa-preloader-area">
    <div class="pulse"></div>
  </div>
  <!-- SCROLL TOP BUTTON -->
  <a class="scrollToTop" href="#"><i class="fa fa-angle-double-up"></i></a>
  <!-- END SCROLL TOP BUTTON -->


  <!-- Start header section -->
  <%@include file="../header.jsp" %>
  <!-- End header section -->
  <!-- Start menu section -->
  <div class="body-hold">
    
    <!-- End menu section -->

    <!-- Start Proerty header  -->
    <section class="parenthold">
      <div class="container-fluid">
        <div class="row">
          

          <div class="col-md-12 search-list">
              
               <%
                  if(session.getAttribute("dd_id")!=null){
                      List<String> dd_id = (List<String>) session.getAttribute("dd_id");
                      List<String> dd_street = (List<String>) session.getAttribute("dd_street");
                      List<String> dd_address = (List<String>) session.getAttribute("dd_address");
                      List<String> dd_floor = (List<String>) session.getAttribute("dd_floor");
                      List<String> dd_dep = (List<String>) session.getAttribute("dd_dep");
                      List<String> dd_rent = (List<String>) session.getAttribute("dd_rent");
                      List<String> dd_area = (List<String>) session.getAttribute("dd_area");
                      List<String> dd_fur = (List<String>) session.getAttribute("dd_fur");
                      List<String> dd_cstatus = (List<String>) session.getAttribute("dd_cstatus");
                      List<String> dd_img = (List<String>) session.getAttribute("dd_img");
                      List<String> dd_doc = (List<String>) session.getAttribute("dd_doc");
                      
                      for(int i=0;i<dd_id.size();i++){
                          String img = "";
                          if(dd_img.get(i)!=null && dd_img.get(i).length()>0){
                              img = "form_pics"+"/"+dd_img.get(i);
                          }else{
                              img = "img/no-image.png";
                          }
                  
              %>
            <div class="card bar-left-success">
              <div class="ribbon-hold green darken-1">
                <span><%=dd_cstatus.get(i)%></span>
              </div>
              <div class="property">
                <div class="card-body d-flex ">
                  <div class="prop-img">
                    <img src="<%=img%>" alt="img" width="200">
                  </div>
                  <div class="prop-details">
                    <div class="list-card-title">
                      <span class="header">
                          <a href="#"> 
                              Property Tag # : <%=dd_doc.get(i)%> <br>
                              <%=dd_floor.get(i)%> In <%=dd_street.get(i)%> 
                            
                          </a>
                      </span>
                      <small class="block">
                        <i class="fa fa-map-marker"></i><%=dd_address.get(i)%>
                      </small>
                    </div>
                    <div class="list-detail my-2">
                      <div class="block-1">
                        <small>Deposit</small>
                        <span class="d-block"> <%=dd_dep.get(i)%></span>
                      </div>
                      <div class="block-1">
                        <small>Rent</small>
                        <span class="d-block"> <%=dd_rent.get(i)%></span>
                      </div>
                      <div class="block-1">
                          <small>Plot area</small>
                          <span class="d-block"><%=dd_area.get(i)%> Sq.Ft</span>
                        </div>
                      <div class="block-1">
                        <small>Furnishing</small>
                        <span class="d-block"><%=dd_fur.get(i)%></span>
                      </div>
                    </div>
                    <div class="list-action mt-15 text-right">
                      <%
                          if(dd_cstatus.get(i).equalsIgnoreCase("Home")){
                      %>
                      <a href="home_edit_client?id=<%=dd_id.get(i)%>" title="Click to Edit Details" class="btn btn-common mb-0 ml-0 pull-left">Edit Details</a>
                      <%}%>
                       <%
                          if(dd_cstatus.get(i).equalsIgnoreCase("Apartment")){
                      %>
                      <a href="apartment_edit_client?id=<%=dd_id.get(i)%>" title="Click to Edit Details" class="btn btn-common mb-0 ml-0 pull-left">Edit Details</a>
                   <%}%>
                   <%
                          if(dd_cstatus.get(i).equalsIgnoreCase("PG")){
                      %>
                      <a href="pg_edit_client?id=<%=dd_id.get(i)%>" title="Click to Edit Details" class="btn btn-common mb-0 ml-0 pull-left">Edit Details</a>
                   <%}%>
                   <%
                          if(dd_cstatus.get(i).equalsIgnoreCase("Office")){
                      %>
                      <a href="office_edit_client?id=<%=dd_id.get(i)%>" title="Click to Edit Details" class="btn btn-common mb-0 ml-0 pull-left">Edit Details</a>
                   <%}%>
                   <%
                          if(dd_cstatus.get(i).equalsIgnoreCase("Shop")){
                      %>
                      <a href="shop_edit_client?id=<%=dd_id.get(i)%>" title="Click to Edit Details" class="btn btn-common mb-0 ml-0 pull-left">Edit Details</a>
                   <%}%>
                   <%
                          if(dd_cstatus.get(i).equalsIgnoreCase("Warehouse")){
                      %>
                      <a href="warehouse_edit_client?id=<%=dd_id.get(i)%>" title="Click to Edit Details" class="btn btn-common mb-0 ml-0 pull-left">Edit Details</a>
                   <%}%>
                   <%
                          if(dd_cstatus.get(i).equalsIgnoreCase("Specials")){
                      %>
                      <a href="specials_edit_client?id=<%=dd_id.get(i)%>" title="Click to Edit Details" class="btn btn-common mb-0 ml-0 pull-left">Edit Details</a>
                   <%}%>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <%
                }
                session.removeAttribute("dd_id");
                session.removeAttribute("dd_street");
                session.removeAttribute("dd_address");
                session.removeAttribute("dd_floor");
                session.removeAttribute("dd_dep");
                session.removeAttribute("dd_rent");
                session.removeAttribute("dd_area");
                session.removeAttribute("dd_fur");
                session.removeAttribute("dd_cstatus");
                session.removeAttribute("dd_img");
                session.removeAttribute("dd_doc");
                }
            %>
          </div>
          <div class="col-md-2">

          </div>
        </div>
      </div>
    </section>
    <!-- End Proerty header  -->

  </div>
  <!-- Footer -->
  <%@include file="../footer.jsp" %>
  <!-- / Footer -->

  <!-- jQuery library -->
  <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> -->
  <script src="js/jquery.min.js"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="js/bootstrap.js"></script>
  <!-- slick slider -->
  <script type="text/javascript" src="js/slick.js"></script>
  <!-- Price picker slider -->
  <script type="text/javascript" src="js/nouislider.js"></script>
  <!-- mixit slider -->
  <script type="text/javascript" src="js/jquery.mixitup.js"></script>
  <!-- Add fancyBox -->
  <script type="text/javascript" src="js/jquery.fancybox.pack.js"></script>
  <script src="js/materialize.min.js"></script>
  <!-- Custom js -->
  <script src="js/custom.js"></script>
  <script>
    $(document).ready(function () {
      $('.body-hold').css('min-height', $(window).height() - 198);
      // Comma, not colon ----^
    });
    $(window).resize(function () {
      $('.body-hold').css('min-height', $(window).height() - 198);
      // Comma, not colon ----^
    });

    $("#example-basic").steps({
      headerTag: "h3",
      bodyTag: "section",
      transitionEffect: "slideLeft",
      autoFocus: true
    });

  </script>
</body>

</html>
