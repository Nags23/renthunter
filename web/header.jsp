<%-- 
    Document   : header
    Created on : 3 Dec, 2019, 7:42:05 PM
    Author     : Nandini
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
    </head>
    <body>
        <header id="aa-header">  
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="aa-header-area">
            <div class="row">
              <div class="col-md-6 col-sm-6 col-xs-6">
                <div class="aa-header-left">
                  <div class="aa-telephone-no">
                    <span class="fa fa-phone"></span>
                    <a href="tel:+91 7610065100" style="color:#fff" title="Click to Call Us">+91 76100 65100</a>
                  </div>
                  <div class="aa-email hidden-xs">
                      <a class="" href="mailTo:contactus@renthunter.in" title="Click to Send Mail" style="color:#fff">
                    <span class="fa fa-envelope-o"></span>  contactus@renthunter.in
                    </a>
                  </div>
                </div>              
              </div>
              <div class="col-md-6 col-sm-6 col-xs-6">
                <div class="aa-header-right">
                    <%
                        if(session.getAttribute("user")!=null){
                    %>
                  <a href="change_password" class="aa-register" title="Click to Change Password">Change Password</a>
                  <a href="logout" class="aa-login" title="Click to Logout">Logout</a>
                  <%}else{%>
                  <a href="register" class="aa-register" title="Click to Register">Register</a>
                  <a href="signin" class="aa-login" title="Click to Login">Login</a>
                  <%}%>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </header>
        
        <section id="aa-menu-area">
    <nav class="navbar navbar-default main-navbar" role="navigation">  
      <div class="container">
        <div class="navbar-header">
          <!-- FOR MOBILE VIEW COLLAPSED BUTTON -->
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <!-- LOGO -->                                               
          <!-- Text based logo -->
           <a class="navbar-brand aa-logo" href="home"> Rent<span>Hunter.in</span></a>
           <!-- Image based logo -->
           <!-- <a class="navbar-brand aa-logo-img" href="index.html"><img src="img/logo.png" alt="logo"></a> -->
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul id="top-menu" class="nav navbar-nav navbar-right aa-main-nav">
            
             <%
                        if(session.getAttribute("user")!=null){
                    %>
                    <%
                        if(session.getAttribute("utype").toString().equalsIgnoreCase("Admin")){
                    %>
                  <li class="active"><a href="welcome_portal">HOME</a></li>
                  
             <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">MASTERS <span
                    class="caret"></span></a>
                <ul class="dropdown-menu" role="menu">
                   
                  <li><a href="define_team">Define Internal Team</a></li>
                  <!--<li><a href="define_locations">Define Locations</a></li>-->
                  <li><a href="define_tenants">Define Tenant Type</a></li>
                  <li><a href="define_bhk">Define BHK</a></li>
                  <li><a href="define_furnished_types">Define Furnished Types</a></li>
                  <li><a href="define_floors">Defines Floors </a></li>
                  <li><a href="define_amenities">Define Amenities </a></li>
                  <li><a href="define_services">Define Services </a></li>
                  <li><a href="define_payment_plans">Define Payment Plans </a></li>
                  <li><a href="define_facings">Define Facings </a></li>
                  <li><a href="define_food_service">Define Food Service </a></li>
                  <li><a href="define_sharing">Define Sharing </a></li>
                  <li><a href="define_near_by">Define Near By </a></li>
                  <li><a href="special_rentals_types">Special Rentals Types </a></li>
                  <li><a href="add_states">Add States </a></li>
                  <li><a href="add_cities">Add Cities </a></li>
                  <li><a href="add_areas">Add Areas </a></li>
                  <li><a href="define_rules">Define Rules </a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">SPECIAL ACCESS<span
                    class="caret"></span></a>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="report_registered_count">Report on Property Count</a></li> 
                  <li><a href="report_registered">Report on Registered Property</a></li>
                 
                </ul>
            </li>
            <li><a href="contact">CONTACT</a></li>
            <li><a href="#">ABOUT US</a></li>
            <li><a href="#">GALLERY</a></li> 
                  <%}else{%>
                  <li class="active"><a href="home">HOME</a></li>
                  <li><a href="welcome_user">PROFILE</a></li>
<!--                  <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">MANAGE PROPERTIES <span
                    class="caret"></span></a>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="manage_home">Home</a></li>
                  <li><a href="#">Apartment</a></li>
                  <li><a href="#">Paying Guests</a></li>
                  <li><a href="#">Office Space</a></li>
                  <li><a href="#">Shops</a></li>
                  <li><a href="#">Warehouse / Godown </a></li>
                  <li><a href="#">Party Hall </a></li>
                </ul>
            </li>-->
             <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">POST PROPERTIES <span
                    class="caret"></span></a>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="post_home">Home</a></li>
                  <li><a href="post_apartment">Apartment</a></li>
                  <li><a href="post_paying_guests">Paying Guests</a></li>
                  <li><a href="office_space">Office Space</a></li>
                  <li><a href="post_shops">Shops</a></li>
                  <li><a href="post_warehouse">Warehouse / Godown </a></li>
                  <li><a href="post_party_hall">Special Rentals </a></li>
                </ul>
            </li>
            <li><a href="contact">CONTACT</a></li>
            <li><a href="#">ABOUT US</a></li>
            <li><a href="#">GALLERY</a></li> 
                  <%}%>
                  <%}else{%>
                 
                  <li class="active"><a href="welcome_portal">HOME</a></li>
                  <li ><a href="#">PRICING PLAN</a></li>
             <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">POST PROPERTIES <span
                    class="caret"></span></a>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="signin">Home</a></li>
                  <li><a href="signin">Apartment</a></li>
                  <li><a href="signin">Paying Guests</a></li>
                  <li><a href="signin">Office Space</a></li>
                  <li><a href="signin">Shops</a></li>
                  <li><a href="signin">Warehouse / Godown </a></li>
                  <li><a href="signin">Party Hall </a></li>
                </ul>
            </li>
            <li><a href="contact">CONTACT</a></li>
            <li><a href="#">ABOUT US</a></li>
            <li><a href="#">GALLERY</a></li> 
                  <%}%>                             
            <!-- <li class="dropdown">
              <a class="dropdown-toggle" data-toggle="dropdown" href="blog-archive.html">BLOG <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">                
                <li><a href="blog-archive.html">BLOG</a></li>
                <li><a href="blog-single.html">BLOG DETAILS</a></li>                                            
              </ul>
            </li> -->
           
           
          </ul>                            
        </div><!--/.nav-collapse -->       
      </div>          
    </nav> 
  </section>
    </body>
</html>
